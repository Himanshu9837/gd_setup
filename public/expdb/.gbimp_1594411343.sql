DROP TABLE IF EXISTS `gd_access_codes`;

CREATE TABLE `gd_access_codes` (
  `access_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `access_code` varchar(6) NOT NULL,
  `usage_type` tinyint(1) NOT NULL COMMENT '1=single, 2=multiple',
  `code_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=inactive, 1=active',
  `times_used` int(11) NOT NULL DEFAULT '0' COMMENT 'number of time a code has been used',
  `access_date_time` varchar(255) NOT NULL,
  `admin_id` int(11) NOT NULL DEFAULT '0' COMMENT 'admin_id if created by admin',
  `date_added` datetime NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`access_id`),
  KEY `accessIndex` (`owner_id`,`access_code`,`code_status`),
  KEY `inCodeStatus` (`code_status`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `gd_access_codes` ( ) VALUES('1','2','068668','2','1','0','1','1','2020-07-10 09:43:25','2020-07-10 09:43:25');



DROP TABLE IF EXISTS `gd_access_logs`;

CREATE TABLE `gd_access_logs` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `access_id` bigint(20) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `owner_name` varchar(255) DEFAULT NULL,
  `owner_mobile` int(11) DEFAULT 0,
  `owner_info` text,
  `date_added` datetime NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO `gd_access_logs` ( ) VALUES('1','1','2','ADA Test','101','{"unit_number":"100","floor":"1"}','2020-07-10 09:43:36','2020-07-10 09:43:36');
INSERT INTO `gd_access_logs` ( ) VALUES('2','1','2','ADA Test','101','{"unit_number":"100","floor":"1"}','2020-07-10 10:52:27','2020-07-10 10:52:27');
INSERT INTO `gd_access_logs` ( ) VALUES('3','1','2','ADA Test','101','{"unit_number":"100","floor":"1"}','2020-07-10 10:52:56','2020-07-10 10:52:56');
INSERT INTO `gd_access_logs` ( ) VALUES('4','1','2','ADA Test','101','{"unit_number":"100","floor":"1"}','2020-07-10 10:53:23','2020-07-10 10:53:23');
INSERT INTO `gd_access_logs` ( ) VALUES('5','1','2','ADA Test','101','{"unit_number":"100","floor":"1"}','2020-07-10 10:53:41','2020-07-10 10:53:41');
INSERT INTO `gd_access_logs` ( ) VALUES('6','1','2','ADA Test','101','{"unit_number":"100","floor":"1"}','2020-07-10 10:54:02','2020-07-10 10:54:02');
INSERT INTO `gd_access_logs` ( ) VALUES('7','1','2','ADA Test','101','{"unit_number":"100","floor":"1"}','2020-07-10 10:54:26','2020-07-10 10:54:26');
INSERT INTO `gd_access_logs` ( ) VALUES('8','1','2','ADA Test','101','{"unit_number":"100","floor":"1"}','2020-07-10 10:54:47','2020-07-10 10:54:47');



DROP TABLE IF EXISTS `gd_admin`;

CREATE TABLE `gd_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `gender` enum('M','F') DEFAULT NULL COMMENT 'M -- Male, F -- Female',
  `role` enum('admin','subadmin','vendor') DEFAULT 'admin',
  `user_type` enum('1','2','3') NOT NULL COMMENT '1-Masteradmin, 2- subadmin, 3-vendor',
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 -- Inactive, 1 -- Active ',
  `created_date` datetime DEFAULT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf16;

INSERT INTO `gd_admin` ( ) VALUES('1','Granddunes','Granddunes','GD','granddunes@gd.com','22222222','d5f7e49392094f304c09b812534fe5d8','M','admin','1','1','2017-06-09 18:41:28','2020-07-10 08:23:02');
INSERT INTO `gd_admin` ( ) VALUES('3','Autogate','Admin','','AutogateAdmin@gd.com','12345','7eb13094a79e6dfab3e5ab41d3ca11ea','M','vendor','3','1','2020-07-10 09:26:55','2020-07-10 09:26:55');



DROP TABLE IF EXISTS `gd_app_settings`;

CREATE TABLE `gd_app_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sitename` varchar(255) DEFAULT NULL,
  `cm_iphost` varchar(255) DEFAULT NULL,
  `cm_port` varchar(255) DEFAULT NULL,
  `cm_name` varchar(255) DEFAULT NULL,
  `cm_channel` varchar(255) DEFAULT NULL,
  `cm_username` varchar(255) DEFAULT NULL,
  `cm_pwd` varchar(255) DEFAULT NULL,
  `cm_streaming` varchar(50) DEFAULT NULL,
  `sip_addr` varchar(255) DEFAULT NULL,
  `sip_port` varchar(255) DEFAULT NULL,
  `sip_transport` varchar(255) DEFAULT NULL,
  `sip_ser1addr` varchar(255) DEFAULT NULL,
  `sip_ser1_port` varchar(255) DEFAULT NULL,
  `sip_ser1_transport` varchar(255) DEFAULT NULL,
  `sip_ser1_expires` varchar(255) DEFAULT NULL,
  `sip_ser1_register` varchar(255) DEFAULT NULL,
  `sip_ser1_retrytmout` varchar(255) DEFAULT NULL,
  `sip_ser1_retrymaxct` varchar(255) DEFAULT NULL,
  `sip_ser1_lnsztmout` varchar(255) DEFAULT NULL,
  `update_config_url` varchar(255) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 - inactive, 1 - active',
  `created_date` datetime DEFAULT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf16;

INSERT INTO `gd_app_settings` ( ) VALUES('1','First','http://96.78.58.229:8081','80','Camera1','1','admin','jcabsam1','NORMAL','96.78.58.229','6063','1','96.78.58.229','6063','3','1','1','5','9','40','http://96.78.58.229/idlogin','1','2017-06-09 18:41:28','2020-07-10 09:25:15');



DROP TABLE IF EXISTS `gd_callendtime`;

CREATE TABLE `gd_callendtime` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `call_end_time` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 - inactive, 1--active',
  `created_date` datetime DEFAULT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf16;

INSERT INTO `gd_callendtime` ( ) VALUES('1','120','1','2017-06-09 18:14:19','2020-07-10 09:24:35');



DROP TABLE IF EXISTS `gd_configs`;

CREATE TABLE `gd_configs` (
  `config_type` int(11) NOT NULL COMMENT '1=access code limit per user, 2= access code button, 3= timezone',
  `value` varchar(255) NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `unConfigType` (`config_type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `gd_configs` ( ) VALUES('1','5','2019-01-22 16:33:01');
INSERT INTO `gd_configs` ( ) VALUES('2','1','2019-10-04 05:17:15');
INSERT INTO `gd_configs` ( ) VALUES('3','America/Vancouver','2020-07-10 09:24:28');



DROP TABLE IF EXISTS `gd_external_ip`;

CREATE TABLE `gd_external_ip` (
  `ip_id` int(11) NOT NULL AUTO_INCREMENT,
  `external_ip` varchar(255) DEFAULT NULL,
  `added_date` datetime DEFAULT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`ip_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE IF EXISTS `gd_firewall_configs`;

CREATE TABLE `gd_firewall_configs` (
  `config_type` tinyint(4) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `mask` varchar(255) DEFAULT NULL,
  `gateway` varchar(255) DEFAULT NULL,
  `dns1` varchar(255) DEFAULT NULL,
  `dns2` varchar(255) DEFAULT NULL,
  `modified_date` datetime NOT NULL,
  UNIQUE KEY `unFirewallConfig` (`config_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf16;

INSERT INTO `gd_firewall_configs` ( ) VALUES('1','10.35.62.1','cisco','GrandDunes8675309!','','','','','2020-07-08 18:23:34');
INSERT INTO `gd_firewall_configs` ( ) VALUES('2','96.78.58.227','','','255.255.255.248','96.78.58.230','75.75.75.75','75.75.76.76','2020-07-08 18:29:29');



DROP TABLE IF EXISTS `gd_flatowner`;

CREATE TABLE `gd_flatowner` (
  `id` int(11) NOT Null AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT 0,
  `parent_id` int(11) DEFAULT 0,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `unit_number` varchar(100) DEFAULT NULL,
  `floor` int(11) DEFAULT 0,
  `mobile` bigint(12) DEFAULT 0,
  `email` varchar(255) DEFAULT NULL,
  `followme1` varchar(100) DEFAULT NULL,
  `followme2` varchar(100) DEFAULT NULL,
  `followme3` varchar(100) DEFAULT NULL,
  `followme4` varchar(100) DEFAULT NULL,
  `followme5` varchar(100) DEFAULT NULL,
  `ring_type` varchar(100) DEFAULT NULL,
  `confirmcall` varchar(3) DEFAULT NULL,
  `voice_mail` varchar(100) DEFAULT NULL,
  `create_sip_user` varchar(100) DEFAULT NULL,
  `camera_only` varchar(100) DEFAULT NULL,
  `sip_username` varchar(255) DEFAULT NULL,
  `sip_password` varchar(255) DEFAULT NULL,
  `sip_extention_num` bigint(20) DEFAULT 0,
  `sip_download_code` bigint(20) DEFAULT 0,
  `locked_to_device` varchar(255) DEFAULT NULL,
  `sites` varchar(255) DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '1 - active, 0 - inactive',
  `created_date` datetime DEFAULT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf16;

INSERT INTO `gd_flatowner` ( ) VALUES('2','0','0','ADA','Test','100','1','101','','101','14253503023#','','','','ringallv2-prim','yes','','','no','101','vkcj71xOgR22wI/iLQppDauVdCyPttdROjK1iN1oSho=','101','718971','No Devicelocked','','1','2020-07-10 09:41:51','2020-07-10 10:50:29');
INSERT INTO `gd_flatowner` ( ) VALUES('3','0','0','','','102','1','103','','103','','','','','ringallv2-prim','yes','','','no','103','YMLyiiUBVKm/xEfEBTwIoz2jzSfc3jX7AMWuYRVdbEA=','103','886727','','','1','2020-07-10 09:42:16','2020-07-10 09:42:16');
INSERT INTO `gd_flatowner` ( ) VALUES('4','0','0','Schmidt','','','21','104','','104','','','','','ringallv2-prim','yes','','','no','104','OF2xJq7v8ApfB94NrcLu02jXJcFX88vO88tcnaLMVug=','104','943932','','','1','2020-07-10 09:42:35','2020-07-10 09:42:35');
INSERT INTO `gd_flatowner` ( ) VALUES('5','0','0','Kelly','Fleming','101','1','105','','105','12064866219#','','','','hunt-prim','yes','','','','105','W0XZzBoOY0HEVCVMW4qdM7zK9l7NYqd2ynmiiQHAOpU=','105','551494','','','1','2020-07-10 09:46:29','2020-07-10 09:46:29');
INSERT INTO `gd_flatowner` ( ) VALUES('6','0','0','Justin','Johansson','101','1','106','','106','12064865476#','','','','hunt-prim','yes','','','','106','437iebmR34qktkzvadz8Y7HiBsfuMWy++mjv4jihJA4=','106','844858','','','1','2020-07-10 09:46:34','2020-07-10 09:46:34');
INSERT INTO `gd_flatowner` ( ) VALUES('7','0','0','Tom','Henderson','102','1','107','','107','12062007399#','','','','hunt-prim','yes','','','','107','e25kZn49ZZvKrKNFhheLmZ1Qzb8gETZZRATL51EbvgI=','106','359227','','','1','2020-07-10 09:46:38','2020-07-10 09:46:38');
INSERT INTO `gd_flatowner` ( ) VALUES('8','0','0','Paul','Waigwa','103','1','108','','108','12533293576#','','','','hunt-prim','yes','','','','108','oz7lS2kSn0J/l3K04RHAYNV4bvlIfkRbE/dW1dTfzUE=','108','283450','','','1','2020-07-10 09:46:42','2020-07-10 09:46:42');
INSERT INTO `gd_flatowner` ( ) VALUES('9','0','0','Tony','Robinson','104','1','109','','109','19167129282#','','','','hunt-prim','yes','','','','109','/mjnsoaBmqahWH+eWAKv0vK9dcgNJtY0uaYyHv38OgY=','109','561645','','','1','2020-07-10 09:46:46','2020-07-10 09:46:46');
INSERT INTO `gd_flatowner` ( ) VALUES('10','0','0','Lucinda','Bryan','105','1','110','','110','12062830255#','','','','hunt-prim','yes','','','','110','UhSpRByn9PbVs9CyihSEI7XrXUARgcc6CQVUf7N5LI4=','110','795997','','','1','2020-07-10 09:46:51','2020-07-10 09:46:51');
INSERT INTO `gd_flatowner` ( ) VALUES('11','0','0','Rob','Ernest','106','1','111','','111','12063901309#','','','','hunt-prim','yes','','','','111','Wscr8T3uV1/pJd6zG9b5EHCrhHS/T+p848UIp+1H8xE=','111','204816','','','1','2020-07-10 09:46:55','2020-07-10 09:46:55');
INSERT INTO `gd_flatowner` ( ) VALUES('12','0','0','Martha','Doker','201','2','112','','112','12063526246#','','','','hunt-prim','yes','','','','112','hrTW5wlI6i33jI5vD98mGjjTy6elhaFyE8ZzREagwTA=','112','636289','','','1','2020-07-10 09:46:59','2020-07-10 09:46:59');
INSERT INTO `gd_flatowner` ( ) VALUES('13','0','0','Jake','Campion','202','2','113','','113','12062839904#','','','','hunt-prim','yes','','','','113','Y/GLUQBPYrT2hFlLnJVknn5TMZUupyOnkucu2Uo2+Eg=','113','305066','','','1','2020-07-10 09:47:04','2020-07-10 09:47:04');
INSERT INTO `gd_flatowner` ( ) VALUES('14','0','0','Jill','Bachman','203','2','114','','114','12066913913#','','','','hunt-prim','yes','','','','114','tV8T+ysDwGXKs4AoQ/IUSrJIbc1iY7Y6bes2eJwgiuM=','114','796160','','','1','2020-07-10 09:47:08','2020-07-10 09:47:08');
INSERT INTO `gd_flatowner` ( ) VALUES('15','0','0','Joe','Thompson','204','2','115','','115','12545923692#','','','','hunt-prim','yes','','','','115','Xe3g7y14jFpFptRMnSX6MNh3Um4aBSrJ7t8pGo9EFSo=','115','698324','','','1','2020-07-10 09:47:13','2020-07-10 09:47:13');
INSERT INTO `gd_flatowner` ( ) VALUES('16','0','0','Sam','Dorris','205','2','116','','116','12064095599#','','','','hunt-prim','yes','','','','116','7sEX+aACKxda2vATJ1pSmkeOL0lGAH4otj8WvW4jcpc=','116','660213','','','1','2020-07-10 09:47:17','2020-07-10 09:47:17');
INSERT INTO `gd_flatowner` ( ) VALUES('17','0','0','Sally','Mitchell','206','2','117','','117','12063623128#','','','','hunt-prim','yes','','','','117','JxH5OaMkotoQ8dN8alFjc72i2nrloYaDVrVP1WljiAo=','117','894439','','','1','2020-07-10 09:47:22','2020-07-10 09:47:22');
INSERT INTO `gd_flatowner` ( ) VALUES('18','0','0','Martha','Denham','301','3','118','','118','12062356328#','','','','hunt-prim','yes','','','','118','CBeajQwkwNtGBKdHT+8mS0zKFap/cy4f11GlWr46n94=','118','619079','','','1','2020-07-10 09:47:26','2020-07-10 09:47:26');
INSERT INTO `gd_flatowner` ( ) VALUES('19','0','0','Yugo','Tucker','999','3','119','','119','14252426441#','','','','hunt-prim','yes','','','no','119','sgbThn+pHoaCEzSRx8kgM4VEsZ0Q8cuLq6fv+W/zEfM=','119','725799','No Devicelocked','','1','2020-07-10 09:47:31','2020-07-10 10:47:41');
INSERT INTO `gd_flatowner` ( ) VALUES('20','0','0','Ted','Knapp','302','3','120','','120','18085616468#','','','','hunt-prim','yes','','','','120','a3mssQBOwOR5BoZru44ND/dTdRAit5kkRJH9+EnQdME=','120','702060','','','1','2020-07-10 09:47:35','2020-07-10 09:47:35');
INSERT INTO `gd_flatowner` ( ) VALUES('21','0','0','Larry','Van Doren','303','3','121','','121','12063010404#','','','','hunt-prim','yes','','','','121','sEPj6ydD7QNkNLJEkJyFyfYtclLT8QMrnhQpPUYo67o=','121','269254','','','1','2020-07-10 09:47:40','2020-07-10 09:47:40');
INSERT INTO `gd_flatowner` ( ) VALUES('22','0','0','Bill','Newell','305','3','122','','122','12063490205#','','','','hunt-prim','yes','','','','122','36Qk89R+HqTuc8rzYaMVToK/kHkFIYXVWLQCpU/6530=','122','534936','','','1','2020-07-10 09:47:45','2020-07-10 09:47:45');
INSERT INTO `gd_flatowner` ( ) VALUES('23','0','0','Lonoy','Cimler','306','3','123','','123','12066506238#','','','','hunt-prim','yes','','','','123','d7jvCP1DnqjNsp8szDYx5RZdeS7I2ATm30NiEdvZ/48=','123','814883','','','1','2020-07-10 09:47:49','2020-07-10 09:47:49');
INSERT INTO `gd_flatowner` ( ) VALUES('24','0','0','Eric','Jensen','401','4','124','','124','19259848440#','','','','hunt-prim','yes','','','','124','wLEDuQX61vuKzwKGuPCRf0HnHDUa990ETq2lc3Bkugw=','124','993927','','','1','2020-07-10 09:47:54','2020-07-10 09:47:54');
INSERT INTO `gd_flatowner` ( ) VALUES('25','0','0','Rinda','Ulbrickson','402','4','125','','125','12063215499#','','','','hunt-prim','yes','','','','125','H+F7ujHttYhX222l5loZBEA+Al5eO2ezEPGEPgyr0hc=','125','470826','','','1','2020-07-10 09:47:58','2020-07-10 09:47:58');
INSERT INTO `gd_flatowner` ( ) VALUES('26','0','0','Daryl','Schlick','403','4','126','','126','12067553463#','','','','hunt-prim','yes','','','','126','MiV/OKj2pwZCu/cuwVI5+BrP2D7Fb/6lRGBgdjWEmO0=','126','997371','','','1','2020-07-10 09:48:03','2020-07-10 09:48:03');
INSERT INTO `gd_flatowner` ( ) VALUES('27','0','0','Barbara','Timms','403','4','127','','127','12067997924#','','','','hunt-prim','yes','','','','127','q5T3LwYBWmK0XOiOzO/Oxvwjv8dxRzH+YasRSqNNcho=','127','490119','','','1','2020-07-10 09:48:08','2020-07-10 09:48:08');
INSERT INTO `gd_flatowner` ( ) VALUES('28','0','0','Keith','Biever','404','4','128','','128','14255019769#','','','','hunt-prim','yes','','','','128','7zfImo/Qk01oJzssIm14FXwro0BYN7Sn4hBHqT1Ytrs=','128','993703','','','1','2020-07-10 09:48:13','2020-07-10 09:48:13');
INSERT INTO `gd_flatowner` ( ) VALUES('29','0','0','Steve','Hansen','405','4','129','','129','12069720020#','','','','hunt-prim','yes','','','','129','KeyYCGtP/+pdHufXI7hvtNer8qE/ZfQvYS7QeDPq080=','129','327309','','','1','2020-07-10 09:48:17','2020-07-10 09:48:17');
INSERT INTO `gd_flatowner` ( ) VALUES('30','0','0','Mary','Berman','501','5','130','','130','12068507195#','','','','hunt-prim','yes','','','','130','+nHClychSZv2Sq218D9l5rN1b9BDARM3trgCTuDETwQ=','130','841711','','','1','2020-07-10 09:48:22','2020-07-10 09:48:22');
INSERT INTO `gd_flatowner` ( ) VALUES('31','0','0','Bill','Desai','501','5','131','','131','16465981827#','','','','hunt-prim','yes','','','','131','+b+ZefFxTjQOMcoHoxytE8aXf5XSfywoYYipsFErmB4=','31','940876','','','1','2020-07-10 09:48:27','2020-07-10 09:48:27');
INSERT INTO `gd_flatowner` ( ) VALUES('32','0','0','Brad','Smith','502','5','132','','132','14254295310#','','','','hunt-prim','yes','','','','132','oA22W4FPRgEZOBp1RCizvyMCstNQATvh2ySMBhlVqZo=','132','882599','','','1','2020-07-10 09:48:31','2020-07-10 09:48:31');
INSERT INTO `gd_flatowner` ( ) VALUES('33','0','0','Ally','Dubois','503','5','133','','133','12069305142#','','','','hunt-prim','yes','','','','133','KpjCSsjGwJrPIyiakEGY9TLQIr+wQkozwgaRq5CBC+k=','133','770369','','','1','2020-07-10 09:48:36','2020-07-10 09:48:36');
INSERT INTO `gd_flatowner` ( ) VALUES('34','0','0','Les','Louia','504','5','134','','134','12069725709#','','','','hunt-prim','yes','','','','134','FQ4BS2jtvkMs6o0GfjppKCsqPVPxWW5QgBkJYkRpRsw=','134','450534','','','1','2020-07-10 09:48:41','2020-07-10 09:48:41');
INSERT INTO `gd_flatowner` ( ) VALUES('35','0','0','Alyssa','Moran','505','5','135','','135','16103126358#','','','','hunt-prim','yes','','','','135','8oVnLfuLUPtrCIxBaBYRalLd8QqpHww4J/25sykK6gU=','135','395438','','','1','2020-07-10 09:48:46','2020-07-10 09:48:46');
INSERT INTO `gd_flatowner` ( ) VALUES('36','0','0','Moran','Moran','505','5','136','','136','16103299564#','','','','hunt-prim','yes','','','','136','xKfksqRl/i4VEXYla86SoWOLLAdqSPOYHEXZ3fjM6EY=','136','340119','','','1','2020-07-10 09:48:50','2020-07-10 09:48:50');
INSERT INTO `gd_flatowner` ( ) VALUES('37','0','0','Mason','Killebrew','601','6','137','','137','12062821586#','','','','hunt-prim','yes','','','','137','eXcURpTe+fScyGMx2ebhLocCOjsG6miWdJJUC/a85A4=','137','664022','','','1','2020-07-10 09:48:55','2020-07-10 09:48:55');
INSERT INTO `gd_flatowner` ( ) VALUES('38','0','0','Noralee','Killebrew','601','6','138','','138','12062821586#','','','','hunt-prim','yes','','','','138','bFdQoQEwYb01kbqPi759YArteZKu185ERoEIQkd2Ts0=','138','832760','','','1','2020-07-10 09:49:00','2020-07-10 09:49:00');
INSERT INTO `gd_flatowner` ( ) VALUES('39','0','0','Ron','Ginter','602','6','139','','139','12062824432#','','','','hunt-prim','yes','','','','139','0Vdhp1Cu06iLD4NerevFWYAoUH5Z2K0uwcq1Bbgq1wM=','39','946612','','','1','2020-07-10 09:49:05','2020-07-10 09:49:05');
INSERT INTO `gd_flatowner` ( ) VALUES('40','0','0','Larry','Strecker','603','6','140','','140','12062823671#','','','','hunt-prim','yes','','','','140','bHlJYJa501JYMP5eXE1SS3X2wTLSrJW2jM4KiT92418=','140','882082','','','1','2020-07-10 09:49:10','2020-07-10 09:49:10');
INSERT INTO `gd_flatowner` ( ) VALUES('41','0','0','Vicki','Evans','604','6','141','','141','12064506742#','12069497902#','','','hunt-prim','yes','','','','141','aPhC0fulCZN5HN4QRAc3Byhy8r8hy7nwUmS+GAMePIg=','141','776796','','','1','2020-07-10 09:49:15','2020-07-10 09:49:15');
INSERT INTO `gd_flatowner` ( ) VALUES('42','0','0','Cynthia','Romano','605','6','142','','142','12065953792#','','','','hunt-prim','yes','','','','142','RZNQbvItAoDPd2YU81CGxTQZdUS6DZSEkc1MWF2V2fE=','142','523225','','','1','2020-07-10 09:49:20','2020-07-10 09:49:20');
INSERT INTO `gd_flatowner` ( ) VALUES('43','0','0','Don','Kunze','701','7','143','','143','12063529220#','','','','hunt-prim','yes','','','','143','BLncTOB33lwz2tNg2fdNU1V8JKf+JNvIsmHO2URZQTg=','143','367175','','','1','2020-07-10 09:49:25','2020-07-10 09:49:25');
INSERT INTO `gd_flatowner` ( ) VALUES('44','0','0','David','Fugelso','702','7','144','','144','15053621674#','','','','hunt-prim','yes','','','','144','943IaZZgyCG2t+ZajHyZJ7fnYw67c6Bp7u1XleTBKA0=','144','518972','','','1','2020-07-10 09:49:30','2020-07-10 09:49:30');
INSERT INTO `gd_flatowner` ( ) VALUES('45','0','0','Sean','Ward','703','7','145','','145','12069922915#','','','','hunt-prim','yes','','','','145','kI23tWkyBHeN4OzzAUQWfOoL6ikWF+eKb+nSQc+cbhk=','145','754243','','','1','2020-07-10 09:49:35','2020-07-10 09:49:35');
INSERT INTO `gd_flatowner` ( ) VALUES('46','0','0','Courtney','Hawkins','704','7','146','','146','12066172548#','','','','hunt-prim','yes','','','','146','S+gz9h+2ZhQanvP0c2G9OoHF+x8lP+O/vNGHILIXf+g=','146','162534','','','1','2020-07-10 09:49:40','2020-07-10 09:49:40');
INSERT INTO `gd_flatowner` ( ) VALUES('47','0','0','Nathalie','Sekula','705','7','147','','147','16307685464#','','','','hunt-prim','yes','','','','147','ga6bRsPX6gQ3ZM6LtK23Vc67V89lvYeuKF2Qpy5RcDA=','147','207440','','','1','2020-07-10 09:49:44','2020-07-10 09:49:44');
INSERT INTO `gd_flatowner` ( ) VALUES('48','0','0','Quentin','Johnson','705','7','148','','148','15099914207#','','','','hunt-prim','yes','','','','148','kbsL/A0JOJsJD2zt+RmShFgYh6MZ4cxqo3Kdyk5Rw6A=','148','273321','','','1','2020-07-10 09:49:49','2020-07-10 09:49:49');
INSERT INTO `gd_flatowner` ( ) VALUES('49','0','0','Cathy','Kutsunai','801','8','149','','149','12065953020#','','','','hunt-prim','yes','','','','149','cj40si+eABpcb5Suv2lcbZKEKN/VJw5ixW88M3FkDtc=','149','625372','','','1','2020-07-10 09:49:54','2020-07-10 09:49:54');
INSERT INTO `gd_flatowner` ( ) VALUES('50','0','0','Cindy','Lin','801','8','150','','150','14258905546#','','','','hunt-prim','yes','','','','150','HJydt4cA/4zvcZvJEOo+6ai5sSBmQPARhFGLCRG4p+s=','150','627738','','','1','2020-07-10 09:49:59','2020-07-10 09:49:59');
INSERT INTO `gd_flatowner` ( ) VALUES('51','0','0','R.J.','Hillman','802','8','151','','151','12062826711#','','','','hunt-prim','yes','','','','151','lzSIqDdUxJj4XHzioIinLqzCbMU+8+EOXDQkO6fAMiw=','151','253007','','','1','2020-07-10 09:50:04','2020-07-10 09:50:04');
INSERT INTO `gd_flatowner` ( ) VALUES('52','0','0','Edward','Hillman','802','8','152','','152','12062826711#','','','','hunt-prim','yes','','','','152','EWSMiOwG8A8QEZ/WzuQwc5XqRNYcZDLGjLScQhM5+Xk=','152','215524','','','1','2020-07-10 09:50:09','2020-07-10 09:50:09');
INSERT INTO `gd_flatowner` ( ) VALUES('53','0','0','Joesph','Fradkin','803','8','153','','153','12067453050#','','','','hunt-prim','yes','','','','153','PmPwniJbt3f1gVmUOWGE13pIHB5IRmi0aq7xr8GstOg=','153','664919','','','1','2020-07-10 09:50:14','2020-07-10 09:50:14');
INSERT INTO `gd_flatowner` ( ) VALUES('54','0','0','Oliver','Silberstain','803','8','154','','154','12067453049#','','','','hunt-prim','yes','','','','154','TNitdMLSXg36aK0T+Att8NTy+NaKnBQxiCeY8Zhzc44=','154','420209','','','1','2020-07-10 09:50:19','2020-07-10 09:50:19');
INSERT INTO `gd_flatowner` ( ) VALUES('55','0','0','Allison','Langolf','804','8','155','','155','12067352388#','','','','hunt-prim','yes','','','','155','WMWn3g45/7Xjal9uv/ZCZzm/VvnecVYNAoEdyT0+vMk=','155','329019','','','1','2020-07-10 09:50:24','2020-07-10 09:50:24');
INSERT INTO `gd_flatowner` ( ) VALUES('56','0','0','Samantha','Kopla','805','8','156','','156','12063079059#','','','','hunt-prim','yes','','','','156','sz+YBj16AIMEgR1ZXU6QQ7TkFqF/Bgtf3LgSPMSwIw4=','156','987226','','','1','2020-07-10 09:50:29','2020-07-10 09:50:29');
INSERT INTO `gd_flatowner` ( ) VALUES('57','0','0','Gail','Gilliland','901','9','157','','157','12062839712#','','','','hunt-prim','yes','','','','157','KfLf0ArLEnnpTwHBLofO/axcdqY6HU+jGRDTgtNqvV4=','157','354526','','','1','2020-07-10 09:50:35','2020-07-10 09:50:35');
INSERT INTO `gd_flatowner` ( ) VALUES('58','0','0','Bart','Malagon Ramirez','902','9','158','','158','12064121484#','','','','hunt-prim','yes','','','','158','H7QhdQtQZ7odnN/hine8rLty88H6DfwcQCQI91/KFdA=','158','815565','','','1','2020-07-10 09:50:40','2020-07-10 09:50:40');
INSERT INTO `gd_flatowner` ( ) VALUES('59','0','0','Bradly','Sundar','903','9','159','','159','13603630009#','','','','hunt-prim','yes','','','','159','grTrGs3h+d3WiEs1D52Zxfg1OzI4cuFE6IE734a4vEE=','159','807439','','','1','2020-07-10 09:50:45','2020-07-10 09:50:45');
INSERT INTO `gd_flatowner` ( ) VALUES('60','0','0','James','Lammert','904','9','160','','160','12539730563#','','','','hunt-prim','yes','','','','160','oLVsfU4RNm8L2A24ebhxFYfX01eaDiqyFObhSsW6rbI=','160','310282','','','1','2020-07-10 09:50:50','2020-07-10 09:50:50');
INSERT INTO `gd_flatowner` ( ) VALUES('61','0','0','Ethan','Trudeau','905','9','161','','161','12064897008#','','','','hunt-prim','yes','','','','161','q0GyqlKlg8rVFgpm+Lq/7gMPCr+9BsoEEmNyx6xoZ5s=','161','784110','','','1','2020-07-10 09:50:55','2020-07-10 09:50:55');
INSERT INTO `gd_flatowner` ( ) VALUES('62','0','0','Dawn','Zietz','905','9','162','','162','13609290647#','','','','hunt-prim','yes','','','','162','RM5qid+UUkuNn3HUDVxvYX70Z1m8W3zNeWwqdROg1ig=','162','172391','','','1','2020-07-10 09:51:00','2020-07-10 09:51:00');
INSERT INTO `gd_flatowner` ( ) VALUES('63','0','0','Blake','Chow','1001','10','163','','163','12062866632#','','','','hunt-prim','yes','','','','163','OYyrfyoUdat6Vq2DbcFfTiiLTv9dlIND2RongAQ50/E=','163','402155','','','1','2020-07-10 09:51:05','2020-07-10 09:51:05');
INSERT INTO `gd_flatowner` ( ) VALUES('64','0','0','Victoria','Hung','1002','10','164','','164','12062866632#','','','','hunt-prim','yes','','','','164','jLUO4OQNCiJxvbljdgSCkSnM8JWB1iZdbiiCgAcgRXI=','164','422584','','','1','2020-07-10 09:51:10','2020-07-10 09:51:10');
INSERT INTO `gd_flatowner` ( ) VALUES('65','0','0','Jason','Furrow','1003','10','165','','165','19097200327# ','','','','hunt-prim','yes','','','','165','KGUqAHqHqaxisiBnK7qTi+2iZYNh7TrPQChqsKHbk2o=','165','976499','','','1','2020-07-10 09:51:15','2020-07-10 09:51:15');
INSERT INTO `gd_flatowner` ( ) VALUES('66','0','0','Lilly','Furrow','1003','10','166','','166','16267869176#','','','','hunt-prim','yes','','','','166','EZmigEx1AIx3LROjsWRqAUkelMuLdgt0Oj99mPIH8Jg=','166','864846','','','1','2020-07-10 09:51:21','2020-07-10 09:51:21');
INSERT INTO `gd_flatowner` ( ) VALUES('67','0','0','Frank','Wilson','1004','10','167','','167','12064275806#','','','','hunt-prim','yes','','','','167','YgbXUyyY/vf8JN/A3c6IMVXXBUHAKrtAHNNT2GFzW34=','167','853059','','','1','2020-07-10 09:51:26','2020-07-10 09:51:26');
INSERT INTO `gd_flatowner` ( ) VALUES('68','0','0','Grant','Doak','1004','11','168','','168','12063310109#','','','','hunt-prim','yes','','','','168','hWPYiyVAZVHP85jNNNgRJVRksdrKKwmhd484OtzQNxM=','168','771932','','','1','2020-07-10 09:51:31','2020-07-10 09:51:31');
INSERT INTO `gd_flatowner` ( ) VALUES('69','0','0','Kat','Dowd','1102','11','169','','169','12062256896#','','','','hunt-prim','yes','','','','169','BrjkoGswI1QrhjxEsy2mxhOuBxDuiH2GDKcaqz+5UiA=','169','369730','','','1','2020-07-10 09:51:36','2020-07-10 09:51:36');
INSERT INTO `gd_flatowner` ( ) VALUES('70','0','0','William','Yamaguchi','1103','11','170','','170','12062284827#','','','','hunt-prim','yes','','','','170','MJkqmcBpsdh2XuCblLpI6jc1UtS7ocjY61XyQEZENRw=','170','562427','','','1','2020-07-10 09:51:41','2020-07-10 09:51:41');
INSERT INTO `gd_flatowner` ( ) VALUES('71','0','0','Ursula','Cummings','1104','11','171','','171','12062170748#','','','','hunt-prim','yes','','','','171','lz3xHi63ok+MZ2ygA7+8tTFcH9hbOKpzaPnzfXufLdI=','171','762359','','','1','2020-07-10 09:51:47','2020-07-10 09:51:47');
INSERT INTO `gd_flatowner` ( ) VALUES('72','0','0','Harry','Wigg','104','1','172','','172','18027609218#','','','','hunt-prim','yes','','','','172','k0Ik1elFGHkDoU1PoHbJ2hqtPcS49M+6JQ5KQqmx9Ik=','172','897092','','','1','2020-07-10 09:51:52','2020-07-10 09:51:52');
INSERT INTO `gd_flatowner` ( ) VALUES('73','0','0','Ian','Henderson','102','1','173','','173','13607310789#','','','','hunt-prim','yes','','','','173','u2jjGKgaWUOlDJvSYMEn001Us99ibgd5oem5Po4JobQ=','173','213116','','','1','2020-07-10 09:51:57','2020-07-10 09:51:57');
INSERT INTO `gd_flatowner` ( ) VALUES('74','0','0','Ryan','Bottoms','204','2','174','','174','16153058059#','','','','hunt-prim','yes','','','','174','A82eRQ2r/vc2BrWD7v/yZWKXUGPh8f9uP3zVfMEo0rk=','174','542916','','','1','2020-07-10 09:52:02','2020-07-10 09:52:02');
INSERT INTO `gd_flatowner` ( ) VALUES('75','0','0','Rand','Oh','401','4','175','','175','19259844471#','','','','hunt-prim','yes','','','','175','Mu3qvfYDlYKwyryJgobHgBiJwDkkFIGSa637livrDjQ=','175','659155','','','1','2020-07-10 09:52:08','2020-07-10 09:52:08');
INSERT INTO `gd_flatowner` ( ) VALUES('76','0','0','Wayne','Biever','404','4','176','','176','12069026663#','','','','hunt-prim','yes','','','','176','6QgSpSjZMdxGP3DEk/TvgbrsPXm9Z7l5dOUYP8gbGlg=','176','443169','','','1','2020-07-10 09:52:13','2020-07-10 09:52:13');
INSERT INTO `gd_flatowner` ( ) VALUES('77','0','0','Wane','Hansen','405','4','177','','177','12064840222#','','','','hunt-prim','yes','','','','177','VmTPs9eWv4HJcbkyEFSf9Fove2Ge9s+6ofvIUdnT7kQ=','177','180994','','','1','2020-07-10 09:52:19','2020-07-10 09:52:19');
INSERT INTO `gd_flatowner` ( ) VALUES('78','0','0','Brook','Trinh','804','8','178','','178','12532292207#','','','','hunt-prim','yes','','','','178','Izg2/K5Hke7wT2A1a1u86FM9Hz84ilgVHpPcDGam7eE=','178','975629','','','1','2020-07-10 09:52:24','2020-07-10 09:52:24');
INSERT INTO `gd_flatowner` ( ) VALUES('79','0','0','Billy','Sims','902','9','179','','179','12067736516#','','','','hunt-prim','yes','','','','179','DBxuL5nrkpbCbCWO/MLUedPIi6a74jltI0SzxOwgIts=','179','504181','','','1','2020-07-10 09:52:29','2020-07-10 09:52:29');
INSERT INTO `gd_flatowner` ( ) VALUES('80','0','0','Beth','Iwamoto','1101','11','180','','180','12067900556#','12062482685#','','','hunt-prim','yes','','','','180','vzwvTwIpTJm/ykXP2cpyuT/a9z8aW61PsmxxN+XlE/Y=','180','585213','','','1','2020-07-10 09:52:35','2020-07-10 09:52:35');
INSERT INTO `gd_flatowner` ( ) VALUES('81','0','0','Ashley','Witherspoon','1101','11','181','','181','12063845366#','12062482685#','','','hunt-prim','yes','','','','181','KvG1dH28kR2CDirJhWMML1x6PE4nFCV5+8ZTqCRRHwg=','181','113469','','','1','2020-07-10 09:52:40','2020-07-10 09:52:40');



DROP TABLE IF EXISTS `gd_inbounds`;

CREATE TABLE `gd_inbounds` (
  `row_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `did_number` varchar(255) NOT NULL,
  `cid_number` varchar(255) NOT NULL,
  `cid_priority_route` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=yes',
  `alert_info` varchar(255) DEFAULT NULL,
  `ringer_volume` tinyint(2) NOT NULL DEFAULT '0',
  `cid_name_prefix` varchar(255) DEFAULT NULL,
  `music_on_hold` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=default, 1=none',
  `set_destination` varchar(1000) DEFAULT NULL,
  `privacy_manager` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=yes',
  `max_attempts` int(11) NOT NULL DEFAULT '0',
  `min_length` int(11) NOT NULL DEFAULT '0',
  `description` varchar(1000) NOT NULL,
  `date_added` datetime NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`row_id`),
  UNIQUE KEY `uniqueExtension` (`did_number`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;




DROP TABLE IF EXISTS `gd_otp`;

CREATE TABLE `gd_otp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT 0 COMMENT 'created user id',
  `otp` varchar(255) DEFAULT NULL,
  `enc_otp` varchar(255) DEFAULT NULL COMMENT 'encrypted otp',
  `device_type` varchar(6) DEFAULT NULL COMMENT '1- IOS, 2-Android',
  `device_token` varchar(255) DEFAULT NULL,
  `unique_id` varchar(255) DEFAULT NULL,
  `otp_status` enum('new','consumed') NOT NULL DEFAULT 'new' COMMENT 'new - during user creation, consumed - usedby user',
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0--inactive, 1--active',
  `created_date` datetime DEFAULT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf16;

INSERT INTO `gd_otp` ( ) VALUES('1','1','145797','ecf858ccd122c0713f57d0f7f5f453e8','0','','','new','1','2020-07-10 09:23:25','2020-07-10 09:23:25');
INSERT INTO `gd_otp` ( ) VALUES('2','2','718971','99dd927c16a0da894737ae406717a852','0','','','new','1','2020-07-10 09:41:51','2020-07-10 09:41:51');
INSERT INTO `gd_otp` ( ) VALUES('3','3','886727','26e2da400ad79f39cd86270b4d407cbf','0','','','new','1','2020-07-10 09:42:16','2020-07-10 09:42:16');
INSERT INTO `gd_otp` ( ) VALUES('4','4','943932','ab48f64346fa7f7274de2d40d312bd57','0','','','new','1','2020-07-10 09:42:35','2020-07-10 09:42:35');
INSERT INTO `gd_otp` ( ) VALUES('5','5','972940','759e70fc63c0c39d39ad5f07581702f1','0','','','new','1','2020-07-10 09:46:29','2020-07-10 09:46:29');
INSERT INTO `gd_otp` ( ) VALUES('6','6','173705','608f36ed83b16eaa7596b25e7cb7a258','0','','','new','1','2020-07-10 09:46:34','2020-07-10 09:46:34');
INSERT INTO `gd_otp` ( ) VALUES('7','7','269353','dd8b90e0ce8c499f6ce41df5d6f6c3eb','0','','','new','1','2020-07-10 09:46:38','2020-07-10 09:46:38');
INSERT INTO `gd_otp` ( ) VALUES('8','8','832625','5dec226a08b4a91c8f604a5791c2889d','0','','','new','1','2020-07-10 09:46:42','2020-07-10 09:46:42');
INSERT INTO `gd_otp` ( ) VALUES('9','9','788457','a6599ffbd691a1c940b0c0f7a8b1d1fa','0','','','new','1','2020-07-10 09:46:46','2020-07-10 09:46:46');
INSERT INTO `gd_otp` ( ) VALUES('10','10','134928','28c6e870b04cc71063977a1f474bc16f','0','','','new','1','2020-07-10 09:46:51','2020-07-10 09:46:51');
INSERT INTO `gd_otp` ( ) VALUES('11','11','692848','ad82189644758f22c3c842e87bcaccf3','0','','','new','1','2020-07-10 09:46:55','2020-07-10 09:46:55');
INSERT INTO `gd_otp` ( ) VALUES('12','12','203473','2dc4edc0abfeadde8b7fb314b4552186','0','','','new','1','2020-07-10 09:46:59','2020-07-10 09:46:59');
INSERT INTO `gd_otp` ( ) VALUES('13','13','592252','22a38d9bb8b4ece0be0c16971c796484','0','','','new','1','2020-07-10 09:47:04','2020-07-10 09:47:04');
INSERT INTO `gd_otp` ( ) VALUES('14','14','837998','ceecfebe5deb89370fe8ac96f8853a52','0','','','new','1','2020-07-10 09:47:08','2020-07-10 09:47:08');
INSERT INTO `gd_otp` ( ) VALUES('15','15','537423','65f78657ba844895d6644d46d6ca6dcf','0','','','new','1','2020-07-10 09:47:13','2020-07-10 09:47:13');
INSERT INTO `gd_otp` ( ) VALUES('16','16','791208','acdd77f1b4de99f0418a480c269c473c','0','','','new','1','2020-07-10 09:47:17','2020-07-10 09:47:17');
INSERT INTO `gd_otp` ( ) VALUES('17','17','700751','87fe0b14e71fee2766e4bde07bcc1949','0','','','new','1','2020-07-10 09:47:22','2020-07-10 09:47:22');
INSERT INTO `gd_otp` ( ) VALUES('18','18','146361','ad0c4b783fc1535960d24e2339d86743','0','','','new','1','2020-07-10 09:47:26','2020-07-10 09:47:26');
INSERT INTO `gd_otp` ( ) VALUES('19','19','239529','e86e210e14d76977153f2c71fe106057','0','','','new','1','2020-07-10 09:47:31','2020-07-10 09:47:31');
INSERT INTO `gd_otp` ( ) VALUES('20','20','356199','b251d905c71e5c0aa40f808c62b0313a','0','','','new','1','2020-07-10 09:47:35','2020-07-10 09:47:35');
INSERT INTO `gd_otp` ( ) VALUES('21','21','569854','f82e0cd83f1258fae94d45f523c97a7a','0','','','new','1','2020-07-10 09:47:40','2020-07-10 09:47:40');
INSERT INTO `gd_otp` ( ) VALUES('22','22','270759','676eb0b473da8df9b9d51b8ac565771e','0','','','new','1','2020-07-10 09:47:45','2020-07-10 09:47:45');
INSERT INTO `gd_otp` ( ) VALUES('23','23','344484','ed893299b94cb396c32b5f807c9f56fe','0','','','new','1','2020-07-10 09:47:49','2020-07-10 09:47:49');
INSERT INTO `gd_otp` ( ) VALUES('24','24','874864','510f44c0f653754c39f7c01affb596a3','0','','','new','1','2020-07-10 09:47:54','2020-07-10 09:47:54');
INSERT INTO `gd_otp` ( ) VALUES('25','25','853818','f393b5c72962c7ce45587267816a8ff3','0','','','new','1','2020-07-10 09:47:58','2020-07-10 09:47:58');
INSERT INTO `gd_otp` ( ) VALUES('26','26','594959','ef2ad49f3caf845c40bc0fd9e4d12b57','0','','','new','1','2020-07-10 09:48:03','2020-07-10 09:48:03');
INSERT INTO `gd_otp` ( ) VALUES('27','27','511786','b6fd1c15335f7bc93e3ce2ee3ff746e2','0','','','new','1','2020-07-10 09:48:08','2020-07-10 09:48:08');
INSERT INTO `gd_otp` ( ) VALUES('28','28','262034','c4d886abab957246da1efa51ff1fcd7d','0','','','new','1','2020-07-10 09:48:13','2020-07-10 09:48:13');
INSERT INTO `gd_otp` ( ) VALUES('29','29','310107','a528d2ac37844c13eb84eb1bc425b61a','0','','','new','1','2020-07-10 09:48:17','2020-07-10 09:48:17');
INSERT INTO `gd_otp` ( ) VALUES('30','30','638685','41829e71d49ded4baf387ef4a91d44f6','0','','','new','1','2020-07-10 09:48:22','2020-07-10 09:48:22');
INSERT INTO `gd_otp` ( ) VALUES('31','31','257964','c1405402d56ee92db1d7cf0b1531b1f6','0','','','new','1','2020-07-10 09:48:27','2020-07-10 09:48:27');
INSERT INTO `gd_otp` ( ) VALUES('32','32','892999','de481fc212ac4df6251f98b3b77157dc','0','','','new','1','2020-07-10 09:48:31','2020-07-10 09:48:31');
INSERT INTO `gd_otp` ( ) VALUES('33','33','272019','6e44c25bbcded1376ab100342c1a7350','0','','','new','1','2020-07-10 09:48:36','2020-07-10 09:48:36');
INSERT INTO `gd_otp` ( ) VALUES('34','34','748416','190712d10eff2b16c0f53bb4dc84d392','0','','','new','1','2020-07-10 09:48:41','2020-07-10 09:48:41');
INSERT INTO `gd_otp` ( ) VALUES('35','35','593483','b88d362d5b8b7561059d7ff18e0f4c1a','0','','','new','1','2020-07-10 09:48:46','2020-07-10 09:48:46');
INSERT INTO `gd_otp` ( ) VALUES('36','36','858669','5d0abb8de05331f9ad64df270ed5f8a3','0','','','new','1','2020-07-10 09:48:50','2020-07-10 09:48:50');
INSERT INTO `gd_otp` ( ) VALUES('37','37','914304','ec890671432520995698b2c5cd1597c1','0','','','new','1','2020-07-10 09:48:55','2020-07-10 09:48:55');
INSERT INTO `gd_otp` ( ) VALUES('38','38','151672','bb4761f6930cc9ff78bf5649916955d0','0','','','new','1','2020-07-10 09:49:00','2020-07-10 09:49:00');
INSERT INTO `gd_otp` ( ) VALUES('39','39','227330','f0b5814b041b2e3c6eacd8271dcb7284','0','','','new','1','2020-07-10 09:49:05','2020-07-10 09:49:05');
INSERT INTO `gd_otp` ( ) VALUES('40','40','615819','502cc688122af81502e3b4f9e1d761a0','0','','','new','1','2020-07-10 09:49:10','2020-07-10 09:49:10');
INSERT INTO `gd_otp` ( ) VALUES('41','41','636697','10d26aa864ad60447f01f9b6eb8c6076','0','','','new','1','2020-07-10 09:49:15','2020-07-10 09:49:15');
INSERT INTO `gd_otp` ( ) VALUES('42','42','924927','18ba1c30cdce510f2bf6887f9f5972a1','0','','','new','1','2020-07-10 09:49:20','2020-07-10 09:49:20');
INSERT INTO `gd_otp` ( ) VALUES('43','43','354378','38b163994b7c35caa3a1e24e80a68378','0','','','new','1','2020-07-10 09:49:25','2020-07-10 09:49:25');
INSERT INTO `gd_otp` ( ) VALUES('44','44','548977','441b95aa7bdfdfaee14ba84724f63b37','0','','','new','1','2020-07-10 09:49:30','2020-07-10 09:49:30');
INSERT INTO `gd_otp` ( ) VALUES('45','45','486015','b4e581f13f0445e29d346a310a31c324','0','','','new','1','2020-07-10 09:49:35','2020-07-10 09:49:35');
INSERT INTO `gd_otp` ( ) VALUES('46','46','238064','e98412de72a0f1292b8226af6893c39d','0','','','new','1','2020-07-10 09:49:40','2020-07-10 09:49:40');
INSERT INTO `gd_otp` ( ) VALUES('47','47','139117','2e55f78ecc6ef21c995f982440f82837','0','','','new','1','2020-07-10 09:49:44','2020-07-10 09:49:44');
INSERT INTO `gd_otp` ( ) VALUES('48','48','318825','e606e8c1f9fef30a1cead1ebe9d9b3f3','0','','','new','1','2020-07-10 09:49:49','2020-07-10 09:49:49');
INSERT INTO `gd_otp` ( ) VALUES('49','49','151373','14a1094b327a3d449f204e203698167b','0','','','new','1','2020-07-10 09:49:54','2020-07-10 09:49:54');
INSERT INTO `gd_otp` ( ) VALUES('50','50','433481','b2032c54cd0ab380402f9f75da36f8ca','0','','','new','1','2020-07-10 09:49:59','2020-07-10 09:49:59');
INSERT INTO `gd_otp` ( ) VALUES('51','51','496857','44ca7d0f19ea9bcdc8c367ea27c55992','0','','','new','1','2020-07-10 09:50:04','2020-07-10 09:50:04');
INSERT INTO `gd_otp` ( ) VALUES('52','52','856137','7e7477721c4ddec3c9f5d12c2aea29da','0','','','new','1','2020-07-10 09:50:09','2020-07-10 09:50:09');
INSERT INTO `gd_otp` ( ) VALUES('53','53','859083','a2732911812d5ed6226d6ed1d6cf7421','0','','','new','1','2020-07-10 09:50:14','2020-07-10 09:50:14');
INSERT INTO `gd_otp` ( ) VALUES('54','54','199287','50a018df41af13e55b73a8cf429bbc15','0','','','new','1','2020-07-10 09:50:19','2020-07-10 09:50:19');
INSERT INTO `gd_otp` ( ) VALUES('55','55','904255','dd426a4dd00540de32d3d581f0431c3d','0','','','new','1','2020-07-10 09:50:24','2020-07-10 09:50:24');
INSERT INTO `gd_otp` ( ) VALUES('56','56','967610','922d4c402a35977f5728fa37e77716c5','0','','','new','1','2020-07-10 09:50:29','2020-07-10 09:50:29');
INSERT INTO `gd_otp` ( ) VALUES('57','57','109216','781beb7ff9b4072e3171ed7f8b5f32d2','0','','','new','1','2020-07-10 09:50:35','2020-07-10 09:50:35');
INSERT INTO `gd_otp` ( ) VALUES('58','58','431888','c58bbb063bde60c5f4e121f7773f9d2c','0','','','new','1','2020-07-10 09:50:40','2020-07-10 09:50:40');
INSERT INTO `gd_otp` ( ) VALUES('59','59','416682','e58455878fa050d2d0908e008d6076d3','0','','','new','1','2020-07-10 09:50:45','2020-07-10 09:50:45');
INSERT INTO `gd_otp` ( ) VALUES('60','60','945133','afdbfc0a50bac9bd817a55d4b2af1367','0','','','new','1','2020-07-10 09:50:50','2020-07-10 09:50:50');
INSERT INTO `gd_otp` ( ) VALUES('61','61','563237','8d2820120be3414a34c2ad43a6d7b527','0','','','new','1','2020-07-10 09:50:55','2020-07-10 09:50:55');
INSERT INTO `gd_otp` ( ) VALUES('62','62','611959','9d14d02c8a672962c5bc2c0935241a22','0','','','new','1','2020-07-10 09:51:00','2020-07-10 09:51:00');
INSERT INTO `gd_otp` ( ) VALUES('63','63','319784','6e6a3f9d002f7d54579f167bc65d05f2','0','','','new','1','2020-07-10 09:51:05','2020-07-10 09:51:05');
INSERT INTO `gd_otp` ( ) VALUES('64','64','643350','35648dbc376a0f2bbacf337e8f97d23a','0','','','new','1','2020-07-10 09:51:10','2020-07-10 09:51:10');
INSERT INTO `gd_otp` ( ) VALUES('65','65','744374','ccba197f73fd2129c3bfd7e04bc99f3d','0','','','new','1','2020-07-10 09:51:15','2020-07-10 09:51:15');
INSERT INTO `gd_otp` ( ) VALUES('66','66','629130','e50264347148f40c55c715cb6ce7dbf9','0','','','new','1','2020-07-10 09:51:21','2020-07-10 09:51:21');
INSERT INTO `gd_otp` ( ) VALUES('67','67','486623','b31c5ae3acbcd8dac4d987e4ed9f9cc0','0','','','new','1','2020-07-10 09:51:26','2020-07-10 09:51:26');
INSERT INTO `gd_otp` ( ) VALUES('68','68','329579','c629c602b4d75c5a4bbfdb817cd0f388','0','','','new','1','2020-07-10 09:51:31','2020-07-10 09:51:31');
INSERT INTO `gd_otp` ( ) VALUES('69','69','344158','dea46bfca1fc078dd8614409bbbbefb8','0','','','new','1','2020-07-10 09:51:36','2020-07-10 09:51:36');
INSERT INTO `gd_otp` ( ) VALUES('70','70','607232','b1deb14fcd7752d25ed261adad6977e7','0','','','new','1','2020-07-10 09:51:41','2020-07-10 09:51:41');
INSERT INTO `gd_otp` ( ) VALUES('71','71','463757','b7d418c6593e086779d2e3568674c9cc','0','','','new','1','2020-07-10 09:51:47','2020-07-10 09:51:47');
INSERT INTO `gd_otp` ( ) VALUES('72','72','579071','3ecb40c3c15f3d2a28683c7e2895757e','0','','','new','1','2020-07-10 09:51:52','2020-07-10 09:51:52');
INSERT INTO `gd_otp` ( ) VALUES('73','73','860533','414940ad98a2703fea6c4ec12a65c19d','0','','','new','1','2020-07-10 09:51:57','2020-07-10 09:51:57');
INSERT INTO `gd_otp` ( ) VALUES('74','74','882378','54147c2b7c4f8bc799078ada6e4cfe5c','0','','','new','1','2020-07-10 09:52:02','2020-07-10 09:52:02');
INSERT INTO `gd_otp` ( ) VALUES('75','75','923771','aa76e0c14c36a6eb06e73abc07f265eb','0','','','new','1','2020-07-10 09:52:08','2020-07-10 09:52:08');
INSERT INTO `gd_otp` ( ) VALUES('76','76','123129','5ea1c82e1872b3242d79c82799c869bb','0','','','new','1','2020-07-10 09:52:13','2020-07-10 09:52:13');
INSERT INTO `gd_otp` ( ) VALUES('77','77','730394','5c76935401cf39da79283b0344a5004b','0','','','new','1','2020-07-10 09:52:19','2020-07-10 09:52:19');
INSERT INTO `gd_otp` ( ) VALUES('78','78','367539','8d9992991d4749cc63447f3b21301b40','0','','','new','1','2020-07-10 09:52:24','2020-07-10 09:52:24');
INSERT INTO `gd_otp` ( ) VALUES('79','79','522228','935c65487f607762dc00baf7f3049396','0','','','new','1','2020-07-10 09:52:29','2020-07-10 09:52:29');
INSERT INTO `gd_otp` ( ) VALUES('80','80','299124','5254323829ce67b4512f7020cd109970','0','','','new','1','2020-07-10 09:52:35','2020-07-10 09:52:35');
INSERT INTO `gd_otp` ( ) VALUES('81','81','239821','8a1024a9534b2f66484e281ebe061e4a','0','','','new','1','2020-07-10 09:52:40','2020-07-10 09:52:40');



DROP TABLE IF EXISTS `gd_outbounds`;

CREATE TABLE `gd_outbounds` (
  `route_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `route_name` varchar(255) DEFAULT NULL,
  `route_cid` varchar(255) DEFAULT NULL,
  `override_extension` tinyint(1) NOT NULL DEFAULT '0',
  `route_password` varchar(255) DEFAULT NULL,
  `emergency` varchar(3) NOT NULL DEFAULT 'no',
  `intra_company` varchar(3) NOT NULL DEFAULT 'no',
  `music_on_hold` tinyint(1) NOT NULL DEFAULT '0',
  `route_position` tinyint(1) NOT NULL DEFAULT '0',
  `trunk_sequence` varchar(255) DEFAULT NULL,
  `optional_destination` varchar(255) DEFAULT NULL,
  `prepend_digits` varchar(1000) DEFAULT NULL,
  `match_prefix` varchar(1000) DEFAULT NULL,
  `match_pattern` varchar(1000) DEFAULT NULL,
  `match_cid` varchar(1000) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`route_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `gd_outbounds` ( ) VALUES('1','Out','3603226394','0','','yes','no','0','2','POTS,,','',',',',','1NXXNXXXXXX,NXXNXXXXXX',',','2020-07-10 09:35:51','2020-07-10 10:53:30');



DROP TABLE IF EXISTS `gd_owner_type`;

CREATE TABLE `gd_owner_type` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_type` tinyint(1) NOT NULL COMMENT '1=pm, 2=pd',
  `owner_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`row_id`),
  UNIQUE KEY `uniqueOwnerType` (`owner_type`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `gd_owner_type` ( ) VALUES('1','3','15','2020-07-10 09:27:05','2020-07-10 09:27:05');



DROP TABLE IF EXISTS `gd_time_profiles`;

CREATE TABLE `gd_time_profiles` (
  `time_profile_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `time_profile_name` varchar(255) NOT NULL,
  `time_profile_config` varchar(255) NOT NULL,
  `time_profile_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Active,0=Inactive',
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`time_profile_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `gd_time_profiles` ( ) VALUES('1','All Day','{"sunday":"00:00 to 23:59","monday":"00:00 to 23:59","tuesday":"00:00 to 23:59","wednesday":"00:00 to 23:59","thursday":"00:00 to 23:59","friday":"00:00 to 23:59","saturday":"00:00 to 23:59"}','1','2020-07-09 05:44:51');
INSERT INTO `gd_time_profiles` ( ) VALUES('2','9 to 5, m to f','{"monday":"09:00 to 17:00","tuesday":"09:00 to 17:00","wednesday":"09:00 to 17:00","thursday":"09:00 to 17:00","friday":"09:00 to 17:00"}','1','2020-07-10 09:24:16');



DROP TABLE IF EXISTS `gd_trunks`;

CREATE TABLE `gd_trunks` (
  `trunk_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tech` varchar(10) NOT NULL,
  `trunk_name` varchar(255) NOT NULL,
  `hide_cid` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=yes',
  `outbound_cid` varchar(255) DEFAULT NULL,
  `cid_options` varchar(4) NOT NULL DEFAULT 'off' COMMENT 'off, on, cnum, all',
  `maximum_channels` int(11) NOT NULL DEFAULT '0',
  `dial_options_button` varchar(10) NOT NULL DEFAULT 'system' COMMENT 'system, override',
  `dial_option_value` varchar(255) DEFAULT NULL,
  `continue_if_busy` varchar(3) NOT NULL DEFAULT 'off' COMMENT 'on, off',
  `disable_trunk` varchar(3) NOT NULL DEFAULT 'off' COMMENT 'on, off',
  `prepend_digits` varchar(1000) DEFAULT NULL,
  `match_prefix` varchar(1000) DEFAULT NULL,
  `match_pattern` varchar(1000) DEFAULT NULL,
  `match_cid` varchar(1000) DEFAULT NULL,
  `outbound_dial_prefix` varchar(255) DEFAULT NULL,
  `channel_id` varchar(255) DEFAULT NULL,
  `peer_details` varchar(1000) DEFAULT NULL,
  `peer_values` varchar(1000) DEFAULT NULL,
  `user_context` varchar(255) DEFAULT NULL,
  `user_details` varchar(1000) DEFAULT NULL,
  `user_values` varchar(1000) DEFAULT NULL,
  `register_string` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`trunk_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `gd_trunks` ( ) VALUES('1','sip','POTS','0','3603226394','all','1','system','','off','off','1,','1,','NXXNXXXXXX,NXXNXXXXXX',',','','8675309','username=type=host=secret=disallow=port=allow=context=dtmfmode','8675309=peer=10.35.62.16=kill123amps=all=5062=ulaw=from-trunk=RFC2833','','','','','2020-07-10 09:31:05','2020-07-10 09:39:38');



DROP TABLE IF EXISTS `gd_userlimit`;

CREATE TABLE `gd_userlimit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `max_user` int(11) DEFAULT 0,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_date` datetime DEFAULT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf16;

INSERT INTO `gd_userlimit` ( ) VALUES('1','250','1','2017-06-09 18:39:43','2020-07-10 09:25:25');



