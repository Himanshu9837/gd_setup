/**
 * This function check html tags in field value
 * @author Nishiaknt
 * @created_date 1st Oct, 2016
 */
$(document).ready(function () {
    $("#subadmin").validate({
        errorClass: 'validation-error',
        rules: {
            'firstname': {
                required: true,
                htmltags: true
            },
            'lastname': {
                required: true,
                htmltags: true
            },
            'email': {
                required: true,
                htmltags: true
            },
            'gender': {
                required: true
            },
            'usertype': {
                required: true
            },
            'pwd': {
                required: true
            },

        },
        messages: {
            'firstname': {
                required: "Please enter firstname",
                htmltags: "HTML Tags are not allowed"

            },
            'lastname': {
                required: "Please enter lastname",
                htmltags: "HTML Tags are not allowed"
            },
            'email': {
                required: "Please enter email",
                 htmltags: "HTML Tags are not allowed"

            },
            'gender':{
                required: "Please select gender",
            },
            'usertype':{
                required: "Please select usertype",
            },
            'pwd':{
                required: "Please enter password",
            },
        },
    });
});
jQuery.validator.addMethod("htmltags", function checkHTMLTags(value, element, params) {
    if (value.match(/([\<])([^\>]{1,})*([\>])/i) == null) {
        return true;
    } else {
        return false;
    }
});
