/** 
 * Js for Datatable
 * @author Nishikant
 * @created_date 8th Oct 2016
*/
var oTable, oSettingsusers, oHeader;
var listUserType ='S';  

$(document).ready(function() {           
    oSettingsusers = $('#flatownerlisting').dataTable( {
		"bDestroy": true,
                "bRetrieve": true,
		"iDisplayLength": 10,
		"aLengthMenu": [[10, 20, 30, 50, 10000], [10, 20, 30, 50, "All"]],
		"bPaginate": true,
		"bStateSave": false,
		"pagingType": "full_numbers",
		"sDom": '<lfr>t<"F"ip>',
		"aoColumns": [{"bSortable": false },{ },{ },{ },{ },{"bSortable": false },{ "bSortable": false, sClass: "waitLoader" },{"bSortable": false}],
		"bProcessing": true,
		"bServerSide": true,
                "bLengthChange": true,
                "sPaginationType": "bootstrap",
                "bSortable": true,
                 "order": [[ 0, "asc" ]],
                "oLanguage": { "sEmptyTable": "No matching results found." },
		"fnRowCallback": function( nRow ){
                        $('#flatownerlisting_filter input').attr("placeholder", "Search from here");										
                },						        		
        "sAjaxSource": FULL_URL_PATH+"admin/index/ajaxdashboard"
    });    
} );
