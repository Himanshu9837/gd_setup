$(document).ready(function () {
    $("#addnewowner").validate({
        errorClass: 'validation-error',
        rules: {
            'fname': {
                required: false,
                htmltags: true,
                maxlength: '50'
                
            },
            'extension': {
                required: true,
                htmltags: true,
                number: true                
            },
            'floor': {
                required: true,
            },
        },
        messages: {
            'fname': {
                required: "Please enter the name",
                htmltags: "HTML Tags are not allowed",
                minlength: "Minimum 2 character required",
                maxlength: "Maximum 50 character allowed"

            },           
            'extension': {
                required: "Please enter mobile number",
                htmltags: "HTML Tags are not allowed",
                number: "Mobile number should be numeric"
            },
            'floor': {
                required: "Please select floor"
            },

        },
        submitHandler: function (form) {
            form.submit();
            return true;
        }
    });
});
jQuery.validator.addMethod("htmltags", function checkHTMLTags(value, element, params) {
    if (value.match(/([\<])([^\>]{1,})*([\>])/i) == null) {
        return true;
    } else {
        return false;
    }
});
