/**
 * This function check html tags in field value
 * @author Nishiaknt
 * 
 */
$(document).ready(function () {
    $("#adminprofile").validate({
        errorClass: 'validation-error',
        rules: {
            'firstname': {
                required: true,
                htmltags: true
            },
            'lastname': {
                required: true,
                htmltags: true
            },
            'email': {
                required: true,
                htmltags: true
            },
            'mobile': {
                required: true,
				htmltags: true
            },
            'gender': {
                required: true
            },

        },
        messages: {
            'firstname': {
                required: "Please enter firstname",
                htmltags: "HTML Tags are not allowed"

            },
            'lastname': {
                required: "Please enter lastname",
                htmltags: "HTML Tags are not allowed"
            },
            'email': {
                required: "Please enter email",
				htmltags: "HTML Tags are not allowed"

            },
            'mobile': {
                required: "Please enter mobile number",
                htmltags: "HTML Tags are not allowed"
            },
            'gender':{
                required: "Please select gender",
            },
        },
    });
});
jQuery.validator.addMethod("htmltags", function checkHTMLTags(value, element, params) {
    if (value.match(/([\<])([^\>]{1,})*([\>])/i) == null) {
        return true;
    } else {
        return false;
    }
});
