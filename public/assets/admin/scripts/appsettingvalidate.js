$(document).ready(function () {
    $("#appsetting").validate({
        errorClass: 'validation-error',
        rules: {
            /*'sitename': {
                required: true,
                htmltags: true,
                minlength: '2',
                maxlength: '250'                
            },*/
            'cmiphost': {
                required: true,
                htmltags: true                            
            },
            'cmport': {
                required: true,
                htmltags: true
            },
            'cmname': {
                required: true,
                htmltags: true
            },
            'cmchannel': {
                required: true,
                htmltags: true
            },
            'cmunmae': {
                required: true,
                htmltags: true
            },
            'cmpwd': {
                required: true,
                htmltags: true,
                minlength: '2',
                maxlength: '250'                
            },
            'sipaddr': {
                required: true,
                htmltags: true
            },
            'sipport': {
                required: true,
                htmltags: true               
            },
            'siptransport': {
                required: true
                              
            },
            'sipser1addr': {
                required: true,
                htmltags: true
            },
            'sipser1port': {
                required: true,
                htmltags: true
            },
            'sipser1transport': {
                required: true                              
            },
            'sipser1expires': {
                required: true,
                htmltags: true
            },
            'sipser1register': {
                required: true,
                htmltags: true
            },
            'sipser1retrytmout': {
                required: true,
                htmltags: true
            },
            'sipser1retrymaxct': {
                required: true,
                htmltags: true
            },
            'sipser1lnsztmout': {
                required: true,
                htmltags: true
            }                        
        },
        messages: {
            /*'sitename': {
                required: "Please enter sitename.",
                htmltags: "HTML Tags are not allowed.",
                minlength: "Minimum 2 character required.",
                maxlength: "Maximum 250 character allowed."
            },*/
            'cmiphost': {
                required: "Please enter IP or Host.",
                htmltags: "HTML Tags are not allowed."
            },
            'cmport': {
                required: "Please enter port.",
                htmltags: "HTML Tags are not allowed."
            },
            'cmname': {
                required: "Please enter name.",
                htmltags: "HTML Tags are not allowed."
            },
            'cmchannel': {
                required: "Please enter channel.",
                htmltags: "HTML Tags are not allowed."
            },
            'cmunmae': {
                required: "Please enter username.",
                htmltags: "HTML Tags are not allowed."
            },
            'cmpwd': {
                required: "Please enter password.",
                htmltags: "HTML Tags are not allowed."
            },
            'sipaddr': {
                required: "Please enter address.",
                htmltags: "HTML Tags are not allowed."
            },
            'sipport': {
                required: "Please enter port.",
                htmltags: "HTML Tags are not allowed."
            },
            'siptransport': {
                required: "Please select transport."                
            },
            'sipser1addr': {
                required: "Please enter address.",
                htmltags: "HTML Tags are not allowed."
            },
            'sipser1port': {
                required: "Please enter port.",
                htmltags: "HTML Tags are not allowed."
            },
            'sipser1transport': {
                required: "Please select transport."
            },
            'sipser1expires': {
                required: "Please enter expiry time.",
                htmltags: "HTML Tags are not allowed."
            },
            'sipser1register': {
                required: "Please enter register.",
                htmltags: "HTML Tags are not allowed."
            },
            'sipser1retrytmout': {
                required: "Please enter retry tme out.",
                htmltags: "HTML Tags are not allowed."
            },
            'sipser1retrymaxct': {
                required: "Please enter retry max count.",
                htmltags: "HTML Tags are not allowed."
            },
            'sipser1lnsztmout': {
                required: "Please enter line seize time Oout.",
                htmltags: "HTML Tags are not allowed."
            },
        },
        submitHandler: function (form) {
            form.submit();
            return true;
        }
    });
});
jQuery.validator.addMethod("htmltags", function checkHTMLTags(value, element, params) {
    if (value.match(/([\<])([^\>]{1,})*([\>])/i) == null) {
        return true;
    } else {
        return false;
    }
});
