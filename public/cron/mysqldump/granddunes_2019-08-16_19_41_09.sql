DROP TABLE IF EXISTS `gd_access_codes`;

CREATE TABLE `gd_access_codes` (
  `access_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `access_code` varchar(6) NOT NULL,
  `usage_type` tinyint(1) NOT NULL COMMENT '1=single, 2=multiple',
  `code_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=inactive, 1=active',
  `times_used` int(11) NOT NULL DEFAULT '0' COMMENT 'number of time a code has been used',
  `admin_id` int(11) NOT NULL DEFAULT '0' COMMENT 'admin_id if created by admin',
  `date_added` datetime NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `access_date_time` varchar(255) NOT NULL,
  PRIMARY KEY (`access_id`),
  KEY `accessIndex` (`owner_id`,`access_code`,`code_status`),
  KEY `inCodeStatus` (`code_status`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

INSERT INTO `gd_access_codes` ( ) VALUES('1','4','978963','1','0','0','1','2019-01-22 16:32:54','2019-01-22 20:55:43''',);
INSERT INTO `gd_access_codes` ( ) VALUES('2','83','771907','2','3','0','1','2019-01-22 20:57:46','2019-01-22 20:57:53''',);
INSERT INTO `gd_access_codes` ( ) VALUES('3','68','098859','2','1','0','1','2019-01-23 10:49:25','2019-01-23 10:49:25''',);
INSERT INTO `gd_access_codes` ( ) VALUES('4','5','035305','1','1','0','14','2019-01-23 15:26:04','2019-01-23 15:26:04''',);
INSERT INTO `gd_access_codes` ( ) VALUES('5','68','334887','2','1','0','14','2019-02-05 22:55:52','2019-02-05 22:55:52''',);
INSERT INTO `gd_access_codes` ( ) VALUES('6','116','343478','1','0','0','14','2019-04-07 07:48:53','2019-04-07 08:05:44''',);
INSERT INTO `gd_access_codes` ( ) VALUES('7','98','908384','2','3','0','14','2019-06-29 01:27:46','2019-08-12 09:22:30''',);
INSERT INTO `gd_access_codes` ( ) VALUES('8','3','553926','1','1','0','1','2019-08-12 09:17:52','2019-08-12 09:17:52''2',);
INSERT INTO `gd_access_codes` ( ) VALUES('9','5','559692','1','0','0','1','2019-08-12 09:19:00','2019-08-12 09:19:24''1',);
INSERT INTO `gd_access_codes` ( ) VALUES('10','5','779870','2','1','0','1','2019-08-12 09:20:08','2019-08-12 09:20:08''1',);



DROP TABLE IF EXISTS `gd_access_logs`;

CREATE TABLE `gd_access_logs` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `access_id` bigint(20) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `owner_name` varchar(255) DEFAULT NULL,
  `owner_mobile` int(11) DEFAULT NULL,
  `owner_info` text,
  `date_added` datetime NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM AUTO_INCREMENT=297 DEFAULT CHARSET=latin1;

INSERT INTO `gd_access_logs` ( ) VALUES('1','1','18','Ewan Android P','118','{"unit_number":"217","floor":"2"}','2019-01-10 16:54:40','2019-01-22 05:10:20');
INSERT INTO `gd_access_logs` ( ) VALUES('178','1','4','Alexander Android P','104','{"unit_number":"328","floor":"3"}','2019-01-22 20:55:43','2019-01-22 07:25:43');
INSERT INTO `gd_access_logs` ( ) VALUES('179','3','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-01-23 10:49:39','2019-01-22 21:19:39');
INSERT INTO `gd_access_logs` ( ) VALUES('180','3','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-01-28 02:14:52','2019-01-27 12:44:52');
INSERT INTO `gd_access_logs` ( ) VALUES('181','6','116','Les Lojo','216','{"unit_number":"","floor":"2"}','2019-04-07 08:05:45','2019-04-06 19:35:45');
INSERT INTO `gd_access_logs` ( ) VALUES('182','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 08:06:02','2019-04-06 19:36:02');
INSERT INTO `gd_access_logs` ( ) VALUES('183','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 08:22:41','2019-04-06 19:52:41');
INSERT INTO `gd_access_logs` ( ) VALUES('184','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 08:23:45','2019-04-06 19:53:45');
INSERT INTO `gd_access_logs` ( ) VALUES('185','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 08:39:55','2019-04-06 20:09:55');
INSERT INTO `gd_access_logs` ( ) VALUES('186','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 08:40:40','2019-04-06 20:10:40');
INSERT INTO `gd_access_logs` ( ) VALUES('187','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 08:41:24','2019-04-06 20:11:24');
INSERT INTO `gd_access_logs` ( ) VALUES('188','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 08:45:56','2019-04-06 20:15:56');
INSERT INTO `gd_access_logs` ( ) VALUES('189','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 08:46:39','2019-04-06 20:16:39');
INSERT INTO `gd_access_logs` ( ) VALUES('190','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 09:12:48','2019-04-06 20:42:48');
INSERT INTO `gd_access_logs` ( ) VALUES('191','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 09:40:01','2019-04-06 21:10:01');
INSERT INTO `gd_access_logs` ( ) VALUES('192','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 09:46:06','2019-04-06 21:16:06');
INSERT INTO `gd_access_logs` ( ) VALUES('193','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 10:01:24','2019-04-06 21:31:24');
INSERT INTO `gd_access_logs` ( ) VALUES('194','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 10:01:42','2019-04-06 21:31:42');
INSERT INTO `gd_access_logs` ( ) VALUES('195','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 10:02:28','2019-04-06 21:32:28');
INSERT INTO `gd_access_logs` ( ) VALUES('196','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 10:04:05','2019-04-06 21:34:05');
INSERT INTO `gd_access_logs` ( ) VALUES('197','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 10:05:23','2019-04-06 21:35:23');
INSERT INTO `gd_access_logs` ( ) VALUES('198','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 10:07:37','2019-04-06 21:37:37');
INSERT INTO `gd_access_logs` ( ) VALUES('199','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 10:10:51','2019-04-06 21:40:51');
INSERT INTO `gd_access_logs` ( ) VALUES('200','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 10:16:22','2019-04-06 21:46:22');
INSERT INTO `gd_access_logs` ( ) VALUES('201','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 10:17:19','2019-04-06 21:47:19');
INSERT INTO `gd_access_logs` ( ) VALUES('202','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 10:23:29','2019-04-06 21:53:29');
INSERT INTO `gd_access_logs` ( ) VALUES('203','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 11:15:00','2019-04-06 22:45:00');
INSERT INTO `gd_access_logs` ( ) VALUES('204','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 11:22:20','2019-04-06 22:52:20');
INSERT INTO `gd_access_logs` ( ) VALUES('205','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 11:22:44','2019-04-06 22:52:44');
INSERT INTO `gd_access_logs` ( ) VALUES('206','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 11:23:49','2019-04-06 22:53:49');
INSERT INTO `gd_access_logs` ( ) VALUES('207','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 11:24:41','2019-04-06 22:54:41');
INSERT INTO `gd_access_logs` ( ) VALUES('208','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 11:28:24','2019-04-06 22:58:24');
INSERT INTO `gd_access_logs` ( ) VALUES('209','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 11:32:25','2019-04-06 23:02:25');
INSERT INTO `gd_access_logs` ( ) VALUES('210','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 12:38:20','2019-04-07 00:08:20');
INSERT INTO `gd_access_logs` ( ) VALUES('211','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 12:38:46','2019-04-07 00:08:46');
INSERT INTO `gd_access_logs` ( ) VALUES('212','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 12:39:17','2019-04-07 00:09:17');
INSERT INTO `gd_access_logs` ( ) VALUES('213','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 12:39:48','2019-04-07 00:09:48');
INSERT INTO `gd_access_logs` ( ) VALUES('214','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 12:40:26','2019-04-07 00:10:26');
INSERT INTO `gd_access_logs` ( ) VALUES('215','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 12:40:47','2019-04-07 00:10:47');
INSERT INTO `gd_access_logs` ( ) VALUES('216','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 12:41:05','2019-04-07 00:11:05');
INSERT INTO `gd_access_logs` ( ) VALUES('217','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 13:12:08','2019-04-07 00:42:08');
INSERT INTO `gd_access_logs` ( ) VALUES('218','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 13:12:28','2019-04-07 00:42:28');
INSERT INTO `gd_access_logs` ( ) VALUES('219','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:28:19','2019-04-07 17:58:19');
INSERT INTO `gd_access_logs` ( ) VALUES('220','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:28:58','2019-04-07 17:58:58');
INSERT INTO `gd_access_logs` ( ) VALUES('221','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:29:21','2019-04-07 17:59:21');
INSERT INTO `gd_access_logs` ( ) VALUES('222','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:30:02','2019-04-07 18:00:02');
INSERT INTO `gd_access_logs` ( ) VALUES('223','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:31:33','2019-04-07 18:01:33');
INSERT INTO `gd_access_logs` ( ) VALUES('224','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:32:37','2019-04-07 18:02:37');
INSERT INTO `gd_access_logs` ( ) VALUES('225','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:33:09','2019-04-07 18:03:09');
INSERT INTO `gd_access_logs` ( ) VALUES('226','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:34:22','2019-04-07 18:04:22');
INSERT INTO `gd_access_logs` ( ) VALUES('227','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:34:43','2019-04-07 18:04:43');
INSERT INTO `gd_access_logs` ( ) VALUES('228','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:34:59','2019-04-07 18:04:59');
INSERT INTO `gd_access_logs` ( ) VALUES('229','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:35:23','2019-04-07 18:05:23');
INSERT INTO `gd_access_logs` ( ) VALUES('230','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:35:55','2019-04-07 18:05:55');
INSERT INTO `gd_access_logs` ( ) VALUES('231','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:36:13','2019-04-07 18:06:13');
INSERT INTO `gd_access_logs` ( ) VALUES('232','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:36:38','2019-04-07 18:06:38');
INSERT INTO `gd_access_logs` ( ) VALUES('233','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:37:14','2019-04-07 18:07:14');
INSERT INTO `gd_access_logs` ( ) VALUES('234','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:37:55','2019-04-07 18:07:55');
INSERT INTO `gd_access_logs` ( ) VALUES('235','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:38:21','2019-04-07 18:08:21');
INSERT INTO `gd_access_logs` ( ) VALUES('236','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:38:51','2019-04-07 18:08:51');
INSERT INTO `gd_access_logs` ( ) VALUES('237','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:39:31','2019-04-07 18:09:31');
INSERT INTO `gd_access_logs` ( ) VALUES('238','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:39:51','2019-04-07 18:09:51');
INSERT INTO `gd_access_logs` ( ) VALUES('239','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:40:18','2019-04-07 18:10:18');
INSERT INTO `gd_access_logs` ( ) VALUES('240','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:41:14','2019-04-07 18:11:14');
INSERT INTO `gd_access_logs` ( ) VALUES('241','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:42:20','2019-04-07 18:12:20');
INSERT INTO `gd_access_logs` ( ) VALUES('242','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:42:49','2019-04-07 18:12:49');
INSERT INTO `gd_access_logs` ( ) VALUES('243','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:43:04','2019-04-07 18:13:04');
INSERT INTO `gd_access_logs` ( ) VALUES('244','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:43:27','2019-04-07 18:13:27');
INSERT INTO `gd_access_logs` ( ) VALUES('245','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:44:10','2019-04-07 18:14:10');
INSERT INTO `gd_access_logs` ( ) VALUES('246','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:44:32','2019-04-07 18:14:32');
INSERT INTO `gd_access_logs` ( ) VALUES('247','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:44:50','2019-04-07 18:14:50');
INSERT INTO `gd_access_logs` ( ) VALUES('248','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:44:59','2019-04-07 18:14:59');
INSERT INTO `gd_access_logs` ( ) VALUES('249','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:45:20','2019-04-07 18:15:20');
INSERT INTO `gd_access_logs` ( ) VALUES('250','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:45:35','2019-04-07 18:15:35');
INSERT INTO `gd_access_logs` ( ) VALUES('251','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:46:05','2019-04-07 18:16:05');
INSERT INTO `gd_access_logs` ( ) VALUES('252','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:46:32','2019-04-07 18:16:32');
INSERT INTO `gd_access_logs` ( ) VALUES('253','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:47:11','2019-04-07 18:17:11');
INSERT INTO `gd_access_logs` ( ) VALUES('254','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:47:21','2019-04-07 18:17:21');
INSERT INTO `gd_access_logs` ( ) VALUES('255','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:47:55','2019-04-07 18:17:55');
INSERT INTO `gd_access_logs` ( ) VALUES('256','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:48:50','2019-04-07 18:18:50');
INSERT INTO `gd_access_logs` ( ) VALUES('257','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:50:07','2019-04-07 18:20:07');
INSERT INTO `gd_access_logs` ( ) VALUES('258','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:50:25','2019-04-07 18:20:25');
INSERT INTO `gd_access_logs` ( ) VALUES('259','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:50:27','2019-04-07 18:20:27');
INSERT INTO `gd_access_logs` ( ) VALUES('260','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:51:32','2019-04-07 18:21:32');
INSERT INTO `gd_access_logs` ( ) VALUES('261','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:55:03','2019-04-07 18:25:03');
INSERT INTO `gd_access_logs` ( ) VALUES('262','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:55:31','2019-04-07 18:25:31');
INSERT INTO `gd_access_logs` ( ) VALUES('263','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:55:52','2019-04-07 18:25:52');
INSERT INTO `gd_access_logs` ( ) VALUES('264','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:56:35','2019-04-07 18:26:35');
INSERT INTO `gd_access_logs` ( ) VALUES('265','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:57:58','2019-04-07 18:27:58');
INSERT INTO `gd_access_logs` ( ) VALUES('266','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 06:58:05','2019-04-07 18:28:05');
INSERT INTO `gd_access_logs` ( ) VALUES('267','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 07:00:49','2019-04-07 18:30:49');
INSERT INTO `gd_access_logs` ( ) VALUES('268','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 07:01:38','2019-04-07 18:31:38');
INSERT INTO `gd_access_logs` ( ) VALUES('269','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 07:02:09','2019-04-07 18:32:09');
INSERT INTO `gd_access_logs` ( ) VALUES('270','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 07:05:33','2019-04-07 18:35:33');
INSERT INTO `gd_access_logs` ( ) VALUES('271','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 07:06:03','2019-04-07 18:36:03');
INSERT INTO `gd_access_logs` ( ) VALUES('272','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 07:06:31','2019-04-07 18:36:31');
INSERT INTO `gd_access_logs` ( ) VALUES('273','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 07:07:01','2019-04-07 18:37:01');
INSERT INTO `gd_access_logs` ( ) VALUES('274','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 07:12:14','2019-04-07 18:42:14');
INSERT INTO `gd_access_logs` ( ) VALUES('275','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 07:12:41','2019-04-07 18:42:41');
INSERT INTO `gd_access_logs` ( ) VALUES('276','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 07:13:19','2019-04-07 18:43:19');
INSERT INTO `gd_access_logs` ( ) VALUES('277','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 07:13:40','2019-04-07 18:43:40');
INSERT INTO `gd_access_logs` ( ) VALUES('278','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 07:14:00','2019-04-07 18:44:00');
INSERT INTO `gd_access_logs` ( ) VALUES('279','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 07:14:22','2019-04-07 18:44:22');
INSERT INTO `gd_access_logs` ( ) VALUES('280','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 07:15:01','2019-04-07 18:45:01');
INSERT INTO `gd_access_logs` ( ) VALUES('281','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-08 07:16:07','2019-04-07 18:46:07');
INSERT INTO `gd_access_logs` ( ) VALUES('282','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-06 12:07:59','2019-04-05 23:37:59');
INSERT INTO `gd_access_logs` ( ) VALUES('283','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-06 12:33:42','2019-04-06 00:03:42');
INSERT INTO `gd_access_logs` ( ) VALUES('284','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-06 15:26:12','2019-04-06 02:56:12');
INSERT INTO `gd_access_logs` ( ) VALUES('285','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-06 15:53:22','2019-04-06 03:23:22');
INSERT INTO `gd_access_logs` ( ) VALUES('286','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-06 19:59:28','2019-04-06 07:29:28');
INSERT INTO `gd_access_logs` ( ) VALUES('287','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-07 12:01:57','2019-04-06 23:31:57');
INSERT INTO `gd_access_logs` ( ) VALUES('288','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-15 13:19:56','2019-04-15 00:49:56');
INSERT INTO `gd_access_logs` ( ) VALUES('289','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-15 13:20:12','2019-04-15 00:50:12');
INSERT INTO `gd_access_logs` ( ) VALUES('290','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-15 13:20:31','2019-04-15 00:50:31');
INSERT INTO `gd_access_logs` ( ) VALUES('291','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-15 13:20:48','2019-04-15 00:50:48');
INSERT INTO `gd_access_logs` ( ) VALUES('292','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-15 13:21:08','2019-04-15 00:51:08');
INSERT INTO `gd_access_logs` ( ) VALUES('293','5','68','Property Manager','168','{"unit_number":"","floor":"1"}','2019-04-15 13:21:37','2019-04-15 00:51:37');
INSERT INTO `gd_access_logs` ( ) VALUES('294','9','5','Alli Gant','105','{"unit_number":"304","floor":"3"}','2019-08-12 09:19:24','2019-08-11 20:49:24');
INSERT INTO `gd_access_logs` ( ) VALUES('295','10','5','Alli Gant','105','{"unit_number":"304","floor":"3"}','2019-08-12 09:20:22','2019-08-11 20:50:22');
INSERT INTO `gd_access_logs` ( ) VALUES('296','10','5','Alli Gant','105','{"unit_number":"304","floor":"3"}','2019-08-12 09:20:52','2019-08-11 20:50:52');



DROP TABLE IF EXISTS `gd_admin`;

CREATE TABLE `gd_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `gender` enum('M','F') DEFAULT NULL COMMENT 'M -- Male, F -- Female',
  `role` enum('admin','subadmin','vendor') DEFAULT 'admin',
  `user_type` enum('1','2','3') NOT NULL COMMENT '1-Masteradmin, 2- subadmin, 3-vendor',
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 -- Inactive, 1 -- Active ',
  `created_date` datetime DEFAULT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf16;

INSERT INTO `gd_admin` ( ) VALUES('1','Granddunes','Granddunes','GD','granddunes@gd.com','22222222','g#d$sip123','M','admin','1','1','2017-06-09 18:41:28','2017-06-09 18:41:28');
INSERT INTO `gd_admin` ( ) VALUES('2','Justin','Gant','','jg@test.com','2344','qwerty','M','subadmin','1','1','2018-05-22 18:22:15','2019-01-10 03:19:07');
INSERT INTO `gd_admin` ( ) VALUES('3','Nick','Sc','','nick@test.com','','qwerty','M','vendor','3','0','2018-05-22 18:23:22','2018-06-08 09:10:52');
INSERT INTO `gd_admin` ( ) VALUES('4','Justin','Gant','','justin.gant@gmail.com','1','test','M','vendor','3','0','2018-06-03 01:13:41','2018-06-08 22:02:47');
INSERT INTO `gd_admin` ( ) VALUES('5','Customer','One','','customer@gd.com','1','test','F','subadmin','2','0','2018-06-03 01:14:12','2018-06-08 22:02:45');
INSERT INTO `gd_admin` ( ) VALUES('6','Vv','Dd','','vendor2@gmail.com','1','vendor','M','vendor','3','0','2018-06-09 10:33:29','2018-06-12 18:51:58');
INSERT INTO `gd_admin` ( ) VALUES('7','Cc','Cc','','customer2@gmail.com','1','customer','F','subadmin','2','0','2018-06-09 10:33:53','2018-06-12 18:51:57');
INSERT INTO `gd_admin` ( ) VALUES('8','Vv','Vvv','','vendor3@gmail.com','6666666','12345','F','vendor','3','0','2018-06-13 07:22:28','2018-06-13 23:43:02');
INSERT INTO `gd_admin` ( ) VALUES('9','Ccc','Cccc','','customer3@gmail.com','7777777','12345','M','subadmin','2','0','2018-06-13 07:23:00','2018-06-13 23:43:04');
INSERT INTO `gd_admin` ( ) VALUES('10','Shannon','Demo','','shannon@gd.com','1234567','$hannon601','M','vendor','3','0','2018-07-29 09:54:06','2018-08-07 07:50:52');
INSERT INTO `gd_admin` ( ) VALUES('11','Sam','Vendor','','vendor@gd.com','1234567','12345','M','vendor','3','0','2018-08-07 20:28:30','2018-08-07 10:55:36');
INSERT INTO `gd_admin` ( ) VALUES('12','User','Person','','user@gd.com','9','12345','F','subadmin','2','0','2018-08-07 20:28:53','2018-08-07 10:55:38');
INSERT INTO `gd_admin` ( ) VALUES('14','Admin','Rules','','admin@gd.com','12','admin@work','M','vendor','3','1','2018-10-03 08:45:53','2018-10-02 20:15:53');
INSERT INTO `gd_admin` ( ) VALUES('15','End','User','','enduser@gd.com','1','enduser@work','F','subadmin','2','1','2018-10-03 08:47:31','2018-10-02 20:17:31');
INSERT INTO `gd_admin` ( ) VALUES('16','User','User12','','tenant@gd.com','8888888888','user','M','subadmin','2','1','2019-04-07 15:36:30','2019-04-07 03:06:30');



DROP TABLE IF EXISTS `gd_app_settings`;

CREATE TABLE `gd_app_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sitename` varchar(255) DEFAULT NULL,
  `cm_iphost` varchar(255) DEFAULT NULL,
  `cm_port` varchar(255) DEFAULT NULL,
  `cm_name` varchar(255) DEFAULT NULL,
  `cm_channel` varchar(255) DEFAULT NULL,
  `cm_username` varchar(255) DEFAULT NULL,
  `cm_pwd` varchar(255) DEFAULT NULL,
  `cm_streaming` varchar(50) DEFAULT NULL,
  `sip_addr` varchar(255) DEFAULT NULL,
  `sip_port` varchar(255) DEFAULT NULL,
  `sip_transport` varchar(255) DEFAULT NULL,
  `sip_ser1addr` varchar(255) DEFAULT NULL,
  `sip_ser1_port` varchar(255) DEFAULT NULL,
  `sip_ser1_transport` varchar(255) DEFAULT NULL,
  `sip_ser1_expires` varchar(255) DEFAULT NULL,
  `sip_ser1_register` varchar(255) DEFAULT NULL,
  `sip_ser1_retrytmout` varchar(255) DEFAULT NULL,
  `sip_ser1_retrymaxct` varchar(255) DEFAULT NULL,
  `sip_ser1_lnsztmout` varchar(255) DEFAULT NULL,
  `update_config_url` varchar(255) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 - inactive, 1 - active',
  `created_date` datetime DEFAULT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf16;

INSERT INTO `gd_app_settings` ( ) VALUES('1','First','http://10.35.62.21:8081','80','Camera1','1','admin','jcabsam1','NORMAL','10.35.62.150','6063','1','10.35.62.150','6063','3','1','1','5','9','40','http://10.35.62.201/idlogin','1','2017-06-09 18:41:28','2019-06-27 14:39:44');



DROP TABLE IF EXISTS `gd_callendtime`;

CREATE TABLE `gd_callendtime` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `call_end_time` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 - inactive, 1--active',
  `created_date` datetime DEFAULT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf16;

INSERT INTO `gd_callendtime` ( ) VALUES('1','20','1','2017-06-09 18:14:19','2019-04-07 02:52:45');



DROP TABLE IF EXISTS `gd_configs`;

CREATE TABLE `gd_configs` (
  `config_type` int(11) NOT NULL COMMENT '1=access code limit per user',
  `value` int(11) NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `unConfigType` (`config_type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `gd_configs` ( ) VALUES('1','5','2019-01-22 16:33:01');
INSERT INTO `gd_configs` ( ) VALUES('2','2','2019-08-16 17:12:26');



DROP TABLE IF EXISTS `gd_external_ip`;

CREATE TABLE `gd_external_ip` (
  `ip_id` int(11) NOT NULL AUTO_INCREMENT,
  `external_ip` varchar(255) DEFAULT NULL,
  `added_date` datetime DEFAULT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`ip_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `gd_external_ip` ( ) VALUES('2','5.5.4.3','2018-08-23 20:27:30','2018-08-23 20:27:30');



DROP TABLE IF EXISTS `gd_firewall_configs`;

CREATE TABLE `gd_firewall_configs` (
  `config_type` tinyint(4) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `mask` varchar(255) DEFAULT NULL,
  `gateway` varchar(255) DEFAULT NULL,
  `dns1` varchar(255) DEFAULT NULL,
  `dns2` varchar(255) DEFAULT NULL,
  `modified_date` datetime NOT NULL,
  UNIQUE KEY `unFirewallConfig` (`config_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf16;

INSERT INTO `gd_firewall_configs` ( ) VALUES('1','10.35.62.1','cisco','GrandDunes8675309!','','','','','2018-08-07 23:26:37');
INSERT INTO `gd_firewall_configs` ( ) VALUES('2','10.0.0.2','','','255.0.0.0','10.0.0.1','10.0.0.1','8.8.8.8','2018-08-08 09:38:59');



DROP TABLE IF EXISTS `gd_flatowner`;

CREATE TABLE `gd_flatowner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0',
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `unit_number` varchar(100) DEFAULT NULL,
  `floor` int(11) DEFAULT NULL,
  `mobile` bigint(12) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `followme1` varchar(100) DEFAULT NULL,
  `followme2` varchar(100) DEFAULT NULL,
  `followme3` varchar(100) DEFAULT NULL,
  `followme4` varchar(100) DEFAULT NULL,
  `followme5` varchar(100) DEFAULT NULL,
  `ring_type` varchar(100) DEFAULT NULL,
  `confirmcall` varchar(3) DEFAULT NULL,
  `voice_mail` varchar(100) DEFAULT NULL,
  `create_sip_user` varchar(100) DEFAULT NULL,
  `camera_only` varchar(100) DEFAULT NULL,
  `sip_username` varchar(255) DEFAULT NULL,
  `sip_password` varchar(255) DEFAULT NULL,
  `sip_extention_num` bigint(20) DEFAULT NULL,
  `sip_download_code` bigint(20) DEFAULT NULL,
  `locked_to_device` varchar(255) DEFAULT NULL,
  `sites` varchar(255) DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '1 - active, 0 - inactive',
  `created_date` datetime DEFAULT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf16;

INSERT INTO `gd_flatowner` ( ) VALUES('3','0','Alexis','Schmidt','414','4','103','','216','14255050552#','','','','','yes','','','no','103','KJVuDTHUEZlAg12OY5y7O6wxUXlauBnUjDJEh/h+Jko=','103','581388','IOS:37FD3B34-01EB-4DA6-927A-75E6F0CCFA09','','1','2018-09-29 03:23:30','2019-06-27 14:38:06');
INSERT INTO `gd_flatowner` ( ) VALUES('5','0','Alli','Gant','304','3','105','','217','12066931479#','','','','','yes','','','no','105','AGgH/qxJ/uPa2dv5Hs34j3bmg63JvMtdG1Fy043XiHM=','105','664405','Android:02:00:00:00:00:00','','1','2018-09-29 03:25:38','2019-07-03 15:45:56');
INSERT INTO `gd_flatowner` ( ) VALUES('6','0','Amanda','Larson','203','2','106','','218','12096142154#','','','','','yes','','','no','106','ptrMH1RIp7UkbVaOlLlR5qKq+dgOUF6xuagJSI46bOo=','106','192289','No Devicelocked','','1','2018-09-29 03:26:41','2019-04-07 00:05:26');
INSERT INTO `gd_flatowner` ( ) VALUES('7','0','Andrew','Schmidt','104','1','107','','216','12063277783#','','','','','yes','','','no','107','+JS0T2GxMiZjy9Vxdp11m801g2n8Rwm7ey760vOdPgc=','107','558658','No Devicelocked','','1','2018-09-29 03:26:44','2019-04-07 00:05:54');
INSERT INTO `gd_flatowner` ( ) VALUES('8','0','Bridget & Jason','Schmidt','405','4','108','','216','13608362592#','','','','','yes','','','no','108','OB2KfdbHckfAvGnwis0yW+7dQQAR64oNRcgHKnW13uc=','108','803660','No Devicelocked','','1','2018-09-29 03:26:48','2019-04-07 00:17:49');
INSERT INTO `gd_flatowner` ( ) VALUES('9','0','Nick','Schmidt','410','4','109','','216','18082387416#','','','','','yes','','','no','109','2E53yajtpxRY9fWSfhr+V32zTV3h1Yo1yOOueF0cj6E=','109','498003','No Devicelocked','','1','2018-09-29 03:26:51','2019-04-07 00:49:02');
INSERT INTO `gd_flatowner` ( ) VALUES('10','0','Christian','Schmidt','326','3','110','','216','12066179475#','','','','','yes','','','no','110','IHdsulXlYISQQYxqMT7Es+OQL7xBMjhL0fsLxLSueIQ=','110','620760','No Devicelocked','','1','2018-09-29 03:26:55','2019-04-07 00:20:56');
INSERT INTO `gd_flatowner` ( ) VALUES('11','0','Christian','Gant','426','4','111','','217','12067476867#','','','','','yes','','','no','111','t7YMY03f0tXkazjBH30odu54Bfy3LiQMqC/GeYxgMCQ=','111','536183','No Devicelocked','','1','2018-09-29 03:28:00','2019-04-07 00:21:30');
INSERT INTO `gd_flatowner` ( ) VALUES('12','0','Christen','Larson','422','4','112','','218','12066501977#','','','','','yes','','','no','112','fbbIlEAkCbeqvM5s/weZadcO+NooBZRzhoZZ4pBaer0=','112','456371','IOS:82E32B83-0205-4797-8B7B-2F1F9D75E75A','','1','2018-09-29 03:29:04','2019-04-07 00:20:25');
INSERT INTO `gd_flatowner` ( ) VALUES('13','0','D.Tolstrup','Larson','418','4','113','','218','15038966066#','','','','','yes','','','no','113','lBkwRI4jkjI12RyiFTQN/+73hCqis3mikkijZKyqMnE=','113','867498','No Devicelocked','','1','2018-09-29 03:30:07','2019-04-07 00:21:53');
INSERT INTO `gd_flatowner` ( ) VALUES('14','0','Daniil','Schmidt','213','2','114','','216','13608884118#','','','','','yes','','','no','114','NQIv3eoNSLKz34TFkrVQ8CnFnMsfoElNLZncU7ApLYY=','114','699349','No Devicelocked','','1','2018-09-29 03:31:11','2019-04-07 00:22:16');
INSERT INTO `gd_flatowner` ( ) VALUES('15','0','Donna','Gant','167','1','115','','217','19257855773#','','','','','yes','','','no','115','jRitTTodWEyANoxk2SDacFVU9E7S55Q5lPc3BUIRLsM=','115','847027','No Devicelocked','','1','2018-09-29 03:32:15','2019-04-07 00:22:52');
INSERT INTO `gd_flatowner` ( ) VALUES('16','0','Emily','Larson','322','3','116','','218','12488810316#','','','','','yes','','','no','116','lxIeUVYytrPRWgaDVRO7vbFSNOkD/iqSNm579AYqsg8=','116','804434','IOS:B5F19922-DF18-46ED-9464-A599856D2377','','1','2018-09-29 03:32:18','2019-04-07 00:24:55');
INSERT INTO `gd_flatowner` ( ) VALUES('17','0','Eva','Schmidt','411','4','117','','216','16517264794#','','','','','yes','','','no','117','3gzrMCorJm5YF7TI6U75gbusVqA6zR6Yb6KFeULcAaA=','117','334874','No Devicelocked','','1','2018-09-29 03:32:22','2019-04-07 00:25:27');
INSERT INTO `gd_flatowner` ( ) VALUES('18','0','Ewan','Gant','217','2','118','','217','15098081369#','','','','','yes','','','no','118','ch3Ibamzr3a7MunorZH9CTydRPZxltr4W1u0ZGymJ3E=','118','617842','No Devicelocked','','1','2018-09-29 03:32:25','2019-04-07 00:25:55');
INSERT INTO `gd_flatowner` ( ) VALUES('19','0','Fernando','Larson','303','3','119','','218','12108602155#','','','','','yes','','','no','119','b8qmCwMA9JICE1NMq6pHJVTmqWlfqM3Grudtqyq2Yi8=','119','598413','No Devicelocked','','1','2018-09-29 03:32:29','2019-04-07 00:26:20');
INSERT INTO `gd_flatowner` ( ) VALUES('20','0','Gloria','Larson','225','2','120','','218','14252745160#','','','','','yes','','','no','120','QkK4hWGGudq0oN145FIxiu001iWMSNnA64pCd635RXA=','120','334780','No Devicelocked','','1','2018-09-29 03:33:33','2019-04-07 00:27:52');
INSERT INTO `gd_flatowner` ( ) VALUES('21','0','Gouzhang','Schmidt','224','2','121','','216','12063078970#','','','','','yes','','','no','121','1dU/JQjR3R7F0M6up4t/2cjW9ae5XO+b6SP5YHIbLBs=','121','731942','Android:02:00:00:00:00:00','','1','2018-09-29 03:34:36','2019-04-07 00:28:21');
INSERT INTO `gd_flatowner` ( ) VALUES('22','0','HingKiu','Gant','316','3','122','','217','17038897531#','','','','','yes','','','no','122','L1AwW86Yh+2bRyBumxGgUfHHUcUEbm7mOvRjdWGGImw=','122','279815','No Devicelocked','','1','2018-09-29 03:35:40','2019-04-07 00:29:49');
INSERT INTO `gd_flatowner` ( ) VALUES('23','0','J.Pak','Larson','312','3','123','','218','16252951563#','','','','','yes','','','no','123','IBDrKtwn9QRFp3Wosq2ocjkyOS1HEAfirhM2+VQZuaE=','123','193353','No Devicelocked','','1','2018-09-29 03:36:44','2019-04-07 00:30:13');
INSERT INTO `gd_flatowner` ( ) VALUES('24','0','Jaclyn','Larson','401','4','124','','218','14253148486#','','','','','yes','','','no','124','uHTak4UzZsWjWLwGybgEnDe1y91QSqaLY3qz33xOFAo=','124','996465','No Devicelocked','','1','2018-09-29 03:36:48','2019-04-07 00:31:43');
INSERT INTO `gd_flatowner` ( ) VALUES('25','0','Jacob Umar','Schmidt','111','1','125','','216','14259414396#','','','','','yes','','','no','125','v4ON37TwCyD+PT+TvvJu3iItzgQV7YxEMqbTctqt93s=','125','746246','Android:02:00:00:00:00:00','','1','2018-09-29 03:36:51','2019-04-07 00:32:07');
INSERT INTO `gd_flatowner` ( ) VALUES('26','0','Jane','Gant','311','3','126','','216','15082694342#','','','','','yes','','','no','126','/0lUto3Jcr0epAIEwMZksClJ0NBRN3dC3gp0MU2KSf4=','126','781243','No Devicelocked','','1','2018-09-29 03:36:55','2019-04-07 00:32:42');
INSERT INTO `gd_flatowner` ( ) VALUES('27','0','Jane','Larson','412','4','127','','218','14258761217#','','','','','yes','','','no','127','adjRSJ4rDbMA8YCUksVErn39TsBZ1RFp2IFgN3ddCuY=','127','187131','No Devicelocked','','1','2018-09-29 03:36:59','2019-04-07 00:33:12');
INSERT INTO `gd_flatowner` ( ) VALUES('28','0','Jaqueline','Gant','323','3','128','','217','12066195225#','','','','','yes','','','no','128','CKUt3DgMA8hZL5Q7H9a9606JZumfhGXmN8FO59IT9TY=','128','221091','No Devicelocked','','1','2018-09-29 03:38:02','2019-04-07 00:34:15');
INSERT INTO `gd_flatowner` ( ) VALUES('29','0','Jaskirat Kaur','Larson','329','3','129','','218','15592170010#','','','','','yes','','','no','129','aCXO9xK0TbDhhtWfTeqfAoQ0Hcv5hdGLGgOdjFdqm5c=','129','955217','No Devicelocked','','1','2018-09-29 03:39:06','2019-04-07 00:34:39');
INSERT INTO `gd_flatowner` ( ) VALUES('30','0','Jayven','Gant','228','2','130','','217','18083824501#','','','','','yes','','','no','130','DjPW1RwXq3mLDYmOC5Dj2z/IU0qiDXzn7IrgbXWISg8=','130','794942','No Devicelocked','','1','2018-09-29 03:40:10','2019-04-07 00:35:27');
INSERT INTO `gd_flatowner` ( ) VALUES('31','0','Julia','Gant','312','3','131','','217','14252951543#','','','','','yes','','','no','131','HFcbY3zINzev5ydjrI/R5wY5ELsVstq2ABiUBj8UYXs=','131','142116','No Devicelocked','','1','2018-09-29 03:41:15','2019-04-07 00:37:03');
INSERT INTO `gd_flatowner` ( ) VALUES('32','0','Justin','Larson','','3','132','','112','218','','','','','yes','','','no','132','gH48VkH4Rh07EzloRaaaAJkl4cVMIMH0CcFe/9V37c0=','132','889060','No Devicelocked','','1','2018-09-29 03:42:19','2019-04-07 00:37:39');
INSERT INTO `gd_flatowner` ( ) VALUES('33','0','Justin','Schmidt','409','4','133','','216','12067753819#','','','','','yes','','','no','133','BPo12jopJyxurmz6QnrzXLVHF4L+HmUaQgCxY47Uv6E=','133','531341','No Devicelocked','','1','2018-09-29 03:42:23','2019-04-07 00:38:02');
INSERT INTO `gd_flatowner` ( ) VALUES('34','0','K.LeBlond','Larson','301','3','134','','218','14156083554#','','','','','yes','','','no','134','7xALyPFL+iPfwBUIP0hwbnowlB5B/RnD0ELM3k0smWQ=','134','475260','No Devicelocked','','1','2018-09-29 03:42:27','2019-04-07 00:38:31');
INSERT INTO `gd_flatowner` ( ) VALUES('35','0','Kayla','Schmidt','223','2','135','','216','13605800464#','','','','','yes','','','no','135','JzZ36wVsRM0dkBpHP88ie8t9nX3ItSY+GAjh4OWMYYs=','135','418902','No Devicelocked','','1','2018-09-29 03:42:30','2019-04-07 00:38:54');
INSERT INTO `gd_flatowner` ( ) VALUES('36','0','Lun','Gant','315','3','136','','217','15102772731#','','','','','yes','','','no','136','/yIGi5oMXPUNxazBnQWAEfIRW2X4VZ031y4LONX/zgg=','136','209192','No Devicelocked','','1','2018-09-29 03:42:34','2019-04-07 00:39:54');
INSERT INTO `gd_flatowner` ( ) VALUES('37','0','Macy','Larson','320','3','137','','218','18175214815#','','','','','yes','','','no','137','3JWHvfpZZMyxyIiwsoohbl7Gz4LKm02y0HpwzTIT/fI=','137','308128','No Devicelocked','','1','2018-09-29 03:43:38','2019-04-07 00:40:20');
INSERT INTO `gd_flatowner` ( ) VALUES('38','0','Madison','Schmidt','227','2','138','','216','14259859694#','','','','','yes','','','no','138','bBcndNrhT5SrDrDdFBjWwfL5ImWS8CUBpgaoBk2nyjs=','138','110295','No Devicelocked','','1','2018-09-29 03:44:42','2019-04-07 00:40:48');
INSERT INTO `gd_flatowner` ( ) VALUES('39','0','Malakai','Gant','330','3','139','','217','13109228996#','','','','','yes','','','no','139','sGjSyRJZnt9Xp/O6ZcP3oGYBo+6/AE4IRo/dIFyNb9I=','139','266127','No Devicelocked','','1','2018-09-29 03:45:45','2019-04-07 00:41:15');
INSERT INTO `gd_flatowner` ( ) VALUES('40','0','Mirabelle','Gant','407','4','140','','217','12064554198#','','','','','yes','','','no','140','x5XgiFSk90oSyVIjVYqVmPRRbWejW1ikqIUo8TF1mpU=','140','161060','No Devicelocked','','1','2018-09-29 03:46:49','2019-04-07 00:47:49');
INSERT INTO `gd_flatowner` ( ) VALUES('41','0','Mitchell','Larson','211','2','141','','218','18014008190#','','','','','yes','','','no','141','N2unelVmIu6qSZ+/QUyafKuFf2DUxegSgyKP2ECyPRw=','141','447247','No Devicelocked','','1','2018-09-29 03:46:55','2019-04-07 00:48:14');
INSERT INTO `gd_flatowner` ( ) VALUES('42','0','Nicole','Gant','','4','142','','217','14157224633#','','','','','yes','','','no','142','Yg3+rtU0bFBRTbzu0f6vkol7aKHqIenMGZhIHMTcnWQ=','142','613074','No Devicelocked','','1','2018-09-29 03:46:59','2019-04-07 00:49:28');
INSERT INTO `gd_flatowner` ( ) VALUES('43','0','Nicole','Larson','406','4','143','','218','13864795964#','','','','','yes','','','no','143','5KqFAWA4vbvltgRRwEJqcP7biM5WkO8wAE9x2mHFx40=','143','987066','No Devicelocked','','1','2018-09-29 03:47:03','2019-04-07 00:49:53');
INSERT INTO `gd_flatowner` ( ) VALUES('44','0','Norbu','Larson','205','2','144','','218','14255296632#','12537379992#','','','','yes','','','no','144','buHX6ezuTNyV7rmQ9s8VsdVRR9H21FrfWmSSSQtfJZ4=','144','951076','No Devicelocked','','1','2018-09-29 03:48:08','2019-04-07 00:50:47');
INSERT INTO `gd_flatowner` ( ) VALUES('45','0','Novin','Schmidt','325','3','145','','216','12069482756#','','','','','yes','','','no','145','EOGVrpdYE93Yq+lyedPPlyBx4xfJHlxGVs4pXSQBD2I=','145','290128','No Devicelocked','','1','2018-09-29 03:49:12','2019-04-07 00:51:15');
INSERT INTO `gd_flatowner` ( ) VALUES('46','0','Osvaldo','Gant','307','3','146','','217','13478373616#','','','','','yes','','','no','146','HMoMUSwlY3mQ1BMAwTuyCG7jeBCJ5/l91QQRq733LJ0=','146','990877','No Devicelocked','','1','2018-09-29 03:50:16','2019-04-07 00:52:41');
INSERT INTO `gd_flatowner` ( ) VALUES('47','0','Perry','Larson','401','4','147','','218','14253754987#','','','','','yes','','','no','147','t+rlyD6GQbdfED2HPZ/IR4W/og77QrP9DTptIMMftME=','147','325391','No Devicelocked','','1','2018-09-29 03:51:20','2019-04-07 00:53:45');
INSERT INTO `gd_flatowner` ( ) VALUES('48','0','Ping-Chun','Gant','208','2','148','','217','12063210856#','','','','','yes','','','no','148','DvtPvwNiFEBg+xxtz7ZrcTUzecJMfwxTQHOC91ySl7U=','148','429323','No Devicelocked','','1','2018-09-29 03:52:24','2019-04-07 00:54:11');
INSERT INTO `gd_flatowner` ( ) VALUES('49','0','Rahik','Larson','306','3','149','','218','15204425377#','','','','','yes','','','no','149','F5k8L7CkBBznbCFHifIw1UpahvysngVaEUaottRHCrE=','149','637190','No Devicelocked','','1','2018-09-29 03:52:28','2019-04-07 00:55:16');
INSERT INTO `gd_flatowner` ( ) VALUES('50','0','Randall','Schmidt','429','4','150','','216','12539513577#','','','','','yes','','','no','150','6DFVtljPr5p/bygmRFQkuSIrONi87kjwl/daH7oBKIU=','150','941229','No Devicelocked','','1','2018-09-29 03:52:33','2019-04-07 00:56:52');
INSERT INTO `gd_flatowner` ( ) VALUES('51','0','Renee','Gant','402','4','151','','217','12062255705#','','','','','yes','','','no','151','hoo4H7vK0iWH0qpt2BItOxUL7MG1EN0uLFz09Jk+RPY=','151','714199','No Devicelocked','','1','2018-09-29 03:52:38','2019-04-07 00:57:31');
INSERT INTO `gd_flatowner` ( ) VALUES('52','0','Robin','Larson','309','3','152','','218','19182695777#','','','','','yes','','','no','152','zD18zNYgZSo4l8Dn1MqwU+N100Sh/W5kBDyouTrEckQ=','152','223479','No Devicelocked','','1','2018-09-29 03:53:44','2019-04-07 00:57:57');
INSERT INTO `gd_flatowner` ( ) VALUES('53','0','Ryan','Gant','102','1','153','','217','13852087669#','','','','','yes','','','no','153','g7oHWYseQdQW4zpl1tJm89OKenBltdW8UxaFz9ki82c=','153','952217','No Devicelocked','','1','2018-09-29 03:54:48','2019-04-07 00:58:55');
INSERT INTO `gd_flatowner` ( ) VALUES('54','0','Sara','Larson','403','4','154','','218','12062009915#','','','','','yes','','','no','154','xvtSYbQr4dJlwrn0SfcX92W11ZjnTF/lO3fR8c0+Nik=','154','418110','No Devicelocked','','1','2018-09-29 03:55:52','2019-04-07 00:59:23');
INSERT INTO `gd_flatowner` ( ) VALUES('55','0','Sarah','Schmidt','103','1','155','','216','12066054983#','','','','','yes','','','no','155','ltI6XAkB+rB6qMjtvAOLqOa6pm6qWROu0V3qae2W08o=','155','866388','IOS:73F12957-E980-497C-BF7B-96A557375BE8','','1','2018-09-29 03:55:56','2019-04-07 00:59:52');
INSERT INTO `gd_flatowner` ( ) VALUES('56','0','Stephanie','Larson','102','1','156','','218','18049436173#','','','','','yes','','','no','156','IyxpDyCl4YBRH69U5h2Kit8wsCR9kjsxuX0vT/38x6U=','156','760278','No Devicelocked','','1','2018-09-29 03:56:00','2019-04-07 01:00:58');
INSERT INTO `gd_flatowner` ( ) VALUES('57','0','Stephanie','Schmidt','426','4','157','','216','14256549212#','','','','','yes','','','no','157','ez2eVHAIaaeSEjFHX+FF99DbtI23c3eWDl/mLPXqYRI=','157','196174','No Devicelocked','','1','2018-09-29 03:56:04','2019-04-07 01:01:23');
INSERT INTO `gd_flatowner` ( ) VALUES('58','0','T.Long','Gant','214','2','158','','217','13603381176#','','','','','yes','','','no','158','nhkLNgcLb09ZnpBwExo540ySqKDrOMwok3lDo5s0RSA=','158','224118','Android:02:00:00:00:00:00','','1','2018-09-29 03:56:08','2019-04-07 01:01:49');
INSERT INTO `gd_flatowner` ( ) VALUES('59','0','Tal Bar','Larson','313','3','159','','218','18579286825#','','','','','yes','','','no','159','oHq5tmmyZqGDQIA4LHvfDeth76RB2kBsfqyGfLyh/jQ=','159','544637','No Devicelocked','','1','2018-09-29 03:57:12','2019-04-07 01:02:20');
INSERT INTO `gd_flatowner` ( ) VALUES('60','0','TK','Gant','209','2','160','','217','16265006724#','','','','','yes','','','no','160','ijo9Ajwj96EE76g9wYglkofMxPLeIQeWoe4PBCcNewY=','160','251190','No Devicelocked','','1','2018-09-29 03:57:16','2019-04-07 01:04:15');
INSERT INTO `gd_flatowner` ( ) VALUES('61','0','Tori','Schmidt','214','2','161','','216','13603381176#','','','','','yes','','','no','161','MstV31s7DzRcH6VDK6eqmQw1UvdOAiV5yEYrVYMdfBM=','161','205466','No Devicelocked','','1','2018-09-29 03:57:21','2019-04-07 01:05:08');
INSERT INTO `gd_flatowner` ( ) VALUES('62','0','Tracy','Gant','327','3','162','','217','14252456543#','','','','','yes','','','no','162','NVGWYNUoTiTgDMrXlbUlYEtDh/puL+6YZTmjwQcws40=','162','566144','No Devicelocked','','1','2018-09-29 03:57:25','2019-04-07 01:05:38');
INSERT INTO `gd_flatowner` ( ) VALUES('63','0','Uyen','Larson','218','2','163','','218','12533275026#','','','','','yes','','','no','163','4BIQmBVMrbzNcjpFUprmOQ0ab00vDfgS9FGnzJjJiW0=','163','134253','No Devicelocked','','1','2018-09-29 03:58:29','2019-04-07 01:07:04');
INSERT INTO `gd_flatowner` ( ) VALUES('64','0','Vanny','Schmidt','230','2','164','','216','15074691113#','','','','','yes','','','no','164','qlmMzBwILF+DSWfbPz4SFtyztMSJn04wQg0rPVGniRI=','164','908266','No Devicelocked','','1','2018-09-29 03:58:36','2019-04-07 01:07:35');
INSERT INTO `gd_flatowner` ( ) VALUES('65','0','Xiaoran','Larson','308','3','165','','218','16265126433#','','','','','yes','','','no','165','NOtKaSXLvDXyNTmfOwrP+I/SGDnwbJZlNw/45bKUfIY=','165','886565','No Devicelocked','','1','2018-09-29 03:58:41','2019-04-07 01:12:44');
INSERT INTO `gd_flatowner` ( ) VALUES('66','0','Xinyi','Gant','302','3','166','','217','16097416867#','','','','','yes','','','no','166','+76Zs6pbkJcQJX6JSmj0g0G6oXCyNf75DPOKTu4DAgQ=','166','797389','No Devicelocked','','1','2018-09-29 03:58:46','2019-04-07 01:13:32');
INSERT INTO `gd_flatowner` ( ) VALUES('67','0','Zara','Gant','410','4','167','','217','12068664463#','','','','','yes','','','no','167','6F3vAqser8942hHwpHlnVXzLYzSuLvCcbvpQALhWMOs=','167','224905','No Devicelocked','','1','2018-09-29 03:58:50','2019-04-07 01:10:42');
INSERT INTO `gd_flatowner` ( ) VALUES('68','0','Property','Manager','','1','168','','218','','','','','','yes','','','no','168','RxTy4vsKTqmlae3g7uScGLReZeXjqwk4SQWWPa8U5Uw=','168','143845','No Devicelocked','','1','2018-09-29 04:01:19','2019-04-07 00:54:36');
INSERT INTO `gd_flatowner` ( ) VALUES('69','0','Package','Delivery','','1','169','','216','','','','','','yes','','','no','169','XSYl1J4b3cYEXFS5Eg3zNEkFRdCG2x8rwjTXvARFU88=','169','806642','No Devicelocked','','1','2018-09-29 04:03:29','2019-04-07 00:53:11');
INSERT INTO `gd_flatowner` ( ) VALUES('72','0','Danny','Schmidt','1','1','172','','216','150','14253503023#','14258703746#','19514540425#','ringallv2-prim','yes','','','no','172','s7JTm/u9tp27BnmwmAoyU8JBBNDxEbXgqffLmS4xI6M=','172','642337','No Devicelocked','','1','2019-01-14 15:31:46','2019-04-07 00:19:10');
INSERT INTO `gd_flatowner` ( ) VALUES('77','3','blindy','test5','1','1','174','','174','','','','','','yes','','','no','174','DuSKa5CaUKtHpYUi2q9056eEmqlnyQBGjh2oKH/Xgy4=','174','676672','','','1','2019-01-14 18:25:38','2019-01-22 08:53:06');
INSERT INTO `gd_flatowner` ( ) VALUES('79','0','Nicholas','Schmidt','1N','1','179','nasticlegend@gmail.com','216','14258703746#','','','','ringallv2-prim','yes','','','no','179','iPXwnwWvvyurqX0hxK1GqVi7c1ghvZRG2mdKPMTBWp8=','179','137292','No Devicelocked','','1','2019-01-18 10:26:22','2019-04-07 00:48:40');
INSERT INTO `gd_flatowner` ( ) VALUES('82','0','Vimal','Larson','43','4','180','vimalmishra09@gmail.com','218','','','','','ringallv2-prim','yes','','','no','180','W8w3pFbteDtpyZ7bGc6crEjViYvJCiziFpeWOp/SYC0=','180','741127','No Devicelocked','','1','2019-01-22 18:58:17','2019-04-07 01:09:32');
INSERT INTO `gd_flatowner` ( ) VALUES('85','0','vc','Gant','','14','185','','217','','','','','hunt-prim','no','','','no','185','EY2BUTB2AbnQiO3/AcOsI/fVsqEK62WmNRXRq5tk//s=','185','408431','No Devicelocked','','1','2019-01-22 23:56:42','2019-04-07 01:08:22');
INSERT INTO `gd_flatowner` ( ) VALUES('86','0','vv','Schmidt','','4','186','email@email.com','216','','','','','ringallv2-prim','yes','','','no','186','s0mz1brVZ/Bv6BJWNux+sTvBaUo4Wm5UP4nmPMsS4Sk=','186','840145','No Devicelocked','','1','2019-01-22 23:57:40','2019-04-07 01:12:16');
INSERT INTO `gd_flatowner` ( ) VALUES('89','0','Andy','Gant','1B','1','189','','217','','','','','ringallv2-prim','yes','','','no','189','hJ9SFOqv4XccL2ff+wKnoqhQG0DMaasxhd3K1iPX2bE=','189','846083','No Devicelocked','','1','2019-01-23 11:35:06','2019-04-07 00:06:24');
INSERT INTO `gd_flatowner` ( ) VALUES('90','0','Todd','Larson','1C','1','190','','218','','','','','ringallv2-prim','yes','','','no','190','q8uGTRoKctUMMvGxjIKzncsscSe8N12wR+rdd0TvBFI=','190','518206','IOS:76FD29B5-68FD-4923-8908-19B6B375AAC7','','1','2019-01-23 11:35:45','2019-04-07 01:04:41');
INSERT INTO `gd_flatowner` ( ) VALUES('91','0','Jack','Schmidt','','14','191','','216','','','','','ringallv2-prim','yes','','','no','191','yO1cz5n3Jb4eX4qdQD7XSuBW3ZW4TmzPs8D6DKdesRE=','191','605356','No Devicelocked','','1','2019-01-23 12:11:29','2019-04-07 00:30:35');
INSERT INTO `gd_flatowner` ( ) VALUES('92','0','Geoff','Schmidt','','15','192','','216','','','','','ringallv2-prim','yes','','','no','192','/1BK+f/RZDZF/BJJRWpLNGuhzoEpTYhlGjDeMe89ITU=','192','902810','No Devicelocked','','1','2019-01-23 12:11:58','2019-04-07 00:26:47');
INSERT INTO `gd_flatowner` ( ) VALUES('93','0','Jason','Schmidt','','14','193','','216','','','','','ringallv2-prim','yes','','','no','193','DhMrkVjjp52VElqHzWM2LwB3Ts55zk+Zc5XvCtVbaWo=','193','563615','No Devicelocked','','1','2019-01-23 12:12:41','2019-04-07 00:35:04');
INSERT INTO `gd_flatowner` ( ) VALUES('94','0','Mark','Larson','','1','194','','218','','','','','ringallv2-prim','yes','','','no','194','wkIS5WZDbHG3v9XXIyejATn5PCaM9d1e0o+ZqubYRKA=','194','485495','No Devicelocked','','1','2019-01-23 12:13:12','2019-04-07 00:41:48');
INSERT INTO `gd_flatowner` ( ) VALUES('95','0','Ashley','Schmidt','','1','195','','216','','','','','ringallv2-prim','yes','','','no','195','gslq2xbm2GsXInf4tbaJsxpV71bdbS/eb54Zn+ZIjf0=','195','358559','Android:02:00:00:00:00:00','','1','2019-01-23 12:13:43','2019-07-03 15:54:08');
INSERT INTO `gd_flatowner` ( ) VALUES('96','0','Brian','Schmidt','','1','196','','216','','','','','ringallv2-prim','yes','','','no','196','3LJ4jxLAD3ckcy41aHocGAlvsUkWs8iRpq/T89Df3xk=','196','782136','No Devicelocked','','1','2019-01-23 12:14:13','2019-04-07 00:08:16');
INSERT INTO `gd_flatowner` ( ) VALUES('97','0','Tim','Schmidt','','15','197','','216','','','','','ringallv2-prim','yes','','','no','197','c9IE+AV8E1Eouh51SdQWizmfsGE8G7hrcWFBu+zAEqA=','197','582299','No Devicelocked','','1','2019-01-23 12:15:07','2019-04-07 01:03:41');
INSERT INTO `gd_flatowner` ( ) VALUES('98','0','Ashley','Gant','','14','198','','217','','','','','ringallv2-prim','yes','','','no','198','ZPvKIZIZt129ubZDTn8Jld5Ttq5GwJOqbsADTkvEEyU=','198','453492','No Devicelocked','','1','2019-01-23 12:15:38','2019-04-07 00:07:51');
INSERT INTO `gd_flatowner` ( ) VALUES('99','0','Chase','Schmidt','','14','199','','216','','','','','ringallv2-prim','yes','','','no','199','GTHP/0tq6O3Ow7005W9D92uXNNsMlwsRGFFwz4ozF+Q=','199','614153','No Devicelocked','','1','2019-01-23 12:16:19','2019-04-07 00:19:37');
INSERT INTO `gd_flatowner` ( ) VALUES('100','0','Nikki','Gant','','14','200','','217','','','','','ringallv2-prim','yes','','','no','200','tJzzldU2OxSDFtYlMP9gyb5VNg1zfSQpKaZKtBHrEvk=','200','311888','No Devicelocked','','1','2019-01-23 12:16:53','2019-04-07 00:50:19');
INSERT INTO `gd_flatowner` ( ) VALUES('101','0','Mike','Schmidt','','14','201','','216','','','','','ringallv2-prim','yes','','','no','201','OZdvqQIJkQqJf8dXpYB0PKWxTRz11ZL0rJQlXWh04lo=','201','549709','No Devicelocked','','1','2019-01-23 12:17:16','2019-04-07 00:46:56');
INSERT INTO `gd_flatowner` ( ) VALUES('102','0','Brooke','Gant','','14','202','','217','','','','','ringallv2-prim','yes','','','no','202','AhOsXF+LICp30Pjm7Yd9zsaiA4DVl+749ZeXa+Fw62c=','202','528943','No Devicelocked','','1','2019-01-23 12:17:44','2019-04-07 00:18:14');
INSERT INTO `gd_flatowner` ( ) VALUES('103','0','Stacy','Gant','','13','203','','217','','','','','ringallv2-prim','yes','','','no','203','iTjCprieOUktVKfALnZnKX9bGAUPdi5PcRcAMzrEPLs=','203','503438','No Devicelocked','','1','2019-01-23 12:18:17','2019-04-07 01:00:19');
INSERT INTO `gd_flatowner` ( ) VALUES('104','0','John','Larson','','13','204','','218','','','','','ringallv2-prim','yes','','','no','204','tSdrCOox92N+3vDAioqAs1oyroVuF92D9BHr6WOtqnY=','204','350851','No Devicelocked','','1','2019-01-23 12:19:11','2019-04-07 00:36:15');
INSERT INTO `gd_flatowner` ( ) VALUES('105','0','Rodney','Schmidt','','14','205','','216','','','','','ringallv2-prim','yes','','','no','205','2IVKmnA0jQwlBkHIRErW7I1QBZmRqqDoPKRDRAyxYJM=','205','258772','No Devicelocked','','1','2019-01-23 12:19:58','2019-04-07 00:58:23');
INSERT INTO `gd_flatowner` ( ) VALUES('106','0','John','Schmidt','','13','206','','216','','','','','ringallv2-prim','yes','','','no','206','HzL1uKhevozbkrwegfbgQ3kLeSXLW+WnUTNCX8W+KXU=','206','671290','No Devicelocked','','1','2019-01-23 12:20:27','2019-04-07 00:36:39');
INSERT INTO `gd_flatowner` ( ) VALUES('107','0','Jack','Gant','','3','207','','217','','','','','ringallv2-prim','yes','','','no','207','TR5N/cbLdr2pgmpjarK9WOp5uKGKS7jK76gwA30WIjo=','207','608637','No Devicelocked','','1','2019-01-23 12:21:15','2019-04-07 00:31:09');
INSERT INTO `gd_flatowner` ( ) VALUES('108','0','Brian','Larson','','3','208','','218','','','','','ringallv2-prim','yes','','','no','208','4da1wCoQutBMrFLqIv3aFxB/L1P1bsBulxiobGXhk4E=','208','773514','No Devicelocked','','1','2019-01-23 12:21:56','2019-04-07 00:17:21');
INSERT INTO `gd_flatowner` ( ) VALUES('109','0','Geoff','Gant','','15','209','','217','','','','','ringallv2-prim','yes','','','no','209','Ua1rNAmw3AbRaH8d4F4BP31Lm7WMsOm7doZ/oBfU8u0=','209','425746','No Devicelocked','','1','2019-01-23 12:22:32','2019-04-07 00:27:14');
INSERT INTO `gd_flatowner` ( ) VALUES('110','0','Chase','Gant','','16','210','','217','','','','','ringallv2-prim','yes','','','no','210','FJNDqvKkbqQKa//r1ZqRr3Zp95gvks+lA1MUtVh0Hlw=','210','681488','No Devicelocked','','1','2019-01-23 12:22:55','2019-04-07 00:20:00');
INSERT INTO `gd_flatowner` ( ) VALUES('111','0','Jane','Schmidt','','17','211','','216','','','','','ringallv2-prim','yes','','','no','211','uYFUq6T2veTohqt/wZglrtYoIOoKCYeqlGelH5yNik4=','211','162034','No Devicelocked','','1','2019-01-23 12:23:19','2019-04-07 00:33:36');
INSERT INTO `gd_flatowner` ( ) VALUES('112','0','Mark','Larson','','16','212','','218','','','','','ringallv2-prim','yes','','','no','212','Na6Gbq3ClbK46I6gKhnFEfvj/D/x3vP4q9q7sfuKxrk=','212','202837','No Devicelocked','','1','2019-01-23 12:23:46','2019-04-07 00:46:27');
INSERT INTO `gd_flatowner` ( ) VALUES('113','0','Mike','Gant','','15','213','','217','','','','','ringallv2-prim','yes','','','no','213','1UkpuyP/c6dIKzq/Q8hWuuWhjfTJYsjXnFy9xpcdo0g=','213','596133','No Devicelocked','','1','2019-01-23 12:24:25','2019-04-07 00:47:23');
INSERT INTO `gd_flatowner` ( ) VALUES('114','0','App','Larson','','1','214','','218','','','','','ringallv2-prim','yes','','','no','214','HrMqxCHa5ZaBnCg9ubVuj+Sa/tmaDR6vR4H/R8MtSwg=','214','753244','Android:02:00:00:00:00:00','','1','2019-03-07 04:11:37','2019-04-07 00:06:51');
INSERT INTO `gd_flatowner` ( ) VALUES('115','0','Brooke','Larson','','2','215','','218','','','','','ringallv2-prim','yes','','','no','215','fBqWBnq6Y1G/lAUODe+guY7x6bCEaaoFQUWON9aMkHI=','215','804286','Android:02:00:00:00:00:00','','1','2019-04-06 11:28:58','2019-04-07 00:18:42');
INSERT INTO `gd_flatowner` ( ) VALUES('116','0','Les','Schmidt','','2','216','','216','','','','','ringallv2-prim','yes','','','no','216','L5Cc0B5o/DDge3wgkekYmb/2KlNCr5wLEPfhOS3xPjw=','216','142610','Android:02:00:00:00:00:00','','1','2019-04-07 06:47:49','2019-04-06 03:58:14');
INSERT INTO `gd_flatowner` ( ) VALUES('117','0','Justin','Gant','','1','217','','217','14253503023#','14258675309#','','','ringallv2-prim','yes','','','no','217','lIpoo4CHpgnl5aAOkxWNxYBNo65lvIGi8lhfF5FLmNg=','217','918621','IOS:27666251-B343-47F3-BE4C-EFE2706E0127','','1','2019-04-07 12:22:53','2019-06-28 12:57:24');
INSERT INTO `gd_flatowner` ( ) VALUES('118','0','Eric','Larson','','3','218','','218','','','','','ringallv2-prim','yes','','','no','218','gOue6/k3/jEJwhYX2qzRTK72QWYzfLX80jmRkV4hx+k=','218','931378','Android:02:00:00:00:00:00','','1','2019-04-07 12:33:53','2019-04-07 18:35:07');
INSERT INTO `gd_flatowner` ( ) VALUES('119','0','Candy','Smith','','3','219','','219','','','','','ringallv2-prim','yes','','','no','219','claXVyshP6Pz2MSKLy3zo/1X7Nh3Y9WK5v/QzfSlDjk=','219','633488','','','1','2019-04-06 12:46:09','2019-04-06 00:16:09');
INSERT INTO `gd_flatowner` ( ) VALUES('120','0','tablet','','','1','220','','220','','','','','ringallv2-prim','yes','','','no','220','wchE7me9LCAHhrXb2l82DuDpnQkhkJqUxhkttlAT2xk=','220','897997','','','1','2019-07-04 04:18:15','2019-07-03 15:48:15');



DROP TABLE IF EXISTS `gd_inbounds`;

CREATE TABLE `gd_inbounds` (
  `row_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `did_number` varchar(255) NOT NULL,
  `cid_number` varchar(255) NOT NULL,
  `cid_priority_route` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=yes',
  `alert_info` varchar(255) DEFAULT NULL,
  `ringer_volume` tinyint(2) NOT NULL DEFAULT '0',
  `cid_name_prefix` varchar(255) DEFAULT NULL,
  `music_on_hold` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=default, 1=none',
  `set_destination` varchar(1000) DEFAULT NULL,
  `privacy_manager` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=yes',
  `max_attempts` int(11) NOT NULL DEFAULT '0',
  `min_length` int(11) NOT NULL DEFAULT '0',
  `description` varchar(1000) NOT NULL,
  `date_added` datetime NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`row_id`),
  UNIQUE KEY `uniqueExtension` (`did_number`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;




DROP TABLE IF EXISTS `gd_otp`;

CREATE TABLE `gd_otp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT 'created user id',
  `otp` varchar(255) DEFAULT NULL,
  `enc_otp` varchar(255) DEFAULT NULL COMMENT 'encrypted otp',
  `device_type` varchar(6) DEFAULT NULL COMMENT '1- IOS, 2-Android',
  `device_token` varchar(255) DEFAULT NULL,
  `unique_id` varchar(255) DEFAULT NULL,
  `otp_status` enum('new','consumed') NOT NULL DEFAULT 'new' COMMENT 'new - during user creation, consumed - usedby user',
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0--inactive, 1--active',
  `created_date` datetime DEFAULT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=339 DEFAULT CHARSET=utf16;

INSERT INTO `gd_otp` ( ) VALUES('1','1','280554','1f0a4fe49967f17bdd9c425673fc2609','1','a235dc776bd42b3018e253fd0580f738eb115ea4960b8b329d14f5b5f4faf85f','37FD3B34-01EB-4DA6-927A-75E6F0CCFA09','consumed','1','2018-08-07 18:08:38','2018-08-07 05:39:58');
INSERT INTO `gd_otp` ( ) VALUES('3','3','936372','40204d95284c07f1b1648a896055a8cb','1','a235dc776bd42b3018e253fd0580f738eb115ea4960b8b329d14f5b5f4faf85f','37FD3B34-01EB-4DA6-927A-75E6F0CCFA09','consumed','1','2018-08-07 18:09:30','2018-08-07 05:40:45');
INSERT INTO `gd_otp` ( ) VALUES('4','4','106916','c8d678d9b5b222698d01711c0f7dd15c','1','1ed7182985ec3c5840b9d05d43a376b7e6aa89ce0ffd6522bd06fd46de8a8558','6C9EC574-D04E-4E4C-B7F8-993807E5A5F8','consumed','1','2018-08-07 19:16:13','2018-08-07 06:48:25');
INSERT INTO `gd_otp` ( ) VALUES('5','5','470659','96242f96645863bcb62ed3332d31f84f','0','','','new','1','2018-08-07 20:23:06','2018-08-07 07:53:06');
INSERT INTO `gd_otp` ( ) VALUES('6','6','116833','e5f7592234b7e5401ce4f4e7028c03da','0','','','new','1','2018-08-07 20:24:40','2018-08-07 07:54:40');
INSERT INTO `gd_otp` ( ) VALUES('7','7','280396','f2d2f24727fbcced8be8cbff382e246a','0','','','new','1','2018-08-07 20:26:12','2018-08-07 07:56:12');
INSERT INTO `gd_otp` ( ) VALUES('8','8','558852','f01315295693d058d414045d6d197de3','0','','','new','1','2018-08-07 20:26:31','2018-08-07 07:56:31');
INSERT INTO `gd_otp` ( ) VALUES('9','9','747447','285b3fbf757d4c8b61576403a0a0ca0f','0','','','new','1','2018-08-07 20:26:45','2018-08-07 07:56:45');
INSERT INTO `gd_otp` ( ) VALUES('10','10','791617','0dc238568d0008b7a263cba02ceb0872','0','','','new','1','2018-08-07 20:37:50','2018-08-07 08:07:50');
INSERT INTO `gd_otp` ( ) VALUES('11','11','726577','1549a2abc5d89c5bb2eaeaed4523e3b9','0','','','new','1','2018-08-08 00:13:25','2018-08-07 11:43:25');
INSERT INTO `gd_otp` ( ) VALUES('12','1','717274','512508db0e80dae19224fa32fb037dfc','2','','02:00:00:00:00:00','consumed','1','2018-08-08 09:17:24','2018-08-07 21:27:19');
INSERT INTO `gd_otp` ( ) VALUES('14','3','574906','a9bb75221bc448cbe191a162627094df','0','','','new','1','2018-08-08 09:18:38','2018-08-07 20:48:38');
INSERT INTO `gd_otp` ( ) VALUES('15','4','254205','787f2d9c6acd81a1b2966b4e9a312ccd','0','','','new','1','2018-08-08 09:19:31','2018-08-07 20:49:31');
INSERT INTO `gd_otp` ( ) VALUES('16','5','586721','ae1e85ce0a9b4cfee13d7ddeb2809cd1','0','','','new','1','2018-08-08 09:20:02','2018-08-07 20:50:02');
INSERT INTO `gd_otp` ( ) VALUES('17','6','646112','15ef12fb3ab6b0760a29c53cf01a7094','0','','','new','1','2018-08-29 18:37:04','2018-08-29 06:07:04');
INSERT INTO `gd_otp` ( ) VALUES('18','7','706507','9287c38a25dbdcce35c7838f0469b826','0','','','new','1','2018-08-29 18:37:08','2018-08-29 06:07:08');
INSERT INTO `gd_otp` ( ) VALUES('19','8','480328','dc7a9936fa681a34e14f5711ed763ef5','0','','','new','1','2018-08-29 18:37:12','2018-08-29 06:07:12');
INSERT INTO `gd_otp` ( ) VALUES('20','9','246630','0fce45d66958741d347c5c127bc2f6fa','0','','','new','1','2018-08-29 18:37:17','2018-08-29 06:07:17');
INSERT INTO `gd_otp` ( ) VALUES('21','10','414611','b757a5cd216ba741fcc6af32d59d790f','0','','','new','1','2018-08-29 18:37:21','2018-08-29 06:07:21');
INSERT INTO `gd_otp` ( ) VALUES('22','11','333969','9f1c019a72fc548528b11bb0f9059766','0','','','new','1','2018-08-29 18:37:25','2018-08-29 06:07:25');
INSERT INTO `gd_otp` ( ) VALUES('23','12','980461','7513def49391a39fb687e004a3d9e56a','0','','','new','1','2018-08-29 18:37:29','2018-08-29 06:07:29');
INSERT INTO `gd_otp` ( ) VALUES('24','13','855829','877c81204f9039557102306e929a0d35','0','','','new','1','2018-08-29 18:37:33','2018-08-29 06:07:33');
INSERT INTO `gd_otp` ( ) VALUES('25','14','128556','8fe21c9aa904320c6504d17ca554e653','0','','','new','1','2018-08-29 18:37:38','2018-08-29 06:07:38');
INSERT INTO `gd_otp` ( ) VALUES('26','15','705132','a0b8d64e86bb0a2b5b17341ed32ee98d','0','','','new','1','2018-08-29 18:37:42','2018-08-29 06:07:42');
INSERT INTO `gd_otp` ( ) VALUES('27','16','855975','b3831df4873db9222f7bd620e293eae7','0','','','new','1','2018-08-29 18:37:46','2018-08-29 06:07:46');
INSERT INTO `gd_otp` ( ) VALUES('28','17','848665','2a86ed1a3a22af8b060d89b8e0013611','0','','','new','1','2018-08-29 18:37:50','2018-08-29 06:07:50');
INSERT INTO `gd_otp` ( ) VALUES('29','18','632619','09c518c6d39d1f83a14cb204760199e9','0','','','new','1','2018-08-29 18:37:54','2018-08-29 06:07:55');
INSERT INTO `gd_otp` ( ) VALUES('30','19','536374','7c83a14a3053b1ead235987ac904b61c','0','','','new','1','2018-08-29 18:37:59','2018-08-29 06:07:59');
INSERT INTO `gd_otp` ( ) VALUES('31','20','862849','efcf18a930b47372937e81795651e4d7','0','','','new','1','2018-08-29 18:38:03','2018-08-29 06:08:03');
INSERT INTO `gd_otp` ( ) VALUES('32','21','898225','764260e42d540d8b33df8e19611a06e2','0','','','new','1','2018-08-29 18:38:07','2018-08-29 06:08:07');
INSERT INTO `gd_otp` ( ) VALUES('33','22','970844','c6a6db9384048bdbf03878eb9eb6f5b1','0','','','new','1','2018-08-29 18:38:11','2018-08-29 06:08:11');
INSERT INTO `gd_otp` ( ) VALUES('34','23','173435','75e5309285dedb926844bc27cbbb9834','0','','','new','1','2018-08-29 18:38:15','2018-08-29 06:08:15');
INSERT INTO `gd_otp` ( ) VALUES('35','24','934527','10bbb9bd25b2cbf79966ef9fb8e42a8d','0','','','new','1','2018-08-29 18:38:20','2018-08-29 06:08:20');
INSERT INTO `gd_otp` ( ) VALUES('36','25','808738','d3ecb0c4d990fe24ce6bf4e4606a2057','0','','','new','1','2018-08-29 18:38:24','2018-08-29 06:08:24');
INSERT INTO `gd_otp` ( ) VALUES('37','26','761504','9048c62a67ac6a958d6ed6c5310834cd','0','','','new','1','2018-08-29 18:38:28','2018-08-29 06:08:28');
INSERT INTO `gd_otp` ( ) VALUES('38','27','672912','3c816472cc84a522830686b0af0a3028','0','','','new','1','2018-08-29 18:38:34','2018-08-29 06:08:34');
INSERT INTO `gd_otp` ( ) VALUES('39','28','106961','abaa9aa65a3f698cf1b3e51d4ffcb0f3','0','','','new','1','2018-08-29 18:38:39','2018-08-29 06:08:39');
INSERT INTO `gd_otp` ( ) VALUES('40','29','519490','24c8bf6e4c2823cdabfc5e36da915109','0','','','new','1','2018-08-29 18:38:43','2018-08-29 06:08:43');
INSERT INTO `gd_otp` ( ) VALUES('41','30','198754','985eb89fa789d9ed18b93fd02559363f','0','','','new','1','2018-08-29 18:38:47','2018-08-29 06:08:47');
INSERT INTO `gd_otp` ( ) VALUES('42','31','311008','b696541b35c57d7cb6818d665ffa666f','0','','','new','1','2018-08-29 18:38:52','2018-08-29 06:08:52');
INSERT INTO `gd_otp` ( ) VALUES('43','32','608385','3f3c248458eed8d9007c76f217112ae5','0','','','new','1','2018-08-29 18:38:56','2018-08-29 06:08:56');
INSERT INTO `gd_otp` ( ) VALUES('44','33','320912','5867bc59ff2f2ce2020f0e1e9fda3c4a','0','','','new','1','2018-08-29 18:39:00','2018-08-29 06:09:00');
INSERT INTO `gd_otp` ( ) VALUES('45','34','237033','a36c1b2624fd899e850204ca39d37677','0','','','new','1','2018-08-29 18:39:05','2018-08-29 06:09:05');
INSERT INTO `gd_otp` ( ) VALUES('46','35','563499','e21a662142ba1206841ce902dbfcd3d1','0','','','new','1','2018-08-29 18:39:09','2018-08-29 06:09:09');
INSERT INTO `gd_otp` ( ) VALUES('47','36','614978','ad4c330e3e863961ddd862269304527d','0','','','new','1','2018-08-29 18:39:13','2018-08-29 06:09:13');
INSERT INTO `gd_otp` ( ) VALUES('48','37','844072','6d262fa3661ef1ea35e65c1550549170','0','','','new','1','2018-08-29 18:39:18','2018-08-29 06:09:18');
INSERT INTO `gd_otp` ( ) VALUES('49','38','518098','f303c91852797f7d3c0b66da72a30549','0','','','new','1','2018-08-29 18:39:22','2018-08-29 06:09:22');
INSERT INTO `gd_otp` ( ) VALUES('50','39','847660','d64ea0d526e08581dcd41173cfdea2fe','0','','','new','1','2018-08-29 18:39:26','2018-08-29 06:09:27');
INSERT INTO `gd_otp` ( ) VALUES('51','40','866844','a983e69834ae1ae9bd9ea2965255076b','0','','','new','1','2018-08-29 18:39:31','2018-08-29 06:09:31');
INSERT INTO `gd_otp` ( ) VALUES('52','41','263156','07d62b33ffa166426550ad9ab26a288f','0','','','new','1','2018-08-29 18:39:35','2018-08-29 06:09:35');
INSERT INTO `gd_otp` ( ) VALUES('53','42','938933','99508bbf8508cfaa4474c0694725fb58','0','','','new','1','2018-08-29 18:39:40','2018-08-29 06:09:40');
INSERT INTO `gd_otp` ( ) VALUES('54','43','431544','8d93f390ad3366ce3aef7e412f1a556e','0','','','new','1','2018-08-29 18:39:44','2018-08-29 06:09:44');
INSERT INTO `gd_otp` ( ) VALUES('55','44','924258','1fa4710d448bb2bda65cbde07f3b8e73','0','','','new','1','2018-08-29 18:39:49','2018-08-29 06:09:49');
INSERT INTO `gd_otp` ( ) VALUES('56','45','291480','0ffe9fd740e4616ecda511a70a698c1d','0','','','new','1','2018-08-29 18:39:53','2018-08-29 06:09:53');
INSERT INTO `gd_otp` ( ) VALUES('57','46','604065','20e1ab36e6e0258d7066f2f7dbd561af','0','','','new','1','2018-08-29 18:39:58','2018-08-29 06:09:58');
INSERT INTO `gd_otp` ( ) VALUES('58','47','600725','28c0434ff40083c763dc989bd99f38b6','0','','','new','1','2018-08-29 18:40:02','2018-08-29 06:10:02');
INSERT INTO `gd_otp` ( ) VALUES('59','48','151780','d6f1d399d06374d6e5192735075e3dcb','0','','','new','1','2018-08-29 18:40:07','2018-08-29 06:10:07');
INSERT INTO `gd_otp` ( ) VALUES('60','49','896831','ceac3cd3ab5b8c5b1c0def053cb32bb0','0','','','new','1','2018-08-29 18:40:11','2018-08-29 06:10:11');
INSERT INTO `gd_otp` ( ) VALUES('61','50','768220','9e54b0b62844f0f9f8a45e5f4eff2fed','0','','','new','1','2018-08-29 18:41:23','2018-08-29 06:11:23');
INSERT INTO `gd_otp` ( ) VALUES('62','51','583666','d6517b56255b46ecb7223d5fa3c5e6dc','0','','','new','1','2018-08-29 18:41:39','2018-08-29 06:11:39');
INSERT INTO `gd_otp` ( ) VALUES('63','52','149065','d8c357848a1fcbbe7736cea63e8d211c','0','','','new','1','2018-08-29 18:41:56','2018-08-29 06:11:56');
INSERT INTO `gd_otp` ( ) VALUES('64','53','696884','3e88886337e59a9108a41867d1587f99','0','','','new','0','2018-08-29 19:00:56','2018-08-30 03:38:15');
INSERT INTO `gd_otp` ( ) VALUES('65','53','785371','35da03b3d46e2df150d3105c2595e75e','0','','','new','0','2018-08-30 16:08:15','2018-08-30 03:38:54');
INSERT INTO `gd_otp` ( ) VALUES('66','53','786182','894d341659c7b72bbf74dcd85a90faf9','0','','','new','1','2018-08-30 16:08:54','2018-08-30 03:38:54');
INSERT INTO `gd_otp` ( ) VALUES('67','25','154797','51a876c8d70af68851a1211012858519','2','','02:00:00:00:00:00','consumed','1','2018-08-30 16:09:32','2018-08-30 03:39:34');
INSERT INTO `gd_otp` ( ) VALUES('68','16','646175','8f25e6920d90d37473a161ad596da9cb','1','b7157bce87f9df5e91a8ba15dfcb5230719fc54ee94c0288296235f9983b11ec','B5F19922-DF18-46ED-9464-A599856D2377','consumed','1','2018-08-30 19:27:23','2018-08-30 06:57:27');
INSERT INTO `gd_otp` ( ) VALUES('69','54','238158','87321945d0b126a4f5213c760a7709f6','0','','','new','1','2018-09-04 11:13:10','2018-09-03 22:43:11');
INSERT INTO `gd_otp` ( ) VALUES('70','55','411176','49d2fa4e40be18c83258fc8c8c16086a','0','','','new','0','2018-09-04 11:13:36','2018-08-28 11:13:32');
INSERT INTO `gd_otp` ( ) VALUES('71','56','710761','3326029ad363ccbf1abf0c8f5422f679','0','','','new','1','2018-09-04 11:14:00','2018-09-03 22:44:00');
INSERT INTO `gd_otp` ( ) VALUES('72','57','814653','40fb26dd7ac8c241f46fcd7db8b0cf78','0','','','new','1','2018-08-28 23:05:25','2018-08-28 10:35:25');
INSERT INTO `gd_otp` ( ) VALUES('73','58','198127','6f6cb44562d50187ad467e56c3c8a1cf','2','','02:00:00:00:00:00','consumed','1','2018-08-28 23:08:25','2018-08-28 10:38:28');
INSERT INTO `gd_otp` ( ) VALUES('74','59','328383','bb16e92e4c609ec2f31a55c732746cdb','0','','','new','1','2018-08-28 23:10:04','2018-08-28 10:40:04');
INSERT INTO `gd_otp` ( ) VALUES('75','55','105863','0a4d50320b95bface947ebed669a494a','0','','','new','0','2018-08-28 23:43:32','2018-08-28 10:52:29');
INSERT INTO `gd_otp` ( ) VALUES('76','55','219603','bc93b0f7be5f51f3c545ae46e06e0a46','1','b7157bce87f9df5e91a8ba15dfcb5230719fc54ee94c0288296235f9983b11ec','73F12957-E980-497C-BF7B-96A557375BE8','consumed','1','2018-08-28 23:22:29','2018-08-28 10:52:47');
INSERT INTO `gd_otp` ( ) VALUES('77','60','372878','1a2b8ce1b3b9740d7cb2ef047d07e77e','0','','','new','1','2018-08-30 06:33:27','2018-08-29 18:03:27');
INSERT INTO `gd_otp` ( ) VALUES('78','61','364402','d10c3671a3c6bb685f072b8001e97170','0','','','new','1','2018-08-30 06:33:31','2018-08-29 18:03:31');
INSERT INTO `gd_otp` ( ) VALUES('79','62','774159','b2ea9665e6c33095109aaaa5a367cd9b','0','','','new','1','2018-08-30 06:33:36','2018-08-29 18:03:36');
INSERT INTO `gd_otp` ( ) VALUES('80','63','881418','79ab2aabe37ee65a3c3ea2815e3d38dd','0','','','new','1','2018-08-30 06:39:48','2018-08-29 18:09:48');
INSERT INTO `gd_otp` ( ) VALUES('81','64','456030','d7452dd79aa8c1c3e9e835fb4f48991a','0','','','new','1','2018-08-30 06:39:52','2018-08-29 18:09:52');
INSERT INTO `gd_otp` ( ) VALUES('82','65','518214','3ec73289ede47ef44e2362bed116e446','0','','','new','1','2018-08-30 06:39:57','2018-08-29 18:09:57');
INSERT INTO `gd_otp` ( ) VALUES('83','66','561324','107a0eca621811538abf3c2dc5e75ab8','0','','','new','1','2018-08-30 19:05:33','2018-08-30 06:35:33');
INSERT INTO `gd_otp` ( ) VALUES('84','67','406427','df0f6ec572114640032f3e88f7cac1a9','0','','','new','1','2018-08-30 19:05:37','2018-08-30 06:35:37');
INSERT INTO `gd_otp` ( ) VALUES('85','68','937581','d2a08c83384d0cebf9593cf470ae18d1','0','','','new','1','2018-08-30 19:05:41','2018-08-30 06:35:41');
INSERT INTO `gd_otp` ( ) VALUES('86','69','899138','c2485166aa2e0461a7b92bc57d219daf','0','','','new','0','2018-08-30 19:05:45','2018-08-30 06:38:42');
INSERT INTO `gd_otp` ( ) VALUES('87','70','426258','3ac12902e4c96e3ff0a5b9da3fb7bcdf','0','','','new','1','2018-08-30 19:05:49','2018-08-30 06:35:49');
INSERT INTO `gd_otp` ( ) VALUES('88','69','277531','cf0f7ad64a5c24a194b53338179ba4e3','0','','','new','1','2018-08-30 19:08:42','2018-08-30 06:38:42');
INSERT INTO `gd_otp` ( ) VALUES('89','71','143379','654ecc5f4d3f0dd9e6a342632a504ddb','0','','','new','1','2018-08-30 19:11:47','2018-08-30 06:41:47');
INSERT INTO `gd_otp` ( ) VALUES('90','72','784956','3c94537d742fe0759c786029568aa8d1','0','','','new','1','2018-08-30 19:11:51','2018-08-30 06:41:51');
INSERT INTO `gd_otp` ( ) VALUES('91','73','399921','99094e657dbcdfaf98e62e7baaf479ed','0','','','new','1','2018-08-30 19:11:55','2018-08-30 06:41:55');
INSERT INTO `gd_otp` ( ) VALUES('92','74','527505','3b12ba7890a64f0622e34632ce49225c','0','','','new','1','2018-08-30 19:11:59','2018-08-30 06:41:59');
INSERT INTO `gd_otp` ( ) VALUES('93','75','668116','f8d5f10e836aad0ed11cc29c1eff96a2','0','','','new','1','2018-08-30 19:12:03','2018-08-30 06:42:03');
INSERT INTO `gd_otp` ( ) VALUES('94','76','674521','846be12a69ca8d5d56a56c10c737edb4','0','','','new','1','2018-08-30 19:12:07','2018-08-30 06:42:07');
INSERT INTO `gd_otp` ( ) VALUES('95','77','124799','7ad09618558fda94d940102ed4cd4c61','0','','','new','1','2018-08-30 19:12:12','2018-08-30 06:42:12');
INSERT INTO `gd_otp` ( ) VALUES('96','78','172746','ea4e969b85d8cd1ba8b2688f5d8cda49','0','','','new','1','2018-08-30 19:12:16','2018-08-30 06:42:16');
INSERT INTO `gd_otp` ( ) VALUES('97','79','325874','4fefa277c8c827d011a4d8f2f9dd5499','0','','','new','1','2018-08-30 19:12:20','2018-08-30 06:42:20');
INSERT INTO `gd_otp` ( ) VALUES('98','80','577868','ee509d3c470c4c4adb012f1aa0baf4a5','0','','','new','1','2018-08-30 19:12:24','2018-08-30 06:42:24');
INSERT INTO `gd_otp` ( ) VALUES('99','81','173054','9e0aabb6eaf99c5a30e9095faffc61b3','0','','','new','1','2018-08-30 19:12:28','2018-08-30 06:42:28');
INSERT INTO `gd_otp` ( ) VALUES('100','82','912473','b5a48ca3982dcd9a54c3df8998e27aea','0','','','new','1','2018-08-30 19:12:32','2018-08-30 06:42:32');
INSERT INTO `gd_otp` ( ) VALUES('101','83','386498','1fba2f5ee563066a986bfabf42e8d6c1','0','','','new','0','2018-08-30 19:12:36','2018-09-04 02:50:12');
INSERT INTO `gd_otp` ( ) VALUES('102','84','202523','cdc768753c1eb300a4f937b6ee5450db','0','','','new','1','2018-08-30 19:12:41','2018-08-30 06:42:41');
INSERT INTO `gd_otp` ( ) VALUES('103','85','487280','365ead0000c1b43012cbe16b8dd385a9','0','','','new','1','2018-08-30 19:12:45','2018-08-30 06:42:45');
INSERT INTO `gd_otp` ( ) VALUES('104','86','181358','656801e702c143975fa0f1ef3512be78','0','','','new','1','2018-08-30 19:12:49','2018-08-30 06:42:49');
INSERT INTO `gd_otp` ( ) VALUES('105','87','583703','daef11f3a37cf8d534b3db17705e9760','0','','','new','1','2018-08-30 19:12:54','2018-08-30 06:42:54');
INSERT INTO `gd_otp` ( ) VALUES('106','88','942548','9936b71eb96fcb85e2f6ef958d59ca34','0','','','new','1','2018-08-30 19:12:58','2018-08-30 06:42:58');
INSERT INTO `gd_otp` ( ) VALUES('107','89','727714','cf87b1f6358df6e7d942f105047c6a2b','0','','','new','1','2018-08-30 19:13:02','2018-08-30 06:43:02');
INSERT INTO `gd_otp` ( ) VALUES('108','90','300128','48388d0bed2aecd7c22974ff30e6fbd4','1','b7157bce87f9df5e91a8ba15dfcb5230719fc54ee94c0288296235f9983b11ec','76FD29B5-68FD-4923-8908-19B6B375AAC7','consumed','1','2018-08-30 19:13:06','2018-09-25 10:45:09');
INSERT INTO `gd_otp` ( ) VALUES('109','91','211545','63e06f027c506d585524e594ad11d8ca','0','','','new','1','2018-08-30 19:13:11','2018-08-30 06:43:11');
INSERT INTO `gd_otp` ( ) VALUES('110','92','514464','4a1555a25df63e3b23a119556a550b91','0','','','new','1','2018-08-30 19:13:15','2018-08-30 06:43:15');
INSERT INTO `gd_otp` ( ) VALUES('111','93','898830','634d5bb4be7b882244bb0ae7f6603090','0','','','new','1','2018-08-30 19:13:19','2018-08-30 06:43:19');
INSERT INTO `gd_otp` ( ) VALUES('112','94','417369','8e4d630e6131553290ffbe3d17c13476','0','','','new','1','2018-08-30 19:13:24','2018-08-30 06:43:24');
INSERT INTO `gd_otp` ( ) VALUES('113','95','380670','d0e3222c5aef8c967de1368ec770908d','0','','','new','1','2018-08-30 19:13:28','2018-08-30 06:43:28');
INSERT INTO `gd_otp` ( ) VALUES('114','96','686764','3d5e67ab80c0c73697e80b5f4a1fb54a','0','','','new','1','2018-08-30 19:13:32','2018-08-30 06:43:32');
INSERT INTO `gd_otp` ( ) VALUES('115','97','273524','fc6f6fde204b89a80ceca3766583704d','0','','','new','1','2018-08-30 19:13:36','2018-08-30 06:43:36');
INSERT INTO `gd_otp` ( ) VALUES('116','98','575168','a2f6aeac87ffc2043c09e7b7a4698332','0','','','new','1','2018-08-30 19:13:41','2018-08-30 06:43:41');
INSERT INTO `gd_otp` ( ) VALUES('117','99','325000','8d5a8a7754b93087f85993a7032a35af','0','','','new','1','2018-08-30 19:13:45','2018-08-30 06:43:45');
INSERT INTO `gd_otp` ( ) VALUES('118','100','295841','cd426d6586b286f5e55713c1caaaaa79','0','','','new','1','2018-08-30 19:13:49','2018-08-30 06:43:49');
INSERT INTO `gd_otp` ( ) VALUES('119','101','360230','d169448326465f78f38867a0e241e00b','0','','','new','1','2018-08-30 19:13:54','2018-08-30 06:43:54');
INSERT INTO `gd_otp` ( ) VALUES('120','102','953366','c963f3e325f359de8f44b1788ae06db5','0','','','new','1','2018-08-30 19:13:58','2018-08-30 06:43:58');
INSERT INTO `gd_otp` ( ) VALUES('121','103','634869','a2fdc8f776eb927c343eafe71b93becd','0','','','new','1','2018-08-30 19:14:02','2018-08-30 06:44:02');
INSERT INTO `gd_otp` ( ) VALUES('122','104','857323','2a51be2bfb47e0d2b6b5f1df2ba62e59','0','','','new','1','2018-08-30 19:14:07','2018-08-30 06:44:07');
INSERT INTO `gd_otp` ( ) VALUES('123','105','383794','93add4a957c4dae6e0f891728dca2356','0','','','new','1','2018-08-30 19:14:11','2018-08-30 06:44:11');
INSERT INTO `gd_otp` ( ) VALUES('124','106','796638','edfc23fbc2f5abc694002825675fe299','0','','','new','1','2018-08-30 19:14:16','2018-08-30 06:44:16');
INSERT INTO `gd_otp` ( ) VALUES('125','107','627785','bd0800b386fbb576c0700604b768947c','0','','','new','1','2018-08-30 19:14:20','2018-08-30 06:44:20');
INSERT INTO `gd_otp` ( ) VALUES('126','108','105993','882bfebeb4261b7c3b697b3d83a6e719','0','','','new','1','2018-08-30 19:14:24','2018-08-30 06:44:24');
INSERT INTO `gd_otp` ( ) VALUES('127','109','832041','edc39f36fcc10ac86ba35361cb9e8fc1','0','','','new','1','2018-08-30 19:14:29','2018-08-30 06:44:29');
INSERT INTO `gd_otp` ( ) VALUES('128','110','922714','3b24b1b8404715a7519876aaf803bf14','0','','','new','1','2018-08-30 19:14:33','2018-08-30 06:44:33');
INSERT INTO `gd_otp` ( ) VALUES('129','111','556624','079fa357a1320b2edafbb12a8373f948','0','','','new','1','2018-08-30 19:14:38','2018-08-30 06:44:38');
INSERT INTO `gd_otp` ( ) VALUES('130','112','644085','ef0dea4a729da253a682732baceaa125','0','','','new','1','2018-08-30 19:14:42','2018-08-30 06:44:42');
INSERT INTO `gd_otp` ( ) VALUES('131','113','443200','db00af3b4d0849d84eeb3c041e856cec','0','','','new','1','2018-08-30 19:14:47','2018-08-30 06:44:47');
INSERT INTO `gd_otp` ( ) VALUES('132','114','868863','cc66b17cc50d4914f9b94eddba65bbca','0','','','new','1','2018-08-30 19:14:51','2018-08-30 06:44:51');
INSERT INTO `gd_otp` ( ) VALUES('133','115','592412','70c9a4d347cf18b0062373800d6de72b','0','','','new','1','2018-08-30 19:18:49','2018-08-30 06:48:49');
INSERT INTO `gd_otp` ( ) VALUES('134','116','820963','1822e2ff7ff7843ea20ecb42d538134a','0','','','new','1','2018-08-30 19:19:17','2018-08-30 06:49:17');
INSERT INTO `gd_otp` ( ) VALUES('135','117','932554','3e87ca6068004abc300b14044f9df0d5','0','','','new','0','2018-08-31 20:53:00','2018-08-31 08:24:28');
INSERT INTO `gd_otp` ( ) VALUES('136','117','291094','0b74fd307f733a4d00c6585cb5b5cfaf','0','','','new','1','2018-08-31 20:54:28','2018-08-31 08:24:28');
INSERT INTO `gd_otp` ( ) VALUES('137','118','861139','995148412a7164b3b2a08ad2bedfacf6','0','','','new','1','2018-08-31 20:55:39','2018-08-31 08:25:39');
INSERT INTO `gd_otp` ( ) VALUES('138','119','411405','54ef25d7ab6a086f3e334dba9222d11c','2','','02:00:00:00:00:00','consumed','1','2018-08-31 20:57:23','2018-08-31 08:29:18');
INSERT INTO `gd_otp` ( ) VALUES('139','120','639818','9030c7d236f2322fc20746a7d8807888','0','','','new','1','2018-09-01 02:48:01','2018-08-31 14:18:01');
INSERT INTO `gd_otp` ( ) VALUES('140','121','382598','cf525bac116e1008c3c488559ade6dbb','0','','','new','0','2018-09-01 03:06:21','2018-09-25 10:45:37');
INSERT INTO `gd_otp` ( ) VALUES('141','122','130833','e92daf1ce554df3d0201e1ba9ce71e86','2','','02:00:00:00:00:00','consumed','1','2018-09-01 03:06:27','2018-08-31 14:48:35');
INSERT INTO `gd_otp` ( ) VALUES('142','123','527830','fce005a61aaad5e15bac11d8e935ba74','0','','','new','1','2018-09-01 12:44:50','2018-09-01 00:14:50');
INSERT INTO `gd_otp` ( ) VALUES('143','124','978104','f136578f759b7d56d942e725556dd9ee','0','','','new','1','2018-09-01 12:46:23','2018-09-01 00:16:23');
INSERT INTO `gd_otp` ( ) VALUES('144','125','345724','aa9dd5e0144bf81b625d28d8cb3ee1e0','0','','','new','1','2018-09-01 12:46:53','2018-09-01 00:16:54');
INSERT INTO `gd_otp` ( ) VALUES('145','126','448449','bc623695d0ccc9176b77ad48be8c2739','0','','','new','1','2018-09-01 12:54:42','2018-09-01 00:24:42');
INSERT INTO `gd_otp` ( ) VALUES('146','127','682775','09a31f46c85cc14062a3e173bd8edca1','0','','','new','1','2018-09-01 12:54:46','2018-09-01 00:24:46');
INSERT INTO `gd_otp` ( ) VALUES('147','128','930895','4d064e26baf76f0484c04f16b6cb3370','0','','','new','1','2018-09-01 12:54:51','2018-09-01 00:24:51');
INSERT INTO `gd_otp` ( ) VALUES('148','129','753285','3d0b52efaf0b7f3985b380f4d9920008','0','','','new','1','2018-09-01 12:54:55','2018-09-01 00:24:55');
INSERT INTO `gd_otp` ( ) VALUES('149','130','212264','d19d0de2b4d2be877c5554ab310f7da4','0','','','new','1','2018-09-01 12:55:00','2018-09-01 00:25:00');
INSERT INTO `gd_otp` ( ) VALUES('150','131','624313','3da7c6ba9f58fed9202d95c81b10207e','0','','','new','1','2018-09-01 12:55:05','2018-09-01 00:25:05');
INSERT INTO `gd_otp` ( ) VALUES('151','132','594626','d82e91b6a01be55def22534237b894f8','0','','','new','1','2018-09-01 12:55:09','2018-09-01 00:25:09');
INSERT INTO `gd_otp` ( ) VALUES('152','133','177251','185a8ee428f6546f8492887a55838987','1','ac704dbbf4d7b159e9dea9b702b8cca8f10b3dfbca02ec7e003c045ff6ba9797','D497BE47-1E40-4350-B2F3-FE91622BC842','consumed','1','2018-09-01 12:55:14','2018-08-28 17:38:18');
INSERT INTO `gd_otp` ( ) VALUES('153','134','454609','822ffeeaa213a43aa2f9407952d49da3','1','a235dc776bd42b3018e253fd0580f738eb115ea4960b8b329d14f5b5f4faf85f','37FD3B34-01EB-4DA6-927A-75E6F0CCFA09','consumed','1','2018-09-01 12:55:19','2018-08-28 17:22:14');
INSERT INTO `gd_otp` ( ) VALUES('154','135','587119','be6a7a3a64ccae4ef1170fb3c40a197b','0','','','new','1','2018-09-01 12:55:24','2018-09-01 00:25:24');
INSERT INTO `gd_otp` ( ) VALUES('155','136','296438','3d24733205f0dc3317c6a6b9181d815d','1','ac704dbbf4d7b159e9dea9b702b8cca8f10b3dfbca02ec7e003c045ff6ba9797','651D941C-FF8E-425C-844F-364BF5D8F872','consumed','1','2018-09-01 12:55:28','2018-08-28 17:50:49');
INSERT INTO `gd_otp` ( ) VALUES('156','137','306858','d349166da0538684deea08d0dc2c0b45','0','','','new','1','2018-09-01 12:55:33','2018-09-01 00:25:33');
INSERT INTO `gd_otp` ( ) VALUES('157','138','998290','73865339dcfb76cedce8f61c53e9238e','0','','','new','1','2018-09-01 12:55:38','2018-09-01 00:25:38');
INSERT INTO `gd_otp` ( ) VALUES('158','139','447648','a9f4eb326939f8060a6f6c3368f66adc','0','','','new','1','2018-09-01 12:55:43','2018-09-01 00:25:43');
INSERT INTO `gd_otp` ( ) VALUES('159','140','437511','07f37c99250706bf3bf7cb6e1ae39aad','0','','','new','1','2018-09-01 12:55:47','2018-09-01 00:25:47');
INSERT INTO `gd_otp` ( ) VALUES('160','141','705153','5d42e25d9a2ce52c0609c4b1411ecc30','0','','','new','1','2018-09-01 12:55:52','2018-09-01 00:25:52');
INSERT INTO `gd_otp` ( ) VALUES('161','142','484380','a9826a4de25ce62ac953eef617534e39','0','','','new','1','2018-09-01 12:55:57','2018-09-01 00:25:57');
INSERT INTO `gd_otp` ( ) VALUES('162','143','490909','67015e0139f44f19c3af4d1821e70b27','0','','','new','1','2018-09-01 12:56:02','2018-09-01 00:26:02');
INSERT INTO `gd_otp` ( ) VALUES('163','144','193862','9d49bb25131cda23ce7f090383219aef','0','','','new','1','2018-09-01 12:56:07','2018-09-01 00:26:07');
INSERT INTO `gd_otp` ( ) VALUES('164','145','288782','82af11f13677819dd9a0eb21b0e75a82','0','','','new','1','2018-09-01 12:56:11','2018-09-01 00:26:11');
INSERT INTO `gd_otp` ( ) VALUES('165','146','838866','c7c49dae774d6fb8032c8b7fbfbbefa1','0','','','new','1','2018-09-01 12:56:16','2018-09-01 00:26:16');
INSERT INTO `gd_otp` ( ) VALUES('166','147','754421','ba7a71f290c3ee2daa6ff6ce6dd11bde','0','','','new','1','2018-09-01 12:56:21','2018-09-01 00:26:21');
INSERT INTO `gd_otp` ( ) VALUES('167','148','856897','695104641ca17631ff0cffe62876eaaf','0','','','new','1','2018-09-01 12:56:26','2018-09-01 00:26:26');
INSERT INTO `gd_otp` ( ) VALUES('168','149','414172','7b1ec14f418d11e3deb94587c089d7c5','0','','','new','1','2018-09-01 12:56:31','2018-09-01 00:26:31');
INSERT INTO `gd_otp` ( ) VALUES('169','150','189237','8b5c960ceed2cdde5d87ceac366d759c','0','','','new','1','2018-09-01 12:58:59','2018-09-01 00:28:59');
INSERT INTO `gd_otp` ( ) VALUES('170','151','362755','d4f77ef87682aa77c6a9682b2548a94e','0','','','new','1','2018-09-01 12:59:04','2018-09-01 00:29:04');
INSERT INTO `gd_otp` ( ) VALUES('171','152','698813','5cc2c3a9e84171ddfa77238b934d9ec9','0','','','new','1','2018-09-01 12:59:09','2018-09-01 00:29:09');
INSERT INTO `gd_otp` ( ) VALUES('172','153','340880','eec1a4b2248b9629d504aeed0d544f87','0','','','new','1','2018-09-01 12:59:14','2018-09-01 00:29:14');
INSERT INTO `gd_otp` ( ) VALUES('173','154','441947','607c505709e4b0baf75602ac7d610de9','0','','','new','1','2018-09-01 12:59:19','2018-09-01 00:29:19');
INSERT INTO `gd_otp` ( ) VALUES('174','155','247187','f821e81593de5133a472257fe1c4b96c','0','','','new','1','2018-09-01 12:59:24','2018-09-01 00:29:24');
INSERT INTO `gd_otp` ( ) VALUES('175','156','983150','998231fb12d27c662dcccd503b1561bf','0','','','new','1','2018-09-01 12:59:29','2018-09-01 00:29:29');
INSERT INTO `gd_otp` ( ) VALUES('176','157','103889','fa98b273db26c3aa8d11755cf5c0267f','0','','','new','1','2018-09-01 12:59:35','2018-09-01 00:29:35');
INSERT INTO `gd_otp` ( ) VALUES('177','158','192825','185323571c3ded88e50631e5cb9c5f76','0','','','new','1','2018-09-01 12:59:40','2018-09-01 00:29:40');
INSERT INTO `gd_otp` ( ) VALUES('178','159','829034','af0f776f3a0cdec81dfa0a20ac8ec5fa','0','','','new','1','2018-09-01 12:59:45','2018-09-01 00:29:45');
INSERT INTO `gd_otp` ( ) VALUES('179','160','759508','f8950bb505d6a5cc5e5fee908984a916','0','','','new','1','2018-09-01 12:59:50','2018-09-01 00:29:50');
INSERT INTO `gd_otp` ( ) VALUES('180','161','880204','3dd81b252e91f9d29cae1de94f1da297','0','','','new','1','2018-09-01 12:59:55','2018-09-01 00:29:55');
INSERT INTO `gd_otp` ( ) VALUES('181','162','493085','70eb041b9c0f15e2ba2375167d7abb5e','0','','','new','1','2018-09-01 13:00:00','2018-09-01 00:30:00');
INSERT INTO `gd_otp` ( ) VALUES('182','163','223851','7d4b116c9ae63f1a2633cc4397b40aaf','0','','','new','1','2018-09-01 13:00:05','2018-09-01 00:30:05');
INSERT INTO `gd_otp` ( ) VALUES('183','164','833791','bb025bfb6ec757981eafd2a9cb4c901b','0','','','new','1','2018-09-01 13:00:10','2018-09-01 00:30:10');
INSERT INTO `gd_otp` ( ) VALUES('184','165','616744','08b3ab0dc405bc0ef8a6cb892c2c7a50','0','','','new','1','2018-09-01 13:00:15','2018-09-01 00:30:15');
INSERT INTO `gd_otp` ( ) VALUES('185','166','906611','bc1e73d57c8f1ae7dab26378d640d6c9','0','','','new','1','2018-09-01 13:00:20','2018-09-01 00:30:20');
INSERT INTO `gd_otp` ( ) VALUES('186','167','859082','c47f257ea07babf6f7f548938750ffe7','0','','','new','1','2018-09-01 13:00:25','2018-09-01 00:30:26');
INSERT INTO `gd_otp` ( ) VALUES('187','168','931335','795f32817ae3d4316cd444e5b3865ddd','0','','','new','1','2018-09-01 13:00:31','2018-09-01 00:30:31');
INSERT INTO `gd_otp` ( ) VALUES('188','169','624802','316814586ca4c474840a6c31052aa390','0','','','new','1','2018-09-01 13:00:36','2018-09-01 00:30:36');
INSERT INTO `gd_otp` ( ) VALUES('189','170','210120','f8f6e1808960766e1d82e27e17254bc9','0','','','new','1','2018-09-01 13:00:41','2018-09-01 00:30:41');
INSERT INTO `gd_otp` ( ) VALUES('190','171','984595','b01fbbcb72ba58f1d9a88aac68306bc9','0','','','new','1','2018-09-01 13:00:46','2018-09-01 00:30:46');
INSERT INTO `gd_otp` ( ) VALUES('191','172','882231','f540c67ac60bb44dd7f23909416b8967','0','','','new','1','2018-09-01 13:00:51','2018-09-01 00:30:51');
INSERT INTO `gd_otp` ( ) VALUES('192','173','886171','2d4e1052a79157e56a857f41b409b49e','0','','','new','1','2018-09-01 13:00:56','2018-09-01 00:30:56');
INSERT INTO `gd_otp` ( ) VALUES('193','174','999195','84c4d205ee02dc0f6e7afc0ce2095ed8','0','','','new','1','2018-09-04 14:08:20','2018-09-04 01:38:20');
INSERT INTO `gd_otp` ( ) VALUES('194','175','261614','304f7e8ea86116ce42cfd66205c2b5e5','0','','','new','1','2018-09-04 15:02:35','2018-09-04 02:32:35');
INSERT INTO `gd_otp` ( ) VALUES('195','83','405206','2d13e7809010931927c90ffe08723ff3','2','','02:00:00:00:00:00','consumed','1','2018-09-04 15:20:12','2018-09-04 02:51:25');
INSERT INTO `gd_otp` ( ) VALUES('196','175','530120','a6bb2a23a7e84a6cef0ad1c25b52a425','0','','','new','1','2018-09-25 06:50:31','2018-09-24 18:20:31');
INSERT INTO `gd_otp` ( ) VALUES('197','176','738428','ebefe603f46d7aa4a9eeb85374a25385','0','','','new','1','2018-09-25 06:51:14','2018-09-24 18:21:15');
INSERT INTO `gd_otp` ( ) VALUES('198','177','291354','a0696132f97eb3de57ae1a0cc726e90d','0','','','new','1','2018-09-25 06:51:59','2018-09-24 18:21:59');
INSERT INTO `gd_otp` ( ) VALUES('199','121','731819','92b2b9e17cb7d99e183073bebba643b5','2','','02:00:00:00:00:00','consumed','1','2018-09-25 23:15:37','2018-09-25 10:45:55');
INSERT INTO `gd_otp` ( ) VALUES('200','1','936570','d9606fb054fc2d095449b39bbd8b7561','0','','','new','1','2018-09-29 03:22:23','2018-09-28 14:52:23');
INSERT INTO `gd_otp` ( ) VALUES('201','2','212885','2d1c5628b20b87d2a56897e0b49115f1','0','','','new','0','2018-09-29 03:22:27','2018-09-28 05:09:55');
INSERT INTO `gd_otp` ( ) VALUES('202','3','516386','c54051db9b63175a46f626121a70091c','0','','','new','0','2018-09-29 03:23:30','2019-06-27 14:38:06');
INSERT INTO `gd_otp` ( ) VALUES('203','4','795573','f23fabfc6735393e366d9ffcc75d4cd3','0','','','new','0','2018-09-29 03:24:34','2019-01-22 22:58:54');
INSERT INTO `gd_otp` ( ) VALUES('204','5','321978','bbf6f19225cab89fa1debf764257ef50','2','','02:00:00:00:00:00','consumed','0','2018-09-29 03:25:38','2019-07-03 15:45:56');
INSERT INTO `gd_otp` ( ) VALUES('205','6','192289','57cb8d5ecad0a32d87f42c3df7557034','0','','','new','1','2018-09-29 03:26:41','2018-09-28 14:56:41');
INSERT INTO `gd_otp` ( ) VALUES('206','7','558658','69f231ffa48026fadd0144ca45a26f3a','0','','','new','1','2018-09-29 03:26:44','2018-09-28 14:56:45');
INSERT INTO `gd_otp` ( ) VALUES('207','8','803660','b96fd89bd698a59ee65e7fd3c749f439','0','','','new','1','2018-09-29 03:26:48','2018-09-28 14:56:48');
INSERT INTO `gd_otp` ( ) VALUES('208','9','498003','bea02341f1569129c24941eb657002de','0','','','new','1','2018-09-29 03:26:51','2018-09-28 14:56:51');
INSERT INTO `gd_otp` ( ) VALUES('209','10','620760','61df13c614f9b641ed5356728fd905c3','0','','','new','1','2018-09-29 03:26:55','2018-09-28 14:56:55');
INSERT INTO `gd_otp` ( ) VALUES('210','11','536183','bc120b1303784201fb94c5e7bfecc7ef','0','','','new','1','2018-09-29 03:28:00','2018-09-28 14:58:00');
INSERT INTO `gd_otp` ( ) VALUES('211','12','446680','f4f88f61e3d0165b097a30efcb800d51','1','','CD4BAABE-F080-4645-91C9-A6C2B6FBAFD1','consumed','0','2018-09-29 03:29:04','2018-10-02 20:19:22');
INSERT INTO `gd_otp` ( ) VALUES('212','13','867498','d0a8cd906313c9da1ec7e9bcbbe94d4b','0','','','new','1','2018-09-29 03:30:07','2018-09-28 15:00:07');
INSERT INTO `gd_otp` ( ) VALUES('213','14','699349','2d1fda785ea162c247b9987a77f4adad','0','','','new','1','2018-09-29 03:31:11','2018-09-28 15:01:11');
INSERT INTO `gd_otp` ( ) VALUES('214','15','847027','e6e826f16fc86a805c34f397d9be2b6c','0','','','new','1','2018-09-29 03:32:15','2018-09-28 15:02:15');
INSERT INTO `gd_otp` ( ) VALUES('215','16','804434','15bc00034e5716dbc09a2537ba8bcb05','0','','','new','1','2018-09-29 03:32:18','2018-09-28 15:02:18');
INSERT INTO `gd_otp` ( ) VALUES('216','17','334874','916e13664f32e2bc7a5a8042cdc28277','0','','','new','1','2018-09-29 03:32:22','2018-09-28 15:02:22');
INSERT INTO `gd_otp` ( ) VALUES('217','18','617842','a9095464c80abe639a8b9e0f255a2250','0','','','new','1','2018-09-29 03:32:25','2018-09-28 15:02:25');
INSERT INTO `gd_otp` ( ) VALUES('218','19','598413','524953149d97211b66662f5d8a38239c','0','','','new','1','2018-09-29 03:32:29','2018-09-28 15:02:29');
INSERT INTO `gd_otp` ( ) VALUES('219','20','334780','0d1eac6d365c6ab2d0de42bd6efb102a','0','','','new','1','2018-09-29 03:33:33','2018-09-28 15:03:33');
INSERT INTO `gd_otp` ( ) VALUES('220','21','731942','af930910044dc72a2009fefc0847a0b3','2','','02:00:00:00:00:00','consumed','1','2018-09-29 03:34:36','2018-09-28 05:52:36');
INSERT INTO `gd_otp` ( ) VALUES('221','22','279815','274e068f01182cc5e4b5e44c263989ff','0','','','new','1','2018-09-29 03:35:40','2018-09-28 15:05:40');
INSERT INTO `gd_otp` ( ) VALUES('222','23','193353','715fb6c48b5c1f09fa76dc74415f04a9','0','','','new','1','2018-09-29 03:36:44','2018-09-28 15:06:44');
INSERT INTO `gd_otp` ( ) VALUES('223','24','996465','d8550dcb768e87d8d12ef0885734a561','0','','','new','1','2018-09-29 03:36:48','2018-09-28 15:06:48');
INSERT INTO `gd_otp` ( ) VALUES('224','25','746246','3e13f2eb6f830b115630f4c53882bddb','0','','','new','1','2018-09-29 03:36:51','2018-09-28 15:06:51');
INSERT INTO `gd_otp` ( ) VALUES('225','26','781243','0b80ca51f9c911e4a20871971ce38c19','0','','','new','1','2018-09-29 03:36:55','2018-09-28 15:06:55');
INSERT INTO `gd_otp` ( ) VALUES('226','27','187131','833ca51fb194636904646b5e898a8cc3','0','','','new','1','2018-09-29 03:36:59','2018-09-28 15:06:59');
INSERT INTO `gd_otp` ( ) VALUES('227','28','221091','80e831b591e3d872661902398aacf64a','0','','','new','1','2018-09-29 03:38:02','2018-09-28 15:08:02');
INSERT INTO `gd_otp` ( ) VALUES('228','29','955217','2df162ba18099aba0f356ea580bc8a76','0','','','new','1','2018-09-29 03:39:06','2018-09-28 15:09:06');
INSERT INTO `gd_otp` ( ) VALUES('229','30','794942','03fd9efa738f3bc9412c9db02b72f150','0','','','new','1','2018-09-29 03:40:10','2018-09-28 15:10:10');
INSERT INTO `gd_otp` ( ) VALUES('230','31','142116','1b63ff05fcc35820ffaefc641ff40217','0','','','new','1','2018-09-29 03:41:15','2018-09-28 15:11:15');
INSERT INTO `gd_otp` ( ) VALUES('231','32','397115','698681db3749b41a72db19e57052f3e8','0','','','new','0','2018-09-29 03:42:19','2018-12-13 17:48:34');
INSERT INTO `gd_otp` ( ) VALUES('232','33','531341','b0b2b0f15d6a87db4cd4ccc496034cdb','0','','','new','1','2018-09-29 03:42:23','2018-09-28 15:12:23');
INSERT INTO `gd_otp` ( ) VALUES('233','34','475260','1a3e0589878e2c8f974448679d811831','0','','','new','1','2018-09-29 03:42:27','2018-09-28 15:12:27');
INSERT INTO `gd_otp` ( ) VALUES('234','35','418902','3bc770f296d9acf1e187f691d50503b4','0','','','new','1','2018-09-29 03:42:30','2018-09-28 15:12:30');
INSERT INTO `gd_otp` ( ) VALUES('235','36','209192','5a7498276fb2500e5372bde7e83361e9','0','','','new','1','2018-09-29 03:42:34','2018-09-28 15:12:34');
INSERT INTO `gd_otp` ( ) VALUES('236','37','308128','e77457075cf68fc1966ee160a1847a2c','0','','','new','1','2018-09-29 03:43:38','2018-09-28 15:13:38');
INSERT INTO `gd_otp` ( ) VALUES('237','38','110295','6494522c14582194fb3f70a5822ed618','0','','','new','1','2018-09-29 03:44:42','2018-09-28 15:14:42');
INSERT INTO `gd_otp` ( ) VALUES('238','39','266127','aa1d5a05bee85d8fa6d50a6425613ee0','0','','','new','1','2018-09-29 03:45:45','2018-09-28 15:15:46');
INSERT INTO `gd_otp` ( ) VALUES('239','40','161060','75f316c9c5c6848658393e40bfdf0da3','0','','','new','1','2018-09-29 03:46:49','2018-09-28 15:16:49');
INSERT INTO `gd_otp` ( ) VALUES('240','41','447247','be5707012ba589b56c6d89343f8f3d10','0','','','new','1','2018-09-29 03:46:55','2018-09-28 15:16:55');
INSERT INTO `gd_otp` ( ) VALUES('241','42','613074','474d4d2dae75a6b3b1a28b8a52040e41','0','','','new','1','2018-09-29 03:46:59','2018-09-28 15:16:59');
INSERT INTO `gd_otp` ( ) VALUES('242','43','987066','17ef814c88bde207c3511e15efe5d3a3','0','','','new','1','2018-09-29 03:47:03','2018-09-28 15:17:03');
INSERT INTO `gd_otp` ( ) VALUES('243','44','951076','622bdaa82399f952f67838342af95d00','0','','','new','1','2018-09-29 03:48:08','2018-09-28 15:18:08');
INSERT INTO `gd_otp` ( ) VALUES('244','45','290128','55f67c1a8c53e3a13a79ecaecf7a0d25','0','','','new','1','2018-09-29 03:49:12','2018-09-28 15:19:12');
INSERT INTO `gd_otp` ( ) VALUES('245','46','990877','0aeaf6e454883f0d62e3bd9300f31c33','0','','','new','1','2018-09-29 03:50:16','2018-09-28 15:20:16');
INSERT INTO `gd_otp` ( ) VALUES('246','47','325391','cb6ad091f7fed89f59ec06b3b1ab68be','0','','','new','1','2018-09-29 03:51:20','2018-09-28 15:21:20');
INSERT INTO `gd_otp` ( ) VALUES('247','48','429323','f3096e02de9742f128aef695fee0227a','0','','','new','1','2018-09-29 03:52:24','2018-09-28 15:22:24');
INSERT INTO `gd_otp` ( ) VALUES('248','49','637190','1312e31574cadae67526489fa3656143','0','','','new','1','2018-09-29 03:52:28','2018-09-28 15:22:28');
INSERT INTO `gd_otp` ( ) VALUES('249','50','941229','6ef34bd85206c3fa9038e5fa585202b7','0','','','new','1','2018-09-29 03:52:33','2018-09-28 15:22:33');
INSERT INTO `gd_otp` ( ) VALUES('250','51','714199','0c0cc2d985fb6012525b12cf4b9944ac','0','','','new','1','2018-09-29 03:52:38','2018-09-28 15:22:38');
INSERT INTO `gd_otp` ( ) VALUES('251','52','223479','5951bfcf8dcb0cc3217bcc6372312d9b','0','','','new','1','2018-09-29 03:53:44','2018-09-28 15:23:44');
INSERT INTO `gd_otp` ( ) VALUES('252','53','952217','fe4e748ee4442572e4b10b7421ca9808','0','','','new','1','2018-09-29 03:54:48','2018-09-28 15:24:48');
INSERT INTO `gd_otp` ( ) VALUES('253','54','418110','d5de9fdff42beaa8020cef12eaa1cc89','0','','','new','1','2018-09-29 03:55:52','2018-09-28 15:25:52');
INSERT INTO `gd_otp` ( ) VALUES('254','55','866388','f80510dba9326653ef5a6310d5f2c057','0','','','new','1','2018-09-29 03:55:56','2018-09-28 15:25:56');
INSERT INTO `gd_otp` ( ) VALUES('255','56','760278','268a2dad1f26cd1697dc0131364cf802','0','','','new','1','2018-09-29 03:56:00','2018-09-28 15:26:00');
INSERT INTO `gd_otp` ( ) VALUES('256','57','196174','745701085c4818145c62533aa374efe7','0','','','new','1','2018-09-29 03:56:04','2018-09-28 15:26:04');
INSERT INTO `gd_otp` ( ) VALUES('257','58','224118','602c46f11ca6f4b1f32c87f484369f6e','0','','','new','1','2018-09-29 03:56:08','2018-09-28 15:26:08');
INSERT INTO `gd_otp` ( ) VALUES('258','59','544637','d0380e81817b085bd3fef6499cbc548c','0','','','new','1','2018-09-29 03:57:12','2018-09-28 15:27:12');
INSERT INTO `gd_otp` ( ) VALUES('259','60','251190','954dd9305896cb008eb100d3d7b780fc','0','','','new','1','2018-09-29 03:57:16','2018-09-28 15:27:16');
INSERT INTO `gd_otp` ( ) VALUES('260','61','205466','22fbb4e63fd70b05e64cf247e8305df2','0','','','new','1','2018-09-29 03:57:21','2018-09-28 15:27:21');
INSERT INTO `gd_otp` ( ) VALUES('261','62','566144','c202fa851801f7bb6a03c90068484a42','0','','','new','1','2018-09-29 03:57:25','2018-09-28 15:27:25');
INSERT INTO `gd_otp` ( ) VALUES('262','63','134253','b2e301718b20b4e91709a47f9e2d7351','0','','','new','1','2018-09-29 03:58:29','2018-09-28 15:28:29');
INSERT INTO `gd_otp` ( ) VALUES('263','64','908266','ea11e365eb43310847e68e7343df43a5','0','','','new','1','2018-09-29 03:58:36','2018-09-28 15:28:36');
INSERT INTO `gd_otp` ( ) VALUES('264','65','886565','61a079aaed71b92cecc5c634135f528b','0','','','new','1','2018-09-29 03:58:41','2018-09-28 15:28:41');
INSERT INTO `gd_otp` ( ) VALUES('265','66','797389','a4df9db7b1ce9489f86df0001d5a2d05','0','','','new','1','2018-09-29 03:58:46','2018-09-28 15:28:46');
INSERT INTO `gd_otp` ( ) VALUES('266','67','224905','4e340ba4eaeb70c87b9205d08b51a2e3','0','','','new','1','2018-09-29 03:58:50','2018-09-28 15:28:50');
INSERT INTO `gd_otp` ( ) VALUES('267','68','143845','7a526f39f8758be1b4f9aac8a89f1d71','0','','','new','1','2018-09-29 04:01:19','2018-09-28 15:31:19');
INSERT INTO `gd_otp` ( ) VALUES('268','69','806642','adbf07107939011be05d7db6981ae668','0','','','new','1','2018-09-29 04:03:29','2018-09-28 15:33:29');
INSERT INTO `gd_otp` ( ) VALUES('269','2','823076','790eb27077f5e62137a3e2d795b810d8','2','','02:00:00:00:00:00','consumed','0','2018-09-28 17:39:55','2019-01-17 23:20:23');
INSERT INTO `gd_otp` ( ) VALUES('270','70','599820','26eb0688ec73024383fd8467777d57f9','0','','','new','1','2018-10-03 07:54:47','2018-10-02 19:24:47');
INSERT INTO `gd_otp` ( ) VALUES('271','12','456371','4724634a31f97a199f9875fbee27b01b','1','b7157bce87f9df5e91a8ba15dfcb5230719fc54ee94c0288296235f9983b11ec','82E32B83-0205-4797-8B7B-2F1F9D75E75A','consumed','1','2018-10-03 08:49:22','2018-10-02 20:19:31');
INSERT INTO `gd_otp` ( ) VALUES('272','70','882895','94c562b6110dc6e76e816d1e5cb2ef0f','0','','','new','1','2018-10-16 20:33:45','2018-10-16 08:03:45');
INSERT INTO `gd_otp` ( ) VALUES('273','70','260783','dd281b2158255197af3d30c8994ab3e7','0','','','new','1','2018-10-19 22:18:47','2018-10-19 09:48:47');
INSERT INTO `gd_otp` ( ) VALUES('274','70','284280','eababe2269a17ba854cf87b6df3c3b18','0','','','new','1','2018-11-29 06:27:01','2018-11-28 16:57:02');
INSERT INTO `gd_otp` ( ) VALUES('275','32','889060','1b14f6abcfe9fd38f0fbb07a21c1c4fc','0','','','new','1','2018-12-14 07:18:34','2018-12-13 17:48:34');
INSERT INTO `gd_otp` ( ) VALUES('276','71','708359','1e355a1b93823e69edeb95497e41df8d','0','','','new','1','2018-12-16 03:44:09','2018-12-15 14:14:09');
INSERT INTO `gd_otp` ( ) VALUES('277','72','642337','7fbb779c16e05da190a2631d126501ec','0','','','new','1','2019-01-14 15:31:46','2019-01-14 02:01:46');
INSERT INTO `gd_otp` ( ) VALUES('278','73','699480','255610685c2e753b0968041c91dc58b5','0','','','new','1','2019-01-14 17:27:03','2019-01-14 03:57:03');
INSERT INTO `gd_otp` ( ) VALUES('279','74','533508','6ca11f3ed40a64613e6c420f64a90394','0','','','new','1','2019-01-14 17:28:24','2019-01-14 03:58:24');
INSERT INTO `gd_otp` ( ) VALUES('280','75','896145','9a1f8761ff309e09e2717761f531f865','0','','','new','1','2019-01-14 17:29:15','2019-01-14 03:59:15');
INSERT INTO `gd_otp` ( ) VALUES('281','76','596736','1eabddaa11a3f9d924c798d2091b2b86','0','','','new','1','2019-01-14 17:29:55','2019-01-14 03:59:55');
INSERT INTO `gd_otp` ( ) VALUES('282','77','676672','7f7ae36443ea906bfff4713d80be6fb7','0','','','new','1','2019-01-14 18:25:38','2019-01-14 04:55:38');
INSERT INTO `gd_otp` ( ) VALUES('283','78','243560','3a6e9b12c7dccaa6a4301e4339d56761','0','','','new','1','2019-01-16 15:30:50','2019-01-16 02:00:50');
INSERT INTO `gd_otp` ( ) VALUES('284','79','137292','055f7c4b822d1f3fbf4e860c4dc528ee','0','','','new','1','2019-01-18 10:26:22','2019-01-17 20:56:22');
INSERT INTO `gd_otp` ( ) VALUES('285','2','360073','cfaa6664d5f52d617101f17bdbf177da','0','','','new','1','2019-01-18 12:50:23','2019-01-17 23:20:23');
INSERT INTO `gd_otp` ( ) VALUES('286','80','861119','e3ff2c25b8daf8463f33d273b4b5de48','0','','','new','1','2019-01-20 10:50:25','2019-01-19 21:20:26');
INSERT INTO `gd_otp` ( ) VALUES('287','81','709387','2626c68d9e5dd02c213f12cafff90508','0','','','new','1','2019-01-20 10:51:20','2019-01-19 21:21:20');
INSERT INTO `gd_otp` ( ) VALUES('288','82','741127','70fc32db5e0bfc481468b549fa0b5b7b','0','','','new','1','2019-01-22 18:58:17','2019-01-22 05:28:17');
INSERT INTO `gd_otp` ( ) VALUES('289','83','641741','68e94aa37b41bc4e2da1a1c29f9ab72b','2','','02:00:00:00:00:00','consumed','1','2019-01-22 19:04:04','2019-04-06 21:25:09');
INSERT INTO `gd_otp` ( ) VALUES('290','84','342191','a6048c5cb2337b13d89beb0dd07836d2','0','','','new','1','2019-01-22 20:59:29','2019-01-22 07:29:29');
INSERT INTO `gd_otp` ( ) VALUES('291','85','408431','27b2aeb68fcfc61dcd480d5bb799ddf6','0','','','new','1','2019-01-22 23:56:42','2019-01-22 10:26:42');
INSERT INTO `gd_otp` ( ) VALUES('292','86','840145','0caf9b4e2846ce5ebc0ce5d9a7fe74e6','0','','','new','1','2019-01-22 23:57:40','2019-01-22 10:27:40');
INSERT INTO `gd_otp` ( ) VALUES('293','87','714699','391f45f3cd1da6c919cde014f8638c4f','0','','','new','1','2019-01-23 10:52:44','2019-01-22 21:22:44');
INSERT INTO `gd_otp` ( ) VALUES('294','88','321596','9ccb6a74efbd05af04b0a787634ac148','0','','','new','1','2019-01-23 11:05:46','2019-01-22 21:35:46');
INSERT INTO `gd_otp` ( ) VALUES('295','89','846083','bbf6694c984a8cc26b6a051190c421eb','0','','','new','1','2019-01-23 11:35:06','2019-01-22 22:05:06');
INSERT INTO `gd_otp` ( ) VALUES('296','90','518206','28a23afdf589f7c9033317ccb5bcc6c8','0','','','new','1','2019-01-23 11:35:45','2019-01-22 22:05:45');
INSERT INTO `gd_otp` ( ) VALUES('297','91','605356','8326419b397a612324a2b2b0da05c17f','0','','','new','1','2019-01-23 12:11:29','2019-01-22 22:41:30');
INSERT INTO `gd_otp` ( ) VALUES('298','92','902810','23a6c2b60dcfb6aa456eebe06fa35cb7','0','','','new','1','2019-01-23 12:11:58','2019-01-22 22:41:58');
INSERT INTO `gd_otp` ( ) VALUES('299','93','563615','bd614bc2e8e6633a2d84be0882d812f5','0','','','new','1','2019-01-23 12:12:41','2019-01-22 22:42:41');
INSERT INTO `gd_otp` ( ) VALUES('300','94','485495','937d1c88fa8c3a695e21b20847a7aa79','0','','','new','1','2019-01-23 12:13:12','2019-01-22 22:43:12');
INSERT INTO `gd_otp` ( ) VALUES('301','95','783508','1969bbb008ad1250622791092208d5a6','0','','','new','0','2019-01-23 12:13:43','2019-07-03 15:51:36');
INSERT INTO `gd_otp` ( ) VALUES('302','96','782136','e71167f8fdc45991568e22e446cc65e3','0','','','new','1','2019-01-23 12:14:13','2019-01-22 22:44:13');
INSERT INTO `gd_otp` ( ) VALUES('303','97','582299','1a8dae76bba45d0378fa762677475cbb','0','','','new','1','2019-01-23 12:15:07','2019-01-22 22:45:07');
INSERT INTO `gd_otp` ( ) VALUES('304','98','453492','a5b27c3b96fca5b9108f25ff7ce76d48','0','','','new','1','2019-01-23 12:15:38','2019-01-22 22:45:38');
INSERT INTO `gd_otp` ( ) VALUES('305','99','614153','1b4e0a2d47e8623d7265df7db9208cda','0','','','new','1','2019-01-23 12:16:19','2019-01-22 22:46:19');
INSERT INTO `gd_otp` ( ) VALUES('306','100','311888','230d39d20e7ef13011579a6523f5d627','0','','','new','1','2019-01-23 12:16:53','2019-01-22 22:46:53');
INSERT INTO `gd_otp` ( ) VALUES('307','101','549709','4d7aa77c3f116c01f57a98f404516dc0','0','','','new','1','2019-01-23 12:17:16','2019-01-22 22:47:16');
INSERT INTO `gd_otp` ( ) VALUES('308','102','528943','0c339cc7a08a4cc6f457303173cd6261','0','','','new','1','2019-01-23 12:17:44','2019-01-22 22:47:44');
INSERT INTO `gd_otp` ( ) VALUES('309','103','503438','452ef97df04f67e6b7ba388fc55a6363','0','','','new','1','2019-01-23 12:18:17','2019-01-22 22:48:17');
INSERT INTO `gd_otp` ( ) VALUES('310','104','350851','b966502c3da9a87783e3fe256783c17e','0','','','new','1','2019-01-23 12:19:11','2019-01-22 22:49:11');
INSERT INTO `gd_otp` ( ) VALUES('311','105','258772','dbd1b53ea3402ad3d791026f75fb705b','0','','','new','1','2019-01-23 12:19:58','2019-01-22 22:49:58');
INSERT INTO `gd_otp` ( ) VALUES('312','106','671290','038ea4d334f2b883e1522dd7137357e6','0','','','new','1','2019-01-23 12:20:27','2019-01-22 22:50:27');
INSERT INTO `gd_otp` ( ) VALUES('313','107','608637','973fe33bdbb340706769a9b0d3504d0c','0','','','new','1','2019-01-23 12:21:15','2019-01-22 22:51:15');
INSERT INTO `gd_otp` ( ) VALUES('314','108','773514','420d203d6bb7ca3510c17436631122bf','0','','','new','1','2019-01-23 12:21:56','2019-01-22 22:51:56');
INSERT INTO `gd_otp` ( ) VALUES('315','109','425746','a8bace5487b403ea36feb4f52e5ef828','0','','','new','1','2019-01-23 12:22:32','2019-01-22 22:52:32');
INSERT INTO `gd_otp` ( ) VALUES('316','110','681488','0c841b2cd1e3630322c0d81131dbe7bd','0','','','new','1','2019-01-23 12:22:55','2019-01-22 22:52:55');
INSERT INTO `gd_otp` ( ) VALUES('317','111','162034','f2ffc3e15445dffc4753337029ae8849','0','','','new','1','2019-01-23 12:23:19','2019-01-22 22:53:19');
INSERT INTO `gd_otp` ( ) VALUES('318','112','202837','f31c76bdd500b73db32393059f0fb221','0','','','new','1','2019-01-23 12:23:46','2019-01-22 22:53:46');
INSERT INTO `gd_otp` ( ) VALUES('319','113','596133','cce15d5ce20c85978f84c032a40c4772','0','','','new','1','2019-01-23 12:24:25','2019-01-22 22:54:25');
INSERT INTO `gd_otp` ( ) VALUES('320','4','289876','7cb641da34af3912c3cc9a29d939dda1','2','','02:00:00:00:00:00','consumed','1','2019-03-07 04:10:03','2019-03-06 14:40:13');
INSERT INTO `gd_otp` ( ) VALUES('321','114','753244','5650379b365e4f8a8ba65430972b8b28','2','','02:00:00:00:00:00','consumed','1','2019-03-07 04:11:37','2019-03-06 14:42:02');
INSERT INTO `gd_otp` ( ) VALUES('322','115','841133','a587ab633087231ac9451018d55afdd1','2','','02:00:00:00:00:00','consumed','0','2019-04-06 11:28:58','2019-04-05 23:06:24');
INSERT INTO `gd_otp` ( ) VALUES('323','115','804286','0c643f51e775d3c81b166d8df16c7e67','2','','02:00:00:00:00:00','consumed','1','2019-04-06 11:36:24','2019-04-05 23:07:11');
INSERT INTO `gd_otp` ( ) VALUES('324','116','496021','a4731b70e1cd097d8760aaa8cdc936f8','2','','02:00:00:00:00:00','consumed','0','2019-04-07 06:47:49','2019-04-06 03:31:01');
INSERT INTO `gd_otp` ( ) VALUES('325','117','918621','3aa460a2c0fe7a4b4d01a52c488a3198','1','','27666251-B343-47F3-BE4C-EFE2706E0127','consumed','1','2019-04-07 12:22:53','2019-04-06 23:53:56');
INSERT INTO `gd_otp` ( ) VALUES('326','118','475904','8bc02028a1f6a1a2d3224e99c4902638','2','','02:00:00:00:00:00','consumed','0','2019-04-07 12:33:53','2019-04-07 18:35:07');
INSERT INTO `gd_otp` ( ) VALUES('327','118','931378','168dc7beb4fc532b0db6ed4a2aeb668e','2','','02:00:00:00:00:00','consumed','1','2019-04-08 07:05:07','2019-04-07 18:36:11');
INSERT INTO `gd_otp` ( ) VALUES('328','119','633488','80c2fd49f5c6e6c0e21a7770651398ac','1','3ed2dbffad00166d5b760d49add3035731e175e060ae67a7dd5b3adc6f20bd59','75CA0090-5431-41C4-B590-FAB3779E4955','consumed','1','2019-04-06 12:46:09','2019-04-06 00:17:46');
INSERT INTO `gd_otp` ( ) VALUES('329','120','479374','e65896b949439849225d45fe1808500b','0','','','new','1','2019-04-06 14:22:13','2019-04-06 01:52:13');
INSERT INTO `gd_otp` ( ) VALUES('330','116','152850','6fe322c274f1807a0504d181eddfd312','2','','02:00:00:00:00:00','consumed','0','2019-04-06 16:01:01','2019-04-06 03:41:15');
INSERT INTO `gd_otp` ( ) VALUES('331','116','139321','aebc013275d17d9e5028453e883b6e0b','2','','02:00:00:00:00:00','consumed','0','2019-04-06 16:11:15','2019-04-06 03:45:09');
INSERT INTO `gd_otp` ( ) VALUES('332','116','346803','65c8b03e3588f665d3f015f2740ffd2c','2','','02:00:00:00:00:00','consumed','0','2019-04-06 16:15:09','2019-04-06 03:58:14');
INSERT INTO `gd_otp` ( ) VALUES('333','116','142610','ae7e2a0b03467cfff79b61151fb0c7fe','2','','02:00:00:00:00:00','consumed','1','2019-04-06 16:28:14','2019-04-06 03:58:26');
INSERT INTO `gd_otp` ( ) VALUES('334','3','581388','eec47be2eefb0aa0e5603280d7aebd9a','2','','02:00:00:00:00:00','consumed','1','2019-06-28 03:08:06','2019-06-27 14:38:15');
INSERT INTO `gd_otp` ( ) VALUES('335','5','664405','bbd4a74eb54e52b012322299026d9a07','2','','02:00:00:00:00:00','consumed','1','2019-07-04 04:15:56','2019-07-03 15:46:08');
INSERT INTO `gd_otp` ( ) VALUES('336','120','897997','6901296cd17e730bf0dd793e7a7493b9','2','','02:00:00:00:00:00','consumed','1','2019-07-04 04:18:15','2019-07-03 15:49:04');
INSERT INTO `gd_otp` ( ) VALUES('337','95','673665','8e2e1aa77d97325790708ae7af37117a','2','','02:00:00:00:00:00','consumed','0','2019-07-04 04:21:36','2019-07-03 15:54:08');
INSERT INTO `gd_otp` ( ) VALUES('338','95','358559','f557e67c30739c4695b8649224048116','2','','02:00:00:00:00:00','consumed','1','2019-07-04 04:24:08','2019-07-03 15:54:27');



DROP TABLE IF EXISTS `gd_outbounds`;

CREATE TABLE `gd_outbounds` (
  `route_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `route_name` varchar(255) DEFAULT NULL,
  `route_cid` varchar(255) DEFAULT NULL,
  `override_extension` tinyint(1) NOT NULL DEFAULT '0',
  `route_password` varchar(255) DEFAULT NULL,
  `emergency` varchar(3) NOT NULL DEFAULT 'no',
  `intra_company` varchar(3) NOT NULL DEFAULT 'no',
  `music_on_hold` tinyint(1) NOT NULL DEFAULT '0',
  `route_position` tinyint(1) NOT NULL DEFAULT '0',
  `trunk_sequence` varchar(255) DEFAULT NULL,
  `optional_destination` varchar(255) DEFAULT NULL,
  `prepend_digits` varchar(1000) DEFAULT NULL,
  `match_prefix` varchar(1000) DEFAULT NULL,
  `match_pattern` varchar(1000) DEFAULT NULL,
  `match_cid` varchar(1000) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`route_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;




DROP TABLE IF EXISTS `gd_owner_type`;

CREATE TABLE `gd_owner_type` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_type` tinyint(1) NOT NULL COMMENT '1=pm, 2=pd',
  `owner_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`row_id`),
  UNIQUE KEY `uniqueOwnerType` (`owner_type`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

INSERT INTO `gd_owner_type` ( ) VALUES('13','1','68','2018-09-29 04:01:19','2018-09-29 04:01:19');
INSERT INTO `gd_owner_type` ( ) VALUES('14','2','69','2018-09-29 04:03:29','2018-09-29 04:03:29');
INSERT INTO `gd_owner_type` ( ) VALUES('12','3','5','2018-09-04 15:01:07','2019-03-07 04:29:09');



DROP TABLE IF EXISTS `gd_time_profiles`;

CREATE TABLE `gd_time_profiles` (
  `time_profile_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `time_profile_name` varchar(255) NOT NULL,
  `time_profile_config` varchar(255) NOT NULL,
  `time_profile_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Active,0=Inactive',
  PRIMARY KEY (`time_profile_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `gd_time_profiles` ( ) VALUES('1','M-F 9-5','{"monday":"09:00 to 17:00","tuesday":"09:00 to 17:00","wednesday":"09:00 to 17:00","thursday":"09:00 to 17:00","friday":"09:00 to 17:00"}','1',);
INSERT INTO `gd_time_profiles` ( ) VALUES('3','Test','{"sunday":"00:00 to 01:00","monday":"01:00 to 02:00"}','1',);



DROP TABLE IF EXISTS `gd_trunks`;

CREATE TABLE `gd_trunks` (
  `trunk_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tech` varchar(10) NOT NULL,
  `trunk_name` varchar(255) NOT NULL,
  `hide_cid` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=yes',
  `outbound_cid` varchar(255) DEFAULT NULL,
  `cid_options` varchar(4) NOT NULL DEFAULT 'off' COMMENT 'off, on, cnum, all',
  `maximum_channels` int(11) NOT NULL DEFAULT '0',
  `dial_options_button` varchar(10) NOT NULL DEFAULT 'system' COMMENT 'system, override',
  `dial_option_value` varchar(255) DEFAULT NULL,
  `continue_if_busy` varchar(3) NOT NULL DEFAULT 'off' COMMENT 'on, off',
  `disable_trunk` varchar(3) NOT NULL DEFAULT 'off' COMMENT 'on, off',
  `prepend_digits` varchar(1000) DEFAULT NULL,
  `match_prefix` varchar(1000) DEFAULT NULL,
  `match_pattern` varchar(1000) DEFAULT NULL,
  `match_cid` varchar(1000) DEFAULT NULL,
  `outbound_dial_prefix` varchar(255) DEFAULT NULL,
  `channel_id` varchar(255) DEFAULT NULL,
  `peer_details` varchar(1000) DEFAULT NULL,
  `peer_values` varchar(1000) DEFAULT NULL,
  `user_context` varchar(255) DEFAULT NULL,
  `user_details` varchar(1000) DEFAULT NULL,
  `user_values` varchar(1000) DEFAULT NULL,
  `register_string` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`trunk_id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;




DROP TABLE IF EXISTS `gd_userlimit`;

CREATE TABLE `gd_userlimit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `max_user` int(11) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_date` datetime DEFAULT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf16;

INSERT INTO `gd_userlimit` ( ) VALUES('1','500','1','2017-06-09 18:39:43','2019-01-19 21:33:00');



