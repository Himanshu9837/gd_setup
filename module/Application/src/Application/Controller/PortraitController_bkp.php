<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class PortraitController extends AbstractActionController {

    public function setEntityManager(EntityManager $em) {
        $this->em = $em;
    }

    public function getEntityManager() {
        if (null === @$this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    public function getAuthService() {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()->get('AuthService');
        }
        return $this->authservice;
    }

    public function indexAction() {
        $callingExtension=@$_GET['callingextension'];
        if(!$callingExtension) {
            // return to 404
            $this->layout('error/404/New');
            $view = new ViewModel();
            return $view;
        }

    	$this->layout('layout/playout');
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $offset = 0;
        $maxlimit = 50;
        $id = 1;
        $endcaltime = $em->getRepository('Admin\Entity\GdCallendtime')->findOneById($id);
        $endcallTime = $endcaltime->getCallEndTime();

        // get PM and PD
        $PM=$this->_getOwnerTypeTable()->getByType(1);
        $PD=$this->_getOwnerTypeTable()->getByType(2);
        // get info
        if($PM)
        $pmInfo=$em->getRepository('Admin\Entity\GdFlatowner')->findOneBy(array('id'=>$PM,'status'=>1));
        if($PD)
        $pdInfo=$em->getRepository('Admin\Entity\GdFlatowner')->findOneBy(array('id'=>$PD,'status'=>1));

        $notIn=$PM.",".$PD;
        $notIn=trim(trim(trim($notIn), ","));

        $totalOwner = $em->getRepository('Admin\Entity\GdFlatowner')->totalOwner($notIn);
        $ownerObj = $em->getRepository('Admin\Entity\GdFlatowner')->ownersNamelist($offset, $maxlimit, $notIn);

        return new ViewModel(array(
            "ownerObj" => $ownerObj, "totalOwner" => $totalOwner, "offset" => $offset, "maxlimit" => $maxlimit, 'endtime' => $endcallTime,
            "pmInfo"=>$pmInfo, "pdInfo"=>$pdInfo, "callingExtension"=>$callingExtension
        ));
    }

     /**
     * This function is used for loading more resident names
     * 
     * @author          Nishikant
     * @return          mixed $result    
     */
    public function morenameAction() {
    	$this->layout('layout/playout');
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $option = $request->getPost('option');
        $offset = $request->getPost('offset');
        $maxlimit = $request->getPost('maxlimit');
        $id = 1;
        $endcaltime = $em->getRepository('Admin\Entity\GdCallendtime')->findOneById($id);
        $endcallTime = $endcaltime->getCallEndTime();
        $totalOwner = $em->getRepository('Admin\Entity\GdFlatowner')->totalOwner();
        if ($option == 'search') {
            $ownerObj = $em->getRepository('Admin\Entity\GdFlatowner')->searchowner($offset, $maxlimit);
        } else {
            $ownerObj = $em->getRepository('Admin\Entity\GdFlatowner')->ownersNamelist($offset, $maxlimit);
        }
        $viewModel = new ViewModel(array("ownerObj" => $ownerObj, "offset" => $offset, "totalOwner" => $totalOwner, 'endtime' => $endcallTime));
        $viewModel->setTerminal(true);
        return $viewModel;
    }

    /**
     * This function is used for loading less resident names
     * 
     * @author          Nishikant
     * @return          mixed $result    
     */
    public function lessnameAction() {
    	$this->layout('layout/playout');
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $offset = $request->getPost('offset');
        $maxlimit = $request->getPost('maxlimit');
        $id = 1;
        $endcaltime = $em->getRepository('Admin\Entity\GdCallendtime')->findOneById($id);
        $endcallTime = $endcaltime->getCallEndTime();
        $ownerObj = $em->getRepository('Admin\Entity\GdFlatowner')->ownersNamelist($offset, $maxlimit);
        $viewModel = new ViewModel(array("ownerObj" => $ownerObj, "offset" => $offset, 'endtime' => $endcallTime));
        $viewModel->setTerminal(true);

        return $viewModel;
    }

     /**
     * This function is used for searching resident names
     * 
     * @author          Nishikant
     * @return          mixed $result    
     */
    public function searchnameAction() {
     	$this->layout('layout/playout');
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $offset = $request->getPost('offset');
        $maxlimit = $request->getPost('maxlimit');
        $skey = $request->getPost('skey');
        $id = 1;
        $endcaltime = $em->getRepository('Admin\Entity\GdCallendtime')->findOneById($id);
        $endcallTime = $endcaltime->getCallEndTime();
        $totalsearch = $em->getRepository('Admin\Entity\GdFlatowner')->totalsearchbyname($skey);
        $ownerObj = $em->getRepository('Admin\Entity\GdFlatowner')->searchowner($skey, $offset, $maxlimit);
        $viewModel = new ViewModel(array("ownerObj" => $ownerObj, "offset" => $offset, 'endtime' => $endcallTime, "totalsearch" => $totalsearch));
        $viewModel->setTerminal(true);
        return $viewModel;
    }


      /**
     * This function is used for loading resident unit number
     * 
     * @author          Nishikant
     * @return          mixed $result    
     */
    public function unitnumAction() {
        $callingExtension=@$_GET['callingextension'];
        if(!$callingExtension) {
            // return to 404
            $this->layout('error/404/New');
            $view = new ViewModel();
            return $view;
        }

	    $this->layout('layout/playout');
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $offset = 0;
        $maxlimit = 50;
        $id = 1;
        $endcaltime = $em->getRepository('Admin\Entity\GdCallendtime')->findOneById($id);
        $endcallTime = $endcaltime->getCallEndTime();

        // get PM and PD
        $PM=$this->_getOwnerTypeTable()->getByType(1);
        $PD=$this->_getOwnerTypeTable()->getByType(2);
        // get info
        if($PM)
        $pmInfo=$em->getRepository('Admin\Entity\GdFlatowner')->findOneBy(array('id'=>$PM,'status'=>1));
        if($PD)
        $pdInfo=$em->getRepository('Admin\Entity\GdFlatowner')->findOneBy(array('id'=>$PD,'status'=>1));
    
        $totalOwner = $em->getRepository('Admin\Entity\GdFlatowner')->totalOwnerUnit();
        $unitObj = $em->getRepository('Admin\Entity\GdFlatowner')->jointsetA($offset, $maxlimit);		
        return new ViewModel(array("unitObj" => $unitObj, "offset" => $offset, "maxlimit" => $maxlimit,
            "totalOwner" => $totalOwner, 'endtime' => $endcallTime, "callingExtension"=>$callingExtension, "pmInfo"=>@$pmInfo, "pdInfo"=>@$pdInfo));
    }

   /**
     * This function is used for loading more unit number
     * 
     * @author          Nishikant
     * @return          mixed $result    
     */
    public function moreunitAction() {
    	$this->layout('layout/playout');
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $option = $request->getPost('option');
        $loffset = $request->getPost('loffset');
        $stopback = $request->getPost('stopback');
        $maxlimit = $request->getPost('maxlimit');
        $lmaxlimit = $request->getPost('lmaxlimit');
        $id = 1;
        $endcaltime = $em->getRepository('Admin\Entity\GdCallendtime')->findOneById($id);
        $endcallTime = $endcaltime->getCallEndTime();
        $totalOwner = $em->getRepository('Admin\Entity\GdFlatowner')->totalOwnerUnit();
        if ($option == 'less') {
            $explode = explode(",", $loffset);
            $offset = $explode[count($explode) - 1];
            array_pop($explode);
            $loffset = implode(",", $explode);
            $maxlimit = $lmaxlimit;
            $unitObj = $em->getRepository('Admin\Entity\GdFlatowner')->moreUnitlist($offset, $maxlimit);
            $viewModel = new ViewModel(array("unitObj" => $unitObj, "offset" => $offset, "totalOwner" => $totalOwner, "loffset" => $loffset, 'endtime' => $endcallTime));
            $viewModel->setTerminal(true);
            return $viewModel;
        } else {
            $offset = $request->getPost('offset');
            $unitObj = $em->getRepository('Admin\Entity\GdFlatowner')->moreUnitlist($offset, $maxlimit);
            $viewModel = new ViewModel(array("unitObj" => $unitObj, "offset" => $offset, "totalOwner" => $totalOwner, "stopback" => $stopback, "loffset" => $loffset, 'endtime' => $endcallTime));
            $viewModel->setTerminal(true);
            return $viewModel;
        }
    }
    
    /**
     * This function is used for searching more resident names
     * 
     * @author          Nishikant
     * @return          mixed $result    
     */

    public function smorenameAction() {
    	$this->layout('layout/playout');
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $offset = $request->getPost('offset');
        $maxlimit = $request->getPost('maxlimit');
        $skey = $request->getPost('skey');
        $id = 1;
        $endcaltime = $em->getRepository('Admin\Entity\GdCallendtime')->findOneById($id);
        $endcallTime = $endcaltime->getCallEndTime();
        $ownerObj = $em->getRepository('Admin\Entity\GdFlatowner')->searchowner($skey, $offset, $maxlimit);
        $totalsearch = $em->getRepository('Admin\Entity\GdFlatowner')->totalsearchbyname($skey);
        $viewModel = new ViewModel(array("ownerObj" => $ownerObj, "offset" => $offset, 'endtime' => $endcallTime, "totalsearch" => $totalsearch));
        $viewModel->setTerminal(true);
        return $viewModel;
    }

    protected $_ownerTypeTable;
    public function _getOwnerTypeTable() {
        if(!$this->_ownerTypeTable) {
            $sm=$this->getServiceLocator();
            $this->_ownerTypeTable=$sm->get("Admin\Model\OwnerTypeTable");
        }
        return $this->_ownerTypeTable;
    }
}
