<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use ZendService\Apple\Apns\Client\Message as Client;
use ZendService\Apple\Apns\Message;
use ZendService\Apple\Apns\Message\Alert;
use ZendService\Apple\Apns\Response\Message as Response;

use Admin\Model\Configs;

class IndexController extends AbstractActionController {

    public function setEntityManager(EntityManager $em) {
        $this->em = $em;
    }

    public function getEntityManager() {
        if (null === @$this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    public function getAuthService() {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()->get('AuthService');
        }
        return $this->authservice;
    }

	 /**
     * This function is used for loading resident names
     * 
     * @author          Nishikant
     * @return          mixed $result    
     */
    public function indexAction() {
        $callingExtension=@$_GET['callingextension'];
        if(!$callingExtension) {
            // return to 404
            $this->layout('error/404/New');
            $view = new ViewModel();
            return $view;
        }

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $offset = 0;
        $maxlimit = 50;
        $id = 1;
        $endcaltime = $em->getRepository('Admin\Entity\GdCallendtime')->findOneById($id);
        $endcallTime = $endcaltime->getCallEndTime();

        // get PM and PD
        $PM=$this->_getOwnerTypeTable()->getByType(1);
        $PD=$this->_getOwnerTypeTable()->getByType(2);
        // get info
        if($PM)
        $pmInfo=$em->getRepository('Admin\Entity\GdFlatowner')->findOneBy(array('id'=>$PM,'status'=>1));
        if($PD)
        $pdInfo=$em->getRepository('Admin\Entity\GdFlatowner')->findOneBy(array('id'=>$PD,'status'=>1));

        $notIn=$PM.",".$PD;
        $notIn=trim(trim(trim($notIn), ","));

        $totalOwner = $em->getRepository('Admin\Entity\GdFlatowner')->totalOwner($notIn);
        $ownerObj = $em->getRepository('Admin\Entity\GdFlatowner')->ownersNamelist($offset, $maxlimit, $notIn);

        $access_code_config_data=$this->_getConfigsTable()->getByType(2); // 2=for i have access code button show/hide
        $access_code_config= $access_code_config_data['value'] ? $access_code_config_data['value'] : 1;
        
        return new ViewModel(array(
            "ownerObj" => $ownerObj, "totalOwner" => $totalOwner, "offset" => $offset, "maxlimit" => $maxlimit, 'endtime' => $endcallTime,
            "pmInfo"=>@$pmInfo, "pdInfo"=>@$pdInfo, "callingExtension"=>$callingExtension, "access_code_config" => $access_code_config
        ));
    }

	 /**
     * This function is used for loading less resident names
     * 
     * @author          Nishikant
     * @return          mixed $result    
     */
	
    public function morenameAction() {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $option = $request->getPost('option');
        $offset = $request->getPost('offset');
        $maxlimit = $request->getPost('maxlimit');
        $id = 1;
        $endcaltime = $em->getRepository('Admin\Entity\GdCallendtime')->findOneById($id);
        $endcallTime = $endcaltime->getCallEndTime();
        $totalOwner = $em->getRepository('Admin\Entity\GdFlatowner')->totalOwner();
        if ($option == 'search') {
            $ownerObj = $em->getRepository('Admin\Entity\GdFlatowner')->searchowner($offset, $maxlimit);
        } else {
            $ownerObj = $em->getRepository('Admin\Entity\GdFlatowner')->ownersNamelist($offset, $maxlimit);
        }
        $viewModel = new ViewModel(array("ownerObj" => $ownerObj, "offset" => $offset, "totalOwner" => $totalOwner, 'endtime' => $endcallTime));
        $viewModel->setTerminal(true);
        return $viewModel;
    }

    public function lessnameAction() {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $offset = $request->getPost('offset');
        $maxlimit = $request->getPost('maxlimit');
        $id = 1;
        $endcaltime = $em->getRepository('Admin\Entity\GdCallendtime')->findOneById($id);
        $endcallTime = $endcaltime->getCallEndTime();
        $ownerObj = $em->getRepository('Admin\Entity\GdFlatowner')->ownersNamelist($offset, $maxlimit);
        $viewModel = new ViewModel(array("ownerObj" => $ownerObj, "offset" => $offset, 'endtime' => $endcallTime));
        $viewModel->setTerminal(true);
        return $viewModel;
    }

	
	 /**
     * This function is used for searching resident names
     * 
     * @author          Nishikant
     * @return          mixed $result    
     */
    public function searchnameAction() {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $offset = $request->getPost('offset');
        $maxlimit = $request->getPost('maxlimit');
        $skey = $request->getPost('skey');
        $id = 1;
        $endcaltime = $em->getRepository('Admin\Entity\GdCallendtime')->findOneById($id);
        $endcallTime = $endcaltime->getCallEndTime();
        $totalsearch = $em->getRepository('Admin\Entity\GdFlatowner')->totalsearchbyname($skey, false);
        $ownerObj = $em->getRepository('Admin\Entity\GdFlatowner')->searchowner($skey, $offset, $maxlimit, false);
        $viewModel = new ViewModel(array("ownerObj" => $ownerObj, "offset" => $offset, 'endtime' => $endcallTime, "totalsearch" => $totalsearch));
        $viewModel->setTerminal(true);
        return $viewModel;
    }
	
	 /**
     * This function is used for loading resident unit number
     * 
     * @author          Nishikant
     * @return          mixed $result    
     */

    public function unitnumAction() {
        $callingExtension=@$_GET['callingextension'];
        if(!$callingExtension) {
            // return to 404
            $this->layout('error/404/New');
            $view = new ViewModel();
            return $view;
        }

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $offset = 0;
        $maxlimit = 51;
        $id = 1;
        $endcaltime = $em->getRepository('Admin\Entity\GdCallendtime')->findOneById($id);
        $endcallTime = $endcaltime->getCallEndTime();

        // get PM and PD
        $PM=$this->_getOwnerTypeTable()->getByType(1);
        $PD=$this->_getOwnerTypeTable()->getByType(2);
        // get info
        if($PM)
        $pmInfo=$em->getRepository('Admin\Entity\GdFlatowner')->findOneBy(array('id'=>$PM,'status'=>1));
        if($PD)
        $pdInfo=$em->getRepository('Admin\Entity\GdFlatowner')->findOneBy(array('id'=>$PD,'status'=>1));

        $totalOwner = $em->getRepository('Admin\Entity\GdFlatowner')->totalOwnerUnit();
        $unitObj = $em->getRepository('Admin\Entity\GdFlatowner')->jointsetA($offset, $maxlimit);

        $access_code_config_data=$this->_getConfigsTable()->getByType(2); // 2=for i have access code button show/hide
        $access_code_config= $access_code_config_data['value'] ? $access_code_config_data['value'] : 1;
        
        return new ViewModel(array("unitObj" => $unitObj, "offset" => $offset, "maxlimit" => $maxlimit,
            "totalOwner" => $totalOwner, 'endtime' => $endcallTime, "callingExtension"=>$callingExtension, "pmInfo"=>@$pmInfo, "pdInfo"=>@$pdInfo, "access_code_config" => $access_code_config));
    }
	
	/**
     * This function is used for loading more unit number
     * 
     * @author          Nishikant
     * @return          mixed $result    
     */

    public function moreunitAction() {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $option = $request->getPost('option');
        $loffset = $request->getPost('loffset');
        $stopback = $request->getPost('stopback');
        $maxlimit = $request->getPost('maxlimit');
        $lmaxlimit = $request->getPost('lmaxlimit');
        $id = 1;
        $endcaltime = $em->getRepository('Admin\Entity\GdCallendtime')->findOneById($id);
        $endcallTime = $endcaltime->getCallEndTime();
        $totalOwner = $em->getRepository('Admin\Entity\GdFlatowner')->totalOwnerUnit();
        if ($option == 'less') {
            $explode = explode(",", $loffset);
            $offset = $explode[count($explode) - 1];
            array_pop($explode);
            $loffset = implode(",", $explode);
            $maxlimit = $lmaxlimit;
            $unitObj = $em->getRepository('Admin\Entity\GdFlatowner')->moreUnitlist($offset, $maxlimit);
            $viewModel = new ViewModel(array("unitObj" => $unitObj, "offset" => $offset, "totalOwner" => $totalOwner, "loffset" => $loffset, 'endtime' => $endcallTime));
            $viewModel->setTerminal(true);
            return $viewModel;
        } else {
            $offset = $request->getPost('offset');
            $unitObj = $em->getRepository('Admin\Entity\GdFlatowner')->moreUnitlist($offset, $maxlimit);
            $viewModel = new ViewModel(array("unitObj" => $unitObj, "offset" => $offset, "totalOwner" => $totalOwner, "stopback" => $stopback, "loffset" => $loffset, 'endtime' => $endcallTime));
            $viewModel->setTerminal(true);
            return $viewModel;
        }
    }

	
	 /**
     * This function is used for searching more resident names
     * 
     * @author          Nishikant
     * @return          mixed $result    
     */
	
    public function smorenameAction() {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $offset = $request->getPost('offset');
        $maxlimit = $request->getPost('maxlimit');
        $skey = $request->getPost('skey');
        $id = 1;
        $endcaltime = $em->getRepository('Admin\Entity\GdCallendtime')->findOneById($id);
        $endcallTime = $endcaltime->getCallEndTime();
        $ownerObj = $em->getRepository('Admin\Entity\GdFlatowner')->searchowner($skey, $offset, $maxlimit);
        $totalsearch = $em->getRepository('Admin\Entity\GdFlatowner')->totalsearchbyname($skey);
        $viewModel = new ViewModel(array("ownerObj" => $ownerObj, "offset" => $offset, 'endtime' => $endcallTime, "totalsearch" => $totalsearch));
        $viewModel->setTerminal(true);
        return $viewModel;
    }

    /**
     * This function is used to send push notification
     * 
     * @author          Nishikant
     * @return          mixed $result
     * @created_date    4rth May, 2017    
     */
    public function pushnotificationAction() { 
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $extension = $request->getPost('extension');       
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $token = $em->getRepository('Admin\Entity\GdFlatowner')->iosdevicetoken($extension);
		print_r($token);
        if (!empty($token)) {
            $client = new Client();
			//SANDBOX_URI
            $client->open(Client::PRODUCTION_URI, DOCUMENT_ROOT_PATH . '/MadsViOPpushcert.pem', '');
            $message = new Message();
            $message->setId('767667676');
            $message->setToken($token['deviceToken']);
            $message->setBadge(0);
            $message->setSound('bingbong.aiff');
            $message->setAlert(PUSHMSG);
            $pushitem = 3;
            $x = 1;
            
            while ($x <= $pushitem) {
                 try {
                $response = $client->send($message);               
                } catch (RuntimeException $e) {
                    echo $e->getMessage() . PHP_EOL;
                    exit(1);
                }
                $x++;                
            }
            $client->close();
            $appleresult = $response->getCode();

            if ($response->getCode() != Response::RESULT_OK) {
                switch ($response->getCode()) {
                    case Response::RESULT_PROCESSING_ERROR:
                        $appleresult = 'Error';
                        break;
                    case Response::RESULT_MISSING_TOKEN:
                        $appleresult = 'you were missing a token';
                        break;
                    case Response::RESULT_MISSING_TOPIC:
                        $appleresult = 'you are missing a message id';
                        break;
                    case Response::RESULT_MISSING_PAYLOAD:
                        $appleresult = 'you need to send a payload';
                        break;
                    case Response::RESULT_INVALID_TOKEN_SIZE:
                        $appleresult = 'the token provided was not of the proper size';
                        break;
                    case Response::RESULT_INVALID_TOPIC_SIZE:
                        $appleresult = 'the topic was too long';
                        break;
                    case Response::RESULT_INVALID_PAYLOAD_SIZE:
                        $appleresult = 'the payload was too large';
                        break;
                    case Response::RESULT_INVALID_TOKEN:
                        $appleresult = 'the token was invalid; remove it from your system';
                        break;
                    case Response::RESULT_UNKNOWN_ERROR:
                        $appleresult = "apple didn't tell us what happened";
                        break;
                }
                $result = $appleresult;
                echo 2;
				$this->getServiceLocator()->get('Zend\Log')->info("Push error");
                exit;
            } else {
                $this->getServiceLocator()->get('Zend\Log')->info("Push Success");
				echo 3;
                exit;
            }
       }
	   $this->getServiceLocator()->get('Zend\Log')->info("Device Token Not Found");
        echo "Device Token Not Found";
        exit;
    }
    
    protected $_configsTable;
    public function _getConfigsTable() {
        if(!$this->_configsTable) {
            $sm=$this->getServiceLocator();
            $this->_configsTable=$sm->get("Admin\Model\ConfigsTable");
        }
        return $this->_configsTable;
    }
    
    protected $_ownerTypeTable;
    public function _getOwnerTypeTable() {
        if(!$this->_ownerTypeTable) {
            $sm=$this->getServiceLocator();
            $this->_ownerTypeTable=$sm->get("Admin\Model\OwnerTypeTable");
        }
        return $this->_ownerTypeTable;
    }
}
