<?php
/**
 * User: Abar Choudhary
 * Date: 02/07/2018
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ScreenSaversController extends AbstractActionController {
    public function indexAction() {
        // get files
        // $data=scandir(SCREEN_SAVER_UPLOAD_PATH);
        // $remove=array(".", "..");
        // $data=array_diff($data, $remove); // remove default unwanted directory data
        // $data=array_merge($data); // re-index

        $pattern="landscape_*";
        $data=glob(SCREEN_SAVER_UPLOAD_PATH.$pattern, GLOB_BRACE);
        $data=str_replace(SCREEN_SAVER_UPLOAD_PATH, "", $data);

        // echo "<pre>";
        // print_r($data);
        // die();

        // get slideshow timeout from db
        $playTime=$this->_getOwnerTypeTable()->getTimeOut(3);
        $playTime=$playTime ? ($playTime*1000) : 2000;

        $this->layout('layout/blank');
        return new ViewModel(array(
            'playTime'=>$playTime,
            'totalFiles'=>count($data),
            'records'=>$data
        ));
    }

    public function portraitAction() {
        $pattern="portraits_*";
        $data=glob(SCREEN_SAVER_UPLOAD_PATH.$pattern, GLOB_BRACE);
        $data=str_replace(SCREEN_SAVER_UPLOAD_PATH, "", $data);

        // get slideshow timeout from db
        $playTime=$this->_getOwnerTypeTable()->getTimeOut(3);
        $playTime=$playTime ? ($playTime*1000) : 2000;

        $this->layout('layout/blank');
        return new ViewModel(array(
            'playTime'=>$playTime,
            'totalFiles'=>count($data),
            'records'=>$data
        ));
    }

    protected $_ownerTypeTable;
    public function _getOwnerTypeTable() {
        if(!$this->_ownerTypeTable) {
            $sm=$this->getServiceLocator();
            $this->_ownerTypeTable=$sm->get("Admin\Model\OwnerTypeTable");
        }
        return $this->_ownerTypeTable;
    }
}
