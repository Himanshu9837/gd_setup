<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'unitnum' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/unitnum',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'unitnum',
                    ),
                ),
            ),
             'morename' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/index/morename',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'morename',
                    ),
                ),
            ),
             'lessname' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/index/lessname',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'lessname',
                    ),
                ),
            ),
            'searchname' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/index/searchname',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'searchname',
                    ),
                ),
            ),
	'smorename' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/index/smorename',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'smorename',
                    ),
                ),
            ),
            'moreunit' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/moreunit',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'moreunit',
                    ),
                ),
            ),
            'pushnotification' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/index/pushnotification',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'pushnotification',
                    ),
                ),
            ),
            
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
          /* 'application' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/application',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
               
            ),*/
			// Portrait
            
            'portrait' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/portrait',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Portrait',
                        'action'     => 'index',
                    ),
                ),
            ),           
            'punitnum' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/portrait/unitnum',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Portrait',
                        'action'     => 'unitnum',
                    ),
                ),
            ),
             'pmorename' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/portrait/morename',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Portrait',
                        'action'     => 'morename',
                    ),
                ),
            ),
             'plessname' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/portrait/lessname',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Portrait',
                        'action'     => 'lessname',
                    ),
                ),
            ),
            'psearchname' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/portrait/searchname',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Portrait',
                        'action'     => 'searchname',
                    ),
                ),
            ),
            'psmorename' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/portrait/smorename',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Portrait',
                        'action'     => 'smorename',
                    ),
                ),
            ),
            'pmoreunit' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/portrait/moreunit',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Portrait',
                        'action'     => 'moreunit',
                    ),
                ),
            ),

            'screensavers' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/screensavers',
                    'defaults' => array(
                        'controller' => 'Application\Controller\ScreenSavers',
                        'action'     => 'index',
                    ),
                ),
            ),
			
			'screensaversvert' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/screensaversvert',
                    'defaults' => array(
                        'controller' => 'Application\Controller\ScreenSavers',
                        'action'     => 'portrait',
                    ),
                ),
            ),
	
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'factories' => array(
            'translator' => 'Zend\Mvc\Service\TranslatorServiceFactory',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => Controller\IndexController::class,
			'Application\Controller\Portrait' => 'Application\Controller\PortraitController',
            'Application\Controller\ScreenSavers' => 'Application\Controller\ScreenSaversController'
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
			'error/404/New'           => __DIR__ . '/../view/error/404_new.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
