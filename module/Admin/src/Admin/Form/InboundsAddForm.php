<?php
namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class InboundsAddForm extends Form {
    public function __construct($ringVolumes, $musicOnHolds) {
        parent::__construct("inbounds");

        /*
        $type=new Element\Select("promotion_type");
        $type->setLabel("Promotion Type");
        $type->setAttribute("class","form-control");
        $type->setAttribute("id","promotion_type");
        $type->setValueOptions($promoTypes);
        $this->add($type);*/

        $this->add(array(
            'type' => 'text',
            'name' => 'description',
            'required' => true,
            'options' => array(
                'label' => 'Description'
            ),
            'attributes' => array(
                'id' => 'description',
                'class' => 'form-control'
            )
        ));

        $this->add(array(
            'type' => 'text',
            'name' => 'extension',
            'required' => true,
            'options' => array(
                'label' => 'DID Number'
            ),
            'attributes' => array(
                'id' => 'extension',
                'class' => 'form-control',
                'placeholder'=>'ANY'
            )
        ));

        $this->add(array(
            'type' => 'text',
            'name' => 'cidnum',
            'required' => true,
            'options' => array(
                'label' => 'CID Number'
            ),
            'attributes' => array(
                'id' => 'cidnum',
                'class' => 'form-control',
                'placeholder'=>'ANY'
            )
        ));

        $this->add(array(
            'type' => 'checkbox',
            'name' => 'pricid',
            'required' => true,
            'options' => array(
                'label' => 'CID Priority Route'
            ),
            'attributes' => array(
                'id' => 'pricid',
                'data-toggle' => 'toggle',
                'data-on'=>'Yes',
                'data-off'=>'No',
                'data-onstyle'=>'success',
                'data-offstyle'=>'default',
                'data-size'=>'small',
                'data-width'=>'100',
                'value' => 0
            )
        ));

        $this->add(array(
            'type' => 'text',
            'name' => 'alertinfo',
            'required' => true,
            'options' => array(
                'label' => 'Alert Info'
            ),
            'attributes' => array(
                'id' => 'alertinfo',
                'class' => 'form-control',
                'placeholder'=>'None'
            )
        ));

        $ringerOverride=new Element\Select("rvolume");
        $ringerOverride->setLabel("Ringer Volume Overide");
        $ringerOverride->setAttribute("class","form-control");
        $ringerOverride->setAttribute("id","rvolume");
        $ringerOverride->setValueOptions($ringVolumes);
        $this->add($ringerOverride);

        $this->add(array(
            'type' => 'text',
            'name' => 'grppre',
            'required' => true,
            'options' => array(
                'label' => 'CID Name Prefix'
            ),
            'attributes' => array(
                'id' => 'grppre',
                'class' => 'form-control'
            )
        ));

        $this->add(array(
            'type' => 'text',
            'name' => 'mohclass',
            'required' => true,
            'options' => array(
                'label' => 'Music On Hold'
            ),
            'attributes' => array(
                'id' => 'mohclass',
                'class' => 'form-control'
            )
        ));

        $musicOnHold=new Element\Select("mohclass");
        $musicOnHold->setLabel("Music On Hold");
        $musicOnHold->setAttribute("class","form-control");
        $musicOnHold->setAttribute("id","mohclass");
        $musicOnHold->setValueOptions($musicOnHolds);
        $this->add($musicOnHold);

        $this->add(array(
            'type' => 'text',
            'name' => 'destination',
            'required' => true,
            'options' => array(
                'label' => 'Set Destination'
            ),
            'attributes' => array(
                'id' => 'destination',
                'class' => 'form-control'
            )
        ));

        $this->add(array(
            'type' => 'checkbox',
            'name' => 'privacyman',
            'required' => true,
            'options' => array(
                'label' => 'Privacy Manager'
            ),
            'attributes' => array(
                'id' => 'privacyman',
                'data-toggle' => 'toggle',
                'data-on'=>'Yes',
                'data-off'=>'No',
                'data-onstyle'=>'success',
                'data-offstyle'=>'default',
                'data-size'=>'small',
                'data-width'=>'100',
                'value' => 0
            )
        ));

        $this->add(array(
            'type' => 'text',
            'name' => 'pmmaxretries',
            'required' => true,
            'options' => array(
                'label' => 'Max Attempts'
            ),
            'attributes' => array(
                'id' => 'pmmaxretries',
                'class' => 'form-control',
                'value'=>3,
                'disabled'=>'disabled'
            )
        ));

        $this->add(array(
            'type' => 'text',
            'name' => 'pmminlength',
            'required' => true,
            'options' => array(
                'label' => 'Min Length'
            ),
            'attributes' => array(
                'id' => 'pmminlength',
                'class' => 'form-control',
                'value'=>10,
                'disabled'=>'disabled'
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Add Inbound Route',
                'class' => 'btn btn-primary',
                'id' => 'submitButton'
            )
        ));
    }
}