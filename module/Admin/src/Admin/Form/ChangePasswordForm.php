<?php

namespace Admin\Form;

use Zend\Form\Form;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ChangePasswordForm extends Form implements ObjectManagerAwareInterface {

    protected $em;

   public function __construct(ObjectManager $em, $name = null, $options = array())
	{
		$this->setObjectManager($em);
        parent::__construct('changepassword', $options);
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
                
        $this->add(array(
                'name' => 'oldpass',
                'type' => 'Zend\Form\Element\Password',                    
                'attributes' => array( 
                    'id' => 'oldpass', 
                    'placeholder' => 'Old Password',
                    'class'=>'form-control'
                ),            
        ));

        $this->add(array(
                'name' => 'newpass',
                'type' => 'Zend\Form\Element\Password',                    
                'attributes' => array( 
                    'id' => 'newpass', 
                    'placeholder' => 'New Password',
                    'class'=>'form-control'
                ),            
        ));

         $this->add(array(
                'name' => 'cnfpass',
                'type' => 'Zend\Form\Element\Password',                    
                'attributes' => array( 
                    'id' => 'cnfpass', 
                    'placeholder' => 'Confirm New Password',
                    'class'=>'form-control'
                ),            
        ));
         
        $this->add(array(
            'name' => 'submitbutton',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Save',
                'id' => 'submitbutton',          
                'class' => 'btn btn-info pull-right',
            ),
        ));
    }

    public function getEntityManager() {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    public function setObjectManager(ObjectManager $em) {
        $this->em = $em;

        return $this;
    }

    public function getObjectManager() {
        return $this->em;
    }

}

?>