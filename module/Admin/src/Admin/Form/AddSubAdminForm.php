<?php

namespace Admin\Form;

use Zend\Form\Form;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AddSubAdminForm extends Form implements ObjectManagerAwareInterface {

    protected $em;
    public function __construct(ObjectManager $em, $name = null, $options = array())
	{
		$this->setObjectManager($em);
        parent::__construct('subadmin', $options);
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(array(
            'name' => 'firstname',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'firstname',
                'placeholder' => 'First Name',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'lastname',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'lastname',
                'placeholder' => 'Last Name',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'uname',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'uname',
                'placeholder' => 'Username',
                'class' => 'form-control',
            ),
        ));
       
        
        $this->add(array(
                'name' => 'usertype',
               'type' => 'Zend\Form\Element\Select',                   
                'attributes' => array( 
                    'id' => 'usertype', 
                    'class'=>'form-control',
                ),
				'options' => array( 
                'empty_option'  => '--- Select usertype ---',
                'value_options' => array("2"=>"Subadmin", "3"=>"Vendor"),
                'disable_inarray_validator' => true
            ), 
        ));        
         
        $this->add(array(
                'name' => 'email',
                'type' => 'Zend\Form\Element\Email',                    
                'attributes' => array( 
                    'id' => 'email', 
                    'placeholder' => 'Email',
                    'class'=>'form-control '
                   // 'readonly'=>'readonly'
                )                
        ));

         $this->add(array(
                'name' => 'pwd',
                'type' => 'Zend\Form\Element\Password',                    
                'attributes' => array( 
                    'id' => 'pwd', 
                    'placeholder' => 'Password',
                    'class'=>'form-control',
                    //'readonly'=>'readonly',
                ),            
        ));
                
        $this->add(array(
                'name' => 'mobile',
                'type' => 'Text',                    
                'attributes' => array( 
                    'id' => 'mobile', 
                    'placeholder' => 'Mobile',
                    'class'=>'form-control '
                    //'disabled' => 'disabled'
                )            
        )); 
        
        $this->add(array(
            'name' => 'gender',
            'type' => 'radio',
            'attributes' => array(
                'id' => 'gender',
                'class' => 'gender',
            ),
            'options' => array(
                'value_options' => array(
                    'M' => 'Male',
                    'F' => 'Female',
                ),
            )
        )); 
       
        
        $this->add(array(
            'name' => 'submitbutton',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Save',
                'id' => 'submitbutton',          
                'class' => 'btn btn-info pull-right',
            ),
        ));
    }

    public function getEntityManager() {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    public function setObjectManager(ObjectManager $em) {
        $this->em = $em;

        return $this;
    }

    public function getObjectManager() {
        return $this->em;
    }

}

?>
