<?php

namespace Admin\Form;

use Zend\Form\Form;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AddNewOwnerForm extends Form implements ObjectManagerAwareInterface {

    protected $em;

   public function __construct(ObjectManager $em, $name = null, $options = array())
	{
		   $this->setObjectManager($em);
        parent::__construct('addnewowner', $options);
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(array(
            'name' => 'fname',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'fname',
                'placeholder' => 'Name 1',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'lname',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'lname',
                'placeholder' => 'Name 2',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'unitNum',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'unitNum',
                'placeholder' => 'Unit Number',
                'class' => 'form-control',
            ),
        ));
        
        $this->add(array(
                'name' => 'floor',
               'type' => 'Zend\Form\Element\Select',                   
                'attributes' => array( 
                    'id' => 'floor', 
                    'class'=>'form-control',
                ),
				'options' => array( 
                'empty_option'  => '--- Select Floor ---',
                'value_options' => array("1"=>"1","2"=>"2","3"=>"3","4"=>"4","5"=>"5","6"=>"6",
                    "7"=>'7',"8"=>"8","9"=>"9","10"=>"10","11"=>"11","12"=>"12","13"=>"13","14"=>"14",
                    "15"=>"15","16"=>"16","17"=>"17","18"=>"18","19"=>"19","20"=>"20","21"=>"21",
                    "22"=>"22","23"=>"23","24"=>"24","25"=>"25","26"=>"26","27"=>"27","28"=>"28","29"=>"29","30"=>"30","31"=>"31","32"=>"32","33"=>"33","34"=>"34","35"=>"35","36"=>"36","37"=>"37","38"=>"38",
"39"=>"39","40"=>"40","41"=>"41","42"=>"42","43"=>"43","44"=>"44","45"=>"45","46"=>"46","47"=>"47","48"=>"48","49"=>"49","50"=>"50","51"=>"51","52"=>"52","53"=>"53","54"=>"54","55"=>"55","56"=>"56","57"=>"57","58"=>"58","59"=>"59","60"=>"60","61"=>"61","62"=>"62","63"=>"63","64"=>"64","65"=>"65","66"=>"66","67"=>"67","68"=>"68","69"=>"69","70"=>"70","71"=>"71","72"=>"72","73"=>"73","74"=>"74","75"=>"75","76"=>"76","77"=>"77","78"=>"78","79"=>"79","80"=>"80","81"=>"81","82"=>"82","83"=>"83","84"=>"84","85"=>"85","86"=>"86",
"87"=>"87","88"=>"88","89"=>"89","90"=>"90","91"=>"91","92"=>"92","93"=>"93","94"=>"94","95"=>"95","96"=>"96","97"=>"97","98"=>"98","99"=>"99","100"=>"100"),
                'disable_inarray_validator' => true
            ), 
        ));        
         
        $this->add(array(
                'name' => 'extension',
                'type' => 'Zend\Form\Element\Text',                    
                'attributes' => array( 
                    'id' => 'extension', 
                    'placeholder' => 'Extention',
                    'class'=>'form-control',
                    'readonly'=>'readonly',                 
                ),          
        )); 
        
        $this->add(array(
                'name' => 'followme1',
                'type' => 'Zend\Form\Element\Text',                    
                'attributes' => array( 
                    'id' => 'followme1', 
                    'placeholder' => 'Follow Me 1',
                    'class'=>'form-control',                 
                )            
        )); 
        
        $this->add(array(
                'name' => 'followme2',
                'type' => 'Zend\Form\Element\Text',                    
                'attributes' => array( 
                    'id' => 'followme2', 
                    'placeholder' => 'Follow Me 2',
                    'class'=>'form-control',               
                ),           
        ));
        
        $this->add(array(
                'name' => 'followme3',
                'type' => 'Zend\Form\Element\Text',                    
                'attributes' => array( 
                    'id' => 'followme3', 
                    'placeholder' => 'Follow Me 3',
                    'class'=>'form-control',                 
                ),            
        ));

        $this->add(array(
                'name' => 'followme4',
                'type' => 'Zend\Form\Element\Text',                    
                'attributes' => array( 
                    'id' => 'followme4', 
                    'placeholder' => 'Follow Me 4',
                    'class'=>'form-control',                 
                ),            
        ));

        $this->add(array(
                'name' => 'followme5',
                'type' => 'Zend\Form\Element\Text',                    
                'attributes' => array( 
                    'id' => 'followme5', 
                    'placeholder' => 'Follow Me 5',
                    'class'=>'form-control',                 
                ),            
        ));

        $this->add(array(
            'name' => 'ring_type',
            'type' => 'Zend\Form\Element\Select',
            'required' => false,
            'attributes' => array(
                'id' => 'ring_type',
                'class'=>'form-control',
            ),
            'options' => array(
                'value_options' => array("ringallv2-prim"=>"Collective Ringing", "hunt-prim"=>"Hunt Ringing")
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'confirmcall',
            'attributes' => array( 
                     'id' => 'confirmcall',
                     'class' => 'flat-red',                
                     'style'=>'width:25px; height:25px;',
                     'checked'=>'checked'
                ),
            'options' => array(               
                'use_hidden_element' => true,
                'checked_value' => 'yes',
                'unchecked_value' => 'no'
            ),
             
        ));
       
        $this->add(array(                
                'name' => 'email',
                'type' => 'Zend\Form\Element\Email',   
                'attributes' => array( 
                    'id' => 'email', 
                    'placeholder' => 'Email',
                    'class'=>'form-control',
                ),                
        ));

        $this->add(array(
                'name' => 'voicemail',
                'type' => 'Zend\Form\Element\Text',                    
                'attributes' => array( 
                    'id' => 'voicemail',
                    'class'=>'form-control',
                    'value'=>'no'
                ),            
        ));
       
        /*
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'voicemail',
            'attributes' => array( 
                     'id' => 'voicemail',
                     'class' => 'flat-red',                
                ),
            'options' => array(               
                'use_hidden_element' => true,
                'checked_value' => 'yes',
                'unchecked_value' => 'no',                
            ),
             
        ));*/
         
       /* $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'createsipuser',
             'attributes' => array( 
                     'id' => 'createsipuser',
                     'class' => 'flat-red',                 
                ),
            'options' => array(               
                'use_hidden_element' => true,
                'checked_value' => 'yes',
                'unchecked_value' => 'no',              
            ),
            
        ));*/
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'cameraonly',
            'attributes' => array( 
                     'id' => 'cameraonly',
                     'class' => 'flat-red',                
                ),
            'options' => array(               
                'use_hidden_element' => true,
                'checked_value' => 'yes',
                'unchecked_value' => 'no'                
            ),
             
        ));
        
        
        $this->add(array(
                'name' => 'sipusername',
                'type' => 'Zend\Form\Element\Text',                    
                'attributes' => array( 
                    'id' => 'sipusername', 
                    'placeholder' => 'SIP Username',
                    'class'=>'form-control',
                    'readonly'=>'readonly',
                 
                ),            
        ));
        
        $this->add(array(
                'name' => 'sippassword',
                'type' => 'Zend\Form\Element\Password',                    
                'attributes' => array( 
                    'id' => 'sippassword', 
                    'placeholder' => 'SIP Password',
                    'class'=>'form-control',
                    'readonly'=>'readonly',
                 
                ),            
        ));
        
       /* $this->add(array(
                'name' => 'sipextentionnum',
                'type' => 'Zend\Form\Element\Text',                    
                'attributes' => array( 
                    'id' => 'sipextentionnum', 
                    'placeholder' => 'SIP Extention Number',
                    'class'=>'form-control',
                    'readonly'=>'readonly',
                 
                ),            
        ));*/
         
        $this->add(array(
                'name' => 'sipdownloadcode',
                'type' => 'Zend\Form\Element\Text',                    
                'attributes' => array( 
                    'id' => 'sipdownloadcode', 
                    'placeholder' => 'SIP Download Code',
                    'class'=>'form-control',
                    'readonly'=>'readonly',                 
                ),            
        ));
         
        $this->add(array(
                'name' => 'lockedtodevice',
                'type' => 'Zend\Form\Element\Text',                    
                'attributes' => array( 
                    'id' => 'lockedtodevice', 
                    'placeholder' => 'Locked to decide',
                    'class'=>'form-control',
                    'readonly'=>'readonly',                 
                ),            
        ));
         
        
        $this->add(array(
            'name' => 'submitbutton',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Save',
                'id' => 'submitbutton',          
                'class' => 'btn btn-info pull-right',
            ),
        ));
    }

    public function getEntityManager() {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    public function setObjectManager(ObjectManager $em) {
        $this->em = $em;

        return $this;
    }

    public function getObjectManager() {
        return $this->em;
    }

}

?>