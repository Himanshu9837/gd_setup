<?php

namespace Admin\Form;

use Zend\Form\Form;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use Doctrine\Common\Persistence\ObjectManager;

class UserlimitForm extends Form implements ObjectManagerAwareInterface {

    protected $em;

    public function __construct(ObjectManager $em, $name = null, $options = array()) {
        $this->setObjectManager($em, $id);
        parent::__construct('userlimit', $options);
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));


        $this->add(array(
            'name' => 'userlimit',
            'type' => 'Zend\Form\Element\Number',
            'attributes' => array(
                'id' => 'userlimit',
                'placeholder' => 'Maximum user can be added',
                'class' => 'form-control',
            ),
        ));
       
        $this->add(array(
            'name' => 'submitbutton',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Save',
                'id' => 'submitbutton',
                'class' => 'btn btn-info pull-right',
            ),
        ));
    }

    public function getEntityManager() {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    public function setObjectManager(ObjectManager $em) {
        $this->em = $em;

        return $this;
    }

    public function getObjectManager() {
        return $this->em;
    }

}

?>
