<?php

namespace Admin\Form;
use Zend\Form\Form;


class AdminProfileForm extends Form {
   
    
     public function __construct($name = null)	
	{	
 
        parent::__construct('adminprofile');
        $this->setAttribute('method', 'post');  
        $this->setAttribute('enctype','multipart/form-data');
        $this->setAttribute('class','form-horizontal');
        
        /* $this->add(array(
                    'name' => 'profileid',
                    'type' => 'Hidden',                    
            ));*/
        
        $this->add(array(
                'name' => 'firstname',
                'type' => 'Text',                    
                'attributes' => array( 
                    'id' => 'firsttitle', 
                    'placeholder' => 'First Name',
                    'class'=>'form-control',
                    'disabled' => 'disabled'
                )                
        )); 
        $this->add(array(
                'name' => 'lastname',
                'type' => 'Text',                    
                'attributes' => array( 
                    'id' => 'lasttitle', 
                    'placeholder' => 'Last Name',
                    'class'=>'form-control ',
                    'disabled' => 'disabled'
                )                
        )); 
		$this->add(array(
                'name' => 'email',
                'type' => 'email',                    
                'attributes' => array( 
                    'id' => 'email', 
                    'placeholder' => 'Email',
                    'class'=>'form-control ',
                    'readonly'=>'readonly'
                )                
        ));
                
        $this->add(array(
                'name' => 'mobile',
                'type' => 'Text',                    
                'attributes' => array( 
                    'id' => 'mobile', 
                    'placeholder' => 'Mobile',
                    'class'=>'form-control ',
                    'disabled' => 'disabled'
                )            
        )); 
        
        $this->add(array(
            'name' => 'gender',
            'type' => 'radio',
            'attributes' => array(
                'id' => 'gender',
                'class' => 'gender',
            ),
            'options' => array(
                'value_options' => array(
                    'M' => 'Male',
                    'F' => 'Female',
                ),
            )
        )); 
		/*$this->add(array(
                'name' => 'agerange',
               'type' => 'Zend\Form\Element\Select',                   
                'attributes' => array( 
                    'id' => 'agerange', 
                    //'placeholder' => 'Last Name',
                    'class'=>'form-control',
                ),
				'options' => array( 
                'empty_option'  => '--- Select Range ---',
                'value_options' => array("0-15"=>"0-15","16-19"=>"16-19","20-29"=>"20-29","30-39"=>"30-39","40-49"=>"40-49","50-59"=>"50-59","60-69"=>'60-69',"70+"=>"70+"),
                'disable_inarray_validator' => true
            ), 
        ));
		$this->add(array(
                'name' => 'city',
                'type' => 'Text',                    
                'attributes' => array( 
                    'id' => 'city',                                         
                    'class'=>'form-control',
                )              
        )); 
		$this->add(array(
                'name' => 'street',
                'type' => 'Text',                    
                'attributes' => array( 
                    'id' => 'street',                                         
                    'class'=>'form-control',
                )              
        )); 
        $this->add(array(
                'name' => 'country',
                'type' => 'Zend\Form\Element\Select',                    
                'attributes' => array( 
                    'id' => 'country',                                         
                    'class'=>'form-control',
                    'onChange'=>'getStates(this.value)'
                ),
                'options' => array(
                    'value_options' => $this->getCountryList(),
                    'empty_option'  => '--- Select Country ---',
                    'disable_inarray_validator' => true
                )
            
            
        )); 
        $this->add(array( 
            'name' => 'state', 
            'type' => 'Zend\Form\Element\Select', 
            'attributes' => array( 
                'id' => 'state', 
				'class'=>'form-control',
            ), 
            'options' => array(
				'value_options' => $this->admingetstateList($country),
                'empty_option'  => '--- Select State ---',
                'disable_inarray_validator' => true
            ), 
         
        ));*/
        $this->add(array(
                'name' => 'submitbutton',
                'type' => 'Submit',                
                'attributes' => array(
                    'value' => 'Save',
                    'id' => 'submitbutton',
                    'class' => 'btn btn-info pull-right'
             ),
        ));
    }
    
    
}

?>
