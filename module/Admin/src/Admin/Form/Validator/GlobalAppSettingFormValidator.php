<?php

namespace Admin\Form\Validator;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator as Validator;
use Zend\Validator\File\Extension;

class GlobalAppSettingFormValidator implements InputFilterAwareInterface {

    protected $inputFilter;

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput([
                        'name' => 'cmiphost',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            ]));

            $inputFilter->add($factory->createInput([
                        'name' => 'cmport',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            ]));

            $inputFilter->add($factory->createInput([
                        'name' => 'cmname',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            ]));

            $inputFilter->add($factory->createInput([
                        'name' => 'cmchannel',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            ]));

            $inputFilter->add($factory->createInput([
                        'name' => 'cmunmae',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            ]));

            $inputFilter->add($factory->createInput([
                        'name' => 'cmpwd',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            ]));

            $inputFilter->add($factory->createInput([
                        'name' => 'sipaddr',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            ]));

            $inputFilter->add($factory->createInput([
                        'name' => 'sipport',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            ]));

            $inputFilter->add($factory->createInput([
                        'name' => 'siptransport',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            ]));

            $inputFilter->add($factory->createInput([
                        'name' => 'sipser1addr',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            ]));

            $inputFilter->add($factory->createInput([
                        'name' => 'sipser1port',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            ]));

            $inputFilter->add($factory->createInput([
                        'name' => 'sipser1transport',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            ]));

            $inputFilter->add($factory->createInput([
                        'name' => 'sipser1expires',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            ]));

            $inputFilter->add($factory->createInput([
                        'name' => 'sipser1register',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            ]));

            $inputFilter->add($factory->createInput([
                        'name' => 'sipser1retrytmout',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            ]));

            $inputFilter->add($factory->createInput([
                        'name' => 'sipser1retrymaxct',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            ]));

            $inputFilter->add($factory->createInput([
                        'name' => 'sipser1lnsztmout',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            ]));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}
?>