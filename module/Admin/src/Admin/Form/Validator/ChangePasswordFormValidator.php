<?php
namespace Admin\Form\Validator;

use Zend\InputFilter\Factory as InputFactory; 
use Zend\InputFilter\InputFilter; 
use Zend\InputFilter\InputFilterAwareInterface; 
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator as Validator;
use Zend\Validator\File\Extension;

class ChangePasswordFormValidator implements InputFilterAwareInterface 
{ 
    protected $inputFilter; 
    
     public function setInputFilter(InputFilterInterface $inputFilter) 
    { 
        throw new \Exception("Not used"); 
    }

     public function getInputFilter() 
    {
        if (!$this->inputFilter) {

        	$inputFilter = new InputFilter(); 
            $factory = new InputFactory();
            
            $inputFilter->add($factory->createInput([ 
                'name' => 'oldpass', 
                'required' => true
            ]));
       
       		$inputFilter->add($factory->createInput([ 
                'name' => 'newpass', 
                'required' => true
            ]));

            $inputFilter->add($factory->createInput([ 
                'name' => 'cnfpass', 
                'required' => true,
                'validators' => array(
				        array(
				            'name' => 'Identical',
				            'options' => array(
				                'token' => 'newpass',
				            ),
				        ),
				    ),
            ]));
      		$this->inputFilter = $inputFilter;            
        }        
        return $this->inputFilter;
   }

}