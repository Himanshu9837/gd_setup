<?php

namespace Admin\Form;

use Zend\Form\Form;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use Doctrine\Common\Persistence\ObjectManager;

class GlobalAppSettingForm extends Form implements ObjectManagerAwareInterface {

    protected $em;

    public function __construct(ObjectManager $em, $name = null, $options = array()) {
        $this->setObjectManager($em, $id);
        parent::__construct('appsetting', $options);
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));


        /* $this->add(array(
          'name' => 'sitename',
          'type' => 'Zend\Form\Element\Text',
          'attributes' => array(
          'id' => 'sitename',
          'placeholder' => 'Site Name',
          'class' => 'form-control',
          ),
          )); */
        $this->add(array(
            'name' => 'cmiphost',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'cmiphost',
                'placeholder' => 'IP Host',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'cmport',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'cmport',
                'placeholder' => 'Port',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'cmname',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'cmname',
                'placeholder' => 'Name',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'cmchannel',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'cmchannel',
                'placeholder' => 'Channel',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'cmunmae',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'cmunmae',
                'placeholder' => 'Username',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'cmpwd',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'cmpwd',
                'placeholder' => 'Password',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'sipaddr',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'sipaddr',
                'placeholder' => 'Address',
                'class' => 'form-control'
            ),
        ));
        $this->add(array(
            'name' => 'sipport',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'sipport',
                'placeholder' => 'Port',
                'class' => 'form-control'
            ),
        ));
        $this->add(array(
            'name' => 'siptransport',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'siptransport',
                'class' => 'form-control',
            ),
            'options' => array(
                //'empty_option'  => '--- Select Floor ---',
                'value_options' => array("1" => "UDP only", "2" => "UDP only", "3" => "UDP only", "4" => "UDP only"),
                'disable_inarray_validator' => true
            ),
        ));

        $this->add(array(
            'name' => 'sipser1addr',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'sipser1addr',
                'placeholder' => 'Address',
                'class' => 'form-control'
            ),
        ));

        $this->add(array(
            'name' => 'sipser1port',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'sipser1port',
                'placeholder' => 'Port',
                'class' => 'form-control'
            ),
        ));
        $this->add(array(
            'name' => 'sipser1transport',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'sipser1transport',
                'class' => 'form-control',
            ),
            'options' => array(
                //'empty_option'  => '--- Select Floor ---',
                'value_options' => array("1" => "UDP only", "2" => "UDP only", "3" => "UDP only", "4" => "UDP only"),
                'disable_inarray_validator' => true
            ),
        ));

        $this->add(array(
            'name' => 'sipser1expires',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'sipser1expires',
                'placeholder' => 'Expires',
                'class' => 'form-control'
            ),
        ));

        $this->add(array(
            'name' => 'sipser1register',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'sipser1register',
                'placeholder' => '1',
                'class' => 'form-control'
            ),
        ));

        $this->add(array(
            'name' => 'sipser1retrytmout',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'sipser1retrytmout',
                'placeholder' => '0',
                'class' => 'form-control'
            ),
        ));

        $this->add(array(
            'name' => 'sipser1retrymaxct',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'sipser1retrymaxct',
                'placeholder' => '0',
                'class' => 'form-control'
            ),
        ));
		
        $this->add(array(
            'name' => 'sipser1lnsztmout',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'sipser1lnsztmout',
                'placeholder' => '30',
                'class' => 'form-control'
            ),
        ));
		
		$this->add(array(
            'name' => 'configurl',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'configurl',
                'placeholder' => 'Update Config URL',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'streaming',
            'type' => 'radio',
            'attributes' => array(
                'id' => 'streaming',
                'class' => 'streaming',
                'value' => 'NORMAL'
            ),
            'options' => array(
                'value_options' => array(
                    'NORMAL' => 'NORMAL',
                    'RTSP' => 'RTSP',
                ),
            )
        ));

        $this->add(array(
            'name' => 'submitbutton',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Save',
                'id' => 'submitbutton',
                'class' => 'btn btn-info pull-right',
            ),
        ));
    }

    public function getEntityManager() {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    public function setObjectManager(ObjectManager $em) {
        $this->em = $em;

        return $this;
    }

    public function getObjectManager() {
        return $this->em;
    }

}

?>
