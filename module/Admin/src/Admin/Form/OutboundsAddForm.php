<?php
namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class OutboundsAddForm extends Form {
    public function __construct($musicOnHolds) {
        parent::__construct("outbounds");

        /*
        $type=new Element\Select("promotion_type");
        $type->setLabel("Promotion Type");
        $type->setAttribute("class","form-control");
        $type->setAttribute("id","promotion_type");
        $type->setValueOptions($promoTypes);
        $this->add($type);*/

        $this->add(array(
            'type' => 'text',
            'name' => 'route_id',
            'required' => true,
            'options' => array(
                'label' => 'Route ID'
            ),
            'attributes' => array(
                'id' => 'route_id',
                'class' => 'form-control'
            )
        ));

        $this->add(array(
            'type' => 'text',
            'name' => 'routename',
            'required' => true,
            'options' => array(
                'label' => 'Route Name'
            ),
            'attributes' => array(
                'id' => 'routename',
                'class' => 'form-control'
            )
        ));

        $this->add(array(
            'type' => 'text',
            'name' => 'outcid',
            'required' => false,
            'options' => array(
                'label' => 'Route CID'
            ),
            'attributes' => array(
                'id' => 'outcid',
                'class' => 'form-control',
                'placeholder'=>''
            )
        ));

        $this->add(array(
            'type' => 'checkbox',
            'name' => 'outcid_mode',
            'required' => true,
            'options' => array(
                'label' => 'Override Extension'
            ),
            'attributes' => array(
                'id' => 'outcid_mode',
                'data-toggle' => 'toggle',
                'data-on'=>'Yes',
                'data-off'=>'No',
                'data-onstyle'=>'success',
                'data-offstyle'=>'default',
                'data-size'=>'small',
                'data-width'=>'100',
                'value' => 0
            )
        ));

        $this->add(array(
            'type' => 'password',
            'name' => 'routepass',
            'required' => true,
            'options' => array(
                'label' => 'Route Password'
            ),
            'attributes' => array(
                'id' => 'routepass',
                'class' => 'form-control',
                'placeholder'=>''
            )
        ));

        $musicOnHold=new Element\Select("mohsilence");
        $musicOnHold->setLabel("Music On Hold");
        $musicOnHold->setAttribute("class","form-control");
        $musicOnHold->setAttribute("id","mohsilence");
        $musicOnHold->setValueOptions($musicOnHolds);
        $this->add($musicOnHold);

        $this->add(array(
            'type' => 'text',
            'name' => 'route_seq',
            'required' => true,
            'options' => array(
                'label' => 'Route Position'
            ),
            'attributes' => array(
                'id' => 'route_seq',
                'class' => 'form-control'
            )
        ));

        $this->add(array(
            'type' => 'text',
            'name' => 'trunkpriority',
            'required' => true,
            'options' => array(
                'label' => 'Trunk Sequence'
            ),
            'attributes' => array(
                'id' => 'trunkpriority',
                'class' => 'form-control'
            )
        ));

        

        $this->add(array(
            'type' => 'text',
            'name' => 'destination',
            'required' => true,
            'options' => array(
                'label' => 'Optional Destination'
            ),
            'attributes' => array(
                'id' => 'destination',
                'class' => 'form-control'
            )
        ));

        

        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Add Outbound Route',
                'class' => 'btn btn-primary',
                'id' => 'submitButton'
            )
        ));
    }
}