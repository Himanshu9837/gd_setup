<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Admin\Form as AdminForms;
use Zend\Mvc\MvcEvent;
use Admin\Entity as Entities;
use Zend\View\Model\JsonModel;
use Zend\Crypt\Password\Bcrypt;

use Admin\Model\OwnerType;

class OwnerController extends AbstractActionController {
    /* This function is like old init() */
    public $maxSubOwners=4;

    public function onDispatch(MvcEvent $e) {
        $admin_session = new Container('admin');
        $username = $admin_session->username;
        if (empty($username)) {
            /* if not logged in redirect the user to login page */
            return $this->redirect()->toRoute('adminlogin');
        }
        $this->layout('layout/adminlayout');
        return parent::onDispatch($e);
    }

    public function setEntityManager(EntityManager $em) {
        $this->em = $em;
    }

    public function getEntityManager() {
        if (null === @$this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    /**
     * Used to add flat owner from admin   
     * @author		Nishikant
     * @created_date	14th Nov, 2016
     * @modified_date	10th Jan, 2017
     */
    public function addnewownerAction() {
        $em = $this->getEntityManager();
       
        $admin_session = new Container('admin');
        $username = $admin_session->username;
        $id = $admin_session->userId;
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        
        $this->layout()->pageTitle = 'My Profile';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;


        $nameType=@trim($_GET['t']);
        $parentId=0;
        if($nameType=="pm") $ownerType=1;
        elseif($nameType=="pd") $ownerType=2;
        elseif($nameType=="sub") {
            $ownerType=3;
            $parentId=@trim($_GET['u']);
            if(!$parentId) $this->redirect()->toUrl("/admin/dashboard");

            // get parent details
            $ownerObj = $em->getRepository('Admin\Entity\GdFlatowner')->findOneBy(array('id' => $parentId));
            if(!$ownerObj) return $this->redirect()->toUrl("/admin/dashboard");
            
            $owner=$ownerObj->getArrayCopy();
            if($owner['parentId']) return $this->redirect()->toUrl("/admin/dashboard"); // no child owner access

            // check max sub users
            $subOwners=$em->getRepository('Admin\Entity\GdFlatowner')->getOwnerChild($parentId);
            if(count($subOwners)>=$this->maxSubOwners) {
                return $this->redirect()->toUrl("/admin/owner/subowners/$parentId");
            }

        }
        else $ownerType=false;
        
        
        $msg = "";
        $form = new AdminForms\AddNewOwnerForm($em);
        $form->get('submitbutton')->setValue('Save');
        $request = $this->getRequest();
        $userObj = new Entities\GdFlatowner();
        if ($request->isPost()) {
            $formValidator = new AdminForms\Validator\AddNewOwnerFormValidator();
            $form->setInputFilter($formValidator->getInputFilter());
            $formdata = $request->getPost();

            $form->setData($request->getPost());

            $confirmCall=@$formdata['confirmcall'];
            $confirmCall=($confirmCall=="yes") ? "CHECKED" : "";

            $followme="";
            $followMe1=@trim($formdata['followme1']);
            $followMe2=@trim($formdata['followme2']);
            $followMe3=@trim($formdata['followme3']);
            $followMe4=@trim($formdata['followme4']);
            $followMe5=@trim($formdata['followme5']);

            if($followMe1) $followme=$followme ? $followme.",".$followMe1 : $followMe1;
            if($followMe2) $followme=$followme ? $followme.",".$followMe2 : $followMe2;
            if($followMe3) $followme=$followme ? $followme.",".$followMe3 : $followMe3;
            if($followMe4) $followme=$followme ? $followme.",".$followMe4 : $followMe4;
            if($followMe5) $followme=$followme ? $followme.",".$followMe5 : $followMe5;

            // Asterik API
            // if ((isset($formdata['followme1'])) && (isset($formdata['followme2'])) && (isset($formdata['followme3']))) {
            //     $followme = $formdata['followme1'] . ',' . $formdata['followme2'] . ',' . $formdata['followme3'];
            // } else if ((isset($formdata['followme1'])) && (isset($formdata['followme2']))) {
            //     $followme = $formdata['followme1'] . ',' . $formdata['followme2'];
            // } elseif (isset($formdata['followme3'])) {
            //     $followme = $formdata['followme3'];
            // }

            switch (ASTERIKAPI) {
                case 'Active':
                    $addextapi = $this->addextensionapi($formdata['fname'], $formdata['lname'], $formdata['extension'], $formdata['sippassword'], $followme, $formdata['email'], $formdata['voicemail'], $formdata['ring_type'], $confirmCall);

                    if (($form->isValid()) && ($addextapi === 1)) {
                        $encriptedPass = $this->passEncryption($formdata['sippassword']);
                        $voicemail = $formdata['voicemail'];
                            if (!$voicemail || $voicemail == "no") {
                                $voiceval = "novm";
                            } else if ($voicemail == "yes") {
                                $voiceval = "default";
                            }
                        $currentDate = date('Y-m-d H:i:s');
                        $userObj->setParentId($parentId);
                        $userObj->setFirstName($formdata['fname']);
                        $userObj->setLastName($formdata['lname']);
                        $userObj->setUnitNumber($formdata['unitNum']);
                        $userObj->setFloor($formdata['floor']);
                        $userObj->setMobile($formdata['extension']);
                        $userObj->setEmail($formdata['email']);
                        $userObj->setFollowme1($formdata['followme1']);
						if($formdata['followme2']!='')
							$userObj->setFollowme2($formdata['followme2']);
						if($formdata['followme3']!='')
							$userObj->setFollowme3($formdata['followme3']);
                        if($followMe4!='') $userObj->setFollowme4($followMe4);
                        if($followMe5!='') $userObj->setFollowme5($followMe5);
                        $userObj->setRingType($formdata['ring_type']);
                        $userObj->setConfirmcall($formdata['confirmcall']);
                        $userObj->setVoiceMail($formdata['voicemail']);
                        $userObj->setCameraOnly($formdata['cameraonly']);
						$userObj->setSipExtentionNum($formdata['extension']);
                        $userObj->setSipUsername($formdata['sipusername']);
                        $userObj->setSipPassword($encriptedPass);
                        $userObj->setSipDownloadCode($formdata['sipdownloadcode']);
                        $userObj->setLockedToDevice($formdata['lockedtodevice']);
                        $userObj->setCreatedDate($currentDate);
                        $em->persist($userObj);
                        $em->flush();

                        $lastUserCreatedId = $userObj->getId();
                        if($lastUserCreatedId && $ownerType) {
                            // insert in owner_type table
                            $typeData=array(
                                "owner_type"=>$ownerType, "owner_id"=>$lastUserCreatedId
                            );
                            $entity=new OwnerType();
                            $entity->exchangeArray($typeData);
                            $this->_getOwnerTypeTable()->insertOrUpdate($entity);
                        }

                        // Creating OTP of users added by Nishikant 29th Dec,2016
                        $randomOtp = mt_rand(100000, 999999);
                        $encOtp = md5($formdata['sipdownloadcode']);
                        $otpObj = new Entities\GdOtp();
                        $otpObj->setUserId($lastUserCreatedId);
                        $otpObj->setOtp($formdata['sipdownloadcode']);
                        $otpObj->setEncOtp($encOtp);
                        $otpObj->setCreatedDate($currentDate);
                        $em->persist($otpObj);
                        $em->flush();
                        $flashMessenger = $this->flashMessenger();
                        $flashMessenger->setNamespace('success');
                        $msg = "User has been added successfully";
                        $flashMessenger->addMessage($msg);

                        if($ownerType==3) {
                            return $this->redirect()->toUrl("/admin/owner/subowners/$parentId");
                        }

                        return $this->redirect()->toRoute('admindashboard');
                    } elseif($addextapi!=1) {
                        $error = "Extension already exists!";
                        return array('form' => $form, 'error' => $error);
                    } else {
                        $allErrors="";
                        $errors = $form->getMessages();
                        foreach($errors as $k=>$thisError) {
                            foreach($thisError as $errorKey=>$errorValue) {
                                $k=ucfirst($k);
                                $allErrors.=" $k: $errorValue,";
                            }
                        }
                        $allErrors=$allErrors ? trim(trim($allErrors),",") : "Something Went wrong! Please Try again.";
                        return array('form' => $form, 'error' => $allErrors);
                    }

                    break;
               case 'Deactive':
                    if ($form->isValid()) {
                        $encriptedPass = $this->passEncryption($formdata['sippassword']);
                        $voicemail = $formdata['voicemail'];
                        if ($voicemail == "no") {
                            $voiceval = "novm";
                        } else if ($voicemail == "yes") {
                            $voiceval = "default";
                        }
                        $currentDate = date('Y-m-d H:i:s');
                        $userObj->setParentId($parentId);
                        $userObj->setFirstName($formdata['fname']);
                        $userObj->setLastName($formdata['lname']);
                        $userObj->setUnitNumber($formdata['unitNum']);
                        $userObj->setFloor($formdata['floor']);
                        $userObj->setMobile($formdata['extension']);
                        $userObj->setEmail($formdata['email']);
                        $userObj->setFollowme1($formdata['followme1']);
						if($formdata['followme2']!='')
							$userObj->setFollowme2($formdata['followme2']);
						if($formdata['followme3']!='')
							$userObj->setFollowme3($formdata['followme3']);
                        if($followMe4!='') $userObj->setFollowme4($followMe4);
                        if($followMe5!='') $userObj->setFollowme5($followMe5);
                        $userObj->setRingType($formdata['ring_type']);
                        $userObj->setConfirmcall($formdata['confirmcall']);
                        $userObj->setVoiceMail($formdata['voicemail']);
                        $userObj->setCameraOnly($formdata['cameraonly']);
						$userObj->setSipExtentionNum($formdata['extension']);
                        $userObj->setSipUsername($formdata['sipusername']);
                        $userObj->setSipPassword($encriptedPass);
                        $userObj->setSipDownloadCode($formdata['sipdownloadcode']);
                        $userObj->setLockedToDevice($formdata['lockedtodevice']);
                        $userObj->setCreatedDate($currentDate);
                        $userObj->setModifiedDate($currentDate);
                        $em->persist($userObj);
                        $em->flush();

                        $lastUserCreatedId = $userObj->getId();
                        if($lastUserCreatedId && $ownerType) {
                            // insert in owner_type table
                            $typeData=array(
                                "owner_type"=>$ownerType, "owner_id"=>$lastUserCreatedId
                            );
                            $entity=new OwnerType();
                            $entity->exchangeArray($typeData);
                            $this->_getOwnerTypeTable()->insertOrUpdate($entity);
                        }

                        // Creating OTP of users added by Nishikant 29th Dec,2016
                        $randomOtp = mt_rand(100000, 999999);
                        $encOtp = md5($formdata['sipdownloadcode']);
                        $otpObj = new Entities\GdOtp();
                        $otpObj->setUserId($lastUserCreatedId);
                        $otpObj->setOtp($formdata['sipdownloadcode']);
                        $otpObj->setEncOtp($encOtp);
                        $otpObj->setCreatedDate($currentDate);
                        $otpObj->setModifiedDate($currentDate);
                        $em->persist($otpObj);
                        $em->flush();
                        $flashMessenger = $this->flashMessenger();
                        $flashMessenger->setNamespace('success');
                        $msg = "User has been added successfully";
                        $flashMessenger->addMessage($msg);

                        if($ownerType==3) {
                            return $this->redirect()->toUrl("/admin/owner/subowners/$parentId");
                        }

                        return $this->redirect()->toRoute('admindashboard');
                    } else {
                        $allErrors="";
                        $errors = $form->getMessages();
                        foreach($errors as $k=>$thisError) {
                            foreach($thisError as $errorKey=>$errorValue) {
                                $k=ucfirst($k);
                                $allErrors.=" $k: $errorValue,";
                            }
                        }
                        $allErrors=$allErrors ? trim(trim($allErrors),",") : "Something Went wrong! Please Try again.";
                        return array('form' => $form, 'error' => $allErrors);
                    }
                    break;
            }
        }
        return array('form' => $form, 'success' => $msg);
    }

    public function subOwnersAction() {
        $ownerId = $this->params('id');
        if(!$ownerId) return $this->redirect()->toUrl("/admin/dashboard");
        
        $em = $this->getEntityManager();
       
        $admin_session = new Container('admin');
        $username = $admin_session->username;
        $adminId = $admin_session->userId;
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        
        $this->layout()->pageTitle = 'Child Users';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        // get owner details
        $userObj = $em->getRepository('Admin\Entity\GdFlatowner')->findOneBy(array('id' => $ownerId));
        if(!$userObj) return $this->redirect()->toUrl("/admin/dashboard");

        $owner=$userObj->getArrayCopy();
        if($owner['parentId']) return $this->redirect()->toUrl("/admin/dashboard"); // no child owner access

        // get current child owners
        $subOwners=$em->getRepository('Admin\Entity\GdFlatowner')->getOwnerChild($ownerId);
        if($subOwners) {
            foreach($subOwners as $k=>$thisOwner) {
                $subOwners[$k]=$thisOwner->getArrayCopy();
            }
        }

        return new ViewModel(array(
            'maxSubOwners'=>$this->maxSubOwners,
            'owner'=>$owner,
            "subOwners"=>$subOwners
        ));
    }

    public function blindUsersAction() {
        $em = $this->getEntityManager();
       
        $admin_session = new Container('admin');
        $username = $admin_session->username;
        $adminId = $admin_session->userId;
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        
        $this->layout()->pageTitle = 'Blind Users';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        // get current child owners
        //$users=$em->getRepository('Admin\Entity\GdFlatowner')->getblindUsers();
        $users = $this->_getFlatOwnerTable()->getBlindUser();

        return new ViewModel(array(
            "users"=>$users
        ));
    }

    /**
     * Used to generate random username  
     * @author		Nishikant
     * @created_date	9th Jan, 2016
     * @modified_date	-------------
     */
    function randomUsername($string) {
        $pattern = " ";
        $firstPart = strstr(strtolower($string), $pattern, true);
        $secondPart = substr(strstr(strtolower($string), $pattern, false), 0, 3);
        $nrRand = rand(0, 100);
        $username = trim($firstPart) . trim($secondPart) . trim($nrRand);
        return $username;
    }

    /**
     * Used for password encryption 
     * @author		    Nishikant
     * @created_date	13th Jan, 2016
     * @modified_date	-------------
     */
    public function passEncryption($pass) {
        $iv = mcrypt_create_iv(
                mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM
        );

        $key = ENCRYPTION_KEY;
        $encryptedpass = base64_encode(
                $iv .
                mcrypt_encrypt(
                        MCRYPT_RIJNDAEL_128, hash('sha256', $key, true), $pass, MCRYPT_MODE_CBC, $iv
                )
        );
        return $encryptedpass;
    }

    /**
     * Used for password decryption
     * @author		Nishikant
     * @created_date	13th Jan, 2016
     * @modified_date	-------------
     */
    public function passDecryption($encrypted) {
        $enpass = base64_decode($encrypted);
        $dkey = ENCRYPTION_KEY;
        $iv1 = substr($enpass, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));
        $decrypted = rtrim(
                mcrypt_decrypt(
                        MCRYPT_RIJNDAEL_128, hash('sha256', $dkey, true), substr($enpass, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)), MCRYPT_MODE_CBC, $iv1
                ), "\0"
        );
        return $decrypted;
    }

    /**
     * Used to generate random password
     * @author		Nishikant
     * @created_date	9th Jan, 2016
     * @modified_date	-------------
     */
    function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'ludms') {
        $sets = array();
        if (strpos($available_sets, 'l') !== false) {
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        }
        if (strpos($available_sets, 'u') !== false) {
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        }
        if (strpos($available_sets, 'd') !== false) {
            $sets[] = '23456789';
        }

        if (strpos($available_sets, 'm') !== false) {
            $sets[] = '98765432';
        }
        if (strpos($available_sets, 's') !== false) {
            $sets[] = '!@#$%&*';
        }
        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];
        $password = str_shuffle($password);
        if (!$add_dashes)
            return $password;
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }

    /**
     * this function is used for generating password
     * @author		Nishikant
     * @created_date	10th Mar, 2017
     * @modified_date	-------------
     */
    public function createSipUserAction() {
        $request = $this->getRequest();
        $postData = $request->getPost();
        $sipuserpass = $this->generateStrongPassword();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $result = array('status' => success,
            'password' => $sipuserpass);
        return new JsonModel($result);
    }

    /**
     * Used to generate OTP 
     * @author		Nishikant
     * @created_date	9th Jan, 2016
     * @modified_date	-------------
     */
    public function generateOtpAction() {
        $randomOtp = mt_rand(100000, 999999);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $otp = array('status' => 'success', 'OTP' => $randomOtp);
        return new JsonModel($otp);
        //exit;
    }

    /**
     * Used to generate OTP 
     * @author		Nishikant
     * @created_date	9th Jan, 2016
     * @modified_date	-------------
     */
    public function reGenerateOtpAction() {
        $randomOtp = mt_rand(100000, 999999);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $newsippass = $this->generateStrongPassword();
        $otp = array('status' => 'success', 'OTP' => $randomOtp, 'sippass' => $newsippass);
        return new JsonModel($otp);
        //exit;
    }

    /**
     * Used to generate extension 
     * @author		Nishikant
     * @created_date	9th Jan, 2016
     * @modified_date	18th Jan, 2017
     */
    public function generateExtensionAction() {
        $em = $this->getEntityManager();
        $offset = 0;
        $maxlimit = 1;
        $randomextension = $em->getRepository('Admin\Entity\GdFlatowner')->lastaddeduser($offset, $maxlimit);
        //$randomextension = mt_rand(100000, 999999);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $extension = array('status' => 'success', 'extension' => $randomextension);
        return new JsonModel($extension);
    }

    /**
     * Description used for editing owner details   
     * @author Nishikant
     */
    public function editownerAction() {
        $em = $this->getEntityManager();
        $this->layout()->pageTitle = 'Edit Details';
        $id = $this->params('id');
        if (empty($id)) {
            echo "2";
        } else {
            $userObj = $em->getRepository('Admin\Entity\GdFlatowner')->findOneBy(array('id' => $id));
			$status = 'consumed';
			$deviceinfo = $em->getRepository('Admin\Entity\GdOtp')->devicetype($id, $status);
			
			if((isset($deviceinfo)) && (!empty($deviceinfo['deviceType']))){
				$device = ($deviceinfo['deviceType'] == 1 ? 'IOS' : 'Android');               
				$lockedtodevice = $device .':'. $deviceinfo['uniqueId'];
			}else {                               
				$lockedtodevice = 'No Devicelocked';
			}

            if ((!empty($userObj)) && isset($userObj)) {
                $parentId=$userObj->getParentId();
                $firstName = $userObj->getFirstName();
                $lastName = $userObj->getLastName();
                $unitNum = $userObj->getUnitNumber();
                $floor = $userObj->getFloor();
                $mobile = $userObj->getMobile();
                $email = $userObj->getEmail();
                $followme1 = $userObj->getFollowme1();
                $followme2 = $userObj->getFollowme2();
                $followme3 = $userObj->getFollowme3();
                $followme4 = $userObj->getFollowme4();
                $followme5 = $userObj->getFollowme5();
                $ringType = $userObj->getRingType();
                $confirmcallDb = $userObj->getConfirmcall();
                $confirmcallDbReal = ($confirmcallDb=="yes") ? "CHECKED" : "";
                $voicemail = $userObj->getVoiceMail();
                $cameraonly = $userObj->getCameraOnly();
                $sipusername = $userObj->getSipUsername();
                $sipdownloadcode = $userObj->getSipDownloadCode();
                //$lockedtodevice = $userObj->getLockedToDevice();

                // Create form object
                $form = new AdminForms\EditOwnerForm($em, $id);
                $form->get('fname')->setValue($firstName);
                $form->get('lname')->setValue($lastName);
                $form->get('unitNum')->setValue($unitNum);
                $form->get('floor')->setValue($floor);
                $form->get('mobile')->setValue($mobile);
                $form->get('email')->setValue($email);
                $form->get('followme1')->setValue($followme1);
                $form->get('followme2')->setValue($followme2);
                $form->get('followme3')->setValue($followme3);
                $form->get('followme4')->setValue($followme4);
                $form->get('followme5')->setValue($followme5);
                $form->get('ring_type')->setValue($ringType);
                $form->get('confirmcall')->setValue($confirmcallDb);
                $form->get('voicemail')->setValue($voicemail);
                $form->get('cameraonly')->setValue($cameraonly);
                $form->get('sipusername')->setValue($sipusername);
                // $form->get('sippassword')->setValue($encryptedpass);
                $form->get('sipdownloadcode')->setValue($sipdownloadcode);
                $form->get('lockedtodevice')->setValue($lockedtodevice);

                $request = $this->getRequest();
                if ($request->isPost()) {
                    $formValidator = new AdminForms\Validator\EditOwnerFormValidator();
                    $form->setInputFilter($formValidator->getInputFilter());
                    $formdata = $request->getPost();					
                    $form->setData($request->getPost());

                    $confirmCall=@$formdata['confirmcall'];
                    $confirmCall=($confirmCall=="yes") ? "CHECKED" : "";

                    $followme="";
                    $followMe1=@trim($formdata['followme1']);
                    $followMe2=@trim($formdata['followme2']);
                    $followMe3=@trim($formdata['followme3']);
                    $followMe4=@trim($formdata['followme4']);
                    $followMe5=@trim($formdata['followme5']);

                    if($followMe1) $followme=$followme ? $followme.",".$followMe1 : $followMe1;
                    if($followMe2) $followme=$followme ? $followme.",".$followMe2 : $followMe2;
                    if($followMe3) $followme=$followme ? $followme.",".$followMe3 : $followMe3;
                    if($followMe4) $followme=$followme ? $followme.",".$followMe4 : $followMe4;
                    if($followMe5) $followme=$followme ? $followme.",".$followMe5 : $followMe5;

                    switch (ASTERIKAPI) {
                        case'Active':
                            // Call Asterik server api
                            if ((isset($formdata['followme1'])) || (isset($formdata['followme2'])) || (isset($formdata['followme3'])) || (isset($formdata['followme4'])) || (isset($formdata['followme5']))) {
                                $existedfollowme = $followme1 . ',' . $followme2 . ',' . $followme3.','.$followme4.','.$followme5;
                                $a1 = explode(",", $followme);
                                $a2 = explode(",", $existedfollowme);
                                $result = array_diff($a1, $a2);									
                                //if ($result) {
								if (($result) || ($sipdownloadcode != $formdata['sipdownloadcode']) || ($ringType != $formdata['ring_type']) || ($confirmcallDbReal != $confirmCall)){
                                   	$updateextapi = $this->updateextensionapi($formdata['mobile'], $followme, $formdata['sippassword'], $formdata['ring_type'], $confirmCall);
									if (($form->isValid()) && ($updateextapi == 3)) {
										$encriptedPass = $this->passEncryption($formdata['sippassword']);										
										$userObj->setFirstName($formdata['fname']);
										$userObj->setLastName($formdata['lname']);
										$userObj->setUnitNumber($formdata['unitNum']);
										$userObj->setFloor($formdata['floor']);
										$userObj->setMobile($formdata['mobile']);
										$userObj->setEmail($formdata['email']);
										$userObj->setFollowme1($formdata['followme1']);
										$userObj->setFollowme2($formdata['followme2']);
										$userObj->setFollowme3($formdata['followme3']);
                                        $userObj->setFollowme4($followMe4);
                                        $userObj->setFollowme5($followMe5);
                                        $userObj->setRingType($formdata['ring_type']);
                                        $userObj->setConfirmcall($formdata['confirmcall']);
										$userObj->setVoiceMail($formdata['voicemail']);
										$userObj->setCameraOnly($formdata['cameraonly']);
										$userObj->setSipUsername($formdata['sipusername']);
										$userObj->setSipPassword($encriptedPass);
										$userObj->setSipDownloadCode($formdata['sipdownloadcode']);
										$userObj->setLockedToDevice($formdata['lockedtodevice']);
										$em->persist($userObj);
										$em->flush();

									// In case of regenerate sipdownload code
									if ($formdata['sipdownloadcode'] != $sipdownloadcode) {
										$currentDate = date('Y-m-d H:i:s');
										$encOtp = md5($formdata['sipdownloadcode']);
										$otpObj = new Entities\GdOtp();
										$otpObj->setUserId($id);
										$otpObj->setOtp($formdata['sipdownloadcode']);
										$otpObj->setEncOtp($encOtp);
										$otpObj->setCreatedDate($currentDate);
										$em->persist($otpObj);
										$em->flush();
										// Disable otp status                          
										$em->getRepository('Admin\Entity\GdOtp')->updateOtpStatus($sipdownloadcode, $id);
										}

										$flashMessenger = $this->flashMessenger();
										$flashMessenger->setNamespace('success');
										$msg = "Details has been updated successfully";
										$flashMessenger->addMessage($msg);

                                        if($parentId) {
                                            return $this->redirect()->toUrl("/admin/owner/subowners/$parentId");
                                        }

										return $this->redirect()->toRoute('admindashboard');

										} elseif($updateextapi!=3) {
                                            $error = "Extension not updated!";
                                            return array('form' => $form, 'error' => $error, id => $id);
                                        } else {
                                            $allErrors="";
                                            $errors = $form->getMessages();
                                            foreach($errors as $k=>$thisError) {
                                                foreach($thisError as $errorKey=>$errorValue) {
                                                    $k=ucfirst($k);
                                                    $allErrors.=" $k: $errorValue,";
                                                }
                                            }
                                            $allErrors=$allErrors ? trim(trim($allErrors),",") : "Something Went wrong!";
                                            return array('form' => $form, 'error' => $allErrors, id => $id);
                                        }										  
                                }else{
									if ($form->isValid()) {
										$encriptedPass = $this->passEncryption($formdata['sippassword']);
										$userObj->setFirstName($formdata['fname']);
										$userObj->setLastName($formdata['lname']);
										$userObj->setUnitNumber($formdata['unitNum']);
										$userObj->setFloor($formdata['floor']);
										$userObj->setMobile($formdata['mobile']);
										$userObj->setEmail($formdata['email']);
										$userObj->setFollowme1($formdata['followme1']);
										$userObj->setFollowme2($formdata['followme2']);
										$userObj->setFollowme3($formdata['followme3']);
                                        $userObj->setFollowme4($followMe4);
                                        $userObj->setFollowme5($followMe5);
                                        $userObj->setRingType($formdata['ring_type']);
                                        $userObj->setConfirmcall($formdata['confirmcall']);
										$userObj->setVoiceMail($formdata['voicemail']);
										$userObj->setCameraOnly($formdata['cameraonly']);
										$userObj->setSipUsername($formdata['sipusername']);
										//$userObj->setSipPassword($encryptedpass);
										$userObj->setSipDownloadCode($formdata['sipdownloadcode']);
										$userObj->setLockedToDevice($formdata['lockedtodevice']);
										$em->persist($userObj);
										$em->flush();

										// In case of regenerate sipdownload code
										if ($formdata['sipdownloadcode'] != $sipdownloadcode) {
											$currentDate = date('Y-m-d H:i:s');
											$encOtp = md5($formdata['sipdownloadcode']);
											$otpObj = new Entities\GdOtp();
											$otpObj->setUserId($id);
											$otpObj->setOtp($formdata['sipdownloadcode']);
											$otpObj->setEncOtp($encOtp);
											$otpObj->setCreatedDate($currentDate);
											$em->persist($otpObj);
											$em->flush();
											// Disable otp status                          
												$em->getRepository('Admin\Entity\GdOtp')->updateOtpStatus($sipdownloadcode, $id);
											}

											$flashMessenger = $this->flashMessenger();
											$flashMessenger->setNamespace('success');
											$msg = "Details has been updated successfully";
											$flashMessenger->addMessage($msg);

                                            if($parentId) {
                                                return $this->redirect()->toUrl("/admin/owner/subowners/$parentId");
                                            }

											return $this->redirect()->toRoute('admindashboard');

											} else {
												$allErrors="";
                                                $errors = $form->getMessages();
                                                foreach($errors as $k=>$thisError) {
                                                    foreach($thisError as $errorKey=>$errorValue) {
                                                        $k=ucfirst($k);
                                                        $allErrors.=" $k: $errorValue,";
                                                    }
                                                }
                                                $allErrors=$allErrors ? trim(trim($allErrors),",") : "Something Went wrong!";
                                                return array('form' => $form, 'error' => $allErrors, id => $id);
											}
									}						
                            }						                           
                            break;
                         case 'Deactive':
                            if ($form->isValid()) {
                                $userObj->setFirstName($formdata['fname']);
                                $userObj->setLastName($formdata['lname']);
                                $userObj->setUnitNumber($formdata['unitNum']);
                                $userObj->setFloor($formdata['floor']);
                                $userObj->setMobile($formdata['mobile']);
                                $userObj->setEmail($formdata['email']);
                                $userObj->setFollowme1($formdata['followme1']);
								$userObj->setFollowme2($formdata['followme2']);
								$userObj->setFollowme3($formdata['followme3']);
                                $userObj->setFollowme4($followMe4);
                                $userObj->setFollowme5($followMe5);
                                $userObj->setRingType($formdata['ring_type']);
                                $userObj->setConfirmcall($formdata['confirmcall']);
                                $userObj->setVoiceMail($formdata['voicemail']);
                                $userObj->setCameraOnly($formdata['cameraonly']);
                                $userObj->setSipUsername($formdata['sipusername']);                
                                $userObj->setSipDownloadCode($formdata['sipdownloadcode']);
                                $userObj->setLockedToDevice($formdata['lockedtodevice']);
                                $em->persist($userObj);
                                $em->flush();

                                // In case of regenerate sipdownload code
                                if ($formdata['sipdownloadcode'] != $sipdownloadcode) {
                                    $currentDate = date('Y-m-d H:i:s');
                                    $encOtp = md5($formdata['sipdownloadcode']);
                                    $otpObj = new Entities\GdOtp();
                                    $otpObj->setUserId($id);
                                    $otpObj->setOtp($formdata['sipdownloadcode']);
                                    $otpObj->setEncOtp($encOtp);
                                    $otpObj->setCreatedDate($currentDate);
                                    $em->persist($otpObj);
                                    $em->flush();
                                    // Disable otp status                          
                                    $em->getRepository('Admin\Entity\GdOtp')
                                            ->updateOtpStatus($sipdownloadcode, $id);
                                }

                                $flashMessenger = $this->flashMessenger();
                                $flashMessenger->setNamespace('success');
                                $msg = "Details has been updated successfully";
                                $flashMessenger->addMessage($msg);

                                if($parentId) {
                                    return $this->redirect()->toUrl("/admin/owner/subowners/$parentId");
                                }

                                return $this->redirect()->toRoute('admindashboard');

                            } else {
                                $allErrors="";
                                $errors = $form->getMessages();
                                foreach($errors as $k=>$thisError) {
                                    foreach($thisError as $errorKey=>$errorValue) {
                                        $k=ucfirst($k);
                                        $allErrors.=" $k: $errorValue,";
                                    }
                                }
                                $allErrors=$allErrors ? trim(trim($allErrors),",") : "Something Went wrong!";
                                return array('form' => $form, 'error' => $allErrors, id => $id);
                            }
                            break;
                    }
                }
            } else {
                echo "3";
            }
        }
        return array('form' => $form, 'success' => @$msg, @id => @$id, 'parentId'=>$parentId);
    }

    public function ownerdeleteAction() {
        $em = $this->getEntityManager(); /* Call Entity Manager */
        $id = $this->params('id');
        if (empty($id)) {
            echo "2";
        } else {
            $userObj=$em->getRepository('Admin\Entity\GdFlatowner')->find($id);
            // get sub users
            $subObj=$em->getRepository('Admin\Entity\GdFlatowner')->getOwnerChild($id);

            if (!empty($userObj)) {
                // $userObj->setStatus(2); 
                //$astDelData=array(); 
                // $extension=$userObj->getMobile();
                //$astDelData['extension']=$extension;
                if(ASTERIKAPI=="Active") {
                    // remove sub user extensions
                    if(!empty($subObj)) {
                        foreach($subObj as $thisObj) {
                            $removeSubExt=$this->deleteextensionapi($thisObj->getMobile());
                            if($removeSubExt!=1) {
                                echo "4";
                                die();
                            }
                        }
                    }

                    // remove parent user
                    $extension=$userObj->getMobile();
                    $removeExt=$this->deleteextensionapi($extension);
                    if($removeExt==1){
                        $em->remove($userObj);
                        // remove sub users from db
                        $em->getRepository('Admin\Entity\GdFlatowner')->removeSubUsers($id);
                        $em->flush();

                        $this->_getOwnerTypeTable()->remove($id);
                        echo "1";
                    }

                } else {

                    $em->remove($userObj);
                    // remove sub users from db
                    $em->getRepository('Admin\Entity\GdFlatowner')->removeSubUsers($id);
                    $em->flush();

                    $this->_getOwnerTypeTable()->remove($id);
                    echo "1";
                }

            } else {
                echo "3";
            }
        }
        exit();
    }

    /**
     * Description : Used to change the status of menu in admin(Active/Deactive)   
     * @author	  : Nishikant
     * @return     : string 1-success
     */
    public function ownerchangestatusAction() {
        $em = $this->getEntityManager(); /* Call Entity Manager */
        $changeType = $this->params('type');
        $id = $this->params('id');
        if (!empty($changeType) && !empty($id)) {
            $userObj = $em->getRepository('Admin\Entity\GdFlatowner')->find($id);
            if (!empty($userObj)) {
                if ($changeType == "active") {
                    $userObj->setStatus('1');
                } else {
                    $userObj->setStatus('0');
                }
                $em->persist($userObj);
                $em->flush();
                echo "1";
                exit;
            } else {
                exit;
            }
        } else {
            exit;
        }
    }

    /*
     * add extension api function
     * this api will be used for adding extension to asterik server
     * @author Nishikant
     * @craeted_date 10th March, 2017
     */

    public function addextensionapi($fname, $lname, $ext, $pass, $followme, $email="", $voicemail="", $ringType="", $confirmCall="") {
        $name = $fname . " " . $lname;
        $service_url = ASTERIK_URL . 'Extension_FollowMe_api/add_extension.php';
        $curl = curl_init($service_url);
        $curl_post_data[] = array(
            "extension" => $ext,
            "password" => $pass,
            "name" => $name,
            "outboundcid" => $ext,
            "email"=>$email,
            "voicemail"=>$voicemail,
            "findmefollow" => $followme,
            "ring_type"=>$ringType,
            "confirm_calls"=>$confirmCall,
            "api_key" => "ce2cf9b55b4f9ed54fd32ea3bf657ae6",
            "transport" => "tcp",
            "action" => "INSERT"
        );

		$curl_post_datajson = json_encode($curl_post_data);
		
		// $this->getServiceLocator()->get('Zend\Log')->info($service_url);
		// $this->getServiceLocator()->get('Zend\Log')->info($curl_post_datajson);
        
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_datajson);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array
            (
            'Content-Type: application/json',
            'Content-Length: ' . strlen($curl_post_datajson))
        );
        $curl_response = curl_exec($curl);
        curl_close($curl);
        $responseSu = json_decode($curl_response);
        $succStatus = $responseSu[0]->status;
        return $succStatus;
    }

    /*
     * add extension api function
     * this api will be used for adding extension to asterik server
     * @author Nishikant
     * @craeted_date 10th March, 2017
     */

    public function updateextensionapi($ext, $followme, $sippass, $ringType="", $confirmCall="") {

        $service_url = ASTERIK_URL . 'Extension_FollowMe_api/add_extension.php';
        $curl = curl_init($service_url);
        $curl_post_data[] = array(
            "extension" => $ext,
			"password" => $sippass,
            "findmefollow" => $followme,
            "ring_type"=>$ringType,
            "confirm_calls"=>$confirmCall,
            "api_key" => "ce2cf9b55b4f9ed54fd32ea3bf657ae6",
            "action" => "UPDATE"
        );
        $curl_post_datajson = json_encode($curl_post_data);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_datajson);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array
            (
            'Content-Type: application/json',
            'Content-Length: ' . strlen($curl_post_datajson))
        );
        $curl_response = curl_exec($curl);
        curl_close($curl);
        $responseSu = json_decode($curl_response);
        $succStatus = $responseSu[0]->status;		
        return $succStatus;
    }

    /*
     * delete extension api function
     * this api will be used for delete extension to asterik server
     * @author Nishikant
     * @craeted_date 5th July, 2017
     */

    public function deleteextensionapi($ext) {      
        $service_url = ASTERIK_DELETE_API;
        $curl = curl_init($service_url);
        $curl_post_data[] = array(
            "extension" => $ext
        );
        $curl_post_datajson = json_encode($curl_post_data);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_datajson);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array
            (
            'Content-Type: application/json',
            'Content-Length: ' . strlen($curl_post_datajson))
        );
        $curl_response = curl_exec($curl);
        curl_close($curl);
        $responseSu = json_decode($curl_response);
        $succStatus = $responseSu[0]->status;        
        return $succStatus;
    }
    
     /*
     * This function is used to add bulk users by uploading CSV file
     * 
     * @author Nishikant
     * @created_date 2 june, 2017
     */
    public function importcsvAction_OLD() {
        $em = $this->getEntityManager();
        
         // Max user can be added
        $maxUserlimit = $em->getRepository('Admin\Entity\GdUserlimit')->userlimitRecord();
        $existingUser = $em->getRepository('Admin\Entity\GdFlatowner')->totalOwner();
        $existingUser= $existingUser[0];
        $usercanadded = $maxUserlimit['maxUser'] - $existingUser['totalmember'];
        
        $fileTemp = $_FILES['myFile']['tmp_name'];
        if ($fileTemp) {
            $path_parts = pathinfo($_FILES["myFile"]["name"]);
            $extension = $path_parts['extension'];
            $filename = 'userlist' . '.' . $extension;
            $fpath = "/userbulkcsv/" . $filename;
            $folderuploadPath = DOCUMENT_ROOT_PATH . $fpath;
            move_uploaded_file($fileTemp, $folderuploadPath);
            $csv_file = $_SERVER['DOCUMENT_ROOT'] . '/userbulkcsv/userlist.csv';
            $record = 0;
            if (($handle = fopen($csv_file, "r")) !== FALSE) {
                fgetcsv($handle);
                while (($data = fgetcsv($handle, 100000, ",")) !== FALSE) {
                    $record++;
                    if($record > $usercanadded){
                        exit;
                    }
                    $num = count($data);
                    for ($c = 0; $c < $num; $c++) {
                        $col[$c] = $data[$c];
                    }
                    // Generate extension
                    $offset = 0;
                    $maxlimit = 1;
                    $randomextension = $em->getRepository('Admin\Entity\GdFlatowner')
                            ->lastaddeduser($offset, $maxlimit);
                    $extension = $randomextension + 100 + 1;
                    //folow me number
                    $followme1 = $randomextension;
                    //otp
                    $randomOtp = mt_rand(100000, 999999);
                    // sippassword
                    $sippassword = $this->generateStrongPassword();
                    $encriptedPass = $this->passEncryption($sippassword);

                    $firstname = $col[0];       // firstname
                    $lastname = $col[1];        // lastname
                    $unitnumber = $col[2];      // unitnumber
                    $floor = $col[3];           // floor
                    $email = $col[4];           // email
                    $followme2 = $col[5];       // followme2
                    $followme3 = $col[6];        // followm3
                    $voicemail = $col[7];        // voicemail
                    $cameraonly = $col[8];

                    if (!empty($col[0]) && !empty($col[3])) {
                        // Asterik API
                        if ((!empty($extension)) && (!empty($followme2)) && (!empty($formdata['followme3']))) {
                            $followme = $extension . ',' . $followme2 . ',' . $followme3;
                        } else if ((!empty($extension)) && (!empty($followme2))) {
                            $followme = $extension . ',' . $followme2;
                        } elseif (!empty($followme3)) {
                            $followme = $followme3;
                        }

                        switch (ASTERIKAPI) {
                            case 'Active':
                                $addextapi = $this->addextensionapi($firstname, $lastname, $extension, $sippassword, $followme);
                                if ($addextapi === 1) {
                                    if ($voicemail == "no") {
                                        $voiceval = "novm";
                                    } else if ($voicemail == "yes") {
                                        $voiceval = "default";
                                    }

                                    $userObj = new Entities\GdFlatowner();
                                    $currentDate = date('Y-m-d H:i:s');
                                    $userObj->setFirstName($firstname);
                                    $userObj->setLastName($lastname);
                                    $userObj->setUnitNumber($unitnumber);
                                    $userObj->setFloor($floor);
                                    $userObj->setMobile($extension);
                                    $userObj->setEmail($email);
                                    $userObj->setFollowme1($extension);
                                    $userObj->setFollowme2($followme2);
                                    $userObj->setFollowme3($followme3);
                                    $userObj->setVoiceMail($voiceval);
                                    $userObj->setCameraOnly($cameraonly);
                                    $userObj->setSipUsername($extension);
                                    $userObj->setSipPassword($encriptedPass);
                                    $userObj->setSipDownloadCode($randomOtp);
                                    //$userObj->setLockedToDevice($formdata['lockedtodevice']);
                                    $userObj->setCreatedDate($currentDate);
                                    $em->persist($userObj);
                                    $em->flush();

                                    // Creating OTP of users added by Nishikant 29th Dec,2016
                                    $lastUserCreatedId = $userObj->getId();
                                    $randomOtp = mt_rand(100000, 999999);
                                    $encOtp = md5($randomOtp);
                                    $otpObj = new Entities\GdOtp();
                                    $otpObj->setUserId($lastUserCreatedId);
                                    $otpObj->setOtp($randomOtp);
                                    $otpObj->setEncOtp($encOtp);
                                    $otpObj->setCreatedDate($currentDate);
                                    $em->persist($otpObj);
                                    $em->flush();
                                } else {
                                    $error = "Extension already exists!";
									$this->deleteextensionapi($extension);
									$flashMessenger = $this->flashMessenger();
									$flashMessenger->setNamespace('error');
									$msg = "Something went wrong, please try again.";
									$flashMessenger->addMessage($msg);
									return $this->redirect()->toRoute('admindashboard');
                                    // return array('form' => $form, 'error' => $error);
                                }

                                break;
                            case 'Deactive':
                                $encriptedPass = $this->passEncryption($sippassword);
                                if ($voicemail == "no") {
                                    $voiceval = "novm";
                                } else if ($voicemail == "yes") {
                                    $voiceval = "default";
                                }

                                $userObj = new Entities\GdFlatowner();
                                $currentDate = date('Y-m-d H:i:s');
                                $userObj->setFirstName($firstname);
                                $userObj->setLastName($lastname);
                                $userObj->setUnitNumber($unitnumber);
                                $userObj->setFloor($floor);
                                $userObj->setMobile($extension);
                                $userObj->setEmail($email);
                                $userObj->setFollowme1($extension);
                                $userObj->setFollowme2($followme2);
                                $userObj->setFollowme3($followme3);
                                $userObj->setVoiceMail($voiceval);
                                $userObj->setCameraOnly($cameraonly);
                                $userObj->setSipUsername($extension);
                                $userObj->setSipPassword($encriptedPass);
                                $userObj->setSipDownloadCode($randomOtp);
                                //$userObj->setLockedToDevice($formdata['lockedtodevice']);
                                $userObj->setCreatedDate($currentDate);
                                $em->persist($userObj);
                                $em->flush();

                                // Creating OTP of users added by Nishikant 29th Dec,2016
                                $lastUserCreatedId = $userObj->getId();
                                $randomOtp = mt_rand(100000, 999999);
                                $encOtp = md5($randomOtp);
                                $otpObj = new Entities\GdOtp();
                                $otpObj->setUserId($lastUserCreatedId);
                                $otpObj->setOtp($randomOtp);
                                $otpObj->setEncOtp($encOtp);
                                $otpObj->setCreatedDate($currentDate);
                                $em->persist($otpObj);
                                $em->flush();
                                break;
                        }
                    }
                }
				fclose($handle);
            }
        }
        $flashMessenger = $this->flashMessenger();
        $flashMessenger->setNamespace('success');
        $msg = "Users hav been added successfully";
        $flashMessenger->addMessage($msg);
        return $this->redirect()->toRoute('admindashboard');
    }

    public function importcsvAction() {
        $em = $this->getEntityManager();
        
         // Max user can be added
        $maxUserlimit = $em->getRepository('Admin\Entity\GdUserlimit')->userlimitRecord();
        $existingUser = $em->getRepository('Admin\Entity\GdFlatowner')->totalOwner();
        $existingUser= $existingUser[0];
        $usercanadded = $maxUserlimit['maxUser'] - $existingUser['totalmember'];
        
        $fileTemp = $_FILES['myFile']['tmp_name'];
        if ($fileTemp) {
            $path_parts = pathinfo($_FILES["myFile"]["name"]);
            $extension = $path_parts['extension'];
            $filename = 'userlist' . '.' . $extension;
            $fpath = "/userbulkcsv/" . $filename;
            $folderuploadPath = DOCUMENT_ROOT_PATH . $fpath;
            move_uploaded_file($fileTemp, $folderuploadPath);
            $csv_file = $_SERVER['DOCUMENT_ROOT'] . '/userbulkcsv/userlist.csv';
            $record = 0;
            if (($handle = fopen($csv_file, "r")) !== FALSE) {
                fgetcsv($handle);
                while (($data = fgetcsv($handle, 100000, ",")) !== FALSE) {
                    $record++;
                    if($record > $usercanadded){
                        exit;
                    }
                    $num = count($data);
                    for ($c = 0; $c < $num; $c++) {
                        $col[$c] = $data[$c];
                    }
                    // Generate extension
                    $offset = 0;
                    $maxlimit = 1;
                    $randomextension = $em->getRepository('Admin\Entity\GdFlatowner')
                            ->lastaddeduser($offset, $maxlimit);
                    $extension = $randomextension + 100 + 1;
                    //folow me number
                    $followme1 = $extension;
                    //otp
                    $randomOtp = mt_rand(100000, 999999);
                    // sippassword
                    $sippassword = $this->generateStrongPassword();
                    $encriptedPass = $this->passEncryption($sippassword);

                    $firstname = $col[0];       // firstname
                    $lastname = $col[1];        // lastname
                    $unitnumber = $col[2];      // unitnumber
                    $floor = $col[3];           // floor
                    $email = $col[4];           // email
                    $followme2 = $col[5];       // followme2
                    $followme3 = $col[6];        // followm3
                    $followme4 = $col[7];        // followm4
                    $followme5 = $col[8];        // followm5
                    $ringType = $col[9];        // ring_type
                    $confirmcall = $col[10];        // confirmcall
                    $voicemail = $col[11];        // voicemail
                    $cameraonly = $col[12];

                    $confirmCall=($confirmcall=="yes") ? "CHECKED" : ""; // for asterisk

                    if (!empty($col[0]) && !empty($col[3])) {
                        // Asterik API
                        $followme=$followme1.",".$followme2.",".$followme3.",".$followme4.",".$followme5;

                        switch (ASTERIKAPI) {
                            case 'Active':
                                $addextapi = $this->addextensionapi($firstname, $lastname, $extension, $sippassword, $followme, $email, "", $ringType, $confirmCall);
                                if ($addextapi === 1) {
                                    if ($voicemail == "no") {
                                        $voiceval = "novm";
                                    } else if ($voicemail == "yes") {
                                        $voiceval = "default";
                                    }

                                    $userObj = new Entities\GdFlatowner();
                                    $currentDate = date('Y-m-d H:i:s');
                                    $userObj->setFirstName($firstname);
                                    $userObj->setLastName($lastname);
                                    $userObj->setUnitNumber($unitnumber);
                                    $userObj->setFloor($floor);
                                    $userObj->setMobile($extension);
                                    $userObj->setEmail($email);
                                    $userObj->setFollowme1($extension);
                                    $userObj->setFollowme2($followme2);
                                    $userObj->setFollowme3($followme3);
                                    $userObj->setFollowme4($followme4);
                                    $userObj->setFollowme5($followme5);
                                    $userObj->setRingType($ringType);
                                    $userObj->setConfirmcall($confirmcall);
                                    $userObj->setVoiceMail($voiceval);
                                    $userObj->setCameraOnly($cameraonly);
                                    $userObj->setSipUsername($extension);
                                    $userObj->setSipPassword($encriptedPass);
                                    $userObj->setSipDownloadCode($randomOtp);
                                    //$userObj->setLockedToDevice($formdata['lockedtodevice']);
                                    $userObj->setCreatedDate($currentDate);
                                    $userObj->setModifiedDate($currentDate);
                                    $em->persist($userObj);
                                    $em->flush();

                                    // Creating OTP of users added by Nishikant 29th Dec,2016
                                    $lastUserCreatedId = $userObj->getId();
                                    $randomOtp = mt_rand(100000, 999999);
                                    $encOtp = md5($randomOtp);
                                    $otpObj = new Entities\GdOtp();
                                    $otpObj->setUserId($lastUserCreatedId);
                                    $otpObj->setOtp($randomOtp);
                                    $otpObj->setEncOtp($encOtp);
                                    $otpObj->setCreatedDate($currentDate);
                                    $otpObj->setModifiedDate($currentDate);
                                    $em->persist($otpObj);
                                    $em->flush();
                                } else {
                                    $error = "Extension already exists!";
                                    $this->deleteextensionapi($extension);
                                    $flashMessenger = $this->flashMessenger();
                                    $flashMessenger->setNamespace('error');
                                    $msg = "Something went wrong, please try again.";
                                    $flashMessenger->addMessage($msg);
                                    return $this->redirect()->toRoute('admindashboard');
                                    // return array('form' => $form, 'error' => $error);
                                }

                                break;
                            case 'Deactive':
                                $encriptedPass = $this->passEncryption($sippassword);
                                if ($voicemail == "no") {
                                    $voiceval = "novm";
                                } else if ($voicemail == "yes") {
                                    $voiceval = "default";
                                }

                                $userObj = new Entities\GdFlatowner();
                                $currentDate = date('Y-m-d H:i:s');
                                $userObj->setFirstName($firstname);
                                $userObj->setLastName($lastname);
                                $userObj->setUnitNumber($unitnumber);
                                $userObj->setFloor($floor);
                                $userObj->setMobile($extension);
                                $userObj->setEmail($email);
                                $userObj->setFollowme1($extension);
                                $userObj->setFollowme2($followme2);
                                $userObj->setFollowme3($followme3);
                                $userObj->setFollowme4($followme4);
                                $userObj->setFollowme5($followme5);
                                $userObj->setRingType($ringType);
                                $userObj->setConfirmcall($confirmcall);
                                $userObj->setVoiceMail($voiceval);
                                $userObj->setCameraOnly($cameraonly);
                                $userObj->setSipUsername($extension);
                                $userObj->setSipPassword($encriptedPass);
                                $userObj->setSipDownloadCode($randomOtp);
                                //$userObj->setLockedToDevice($formdata['lockedtodevice']);
                                $userObj->setCreatedDate($currentDate);
                                $userObj->setModifiedDate($currentDate);
                                $em->persist($userObj);
                                $em->flush();

                                // Creating OTP of users added by Nishikant 29th Dec,2016
                                $lastUserCreatedId = $userObj->getId();
                                $randomOtp = mt_rand(100000, 999999);
                                $encOtp = md5($randomOtp);
                                $otpObj = new Entities\GdOtp();
                                $otpObj->setUserId($lastUserCreatedId);
                                $otpObj->setOtp($randomOtp);
                                $otpObj->setEncOtp($encOtp);
                                $otpObj->setCreatedDate($currentDate);
                                $otpObj->setModifiedDate($currentDate);
                                $em->persist($otpObj);
                                $em->flush();
                                break;
                        }
                    }
                }
                fclose($handle);
            }
        }
        $flashMessenger = $this->flashMessenger();
        $flashMessenger->setNamespace('success');
        $msg = "Users have been added successfully";
        $flashMessenger->addMessage($msg);
        return $this->redirect()->toRoute('admindashboard');
    }

    protected $_ownerTypeTable;
    public function _getOwnerTypeTable() {
        if(!$this->_ownerTypeTable) {
            $sm=$this->getServiceLocator();
            $this->_ownerTypeTable=$sm->get("Admin\Model\OwnerTypeTable");
        }
        return $this->_ownerTypeTable;
    }

    protected $_flatownerTable;
    protected function _getFlatOwnerTable() {
        if(!$this->_flatownerTable) {
            $sm=@$this->getServiceLocator();
            $this->_flatownerTable=$sm->get("Admin\Model\GdFlatownerTable");
        }
        return $this->_flatownerTable;
    }
}
