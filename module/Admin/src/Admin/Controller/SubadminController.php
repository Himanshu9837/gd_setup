<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Admin\Form as AdminForms;
use Zend\Mvc\MvcEvent;
use Admin\Entity as Entities;
use Zend\View\Model\JsonModel;
use Zend\Crypt\Password\Bcrypt;

class SubadminController extends AbstractActionController {
    /* This function is like old init() */

    public function onDispatch(MvcEvent $e) {
        $admin_session = new Container('admin');
        $username = $admin_session->username;
        $usertype = $admin_session->usertype;
        if (empty($username)) {            
            return $this->redirect()->toRoute('adminlogin');
        }
        $this->layout('layout/adminlayout');
        return parent::onDispatch($e);
    }

    public function setEntityManager(EntityManager $em) {
        $this->em = $em;
    }

    public function getEntityManager() {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    /**
     * Used to add sub-admin by owner
     * @author Nishikant
     * @created_date  1st june, 2017
     * @modified_date -------------
     */
     public function subadminAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;
        $this->layout()->pageTitle = 'Add Sub admin';
        
        if($usertype == 2) {
            return $this->redirect()->toRoute('admindashboard');
            $flashMessenger = $this->flashMessenger();
            $flashMessenger->setNamespace('Not Authorised');
            $msg = "You are not authorised to access";
            $flashMessenger->addMessage($msg);
        }
        
        $msg = "";
        $form = new AdminForms\AddSubAdminForm($em);
        $form->get('submitbutton')->setValue('Save');
        $request = $this->getRequest();
        $userObj = new Entities\GdAdmin();
        if ($request->isPost()) {
            $formValidator = new AdminForms\Validator\AddSubAdminFormValidator();
            $form->setInputFilter($formValidator->getInputFilter());
            $formdata = $request->getPost();
            $checkvalidemail = $em->getRepository('Admin\Entity\GdAdmin')->checkIfEmailExist($formdata['email']);
            if(empty($checkvalidemail)){
                 $form->setData($request->getPost());
                if ($form->isValid()) {                       
                    $currentDate = date('Y-m-d H:i:s');
                    $usertRole = $formdata['usertype']==3 ? 'vendor' : 'subadmin';
                   // $encriptedUserttype = $this->passEncryption($formdata['usertype']);
                   // $decriptedUserttype = $this->passDecryption($encriptedUserttype);
                    $currentDate = date('Y-m-d H:i:s');
                    $userObj->setFirstName(ucwords($formdata['firstname']));
                    $userObj->setLastName(ucwords($formdata['lastname']));
                    $userObj->setUsername($formdata['uname']);                       
                    $userObj->setPhone($formdata['mobile']);
                    $userObj->setEmail(trim($formdata['email']));  
                    $userObj->setPassword(trim($formdata['pwd']));
                    $userObj->setRole($usertRole);
                    $userObj->setGender($formdata['gender']); 
                    $userObj->setUserType(trim($formdata['usertype']));
                    $userObj->setCreatedDate($currentDate);
                    $em->persist($userObj);
                    $em->flush();
                 
                    $flashMessenger = $this->flashMessenger();
                    $flashMessenger->setNamespace('success');
                    $msg = "Sub admin added successfully!";
                    $flashMessenger->addMessage($msg);
                    return $this->redirect()->toRoute('subadminlists');
                } else {
                    $error = "Something Went wrong! Please Try again.";
                    return array('form' => $form, 'error' => $error);
                }
            }else{                 
                $error = "eamil Id already exists.";
                return array('form' => $form, 'error' => $error);
            }
           
        }
        return array('form' => $form, 'success' => $msg);
    }


    /**
     * This function is used to get the lists of sub admin
     * @author Nishikant
     * @created_date  1st june, 2017
     * @modified_date -------------
     */
    public function subadminlistsAction(){

        $em = $this->getEntityManager();
        $this->layout()->pageTitle = 'Add Sub admin';

        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;
        
        if($usertype == 2) {
            return $this->redirect()->toRoute('admindashboard');
            $flashMessenger = $this->flashMessenger();
            $flashMessenger->setNamespace('Not Authorised');
            $msg = "You are not authorised to access";
            $flashMessenger->addMessage($msg);
        }

        $userType = '1';
        $subadminlists = $em->getRepository('Admin\Entity\GdAdmin')->getSubAdminLists($userType);
        return array('lists' => $subadminlists, 'usertype' =>$usertype);
    }

    /**
     * Used to delete sub admin by master admin
     * @author Nishikant
     * @created_date  1st june, 2017
     * @modified_date -------------
     */
      public function sbadmindeleteAction() {
        $em = $this->getEntityManager();
        $id = $this->params('id');
        if (empty($id)) {
            echo "2";
        } else {
            $userObj = $em->getRepository('Admin\Entity\GdAdmin')->find($id);
            if (!empty($userObj)) {
                // $userObj->setStatus(0);                                            
                $em->remove($userObj);
                $em->flush();
                echo 1;
            } else {
                echo "3";
            }
        }
        exit();
    }


     /**
     * Used for password encryption 
     * @author      Nishikant
     * @created_date    13th Jan, 2016
     * @modified_date   -------------
     */
    public function passEncryption($pass) {
        $iv = mcrypt_create_iv(
                mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM
        );

        $key = ENCRYPTION_KEY;
        $encryptedpass = base64_encode(
                $iv .
                mcrypt_encrypt(
                        MCRYPT_RIJNDAEL_128, hash('sha256', $key, true), $pass, MCRYPT_MODE_CBC, $iv
                )
        );
        return $encryptedpass;
    }

    /**
     * Used for password decryption
     * @author      Nishikant
     * @created_date    13th Jan, 2016
     * @modified_date   -------------
     */
    public function passDecryption($encrypted) {
        $enpass = base64_decode($encrypted);
        $dkey = ENCRYPTION_KEY;
        $iv1 = substr($enpass, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));
        $decrypted = rtrim(
                mcrypt_decrypt(
                        MCRYPT_RIJNDAEL_128, hash('sha256', $dkey, true), substr($enpass, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)), MCRYPT_MODE_CBC, $iv1
                ), "\0"
        );
        return $decrypted;
    }



}

