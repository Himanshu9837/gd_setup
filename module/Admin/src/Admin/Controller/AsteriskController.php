<?php
/**
 * Zend Framework (http://framework.zend.com/)
 * This class is used for Login, Log out, Forgot Password and Change Password.
 */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Mvc\MvcEvent;
use Admin\Entity as Entities;
use Zend\View\Model\JsonModel;

class AsteriskController extends AbstractActionController {

    protected $em;
    protected $authservice;

    /* This function is like old init() */
    public function onDispatch(MvcEvent $e)
    {
    	$admin_session = new Container('admin');
        $username = $admin_session->username;
        $usertype = $admin_session->usertype;
        if (empty($username) || $usertype == 2) {            
            return $this->redirect()->toRoute('adminlogin');
        }
       $this->layout('layout/adminlayout');
        return parent::onDispatch($e);
    }
    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }
    
    public function getEntityManager()
    {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }
    
    public function getAuthService()
    {
        if (! $this->authservice) {
            $this->authservice = $this->getServiceLocator()->get('AuthService');
        }
        return $this->authservice;
    }

    public function asteriskAction(){
    	$em = $this->getEntityManager();        
        $this->layout()->pageTitle = 'Asterisk Restore'; 
        $admin_session = new Container('admin');
        $username = $admin_session->username;
        $id = $admin_session->userId;
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;
    }

    public function restoreAction(){
        $em = $this->getEntityManager(); 
        $viewModel = new ViewModel();        
        $viewModel->setTerminal(true); 
        $users=$em->getRepository('Admin\Entity\GdFlatowner')->userslists();

        // Add Extension data array
        $astAddData=array();

        // Delete Extension array
        $astDelData=array();

        foreach($users as $key=>$value){
        	// delete extension
            $astDelData[$key]['extension']=$value['mobile'];

            $confirmCall=@$value['confirmcall'];
            $confirmCall=($confirmCall=="yes") ? "CHECKED" : "";

            // add extension data
            $f1=$value['followme1'];
            $f2=$value['followme2'];
            $f3=$value['followme3'];
            $f4=$value['followme4'];
            $f5=$value['followme5'];

            $followme=$f1;
            if($f2) $followme=$followme ? $followme.",".$f2 : $f2;
            if($f3) $followme=$followme ? $followme.",".$f3 : $f3;
            if($f4) $followme=$followme ? $followme.",".$f4 : $f4;
            if($f5) $followme=$followme ? $followme.",".$f5 : $f5;

            $astAddData[$key]['extension']=$value['mobile'];
           	$astAddData[$key]['password']=$this->passDecryption($value['sipPassword']);
            $astAddData[$key]['name']=$value['firstName'] .' '. $value['lastName'];
            $astAddData[$key]['outboundcid']=$value['mobile'];
            $astAddData[$key]['email']=@$value['email'];
            $astAddData[$key]['voicemail']=@$value['voiceMail'];
            $astAddData[$key]['followme']=$followme;
            $astAddData[$key]['ring_type']=@$value['ringType'];
            $astAddData[$key]['confirm_calls']=(@$value['confirmcall']=="yes") ? "CHECKED" : "";
            $astAddData[$key]['api_key']='ce2cf9b55b4f9ed54fd32ea3bf657ae6'; 
            $astAddData[$key]['transport']='tcp'; 
            $astAddData[$key]['action']='INSERT'; 
        }


        $deleteExtapi = $this->deleteextensionapi($astDelData); 
        if(count($users) === count($astAddData)){
             $addExtapi = $this->addextensionapi($astAddData);
         }
        $restoreData = array('status' =>200,  'message' =>'Restore Sucess');        
        return new JsonModel($restoreData);        
        exit;
    }


     /*
     * add extension api function
     * this api will be used for adding extension to asterik server
     * @author Nishikant
     * @craeted_date 4th july, 2017
     */

    public function addextensionapi($curl_post_data) {
      
        $service_url = ASTERIK_URL . 'Extension_FollowMe_api/add_extension.php';
        $curl = curl_init($service_url);
        $curl_post_datajson = json_encode($curl_post_data);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_datajson);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array
            (
            'Content-Type: application/json',
            'Content-Length: ' . strlen($curl_post_datajson))
        );
        $curl_response = curl_exec($curl);
        curl_close($curl);
        $responseSu = json_decode($curl_response);
        $succStatus = $responseSu[0]->status;
        return $succStatus;
    }


    /*
     * delete extension api function
     * this api will be used for delete extension to asterik server
     * @author Nishikant
     * @craeted_date 4th July, 2017
     */

    public function deleteextensionapi($curl_post_data) {
      
        $service_url = ASTERIK_DELETE_API;
        $curl = curl_init($service_url);
        $curl_post_datajson = json_encode($curl_post_data);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_datajson);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array
            (
            'Content-Type: application/json',
            'Content-Length: ' . strlen($curl_post_datajson))
        );
        $curl_response = curl_exec($curl);
        curl_close($curl);
        $responseSu = json_decode($curl_response);
        $succStatus = $responseSu[0]->status;
        return $succStatus;
    }


    /**
     * Used for password decryption
     * @author      Nishikant
     * @created_date    14th March, 2017
     * @modified_date   -------------
     */
    public function passDecryption($encrypted) { 
        $enpass = base64_decode(trim($encrypted));
    $dkey = ENCRYPTION_KEY;
        $iv1 = substr($enpass, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));
        $decrypted = rtrim(
                mcrypt_decrypt(
                        MCRYPT_RIJNDAEL_128, hash('sha256', $dkey, true), substr($enpass, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)), MCRYPT_MODE_CBC, $iv1
                ), "\0"
        );
        
       
        return $decrypted;
    }




}