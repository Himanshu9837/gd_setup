<?php
/**
 * Zend Framework (http://framework.zend.com/)
 * This class is used for Login, Log out, Forgot Password and Change Password.
 */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\AbstractRestfulController as RestfulController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Zend\Session\Container;
use Admin\Form as AdminForms;
use Zend\Mail as Mail;
use Zend\Mvc\MvcEvent;
use Admin\Entity as Entities;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

use Admin\Model\Configs;
use Admin\Model\AccessCodes;

use Admin\Model\TimeProfiles;
use Zend\Validator\Digits;


class AccessCodesController extends AbstractActionController {

    protected $em;
    protected $authservice;
    
     public function onDispatch(MvcEvent $e) {

        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $username = $admin_session->username;
        $usertype = $admin_session->usertype;
         if (empty($username) && $usertype != 1) {
            /* if not logged in redirect the user to login page */
            return $this->redirect()->toRoute('adminlogin');
        }
        $this->layout('layout/adminlayout');
        return parent::onDispatch($e);
    }

    public function setEntityManager(EntityManager $em) {
        $this->em = $em;
    }
    
    public function getEntityManager() {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }
    
    public function getAuthService() {
        if (! $this->authservice) {
            $this->authservice = $this->getServiceLocator()
                                      ->get('AuthService');
        }
         
        return $this->authservice;
    }
    
    public function indexAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        $data=$this->_getAccessCodesTable()->getList(1); // 1=for active codes 
//         echo "<pre>";
//         print_r($data);
//         die();

        return new ViewModel(array(
            'records' => $data
        ));
    }

    public function addNewAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        $data=$this->_getAccessCodesTable()->getOwners();
        $timeProfiles=$this->_getTimeProfilesTable()->getList();

        // echo "<pre>";
        // print_r($data);
        // die();

        return new ViewModel(array(
            'flatOwners' => $data,
            'timeProfiles' => $timeProfiles
        ));
    }

    public function saveCodeAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost()->toArray();

            $ownerId=@trim($data['owner']);
            $accessType=@trim($data['type']);
            $accessDateTime=@trim($data['accessDateTime']);
            // $accessCode=@trim($data['code']);
            
            if(!$ownerId || !$accessType || !$accessDateTime)
            return new JsonModel($this->createResponse(400, "error", "All fields are required"));

            // validate owner
            $validOwner=$this->validateDigits($ownerId, "Owner");
            if($validOwner['status']!=200)
            return new JsonModel($this->createResponse(400, "error", "Invalid owner"));

            // valiate type
            if(!in_array($accessType, array(1,2)))
            return new JsonModel($this->createResponse(400, "error", "Access type is invalid"));

            // generate access code
            $maxTry=10;
            $accessCode="";
            $tryId=0;
            for($try=1; $try<=$maxTry; $try++) {
                $tryId++;

                $accessCode=rand(1, 999999);
                $accessCode=str_pad($accessCode, 6, "0", STR_PAD_LEFT);

                // check code already exists and active
                $where=array("access_code"=>$accessCode, "code_status"=>1);
                @list($exists)=$this->_getAccessCodesTable()->getWhere($where);

                if(!$exists) $try=$maxTry+10;
                else $accessCode="";
            }

            if(!$accessCode)
            return new JsonModel($this->createResponse(400, "max_try", "Code not generated, please try again.", $tryId));

            $adminSession = new Container('admin');
            $adminId=$adminSession->userId;
            if(!$adminId) return new JsonModel($this->createResponse(400, "error", "Login required"));

            // update now
            $codeData=array(
                "owner_id"=>$ownerId, 
                "access_code"=>$accessCode,
                "usage_type"=>$accessType, 
                "access_date_time"=>$accessDateTime,
                "code_status"=>1,
                "admin_id"=>$adminId
            );
            $entity=new AccessCodes();
            $entity->exchangeArray($codeData);

            $accessId=$this->_getAccessCodesTable()->addCode($entity);
            if(!$accessId) return new JsonModel($this->createResponse(400, "db_error", SOME_ERROR));

            return new JsonModel($this->createResponse(200, "success", "Created successfully"));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }
    
    public function accessLimitsAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        $data=$this->_getConfigsTable()->getByType(1); // 1=for access code limit
        $limit=@$data['value'] ? @$data['value'] : 0;

        // echo "<pre>";
        // print_r($data);
        // die();

        return new ViewModel(array(
            'limit' => $limit
        ));
    }

    public function updateLimitAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost()->toArray();

            $limit=@trim($data['limit']);
            if(!$limit) return new JsonModel($this->createResponse(400, "error", "Enter the value greater than 1"));

            // validate limit
            $validNumber=$this->validateDigits($limit, "Value");
            if($validNumber['status']!=200) return new JsonModel($validNumber);

            // update now
            $limitData=array("config_type"=>1, "value"=>$limit);
            $entity=new Configs();
            $entity->exchangeArray($limitData);

            $this->_getConfigsTable()->insertOrUpdate($entity);

            return new JsonModel($this->createResponse(200, "success", "Updated successfully"));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }
    
     public function accessCodeConfigAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        $data=$this->_getConfigsTable()->getByType(2); // 2=for i have access code button show/hide
        $access_code_config=@$data['value'] ? @$data['value'] : 1;

//         echo "<pre>";
//         print_r($data);
//         die();

        return new ViewModel(array(
            'access_code_config' => $access_code_config
        ));
    }
    
    public function updateAccessCodeConfigAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost()->toArray();

            $access_code_config=@trim($data['access_code_config']);    

            // update now
            $access_code_config_data=array("config_type"=>2, "value"=>$access_code_config);
            $entity=new Configs();
            $entity->exchangeArray($access_code_config_data);

            $this->_getConfigsTable()->insertOrUpdate($entity);

            return new JsonModel($this->createResponse(200, "success", "Updated successfully"));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function accessLogsAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        $data=$this->_getAccessLogsTable()->getList();

        // echo "<pre>";
        // print_r($data);
        // die();

        return new ViewModel(array(
            'records' => $data
        ));
    }

    public function deleteAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost()->toArray();

            if(!isset($data['id']))
            return new JsonModel($this->createResponse(400, "wrong_data", WRONG_DATA));

            // update now
            $accessId=$data['id'];
            $this->_getAccessCodesTable()->updateStatus(3, $accessId);

            return new JsonModel($this->createResponse(200, "success", "Data Deleted!"));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function checkAdminLoggedInOrNot(){
         /* checking if user logged in or not */
        $admin_session = new Container('admin');
        $username = $admin_session->username;
        if(empty($username)) {
            /* if not logged in redirect the user to login page */
            return $this->redirect()->toRoute('adminlogin');
        }
    }
    
    public function exportAccessLogsAction() {

        $request = $this->getRequest();
        if($request->isPost()) {
//            $data=$request->getPost()->toArray();

            $accessLogs = $this->_getAccessLogsTable()->getExportAccessLogsList();
            if(!$accessLogs)
            return new JsonModel($this->createResponse(400, "no_access_log", "No access log to export."));

//             echo "<pre>";
//             print_r($accessLogs);
//             die();

            $filename="access_logs_" . $this->generateSecretKey() . ".csv";
            $filePath=EXPORT_ACCESS_LOGS_UPLOAD_PATH.$filename;
            $fopen=fopen($filePath, 'w');

            $headerDisplayed=false; // "Log Id", "Access Id", "Owner Id",
            $headers=array(
                 "Owner Name", "Owner Mobile", "Unit Number", "Floor", "Access Code", "Access Type", "Issued By", "Date Used"
            );
            foreach($accessLogs as $row) {
                // Add a header row if it hasn't been added yet
                if(!$headerDisplayed) {
                    // Use the headers as the titles
                    fputcsv($fopen, $headers);
                    
                    // Use the keys from $row as the titles
                    //  fputcsv($fopen, array_keys($row));
                    $headerDisplayed = true;
                }
                
                $usageTypes=array("1"=>"Single", "2"=>"Multiple");
//                    $row['log_id'],
//                    $row['access_id'],
//                    $row['owner_id'],
                $dataRow=array(
                    $row['owner_name'],
                    $row['owner_mobile'],
                    json_decode($row['owner_info'])->unit_number,
                    json_decode($row['owner_info'])->floor,
                    $row['access_code'],
                    $usageTypes[$row['usage_type']],
                    $row['admin_id'] ? "Admin" : "Owner",
                    $row['date_added']
                );
                
                // Put the row into the stream
                fputcsv($fopen, $dataRow);
            }
            // Close the file
            fclose($fopen);

            $downloadPath=EXPORT_ACCESS_LOGS_SERVER_PATH.$filename;
            if(file_exists($filePath))
            return new JsonModel($this->createResponse(200, "success", "success", $downloadPath));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function createResponse($status=400, $title="bad_request", $message=BAD_REQUEST, $data=NULL) {
        $response = array(
            'status'        =>  "$status",
            'title'         =>  $title,
            'message'       =>  $message,
            'response'      =>  $data
        );
        return $response;
    }

    public function validateDigits($value, $label, $min=1, $max=false, $minValue=false, $maxValue=false) {
        // min max length
        if($min && strlen($value)<$min) {
            return $response=$this->createResponse(400, 'short',  "$label should be at least $min digit long.");
        }
        if($max && strlen($value)>$max) {
            return $response=$this->createResponse(400, 'big', "$label should be at most $max digit long.");
        }

        // min max value
        if($minValue && $value<$minValue) {
            return $response=$this->createResponse(400, 'min', "$label should be greater or equal to $minValue");
        }
        if($maxValue && $value>$maxValue) {
            return $response=$this->createResponse(400, 'max', "$label should be less or equal to $minValue");
        }

        // valid digits
        $validator = new digits();
        if($validator->isValid($value)) {
            return $response=$this->createResponse(200, 'valid', "$label is valid");
        }

        return $response=$this->createResponse(400, 'invalid', "$label is invalid, use only digits");
    }
    
    public function crypto_rand_secure($min, $max) {
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }

    public function generateSecretKey($length=10) {
        $scretKey = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet);

        for ($i=0; $i < $length; $i++) {
            $scretKey .= $codeAlphabet[$this->crypto_rand_secure(0, $max)];
        }
        
        return $scretKey;
    }
    
    protected $_configsTable;
    public function _getConfigsTable() {
        if(!$this->_configsTable) {
            $sm=$this->getServiceLocator();
            $this->_configsTable=$sm->get("Admin\Model\ConfigsTable");
        }
        return $this->_configsTable;
    }

    protected $_accessCodesTable;
    public function _getAccessCodesTable() {
        if(!$this->_accessCodesTable) {
            $sm=$this->getServiceLocator();
            $this->_accessCodesTable=$sm->get("Admin\Model\AccessCodesTable");
        }
        return $this->_accessCodesTable;
    }

    protected $_accessLogsTable;
    public function _getAccessLogsTable() {
        if(!$this->_accessLogsTable) {
            $sm=$this->getServiceLocator();
            $this->_accessLogsTable=$sm->get("Admin\Model\AccessLogsTable");
        }
        return $this->_accessLogsTable;
    }    
    
    protected $_timeProfilesTable;
    public function _getTimeProfilesTable() {
        if(!$this->_timeProfilesTable) {
            $sm=$this->getServiceLocator();
            $this->_timeProfilesTable=$sm->get("Admin\Model\TimeProfilesTable");
        }
        return $this->_timeProfilesTable;
    }  
    
}
