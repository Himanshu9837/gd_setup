<?php
/**
 * Zend Framework (http://framework.zend.com/)
 * This class is used for Login, Log out, Forgot Password and Change Password.
 */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\AbstractRestfulController as RestfulController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Admin\Form as AdminForms;
use Zend\Mail as Mail;
use Zend\Mvc\MvcEvent;
//use Zend\Mail\Message;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\View\Model\JsonModel;

use Admin\Form\InboundsAddForm;
use Admin\Form\Validator\InboundsAddFilter;

use Admin\Form\OutboundsAddForm;

use Admin\Model\AsteriksList;
use Admin\Model\Inbounds;
use Admin\Model\Outbounds;
use Admin\Model\Trunks;
use phpseclib\Net\SSH2;

use Admin\Model\OwnerType;

class IndexController extends AbstractActionController
{
    protected $em;
    protected $authservice;
	
    /* This function is like old init() */
    public function onDispatch(MvcEvent $e)
    {
       $this->layout('layout/adminlayout');
        return parent::onDispatch($e);
    }
    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }
    
    public function getEntityManager()
    {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }
    
    public function getAuthService()
    {
        if (! $this->authservice) {
            $this->authservice = $this->getServiceLocator()
                                      ->get('AuthService');
        }
         
        return $this->authservice;
    }
    
    public function indexAction()
    {
        return new ViewModel();
    }
	
   /**
     * This function is used for admin login
     * 
     * @author          Nishikant
     * @param           ---------
     * @return          mixed $result
     * @created_date    9thNov, 2016
     * @modified_date   --------------
     */
    public function loginAction()
    {  
	$this->layout('layout/layout-login');
	/* Check if Admin is already logged-in */
	$admin_session = new Container('admin');
        $username = $admin_session->username;
	if(!empty($username)) {
		return $this->redirect()->toRoute('admindashboard');		
	}
    $error = "";
	$form = new AdminForms\LoginForm();
	$request = $this->getRequest();
	if($request->isPost()) 
	{
        /* If request method is Post */
            $formValidator = new AdminForms\Validator\LoginFormValidator();	
            /* Login Form Validator Object */
            $form->setInputFilter($formValidator->getInputFilter()); /*Apply filter to form*/
            $form->setData($request->getPost()); /* set post data to form */
            $data = $this->getRequest()->getPost(); /* Fetch Post Variables */
            
            // check if form is valid
            if ($form->isValid()) {
                $em = $this->getEntityManager(); /* Call Entity Manager */
                /* Verify Admin Credentials */                 
                $results   = $em->getRepository('Admin\Entity\GdAdmin')->verifyAdmin( $data );            
                if(!empty($results)) {
                        $admin_session = new Container('admin');			
                        $admin_session->userId = $results[0]['id'];
                        $admin_session->username = $results[0]['email']; 
                        $admin_session->usertype = $results[0]['userType'];
                        $admin_session->firstname = $results[0]['firstName'];                                                      
                        $flashMessenger = $this->flashMessenger();
                        $flashMessenger->setNamespace('success');
                        $flashMessenger->addMessage("You have been logged in successfully.");
                        $url = $this->getRequest()->getHeader('Referer')->getUri();                          
                        $this->redirect()->toUrl($url); 
                } else {
                    $error = 'Sorry! You have entered an incorrect email or password. Please enter correct login details to proceed';                    
                }
            }
        }
        return new ViewModel(array(
                'error' => $error,
                'form' => $form,
        ));
    }
	
    /**
     * This function is used to logout admin.
     *
     * @author        Nishikant
     * @created_date  10th Nov, 2016
     */
    
    public function logoutAction () {
        $admin_session = new Container('admin');
        $username = $admin_session->username;
        if(!empty($username)) {
                $admin_session->getManager()->getStorage()->clear('admin');
        }
        return $this->redirect()->toRoute('adminlogin');	
    }
	
    /**
     *  This function is used for Dashboard just after login.
     * 
     * @author        Nishikant
     * @return        Zend\View\Model\ViewModel
     * @created_date  9th Nov, 2016
     * @modified_date -------------
     */
    public function dashboardAction(){
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');
        $maxUserlimit = $em->getRepository('Admin\Entity\GdUserlimit')->userlimitRecord();
        $existingUser = $em->getRepository('Admin\Entity\GdFlatowner')->totalusers();
        $existingUser=$existingUser[0]['totalmember'];
        $maxUserlimit=$maxUserlimit['maxUser'];
        $this->layout()->pageTitle = 'Grand Ducos | Admin'; /* Setting page title */
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        // get owner type
        $PM=$this->_getOwnerTypeTable()->getByType(1);
        $PD=$this->_getOwnerTypeTable()->getByType(2);

        return new ViewModel(array(
            'maxuserlimit' => $maxUserlimit,
            'existeduser' => $existingUser,
            'pManager' => $PM,
            'pDelivery' => $PD
        ));
    }
	 
	/**  
    * Description Used for ajaxname listing
	*
    * @author     Nishikant
    * @return     Json Data for Ajax Request
    */
      public function ajaxdashboardAction(){
        $em = $this->getEntityManager();
        $basePath = $this->getRequest()->getBasePath();
        $request = $this->getRequest(); 
        $uri = $this->getRequest()->getUri();
        $basePath = BASE_URL ;
        $ChangeStatusUrl = $this->url()->fromRoute('userchangestatus');		
        $ownerDeleteUrl = $this->url()->fromRoute('ownerdelete');		
        $sqlArr['searchKey'] =  $request->getQuery('sSearch');
        $sqlArr['sortcolumn'] =  $request->getQuery('iSortCol_0');        
        $sqlArr['sorttype'] =  $request->getQuery('sSortDir_0');    // desc or asc 
        $sqlArr['iDisplayStart'] =  $request->getQuery('iDisplayStart');  // offset
        $sqlArr['sEcho'] =  $request->getQuery('sEcho');
        $sqlArr['limit'] =  $request->getQuery('iDisplayLength'); 	
	$flatownerData   = $em->getRepository('Admin\Entity\GdFlatowner')->getOwnerListingAtAdminForDataTable($sqlArr, $basePath, $ownerDeleteUrl, $ChangeStatusUrl);        
        echo json_encode($flatownerData);
        exit();
    }      
    /**
     * This function is used for checkAdminLoggedInOrNot
     * 
     * @author        Nishikant
     * @return        Zend\View\Model\ViewModel
     * @created_date  9th Nov, 2016
     * @modified_date -------------
     */
    public function checkAdminLoggedInOrNot(){
         /* checking if user logged in or not */
        $admin_session = new Container('admin');
        $username = $admin_session->username;
        if(empty($username)) {
            /* if not logged in redirect the user to login page */
            return $this->redirect()->toRoute('adminlogin');
        }
    }
    
    
    /**
     * This function is used for Admin to view Profile or Edit
     * 
     * @author        Nishikant
     * @return        Zend\View\Model\ViewModel
     * @created_date  10th Nov, 2016
     * @modified_date 10th Nov, 2016
     */
    public function adminprofileAction() {
       	$em = $this->getEntityManager();   
       
        $this->layout('layout/adminlayout');
        
        $admin_session = new Container('admin');
        $username = $admin_session->username;
        $id = $admin_session->userId;
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        
        $this->layout()->pageTitle = 'My Profile';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;
        
        $this->checkAdminLoggedInOrNot();
                
       
        $adminData = $em->getRepository('Admin\Entity\GdAdmin')->findOneBy(array('email'=>$username));
        /*  Form */
        $form = new AdminForms\AdminProfileForm();        
        $FirstName = $adminData->getFirstName();
        $LastName = $adminData->getLastName();
        $Email = $adminData->getEmail();
        $Phone = $adminData-> getPhone();
        $gender = $adminData-> getGender();
        
        if(!empty($adminData)){ 
            $form->get('firstname')->setValue($FirstName);
            $form->get('lastname')->setValue($LastName);
            $form->get('email')->setValue($Email);
            $form->get('mobile')->setValue($Phone);
            $form->get('gender')->setValue($gender);
            $request = $this->getRequest();
             if($request->isPost()){
                $formData = $request->getPost();
                $formValidator = new AdminForms\Validator\AdminProfileFormValidator();
                $form->setInputFilter($formValidator->getInputFilter());                
                $form->setData($request->getPost()); 
                if ($form->isValid()) { 
                    $currentDate = date_create(date('Y-m-d H:i:s'));                            
                    $adminObj = $em->getRepository('Admin\Entity\GdAdmin')->find($id);
                    $adminObj->setFirstName($formData['firstname']);
                    $adminObj->setLastName($formData['lastname']);
                    $adminObj->setEmail($formData['email']);
                    $adminObj->setPhone($formData['mobile']);
                    $adminObj->setGender($formData['gender']);              
                    $em->persist($adminObj);
                    $em->flush();                    
                    $flashMessenger = $this->flashMessenger();
                    $flashMessenger->setNamespace('success');                
                    $msg = "Profile has been updated successfully!";                
                    $flashMessenger->addMessage($msg);                
                    return $this->redirect()->toRoute('admindashboard');
                }
             }
        }
        return array(
                'form' => $form,
                'profileid'=> $id,
                'success'=>$msg,
                'usertype' => $usertype
            );           
    }

     /**
     * This function is used for Updatinf End call time
     * 
     * @author        Nishikant
     * @return        Zend\View\Model\ViewModel
     * @created_date  10th Nov, 2016
     * @modified_date 10th Nov, 2016
     */
    
    public function timeAction() {
         $em = $this->getEntityManager();
         
        $admin_session = new Container('admin');
        $username = $admin_session->username;
        $id = $admin_session->userId;
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        
        $this->layout()->pageTitle = 'Set Call time';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;
        
        
        if($usertype == 2) {
            return $this->redirect()->toRoute('admindashboard');
            $flashMessenger = $this->flashMessenger();
            $flashMessenger->setNamespace('Not Authorised');
            $msg = "You are not authorised to access";
            $flashMessenger->addMessage($msg);
        }

         $id = 1;
         $userObj = $em->getRepository('Admin\Entity\GdCallendtime')->find($id);     
         if ((!empty($userObj)) && isset($userObj)) { 
             $endtme = $userObj->getCallEndTime();
             $endtm = $this->endtime = $endtme;
             $request = $this->getRequest();       
             $tm = $_POST['clendtm'];
                 if ($request->isPost()) {
                     $userObj->setCallEndTime($tm);
                     $em->persist($userObj);
                     $em->flush(); 
                     $flashMessenger = $this->flashMessenger();
                     $flashMessenger->setNamespace('success');
                     $msg = "Call End Time Updated Successfully!!";
                     $flashMessenger->addMessage($msg);
                     return $this->redirect()->toRoute('admindashboard');
                 }
         }         
          return array('endtime' => $endtm );
     }

    public function dbexportAction(){        
        $em = $this->getEntityManager();        
        $connection= $em->getConnection();        
        $sm = $connection->getSchemaManager();        
        $tables = $sm->listTableNames();
               
        $con = mysqli_connect(DBHOST, DBUSERNAME, DBPASS,DBNAME); 
		
        //cycle through 
        foreach($tables as $table)
        {
          $result = mysqli_query($con,'SELECT * FROM '.$table);
          $num_fields = mysqli_num_fields($result);
              $fields = mysqli_query($con,"SHOW COLUMNS FROM" .' '.$table);
              if (mysqli_num_rows($resulc) > 0) {
				//  print_r(mysqli_fetch_assoc($fields));
                   while ($rowc = mysqli_fetch_assoc($fields)) {
                       $columns .='`'.$rowc['Field'].'`,';
                   }
               }        
          $return.= 'DROP TABLE IF EXISTS '.'`'.$table.'`'.';';
          $row2 = mysqli_fetch_assoc(mysqli_query($con,'SHOW CREATE TABLE '.$table));
		  
          $return.= "\n\n".$row2['Create Table'].";\n\n";
          for ($i = 0; $i < $num_fields; $i++) 
          {
			while($row = mysqli_fetch_assoc($result))
				{ 
				  $return.= 'INSERT INTO '.'`'.$table.'`'.' ('.$columns.' ) VALUES(';
				 
				  foreach($row as $key => $value) 
				  {	
					if ($value) { 
						$return.= '\''.$value.'\'' ; 
					} 
					else { 
						if($value==0)
							$return.= '\''.$value.'\'' ; 
						else
							$return.= '""'; 
					}
					if($key!='modified_date')
						$return.= ',';
				  }
				  $return.= ");\n";
				}
          }
          $return.="\n\n\n";
        }
		// exit;
        //save file
        $pub = $_SERVER['DOCUMENT_ROOT'] .'/expdb'; 
       // $pub = $_SERVER['DOCUMENT_ROOT'] .'/mysqldump'; 
		$filename = '/gbexb_'.time().'.sql';
        $handle = fopen($pub .$filename,'w');  
        fwrite($handle,$return); 
        fclose($handle);
        $viewModel = new ViewModel();        
        $viewModel->setTerminal(true);        
        //$downloadfilename = BASE_URL . '/mysqldump/granddunes_'.time().'.sql';        
        $downloadfilename = BASE_URL . '/expdb_'.$filename;        
        $sfile = array('status' => 'success',  'file' => $downloadfilename);        
        return new JsonModel($sfile);        
        exit;
    }
	
    public function dbbackupAction(){        
        $em = $this->getEntityManager();        
        $this->layout()->pageTitle = 'DB Backup'; 
       	$admin_session = new Container('admin');
        $username = $admin_session->username;
        $id = $admin_session->userId;
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;
	
	if($usertype == 2) {
            return $this->redirect()->toRoute('admindashboard');
            $flashMessenger = $this->flashMessenger();
            $flashMessenger->setNamespace('Not Authorised');
            $msg = "You are not authorised to access";
            $flashMessenger->addMessage($msg);
        }
	      
        // Import sql to database        
        $fileTemp = $_FILES['myFile']['tmp_name'];        
        if($fileTemp){            
          $path_parts = pathinfo($_FILES["myFile"]["name"]);            
          $extension = $path_parts['extension'];            
          $filename = '.gbimp_'.time().'.sql';          
          $fpath = "expdb/" .$filename;            
          $folderuploadPath = DOCUMENT_ROOT_PATH . $fpath;    
           move_uploaded_file($fileTemp, $folderuploadPath);                        
           // $filename = $_SERVER['DOCUMENT_ROOT'] . '/expdb/granddunes_'.time().'.sql';            
           $templine = '';            
           $lines = file($folderuploadPath);            
           foreach ($lines as $line) {            
               // Skip it if it's a comment                
               if (substr($line, 0, 2) == '--' || $line == '')                    
                continue;               
                $templine .= $line;                
                // If it has a semicolon at the end, it's the end of the query                
                    if (substr(trim($line), -1, 1) == ';') {                    
                      // Perform the query                                  
                        $stmt = $em->getConnection()->prepare($templine);                   
                        $stmt->execute();                   
                        // Reset temp variable to empty                   
                        $templine = '';               
                    }            
            }        
          }
        // die;       
       //$viewModel = new ViewModel(array('success' => 'Imported well'));
       //$viewModel->setTerminal(true);
       //return $viewModel;    
      
     }
     
    public function exportUsersAction() {  
        
        $em = $this->getEntityManager();  
        $request = $this->getRequest();
        if($request->isPost()) {
            $users = $em->getRepository('Admin\Entity\GdFlatowner')->getOwnerListing([], 0);
            if(!$users)
            return new JsonModel($this->createResponse(400, "no_user", "No user to export."));

          //   echo "<pre>";
          //   print_r($users);
         //   die();

            $filename="users_" . $this->generateSecretKey() . ".csv";
            $filePath=EXPORT_USER_UPLOAD_PATH.$filename;
            $fopen=fopen($filePath, 'w');

            $headerDisplayed=false;
            $headers=array(
                "First Name", "Last Name", "Email", "Mobile", "Unit Number", "Floor", "Follow Me1", "Follow Me2", "Follow Me3", "Follow Me4", "Follow Me5", "Ring Type", "Confirm Call", "Date Added");
            
            foreach($users as $row) {
                // Add a header row if it hasn't been added yet
                if(!$headerDisplayed) {
                    // Use the headers as the titles
                      fputcsv($fopen, $headers);
//                    fputcsv($fopen, array_keys($row));
                    $headerDisplayed = true;
                }
                
                $dataRow=array(
                    $row['firstName'],
                    $row['lastName'],
                    $row['email'],
                    $row['mobile'],
                    $row['unitNumber'],
                    $row['floor'],
                    $row['followme1'],
                    $row['followme2'],
                    $row['followme3'],
                    $row['followme4'],
                    $row['followme5'],
                    $row['ringType'],
                    $row['confirmcall'],
                    $row['createdDate']
                );
                
                // Put the row into the stream
                fputcsv($fopen, $dataRow);
            }
            // Close the file
            fclose($fopen);

            $downloadPath=EXPORT_USER_SERVER_PATH.$filename;
            if(file_exists($filePath))
            return new JsonModel($this->createResponse(200, "success", "success", $downloadPath));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }
    
    public function crypto_rand_secure($min, $max) {
           $range = $max - $min;
           if ($range < 1) return $min; // not so random...
           $log = ceil(log($range, 2));
           $bytes = (int) ($log / 8) + 1; // length in bytes
           $bits = (int) $log + 1; // length in bits
           $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
           do {
               $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
               $rnd = $rnd & $filter; // discard irrelevant bits
           } while ($rnd >= $range);
           return $min + $rnd;
       }

    public function generateSecretKey($length=10) {
        $scretKey = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet);

        for ($i=0; $i < $length; $i++) {
            $scretKey .= $codeAlphabet[$this->crypto_rand_secure(0, $max)];
        }
        
        return $scretKey;
    }

    // BY ABAR

    public function __construct() {
        // for inbounds
        $this->irFields=array(
            "description"=>"Description",
            "extension"=>"DID Number",
            "cidnum"=>"CID Number",
            "pricid"=>"CID Priority Route",
            "alertinfo"=>"Alert Info",
            "rvolume"=>"Ringer Volume Override",
            "grppre"=>"CID name prefix",
            "mohclass"=>"Music On Hold",
            "destination"=>"Destination",
            "privacyman"=>"Privacy Manager",
            "pmmaxretries"=>"Max Attempts",
            "pmminlength"=>"Min Length"
        );

        $this->irRequired=array(
            "description"=>false,
            "extension"=>true,
            "cidnum"=>true,
            "pricid"=>false,
            "alertinfo"=>false,
            "rvolume"=>false,
            "grppre"=>false,
            "mohclass"=>false,
            "destination"=>true,
            "privacyman"=>false,
            "pmmaxretries"=>false,
            "pmminlength"=>false
        );

        $this->irDefaults=array(
            "pricid"=>0,
            "rvolume"=>0,
            "mohclass"=>0,
            "privacyman"=>0,
            "pmmaxretries"=>3,
            "pmminlength"=>10
        );

        // for outbounds
        $this->orFields=array(
            "route_id"=>"Route ID",
            "routename"=>"Route Name",
            "outcid"=>"Route CID",
            "outcid_mode"=>"Override Extension",
            "routepass"=>"Route Password",
            "mohsilence"=>"Music on Hold",
            "emergency"=>"Emergency",
            "intracompany"=>"Intra-Company",
            "route_seq"=>"Route Position",
            "trunkpriority"=>"Trunk Sequence",
            "destination"=>"Optional Destination"
        );

        $this->orRequired=array(
            "routename"=>true,
            "outcid"=>false,
            "outcid_mode"=>false,
            "routepass"=>false,
            "mohsilence"=>false,
            "emergency"=>false,
            "intracompany"=>false,
            "route_seq"=>false,
            "trunkpriority"=>false,
            "destination"=>false
        );
    }

    public function syncData($page) {
        if($page=="inbound") {
            $action="inbound_route.php";
            $type=1;
        } elseif($page=="outbound") {
            $action="outbound_route.php";
            $type=2;
        } elseif($page=="trunk") {
            $action="trunk.php";
            $type=3;
        } else return false;

        $asterisk=new AsterisksController();
        $data=$asterisk->getList($action);

        $data=addcslashes(json_encode($data), "'");

        $inboundData=array("type_id"=>$type, "data"=>$data);
        $inboundEntity=new AsteriksList();
        $inboundEntity->exchangeArray($inboundData);
        $this->_getListsTable()->insertOrUpdate($inboundEntity);

        return true;
    }

    public function inboundsSyncAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost()->toArray();

            $this->syncData("inbound"); // update records

            return new JsonModel($this->createResponse(200, "success", "Syncing successful."));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function screenSaverAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        // get files
        $data=scandir(SCREEN_SAVER_UPLOAD_PATH);
        $remove=array(".", "..");
        $data=array_diff($data, $remove); // remove default unwanted directory data
        $data=array_merge($data); // re-index

        // print_r($data);
        // die();

        // get slideshow timeout from db
        $playTime=$this->_getOwnerTypeTable()->getTimeOut(3);
        $playTime=$playTime ? $playTime : 2;

        return new ViewModel(array(
            'totalFiles' => count($data),
            'records'=>$data,
            "playTime"=>$playTime
        ));
    }
	
    public function screenSaverUploadAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost()->toArray();
            $mode=@$data['mode'];
            if(!$mode || !in_array($mode, array("landscape", "portraits")))
            return new JsonModel($this->createResponse(400, "no_mode", "Please choose the correct file mode."));

            $file=@$_FILES['file'];
            if(!$file) return new JsonModel($this->createResponse(400, "no_file", "Please choose the file to upload."));

            $filename=@trim($file['name']);
            if(!$filename) return new JsonModel($this->createResponse(400, "no_filename", "Please choose the correct filename."));

            // make the name with mode
            $filename=$mode."_".$filename;

            $path=SCREEN_SAVER_UPLOAD_PATH;
            $filePath=$path.$filename;

            // check file alredy exists
            if(file_exists($filePath))
            return new JsonModel($this->createResponse(400, "exists", "File with the same name already exists."));

            // upload now
            if($file['size']>0 && $file['error']!=4) {
                // check image extensions
                $validator = new \Zend\Validator\File\Extension("jpg,jpeg,gif,png");
                if(!$validator->isValid($file)) {
                    return new JsonModel($this->createResponse(400, "invalid", "Only JPG, GIF and PNG files are allowed."));
                }

                if(!move_uploaded_file($file['tmp_name'], $filePath))
                return new JsonModel($this->createResponse(400, "not_uploaded", "File not uploaded, please try again."));

                return new JsonModel($this->createResponse(200, "success", "File uploaded successfully."));
            }

            return new JsonModel($this->createResponse(400, "error", "File contains error, please choose the correct file."));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function screenSaverViewAction() {
        $file=@$_GET['f'];
        if(!$file) die("File not found.");

        $path=SCREEN_SAVER_UPLOAD_PATH."$file";

        if(!file_exists($path))
        die("File not found, it might be deleted.");

        $serverPath=SCREEN_SAVER_SERVER_PATH."$file";

        $this->layout('layout/blank');
        return new ViewModel(array(
            'records' => $serverPath
        ));
    }

    public function screenSaverDeleteAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost()->toArray();

            $type=@$data['type'];
            $file=@$data['name'];

            if(!$type) return new JsonModel($this->createResponse(400, "wrong_data", WRONG_DATA));
            elseif($type==1) {
                if(!$file)
                return new JsonModel($this->createResponse(400, "wrong_data", WRONG_DATA));

                // check file exists
                $path=SCREEN_SAVER_UPLOAD_PATH."$file";
                if(!file_exists($path))
                return new JsonModel($this->createResponse(400, "not_found", "File not found, it might be already deleted."));

                // delete now
                unlink($path);

                // check deleted
                if(file_exists($path))
                return new JsonModel($this->createResponse(400, "found", "Failed, please try again."));

                // success
                return new JsonModel($this->createResponse(200, "success", "File Deleted!"));
            }
            elseif($type==2) {
                // remove all files
                $files=glob(SCREEN_SAVER_UPLOAD_PATH.'*'); // fetch all files

                foreach($files as $file) {
                    if(is_file($file)) unlink($file);
                }

                // success
                return new JsonModel($this->createResponse(200, "success", "File Deleted!"));
            }

            return new JsonModel($this->createResponse(400, "wrong_data", WRONG_DATA));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function screenSaverPlayTimeAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost()->toArray();

            $playTime=@$data['play_time'];

            if(!$playTime) return new JsonModel($this->createResponse(400, "wrong_data", "Play time is required."));
            elseif(!is_numeric($playTime)) return new JsonModel($this->createResponse(400, "non_numeric", "Play time must be greater than 0."));
            
            // insert in owner_type table
            $typeData=array(
                "owner_type"=>3, "owner_id"=>$playTime
            );
            $entity=new OwnerType();
            $entity->exchangeArray($typeData);
            $this->_getOwnerTypeTable()->insertOrUpdate($entity);

            return new JsonModel($this->createResponse(200, "success", "Play time updated successfully."));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function inboundsAction() {
        $em = $this->getEntityManager();
       

        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        $data=$this->_getInboundsTable()->getList();

        // echo "<pre>";
        // print_r($data);
        // die();

        return new ViewModel(array(
            'records' => $data
        ));
    }

    public function inboundsAddAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        $ringVolumes=array("None",1,2,3,4,5,6,7,8,9,10,11,12,13,14);
        $musicOnHolds=array("Default", "None");

        //$form=new InboundsAddForm($ringVolumes, $musicOnHolds);

        return new ViewModel(array(
            "ringVolumes"=>$ringVolumes
        ));
    }

    public function inboundsSaveAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost()->toArray();

            $extension=@trim($data['extension']);
            $cidnum=@trim($data['cidnum']);
            $pricid=@trim($data['pricid']);
            $alertinfo=@trim($data['alertinfo']);
            $rvolume=@trim($data['rvolume']);
            $grppre=@trim($data['grppre']);
            $mohclass=@trim($data['mohclass']);
            $destination=@trim($data['destination']);
            $privacyman=@trim($data['privacyman']);
            $pmmaxretries=@trim($data['pmmaxretries']);
            $pmminlength=@trim($data['pmminlength']);
            $description=@trim($data['description']);
            

            $extension=preg_replace('/\s+/', '', $extension);
            $cidnum=preg_replace('/\s+/', '', $cidnum);

            // check extension already exists
            $found=$this->_getInboundsTable()->getByExt($extension);
            if($found)
            return new JsonModel($this->createResponse(400, "exists", "Did number already exists."));

            // validate
            foreach($this->irRequired as $key=>$value) {
                if($value && !$$key) {
                    $message=$this->irFields[$key]." is required.";
                    return new JsonModel($this->createResponse(400, "required", $message));
                }
            }

            ##### defaults
            if(!$pricid) $pricid=0;
            if(!$rvolume) $rvolume=0;
            $mohclass=$mohclass ? 1 : 0;

            $privacyman=$privacyman ? 1 : 0;

            if(!$pmmaxretries) 3;
            if(!$pmminlength) 10;
            ###############

            // add to db
            $dbData=array(
                "did_number"=>$extension,
                "cid_number"=>$cidnum,
                "cid_priority_route"=>$pricid,
                "alert_info"=>$alertinfo,
                "ringer_volume"=>$rvolume,
                "cid_name_prefix"=>$grppre,
                "music_on_hold"=>$mohclass,
                "set_destination"=>$destination,
                "privacy_manager"=>$privacyman,
                "max_attempts"=>$pmmaxretries,
                "min_length"=>$pmminlength,
                "description"=>$description,
            );
            $entity=new Inbounds();
            $entity->exchangeArray($dbData);
            $inboundId=$this->_getInboundsTable()->addInbound($entity);

            if(!$inboundId)
            return new JsonModel($this->createResponse(400, "db_error", "Not inserted, please try again."));

            // add to Asterisk
            $inputData=array();
            $inputData[] = array(
                "DID Number - Extension"=>$extension,
                "CallerID Number"=>$cidnum,
                "CID Priority Route"=>$pricid,
                "Alert Info"=>$alertinfo,
                "Ringer Volume Override"=>$rvolume,
                "CID name prefix "=>$grppre,
                "Music On Hold"=>$mohclass,
                "Set Destination"=>$destination,
                "Privacy Manager"=>$privacyman,
                "Max attempts"=>$pmmaxretries,
                "Min Length"=>$pmminlength,
                "Description"=>$description
            );

            $asterisk=new AsterisksController();
            @list($response)=$asterisk->addInbound($inputData);

            $status=@$response['status'];
            $statusMsg=@$response['msg'];
            $responseMsg=$statusMsg ? $statusMsg : SOME_ERROR;

            // if success
            if($statusMsg=="Addition Done!") {
                //$this->syncData("inbound"); // update records
                return new JsonModel($this->createResponse(200, "success", $responseMsg));
            }

            // return error
            return new JsonModel($this->createResponse(400, "not_added", $responseMsg));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function inboundsEditAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        $ringVolumes=array("None",1,2,3,4,5,6,7,8,9,10,11,12,13,14);
        $musicOnHolds=array("Default", "None");

        // get inbound info
        $extension=@trim($_GET['ext']);
        if(!$extension) $this->redirect()->toUrl("/admin/inbounds");

        $inbound=$this->_getInboundsTable()->getByExt($extension);
        if(!$inbound) $this->redirect()->toUrl("/admin/inbounds");

        return new ViewModel(array(
            "ringVolumes"=>$ringVolumes,
            "inbound"=>$inbound
        ));
    }

    public function inboundsUpdateAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost()->toArray();

            // echo "<pre>";
            // print_r($data);
            // die();

            $extension=@trim($data['extension']);
            $cidnum=@trim($data['cidnum']);
            $pricid=@trim($data['pricid']);
            $alertinfo=@trim($data['alertinfo']);
            $rvolume=@trim($data['rvolume']);
            $grppre=@trim($data['grppre']);
            $mohclass=@trim($data['mohclass']);
            $destination=@trim($data['destination']);
            $privacyman=@trim($data['privacyman']);
            $pmmaxretries=@trim($data['pmmaxretries']);
            $pmminlength=@trim($data['pmminlength']);
            $description=@trim($data['description']);

            $extension=preg_replace('/\s+/', '', $extension);
            $cidnum=preg_replace('/\s+/', '', $cidnum);

            // check extension exists
            $found=$this->_getInboundsTable()->getByExt($extension);
            if(!$found)
            return new JsonModel($this->createResponse(400, "not_exists", "Did number not exists."));

            // validate
            foreach($this->irRequired as $key=>$value) {
                if($value && !$$key) {
                    $message=$this->irFields[$key]." is required.";
                    return new JsonModel($this->createResponse(400, "required", $message));
                }
            }

            ##### defaults
            if(!$pricid) $pricid=0;
            if(!$rvolume) $rvolume=0;
            $mohclass=$mohclass ? 1 : 0;

            $privacyman=$privacyman ? 1 : 0;

            if(!$pmmaxretries) 3;
            if(!$pmminlength) 10;
            ###############

            // update to db
            $dbData=array(
                "row_id"=>$found['row_id'],
                "did_number"=>$extension,
                "cid_number"=>$cidnum,
                "cid_priority_route"=>$pricid,
                "alert_info"=>$alertinfo,
                "ringer_volume"=>$rvolume,
                "cid_name_prefix"=>$grppre,
                "music_on_hold"=>$mohclass,
                "set_destination"=>$destination,
                "privacy_manager"=>$privacyman,
                "max_attempts"=>$pmmaxretries,
                "min_length"=>$pmminlength,
                "description"=>$description,
                "date_added"=>$found['date_added']
            );
            $entity=new Inbounds();
            $entity->exchangeArray($dbData);
            $updated=$this->_getInboundsTable()->updateInbound($entity);

            if(!$updated)
            return new JsonModel($this->createResponse(400, "db_error", "Not updated, please try again."));

            $inputData=array();
            $inputData[] = array(
                "DID Number - Extension"=>$extension,
                "CallerID Number"=>$cidnum,
                "CID Priority Route"=>$pricid,
                "Alert Info"=>$alertinfo,
                "Ringer Volume Override"=>$rvolume,
                "CID name prefix "=>$grppre,
                "Music On Hold"=>$mohclass,
                "Set Destination"=>$destination,
                "Privacy Manager"=>$privacyman,
                "Max attempts"=>$pmmaxretries,
                "Min Length"=>$pmminlength,
                "Description"=>$description
            );

            // print_r($inputData);
            // die();

            $asterisk=new AsterisksController();
            @list($response)=$asterisk->updateInbound($inputData);

            $status=@$response['status'];
            $statusMsg=@$response['msg'];
            $responseMsg=$statusMsg ? $statusMsg : SOME_ERROR;

            // if success
            if($statusMsg=="Updation Done!") {
                //$this->syncData("inbound"); // update records
                return new JsonModel($this->createResponse(200, "success", $responseMsg));
            }

            // return error
            return new JsonModel($this->createResponse(400, "not_updated", $responseMsg));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function inboundsDeleteAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost()->toArray();

            if(!isset($data['did_number']))
            return new JsonModel($this->createResponse(400, "wrong_data", WRONG_DATA));

            $extension=$data['did_number'];
            

            $inputData=array();
            $inputData[] = array(
                'DID Number'=>$extension
            );

            $asterisk=new AsterisksController();
            @list($response)=$asterisk->deleteInbound($inputData);

            $status=@$response['status'];
            $statusMsg=@$response['msg'];
            $responseMsg=$statusMsg ? $statusMsg : SOME_ERROR;

            // if success
            if($statusMsg=="Delete Data!") {
                //$this->syncData("inbound"); // update records

                // delete from db also
                $this->_getInboundsTable()->removeInbound($extension);

                return new JsonModel($this->createResponse(200, "success", "Data Deleted!"));
            }

            // return error
            return new JsonModel($this->createResponse(400, "not_deleted", $responseMsg));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    // OUTBOUND ROUTES

    public function outboundsSyncAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost()->toArray();

            $this->syncData("outbound"); // update records

            return new JsonModel($this->createResponse(200, "success", "Syncing successful."));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function outboundsAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        $data=$this->_getOutboundsTable()->getList();

        // echo "<pre>";
        // print_r($data);
        // die();

        return new ViewModel(array(
            'records' => $data
        ));
    }

    public function outboundsAddAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        $musicOnHolds=array("Default", "None");
        $routePositions=array("Select", "First Before", "No Change", "Last After");
        $trunkSequences=array("Select", "Trunk Sequence1", "Trunk Sequence2", "Trunk Sequence3");

        //$form=new OutboundsAddForm($musicOnHolds, $routePositions, $trunkSequences);

        return new ViewModel(array(
            
        ));
    }

    public function outboundsSaveAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost()->toArray();

            $routename=@trim($data['routename']);
            $outcid=@trim($data['outcid']);
            $outcid_mode=@trim($data['outcid_mode']);
            $routepass=@$data['routepass'];
            $mohsilence=@trim($data['mohsilence']);
            //$emergency=@trim($data['emergency']);
            //$intracompany=@trim($data['intracompany']);
            $emergency= !empty(@trim($data['route_type'])) && ($data['route_type'] == 'emergency') ? true : false ;
            $intracompany= !empty(@trim($data['route_type'])) && ($data['route_type'] == 'intracompany') ? true : false ;

            $route_seq=@trim($data['route_seq']);
            $trunkpriority=$data['trunkpriority'];
            $destination=@trim($data['destination']);
            

            // validate
            foreach($this->orRequired as $key=>$value) {
                if($value && !$$key) {
                    $message=$this->orFields[$key]." is required.";
                    return new JsonModel($this->createResponse(400, "required", $message));
                }
            }

            // for trunk sequence
            $trunkData=array();
            $trunkData[]=trim($trunkpriority[0]);
            $trunkData[]=trim($trunkpriority[1]);
            $trunkData[]=trim($trunkpriority[2]);

            if(!$trunkData[0])
            return new JsonModel($this->createResponse(400, "required", "Trunk sequence 1 is required."));

            // for dial pattern
            $dialPrepend=@$data['prepend'];
            $dialPrefix=@$data['prefix'];
            $dialPattern=@$data['pattern'];
            $dialCid=@$data['cid'];

            $prepend_digits=$match_pattern_prefix=$match_pattern_pass=$match_cid=array();

            foreach($dialPrepend as $k=>$v) {
                $thisPrepend=trim($v);
                $thisPrefix=trim($dialPrefix[$k]);
                $thisPattern=trim($dialPattern[$k]);
                $thisCid=trim($dialCid[$k]);

                if($thisPrepend || $thisPrefix || $thisPattern || $thisCid) {
                    $prepend_digits[]=$thisPrepend;
                    $match_pattern_prefix[]=$thisPrefix;
                    $match_pattern_pass[]=$thisPattern;
                    $match_cid[]=$thisCid;
                }
            }

            $prepend_digits=implode(",", $prepend_digits);
            $match_pattern_prefix=implode(",", $match_pattern_prefix);
            $match_pattern_pass=implode(",", $match_pattern_pass);
            $match_cid=implode(",", $match_cid);


            ##### defaults
            if(!$outcid_mode) $outcid_mode="0";
            $mohsilence=$mohsilence ? "1" : "0";

            $emergency=$emergency ? "yes" : "no";
            $intracompany=$intracompany ? "yes" : "no";
            ###############

            // Route Position not working on Asterisk, send default value as 2 for now
            $route_seq=2;

            $dbData=array(
                "route_name"=>$routename,
                "route_cid"=>$outcid,
                "override_extension"=>$outcid_mode,
                "route_password"=>$routepass,
                "music_on_hold"=>$mohsilence,
                "emergency"=>$emergency,
                "intra_company"=>$intracompany,
                "route_position"=>$route_seq,
                "trunk_sequence"=>@implode(",", $trunkData),
                "optional_destination"=>$destination,
                "prepend_digits"=>$prepend_digits,
                "match_prefix"=>$match_pattern_prefix,
                "match_pattern"=>$match_pattern_pass,
                "match_cid"=>$match_cid
            );

            $entity=new Outbounds();
            $entity->exchangeArray($dbData);
            $routeId=$this->_getOutboundsTable()->addOutbound($entity);

            if(!$routeId)
            return new JsonModel($this->createResponse(400, "db_error", "Not inserted, please try again."));

            $inputData=array();
            $inputData[] = array(
                "route_id"=>$routeId,
                "Route Name"=>$routename,
                "Route CID "=>$outcid,
                "Override Extension"=>$outcid_mode,
                "Route Password"=>$routepass,
                "Music On Hold"=>$mohsilence,
                "emergency_route"=>$emergency,
                "intracompany_route"=>$intracompany,
                "Route Position"=>"", //$route_seq
                "Trunk Sequence for Matched Routes"=>@implode(",", $trunkData),
                "Optional Destination on Congestion"=>$destination,
                "prepend_digits"=>$prepend_digits,
                "match_pattern_prefix"=>$match_pattern_prefix,
                "match_pattern_pass"=>$match_pattern_pass,
                "match_cid"=>$match_cid
            );

            // echo json_encode($inputData);
            // die();

            $asterisk=new AsterisksController();
            @list($response)=$asterisk->addOutbound($inputData);

            $status=@$response['status'];
            $statusMsg=@$response['msg'];
            $responseMsg=$statusMsg ? $statusMsg : SOME_ERROR;

            // if success
            if($statusMsg=="Addition Done!") {
                //$this->syncData("outbound"); // update records
                return new JsonModel($this->createResponse(200, "success", $responseMsg));
            }

            // return error
            return new JsonModel($this->createResponse(400, "not_added", $responseMsg));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function outboundsEditAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        // get route info
        $routeId=@trim($_GET['rid']);
        if(!$routeId) $this->redirect()->toUrl("/admin/outbounds");

        $outbound=$this->_getOutboundsTable()->getByRoute($routeId);
        if(!$outbound) $this->redirect()->toUrl("/admin/outbounds");

        return new ViewModel(array("outbound"=>$outbound));
    }

    public function outboundsUpdateAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost()->toArray();

            $route_id=@trim($data['route_id']);
            $routename=@trim($data['routename']);
            $outcid=@trim($data['outcid']);
            $outcid_mode=@trim($data['outcid_mode']);
            $routepass=@$data['routepass'];
            $mohsilence=@trim($data['mohsilence']);
            //$emergency=@trim($data['emergency']);
            //$intracompany=@trim($data['intracompany']);
            $emergency= !empty(@trim($data['route_type'])) && ($data['route_type'] == 'emergency') ? true : false ;
            $intracompany= !empty(@trim($data['route_type'])) && ($data['route_type'] == 'intracompany') ? true : false ;
            $route_seq=@trim($data['route_seq']);
            $trunkpriority=$data['trunkpriority'];
            $destination=@trim($data['destination']);

            // check extension exists
            $found=$this->_getOutboundsTable()->getByRoute($route_id);
            if(!$found)
            return new JsonModel($this->createResponse(400, "not_exists", "Route not exists."));

            // validate
            foreach($this->orRequired as $key=>$value) {
                if($value && !$$key) {
                    $message=$this->orFields[$key]." is required.";
                    return new JsonModel($this->createResponse(400, "required", $message));
                }
            }

            // for trunk sequence
            $trunkData=array();
            $trunkData[]=trim($trunkpriority[0]);
            $trunkData[]=trim($trunkpriority[1]);
            $trunkData[]=trim($trunkpriority[2]);

            if(!$trunkData[0])
            return new JsonModel($this->createResponse(400, "required", "Trunk sequence 1 is required."));

            // for dial pattern
            $dialPrepend=@$data['prepend'];
            $dialPrefix=@$data['prefix'];
            $dialPattern=@$data['pattern'];
            $dialCid=@$data['cid'];

            $prepend_digits=$match_pattern_prefix=$match_pattern_pass=$match_cid=array();

            foreach($dialPrepend as $k=>$v) {
                $thisPrepend=trim($v);
                $thisPrefix=trim($dialPrefix[$k]);
                $thisPattern=trim($dialPattern[$k]);
                $thisCid=trim($dialCid[$k]);

                if($thisPrepend || $thisPrefix || $thisPattern || $thisCid) {
                    $prepend_digits[]=$thisPrepend;
                    $match_pattern_prefix[]=$thisPrefix;
                    $match_pattern_pass[]=$thisPattern;
                    $match_cid[]=$thisCid;
                }
            }

            $prepend_digits=implode(",", $prepend_digits);
            $match_pattern_prefix=implode(",", $match_pattern_prefix);
            $match_pattern_pass=implode(",", $match_pattern_pass);
            $match_cid=implode(",", $match_cid);


            ##### defaults
            if(!$outcid_mode) $outcid_mode="0";
            $mohsilence=$mohsilence ? "1" : "0";

            $emergency=$emergency ? "yes" : "no";
            $intracompany=$intracompany ? "yes" : "no";
            ###############

            // Route Position not working on Asterisk, send default value as 2 for now
            $route_seq=2;

            $dbData=array(
                "route_id"=>$found['route_id'],
                "route_name"=>$routename,
                "route_cid"=>$outcid,
                "override_extension"=>$outcid_mode,
                "route_password"=>$routepass,
                "music_on_hold"=>$mohsilence,
                "emergency"=>$emergency,
                "intra_company"=>$intracompany,
                "route_position"=>$route_seq,
                "trunk_sequence"=>@implode(",", $trunkData),
                "optional_destination"=>$destination,
                "prepend_digits"=>$prepend_digits,
                "match_prefix"=>$match_pattern_prefix,
                "match_pattern"=>$match_pattern_pass,
                "match_cid"=>$match_cid,
                "date_added"=>$found['date_added']
            );
            $entity=new Outbounds();
            $entity->exchangeArray($dbData);
            $updated=$this->_getOutboundsTable()->updateOutbound($entity);

            if(!$updated)
            return new JsonModel($this->createResponse(400, "db_error", "Not updated, please try again."));

            $inputData=array();
            $inputData[] = array(
                "route_id"=>$route_id,
                "Route Name"=>$routename,
                "Route CID "=>$outcid,
                "Override Extension"=>$outcid_mode,
                "Route Password"=>$routepass,
                "Music On Hold"=>$mohsilence,
                "emergency_route"=>$emergency,
                "intracompany_route"=>$intracompany,
                "Route Position"=>"", //$route_seq
                "Trunk Sequence for Matched Routes"=>@implode(",", $trunkData),
                "Optional Destination on Congestion"=>$destination,
                "prepend_digits"=>$prepend_digits,
                "match_pattern_prefix"=>$match_pattern_prefix,
                "match_pattern_pass"=>$match_pattern_pass,
                "match_cid"=>$match_cid
            );

            // echo json_encode($inputData);
            // die();

            // echo "<pre>";
            // print_r($inputData);
            // die();

            $asterisk=new AsterisksController();
            @list($response)=$asterisk->updateOutbound($inputData);

            $status=@$response['status'];
            $statusMsg=@$response['msg'];
            $responseMsg=$statusMsg ? $statusMsg : SOME_ERROR;

            // if success
            if($statusMsg=="Updation Done!") {
                //$this->syncData("outbound"); // update records
                return new JsonModel($this->createResponse(200, "success", $responseMsg));
            }

            // return error
            return new JsonModel($this->createResponse(400, "not_updated", $responseMsg));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function outboundsDeleteAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost()->toArray();

            if(!isset($data['route_id']))
            return new JsonModel($this->createResponse(400, "wrong_data", WRONG_DATA));

            $routeId=$data['route_id'];

            $inputData=array();
            $inputData[] = array(
                'route_id'=>$routeId
            );

            $asterisk=new AsterisksController();
            @list($response)=$asterisk->deleteOutbound($inputData);

            $status=@$response['status'];
            $statusMsg=@$response['msg'];
            $responseMsg=$statusMsg ? $statusMsg : SOME_ERROR;

            // if success
            if($statusMsg=="Delete Data!") {
                //$this->syncData("outbound"); // update records

                // delete from db also
                $this->_getOutboundsTable()->removeOutbound($routeId);

                return new JsonModel($this->createResponse(200, "success", "Data Deleted!"));
            }

            // return error
            return new JsonModel($this->createResponse(400, "not_deleted", $responseMsg));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    // TRUNKS
    public function trunksSyncAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost()->toArray();

            $this->syncData("trunk"); // update records

            return new JsonModel($this->createResponse(200, "success", "Syncing successful."));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function trunksAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        $data=$this->_getTrunksTable()->getList();

        // echo "<pre>";
        // print_r($data);
        // die();

        return new ViewModel(array(
            'records' => $data
        ));
    }

    public function trunksAddAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        return new ViewModel(array());
    }

    public function trunksSaveAction() {
        $cidOptions=array(
            "off"=>"Allow Any CID", "on"=>"Block Foreign CIDs",
            "cnum"=>"Remove CNAM", "all"=>"Force Trunk CID"
        );

        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost()->toArray();

            // print_r($data);
            // die();

            $tech=@strtolower(trim($data['tech']));
            $name=@trim($data['name']);
            $hideCid=@trim($data['hcid']);
            $outcid=@trim($data['outcid']);
            $cidOption=@trim($data['keepcid']);
            $maxChannels=@trim($data['maxchans']);
            $dialOptions=@trim($data['dialopts']);
            $failscript=@trim($data['dialoutopts_cb']);
            $continueIfBusy=@trim($data['continue']);
            $disableTrunk=@trim($data['disabletrunk']);
            $outDialPrefix=@trim($data['dialoutprefix']);
            
            // out settings
            $channelId=@trim($data['channelid']);
            $userDetails=@trim($data['user_details']);
            $userValues=@trim($data['user_values']);

            // in settings
            $userContext=@trim($data['usercontext']);
            $peerDetails=@trim($data['peer_details']);
            $peerValues=@trim($data['peer_values']);
            $registerString=@trim($data['register']);

            if(!$name)
            return new JsonModel($this->createResponse(400, "required", "Trunk name is required."));

            ##### defaults
            $hideCid=$hideCid ? 1 : 0;
            $outcid=$hideCid ? "" : $outcid;

            if($failscript) {
                $failscript="Override";
                if(!$dialOptions)
                return new JsonModel($this->createResponse(400, "required", "Dial options is required."));
            }
            else {
                $failscript="system";
                $dialOptions="";
            }

            $cidOptionDB=$cidOption;
            $cidOption=$cidOptions[$cidOption];
            $continueIfBusy=$continueIfBusy ? "on" : "off";
            $disableTrunk=$disableTrunk ? "on" : "off";

            ###############

            // for dial pattern
            $dialPrepend=@$data['prepend'];
            $dialPrefix=@$data['prefix'];
            $dialPattern=@$data['pattern'];
            $dialCid=@$data['cid'];

            $prepend_digits=$match_pattern_prefix=$match_pattern_pass=$match_cid=array();

            foreach($dialPrepend as $k=>$v) {
                $thisPrepend=trim($v);
                $thisPrefix=trim($dialPrefix[$k]);
                $thisPattern=trim($dialPattern[$k]);
                $thisCid=trim($dialCid[$k]);

                if($thisPrepend || $thisPrefix || $thisPattern || $thisCid) {
                    $prepend_digits[]=$thisPrepend;
                    $match_pattern_prefix[]=$thisPrefix;
                    $match_pattern_pass[]=$thisPattern;
                    $match_cid[]=$thisCid;
                }
            }

            $prepend_digits=implode(",", $prepend_digits);
            $match_pattern_prefix=implode(",", $match_pattern_prefix);
            $match_pattern_pass=implode(",", $match_pattern_pass);
            $match_cid=implode(",", $match_cid);

            // add to db
            $dbData=array(
                "tech"=>$tech,
                "trunk_name"=>$name,
                "hide_cid"=>$hideCid,
                "outbound_cid"=>$outcid,
                "cid_options"=>$cidOptionDB,
                "maximum_channels"=>$maxChannels,
                'dial_options_button'=>$failscript,
                "dial_option_value"=>$dialOptions,
                "continue_if_busy"=>$continueIfBusy,
                "disable_trunk"=>$disableTrunk,
                "prepend_digits"=>$prepend_digits,
                "match_prefix"=>$match_pattern_prefix,
                "match_pattern"=>$match_pattern_pass,
                "match_cid"=>$match_cid,
                "outbound_dial_prefix"=>$outDialPrefix,
                "channel_id"=>$channelId,
                "peer_details"=>$peerDetails,
                "peer_values"=>$peerValues,
                "user_context"=>$userContext,
                "user_details"=>$userDetails,
                "user_values"=>$userValues,
                "register_string"=>$registerString
            );

            $entity=new Trunks();
            $entity->exchangeArray($dbData);

            // print_r($entity);
            // die();

            $trunkId=$this->_getTrunksTable()->addTrunk($entity);

            if(!$trunkId)
            return new JsonModel($this->createResponse(400, "db_error", "Not inserted, please try again."));

            $inputData=array();
            $inputData[] = array(
                "tech"=>$tech,
                "trunkid"=>$trunkId,
                "Trunk Name"=>$name,
                "Outbound CallerID"=>$outcid,
                "CID Options"=>$cidOption,
                "Maximum Channels"=>$maxChannels,
                'Asterisk Trunk Dial Options'=>$failscript,
                "Asterisk Trunk Dial Options Override"=>$dialOptions,
                "Continue if Busy"=>$continueIfBusy,
                "Disable Trunk"=>$disableTrunk,
                "prepend_digit"=>$prepend_digits,
                "pattern_prefix"=>$match_pattern_prefix,
                "pattern_pass"=>$match_pattern_pass,
                "match_cid"=>$match_cid,
                "Outbound Dial Prefix"=>$outDialPrefix,

                "incoming"=>"user",
                "outgoing"=>"peer",

                "provider"=>$channelId,
                "PEER Details"=>$peerDetails,
                "PEER Value"=>$peerValues,

                "USER Context"=>$userContext,
                "USER Details"=>$userDetails,
                "USER-Value"=>$userValues,
                "Register String"=>$registerString
            );

            //echo json_encode($inputData); die();

            // echo "<pre>";
            // print_r($inputData);
            // die();

            $asterisk=new AsterisksController();
            @list($response)=$asterisk->addTrunk($inputData);

            // print_r($response);
            // die();

            $status=@$response[0]['status'];
            $statusMsg=@$response[0]['msg'];
            $responseMsg=$statusMsg ? $statusMsg : SOME_ERROR;

            // if success
            if($statusMsg=="Addition Done!") {
                //$this->syncData("trunk"); // update records
                return new JsonModel($this->createResponse(200, "success", $responseMsg));
            }

            // return error
            return new JsonModel($this->createResponse(400, "not_added", $responseMsg));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function trunksEditAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        // get route info
        // get route info
        $trunkId=@trim($_GET['tid']);
        if(!$trunkId) $this->redirect()->toUrl("/admin/trunks");

        $trunk=$this->_getTrunksTable()->getByTrunk($trunkId);
        if(!$trunk) $this->redirect()->toUrl("/admin/trunks");
        
        return new ViewModel(array(
            "trunk"=>$trunk
        ));
    }

    public function trunksUpdateAction() {
        $cidOptions=array(
            "off"=>"Allow Any CID", "on"=>"Block Foreign CIDs",
            "cnum"=>"Remove CNAM", "all"=>"Force Trunk CID"
        );

        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost()->toArray();

            // print_r($data);
            // die();

            $trunkid=@trim($data['trunkid']);
			$tech=@strtolower(trim($data['tech']));
            $name=@trim($data['name']);
            $hideCid=@trim($data['hcid']);
            $outcid=@trim($data['outcid']);
            $cidOption=@trim($data['keepcid']);
            $maxChannels=@trim($data['maxchans']);
            $dialOptions=@trim($data['dialopts']);
            $failscript=@trim($data['dialoutopts_cb']);
            $continueIfBusy=@trim($data['continue']);
            $disableTrunk=@trim($data['disabletrunk']);
            $outDialPrefix=@trim($data['dialoutprefix']);
            
            // out settings
            $channelId=@trim($data['channelid']);
            $userDetails=@trim($data['user_details']);
            $userValues=@trim($data['user_values']);

            // in settings
            $userContext=@trim($data['usercontext']);
            $peerDetails=@trim($data['peer_details']);
            $peerValues=@trim($data['peer_values']);
            $registerString=@trim($data['register']);

            // check trunk exists
            $found=$this->_getTrunksTable()->getByTrunk($trunkid);
            if(!$found)
            return new JsonModel($this->createResponse(400, "not_exists", "Trunk not exists."));

            if(!$name)
            return new JsonModel($this->createResponse(400, "required", "Trunk name is required."));

            ##### defaults
            $hideCid=$hideCid ? 1 : 0;
            $outcid=$hideCid ? "" : $outcid;

            if($failscript) {
                $failscript="Override";
                if(!$dialOptions)
                return new JsonModel($this->createResponse(400, "required", "Dial options is required."));
            }
            else {
                $failscript="system";
                $dialOptions="";
            }

            $cidOptionDB=$cidOption;
            $cidOption=$cidOptions[$cidOption];
            $continueIfBusy=$continueIfBusy ? "on" : "off";
            $disableTrunk=$disableTrunk ? "on" : "off";

            ###############

            // for dial pattern
            $dialPrepend=@$data['prepend'];
            $dialPrefix=@$data['prefix'];
            $dialPattern=@$data['pattern'];
            $dialCid=@$data['cid'];

            $prepend_digits=$match_pattern_prefix=$match_pattern_pass=$match_cid=array();

            foreach($dialPrepend as $k=>$v) {
                $thisPrepend=trim($v);
                $thisPrefix=trim($dialPrefix[$k]);
                $thisPattern=trim($dialPattern[$k]);
                $thisCid=trim($dialCid[$k]);

                if($thisPrepend || $thisPrefix || $thisPattern || $thisCid) {
                    $prepend_digits[]=$thisPrepend;
                    $match_pattern_prefix[]=$thisPrefix;
                    $match_pattern_pass[]=$thisPattern;
                    $match_cid[]=$thisCid;
                }
            }

            $prepend_digits=implode(",", $prepend_digits);
            $match_pattern_prefix=implode(",", $match_pattern_prefix);
            $match_pattern_pass=implode(",", $match_pattern_pass);
            $match_cid=implode(",", $match_cid);

            // add to db
            $dbData=array(
                "trunk_id"=>$found['trunk_id'],
                "tech"=>$found['tech'],
                "trunk_name"=>$name,
                "hide_cid"=>$hideCid,
                "outbound_cid"=>$outcid,
                "cid_options"=>$cidOptionDB,
                "maximum_channels"=>$maxChannels,
                'dial_options_button'=>$failscript,
                "dial_option_value"=>$dialOptions,
                "continue_if_busy"=>$continueIfBusy,
                "disable_trunk"=>$disableTrunk,
                "prepend_digits"=>$prepend_digits,
                "match_prefix"=>$match_pattern_prefix,
                "match_pattern"=>$match_pattern_pass,
                "match_cid"=>$match_cid,
                "outbound_dial_prefix"=>$outDialPrefix,
                "channel_id"=>$channelId,
                "peer_details"=>$peerDetails,
                "peer_values"=>$peerValues,
                "user_context"=>$userContext,
                "user_details"=>$userDetails,
                "user_values"=>$userValues,
                "register_string"=>$registerString,
                "date_added"=>$found['date_added']
            );

            $entity=new Trunks();
            $entity->exchangeArray($dbData);

            // print_r($entity);
            // die();

            $updated=$this->_getTrunksTable()->updateTrunk($entity);

            if(!$updated)
            return new JsonModel($this->createResponse(400, "db_error", "Not updated, please try again."));

            $inputData=array();
            $inputData[] = array(
                'trunkid'=>$found['trunk_id'],
                "tech"=>$found['tech'],
                'Trunk Name'=>$name,
                'Outbound CallerID'=>$outcid,
                'CID Options'=>$cidOption,
                'Maximum Channels'=>$maxChannels,
                'Asterisk Trunk Dial Options'=>$failscript,
                "Asterisk Trunk Dial Options Override"=>$dialOptions,
                'Continue if Busy'=>$continueIfBusy,
                'Disable Trunk'=>$disableTrunk,
                "prepend_digit"=>$prepend_digits,
                "pattern_prefix"=>$match_pattern_prefix,
                "pattern_pass"=>$match_pattern_pass,
                "match_cid"=>$match_cid,
                'Outbound Dial Prefix'=>$outDialPrefix,

                "incoming"=>"user",
                "outgoing"=>"peer",

                'provider'=>$channelId,
                "PEER Details"=>$peerDetails,
                "PEER Value"=>$peerValues,

                'USER Context'=>$userContext,
                "USER Details"=>$userDetails,
                "USER-Value"=>$userValues,
                "Register String"=>$registerString
            );

            //echo json_encode($inputData); die();

            // echo "<pre>";
            // print_r($inputData);
            // die();

            $asterisk=new AsterisksController();
            @list($response)=$asterisk->updateTrunk($inputData);

            $status=@$response['status'];
            $statusMsg=@$response['msg'];
            $responseMsg=$statusMsg ? $statusMsg : SOME_ERROR;

            // if success
            if($statusMsg=="Updation Done!") {
                //$this->syncData("trunk"); // update records
                return new JsonModel($this->createResponse(200, "success", $responseMsg));
            }

            // return error
            return new JsonModel($this->createResponse(400, "not_updated", $responseMsg));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }


    public function trunksDeleteAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost()->toArray();

            if(!isset($data['trunkid']))
            return new JsonModel($this->createResponse(400, "wrong_data", WRONG_DATA));

            $trunkid=$data['trunkid'];

            $inputData=array();
            $inputData[] = array(
                'trunkid'=>$trunkid
            );

            $asterisk=new AsterisksController();
            @list($response)=$asterisk->deleteTrunk($inputData);

            $status=@$response['status'];
            $statusMsg=@$response['msg'];
            $responseMsg=$statusMsg ? $statusMsg : SOME_ERROR;

            // if success
            if($statusMsg=="Delete Data!") {
                //$this->syncData("trunk"); // update records

                // delete from db also
                $this->_getTrunksTable()->removeTrunk($trunkid);

                return new JsonModel($this->createResponse(200, "success", "Data Deleted!"));
            }

            // return error
            return new JsonModel($this->createResponse(400, "not_deleted", $responseMsg));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    // Restore to Asterisk
    public function inboundsRestoreAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$this->_getInboundsTable()->fetchAll();

            if(!$data)
            return new JsonModel($this->createResponse(400, "no_records", "Nothing found to restore."));

            // make ids object and retore data object
            $ids=$restoreData=array();

            foreach($data as $v) {
                $ids[]=array("DID Number"=>$v['did_number']);
                $restoreData[]=array(
                    "DID Number - Extension"=>$v['did_number'],
                    "CallerID Number"=>$v['cid_number'],
                    "CID Priority Route"=>$v['cid_priority_route'],
                    "Alert Info"=>$v['alert_info'],
                    "Ringer Volume Override"=>$v['ringer_volume'],
                    "CID name prefix "=>$v['cid_name_prefix'],
                    "Music On Hold"=>$v['music_on_hold'],
                    "Set Destination"=>$v['set_destination'],
                    "Privacy Manager"=>$v['privacy_manager'],
                    "Max attempts"=>$v['max_attempts'],
                    "Min Length"=>$v['min_length'],
                    "Description"=>$v['description']
                );
            }

            // $ids=json_encode($ids);
            // $restoreData=json_encode($restoreData);

            // print_r($ids);
            // print_r($restoreData);
            // die();

            $asterisk=new AsterisksController();
            @list($response)=$asterisk->deleteInbound($ids);

            // print_r($response);
            // die();

            $status=@$response['status'];
            $statusMsg=@$response['msg'];
            $responseMsg=$statusMsg ? $statusMsg : SOME_ERROR;

            // if success
            if($statusMsg=="Delete Data!") {
                // restore inbounds now
                $asterisk=new AsterisksController();
                @list($response)=$asterisk->addInbound($restoreData);

                $status=@$response['status'];
                $statusMsg=@$response['msg'];
                $responseMsg=$statusMsg ? $statusMsg : SOME_ERROR;

                // if success
                if($statusMsg=="Addition Done!") {
                    //$this->syncData("inbound"); // update records
                    return new JsonModel($this->createResponse(200, "success", "Restore Success."));
                }

                // return error
                return new JsonModel($this->createResponse(400, "not_added", $responseMsg));
            }

            // return error
            return new JsonModel($this->createResponse(400, "not_deleted", $responseMsg));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function outboundsRestoreAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$this->_getOutboundsTable()->fetchAll();

            if(!$data)
            return new JsonModel($this->createResponse(400, "no_records", "Nothing found to restore."));

            // make ids object and retore data object
            $ids=$restoreData=array();

            foreach($data as $v) {
                $ids[]=array("route_id"=>$v['route_id']);
                $restoreData[]=array(
                    "route_id"=>$v['route_id'],
                    "Route Name"=>$v['route_name'],
                    "Route CID "=>$v['route_cid'],
                    "Override Extension"=>$v['override_extension'],
                    "Route Password"=>$v['route_password'],
                    "Music On Hold"=>$v['music_on_hold'],
                    "emergency_route"=>$v['emergency'],
                    "intracompany_route"=>$v['intra_company'],
                    "Route Position"=>"", //$v['route_position'],
                    "Trunk Sequence for Matched Routes"=>$v['trunk_sequence'],
                    "Optional Destination on Congestion"=>$v['optional_destination'],
                    "prepend_digits"=>$v['prepend_digits'],
                    "match_pattern_prefix"=>$v['match_prefix'],
                    "match_pattern_pass"=>$v['match_pattern'],
                    "match_cid"=>$v['match_cid']
                );
            }

            // $ids=json_encode($ids);
            // $restoreData=json_encode($restoreData);

            // print_r($ids);
            // print_r($restoreData);
            // die();

            $asterisk=new AsterisksController();
            @list($response)=$asterisk->deleteOutbound($ids);

            // print_r($response);
            // die();

            $status=@$response['status'];
            $statusMsg=@$response['msg'];
            $responseMsg=$statusMsg ? $statusMsg : SOME_ERROR;

            // if success
            if($statusMsg=="Delete Data!") {
                // restore inbounds now
                $asterisk=new AsterisksController();
                @list($response)=$asterisk->addOutbound($restoreData);

                $status=@$response['status'];
                $statusMsg=@$response['msg'];
                $responseMsg=$statusMsg ? $statusMsg : SOME_ERROR;

                // if success
                if($statusMsg=="Addition Done!") {
                    //$this->syncData("inbound"); // update records
                    return new JsonModel($this->createResponse(200, "success", "Restore Success."));
                }

                // return error
                return new JsonModel($this->createResponse(400, "not_added", $responseMsg));
            }

            // return error
            return new JsonModel($this->createResponse(400, "not_deleted", $responseMsg));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function trunksRestoreAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$this->_getTrunksTable()->fetchAll();

            if(!$data)
            return new JsonModel($this->createResponse(400, "no_records", "Nothing found to restore."));

            // make ids object and retore data object
            $ids=$restoreData=array();

            foreach($data as $v) {
                $ids[]=array("trunkid"=>$v['trunk_id']);
                $restoreData[]=array(
                    'trunkid'=>$v['trunk_id'],
                    "tech"=>strtolower($v['tech']),
                    'Trunk Name'=>$v['trunk_name'],
                    'Outbound CallerID'=>$v['outbound_cid'],
                    'CID Options'=>$v['cid_options'],
                    'Maximum Channels'=>$v['maximum_channels'],
                    'Asterisk Trunk Dial Options'=>$v['dial_options_button'],
                    "Asterisk Trunk Dial Options Override"=>$v['dial_option_value'],
                    'Continue if Busy'=>$v['continue_if_busy'],
                    'Disable Trunk'=>$v['disable_trunk'],
                    "prepend_digit"=>$v['prepend_digits'],
                    "pattern_prefix"=>$v['match_prefix'],
                    "pattern_pass"=>$v['match_pattern'],
                    "match_cid"=>$v['match_cid'],
                    'Outbound Dial Prefix'=>$v['outbound_dial_prefix'],

                    "incoming"=>"user",
                    "outgoing"=>"peer",

                    'provider'=>$v['channel_id'],
                    "PEER Details"=>$v['peer_details'],
                    "PEER Value"=>$v['peer_values'],

                    'USER Context'=>$v['user_context'],
                    "USER Details"=>$v['user_details'],
                    "USER-Value"=>$v['user_values'],
                    "Register String"=>$v['register_string']
                );
            }

            // $ids=json_encode($ids);
            // $restoreData=json_encode($restoreData);

            // print_r($ids);
            // print_r($restoreData);
            // die();

            $asterisk=new AsterisksController();
            @list($response)=$asterisk->deleteTrunk($ids);

            // print_r($response);
            // die();

            $status=@$response['status'];
            $statusMsg=@$response['msg'];
            $responseMsg=$statusMsg ? $statusMsg : SOME_ERROR;

            // if success
            if($statusMsg=="Delete Data!") {
                // restore inbounds now
                $asterisk=new AsterisksController();
                @list($response)=$asterisk->addTrunk($restoreData);

                $status=@$response[0]['status'];
                $statusMsg=@$response[0]['msg'];
                $responseMsg=$statusMsg ? $statusMsg : SOME_ERROR;

                // if success
                if($statusMsg=="Addition Done!") {
                    //$this->syncData("inbound"); // update records
                    return new JsonModel($this->createResponse(200, "success", "Restore Success."));
                }

                // return error
                return new JsonModel($this->createResponse(400, "not_added", $responseMsg));
            }

            // return error
            return new JsonModel($this->createResponse(400, "not_deleted", $responseMsg));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }
    ###########################

    public function callLogsAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        $page = (int) $this->params()->fromQuery('page', 1);
        $page = $page>=1 ? $page : 1;
        if($page>50) $page=50;

        $request=array("number of record show"=>20, "page_number"=>$page);

        //$asterisk=new AsterisksController();
        //$data=$asterisk->getCallLogs($request);
        //echo '<pre>'; print_r($data); exit;

        $json='
            [
                {
                    "calldate": "2018-02-22 20:02:36",
                    "clid": "\"600\" <600>",
                    "src": "600",
                    "dst": "s",
                    "dcontext": "from-sip-external",
                    "channel": "SIP/10.35.62.150-000031c5",
                    "dstchannel": "",
                    "lastapp": "Congestion",
                    "lastdata": "5",
                    "duration": "12",
                    "billsec": "12",
                    "disposition": "ANSWERED",
                    "amaflags": "3",
                    "accountcode": "",
                    "uniqueid": "1519329756.12783",
                    "userfield": "",
                    "did": "",
                    "recordingfile": "",
                    "cnum": "",
                    "cnam": "",
                    "outbound_cnum": "",
                    "outbound_cnam": "",
                    "dst_cnam": ""
                },
                {
                    "calldate": "2018-02-22 19:54:22",
                    "clid": "\"600\" <600>",
                    "src": "600",
                    "dst": "s",
                    "dcontext": "from-sip-external",
                    "channel": "SIP/10.35.62.150-000031c4",
                    "dstchannel": "",
                    "lastapp": "Congestion",
                    "lastdata": "5",
                    "duration": "12",
                    "billsec": "12",
                    "disposition": "ANSWERED",
                    "amaflags": "3",
                    "accountcode": "",
                    "uniqueid": "1519329262.12782",
                    "userfield": "",
                    "did": "",
                    "recordingfile": "",
                    "cnum": "",
                    "cnam": "",
                    "outbound_cnum": "",
                    "outbound_cnam": "",
                    "dst_cnam": ""
                }
            ]
        ';
        $data=json_decode($json, true);

        //~ echo "<pre>";
        //~ print_r($data);
        //~ die();

        return new ViewModel(array(
            'currentPage' => $page,
            'records' => $data
        ));
    }

    public function chanSipPeersAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        $asterisk=new AsterisksController();
        $data=$asterisk->getList("sip_peers.php");

        return new ViewModel(array(
            'records' => $data
        ));
    }

    public function chanSipChannelsAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        $asterisk=new AsterisksController();
        $data=$asterisk->getList("sip_channels.php");

        return new ViewModel(array(
            'records' => $data
        ));
    }

    public function chanSipRegistryAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        $asterisk=new AsterisksController();
        $data=$asterisk->getList("sip_registry.php");

        return new ViewModel(array(
            'records' => $data
        ));
    }

    public function iax2PeersAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        $asterisk=new AsterisksController();
        $data=$asterisk->getList("iax2_peers.php");

        return new ViewModel(array(
            'records' => $data
        ));
    }

    public function uptimeAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        $asterisk=new AsterisksController();
        $data=$asterisk->getUptime("uptime.php");

        return new ViewModel(array(
            'records' => $data
        ));
    }

    public function ownerDetailsAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost()->toArray();

            if(!isset($data['owner_id']))
            return new JsonModel($this->createResponse(400, "wrong_data", WRONG_DATA));

            // get owner details
            $ownerId=trim($data['owner_id']);

            $em = $this->getEntityManager(); /* Call Entity Manager */
            $userObj = $em->getRepository('Admin\Entity\GdFlatowner')->findOneBy(array('id' => $ownerId));

            if(!$userObj) return new JsonModel($this->createResponse(400, "not_found", "User not found."));

            $ownerInfo=array();

            $ownerInfo['firstName'] = $userObj->getFirstName();
            $ownerInfo['lastName'] = $userObj->getLastName();
            $ownerInfo['unitNum'] = $userObj->getUnitNumber();
            $ownerInfo['floor'] = $userObj->getFloor();
            $ownerInfo['mobile'] = $userObj->getMobile();
            $ownerInfo['email'] = $userObj->getEmail();
            $ownerInfo['followme1'] = $userObj->getFollowme1();
            $ownerInfo['followme2'] = $userObj->getFollowme2();
            $ownerInfo['followme3'] = $userObj->getFollowme3();
            $ownerInfo['voicemail'] = $userObj->getVoiceMail();
            $ownerInfo['cameraonly'] = $userObj->getCameraOnly();
            $ownerInfo['sipusername'] = $userObj->getSipUsername();
            $ownerInfo['sipdownloadcode'] = $userObj->getSipDownloadCode();

            $response=array();
            foreach($ownerInfo as $key=>$info) {
                if(is_numeric($info)) $response[$key]=$info;
                else $response[$key]=$info ? $info : "";
            }

            // print_r($ownerInfo);
            // die();

            // return success
            return new JsonModel($this->createResponse(200, "success", "success", $response));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function firewallAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        $config1=$this->_getFirewallTable()->getByType(1);
        $config2=$this->_getFirewallTable()->getByType(2);

        return new ViewModel(array(
            'config1'=>$config1,
            'config2'=>$config2
        ));
    }

    public function firewallCheckStatusAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $config=$this->_getFirewallTable()->getByType(1);
            if(!$config) {
                return new JsonModel($this->createResponse(401, "no_config", "No config found."));
            }

            // return success
            return new JsonModel($this->createResponse(200, "success", "success"));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function firewallGetByTypeAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost();
            $type=@$data['type'];

            $config=$this->_getFirewallTable()->getByType($type);
            $response=array(
                "config_type"=>@$config['config_type'],
                "ip_address"=>@$config['ip_address'],
                "username"=>"******",
                "password"=>"********",
                "mask"=>@$config['mask'],
                "gateway"=>@$config['gateway'],
                "dns1"=>@$config['dns1'],
                "dns2"=>@$config['dns2'],
                "modified_date"=>@$config['modified_date']
            );

            // return success
            return new JsonModel($this->createResponse(200, "success", "success", $response));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function firewallAddConnectionAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost();

            $ip=trim(@$data['ip']);
            $username=trim(@$data['username']);
            $password=@$data['password'];

            if(!$ip || !$username || !$password)
            return new JsonModel($this->createResponse(400, "error", "All fields are required."));

            // add configs
            $input=array(
                "config_type"=>1,
                "ip_address"=>$ip, "username"=>$username, "password"=>$password
            );
            $entity=new \Admin\Model\Firewall();
            $entity->exchangeArray($input);
            
            $this->_getFirewallTable()->insertOrUpdate($entity);
            
            // return success
            return new JsonModel($this->createResponse(200, "success", "Configurations are set successfully."));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function firewallAddStaticAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $data=$request->getPost();

            $ip=trim(@$data['ip']);
            $mask=trim(@$data['mask']);
            $gateway=@$data['gateway'];
            $dns1=@$data['dns1'];
            $dns2=@$data['dns2'];

            if(!$ip || !$mask || !$gateway || !$dns1 || !$dns2)
            return new JsonModel($this->createResponse(400, "error", "All fields are required."));

            // add configs
            $input=array(
                "config_type"=>2,
                "ip_address"=>$ip, "mask"=>$mask, "gateway"=>$gateway,
                "dns1"=>$dns1, "dns2"=>$dns2
            );
            $entity=new \Admin\Model\Firewall();
            $entity->exchangeArray($input);
            
            $this->_getFirewallTable()->insertOrUpdate($entity);
            
            // return success
            return new JsonModel($this->createResponse(200, "success", "success."));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function firewallShowStatusAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $config=$this->_getFirewallTable()->getByType(1);
            if(!$config) {
                return new JsonModel($this->createResponse(401, "no_config", "No config found."));
            }

            // SSH Connection
            //set_include_path(get_include_path() . PATH_SEPARATOR . 'phpseclib');
            //set_include_path("C:\wamp64\bin\php\php5.6.25\pear");
            //require 'Net/SSH2.php';

            // define('NET_SSH2_LOGGING', 3);
            //$ssh = new \Net_SSH2($config['ip_address']);
            $ssh = new SSH2($config['ip_address']);
            if(!$ssh->login($config['username'], $config['password'])) {
                return new JsonModel($this->createResponse(400, "error", "Connection Error"));
            }

            $ssh->write("enable\n");
            $ssh->read('[prompt]');
            $ssh->write("wan show\n");
            $data=$ssh->read('[prompt]');
            $ssh->exec('logout');

            // return success
            return new JsonModel($this->createResponse(200, "success", "success", $data));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function firewallShowStaticAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $config1=$this->_getFirewallTable()->getByType(1);
            if(!$config1) {
                return new JsonModel($this->createResponse(400, "no_config", "Connection is not configured, please add the connection first."));
            }

            $config2=$this->_getFirewallTable()->getByType(2);
            if(!$config2) {
                return new JsonModel($this->createResponse(400, "no_config", "Connection is not configured, please configure static first."));
            }

            // SSH Connection
            //set_include_path(get_include_path() . PATH_SEPARATOR . 'phpseclib');
            //require 'Net/SSH2.php';

            //define('NET_SSH2_LOGGING', 3);
            //$ssh = new \Net_SSH2($config1['ip_address']);
            $ssh = new SSH2($config1['ip_address']);
            if(!$ssh->login($config1['username'], $config1['password'])) {
                return new JsonModel($this->createResponse(400, "error", "Connection Error"));
            }

            $ssh->write("enable\n");
            $ssh->read('[prompt]');
            $ssh->write("wan edit service eth --protocol static --vlan -1 --IPaddr $config2[ip_address] $config2[mask] --gateway $config2[gateway] --dns $config2[dns1],$config2[dns2]\n");
			$ssh->write('save');
            $data=$ssh->read('[prompt]');
            $ssh->exec('logout');

            // $data="xyz";

            // return success
            return new JsonModel($this->createResponse(200, "success", "success", $data));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function firewallShowDhcpAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $config1=$this->_getFirewallTable()->getByType(1);
            if(!$config1) {
                return new JsonModel($this->createResponse(400, "no_config", "Connection is not configured, please add the connection first."));
            }

            // SSH Connection
            //set_include_path(get_include_path() . PATH_SEPARATOR . 'phpseclib');
            //require 'Net/SSH2.php';

            //define('NET_SSH2_LOGGING', 3);
            //$ssh = new \Net_SSH2($config1['ip_address']);
            $ssh = new SSH2($config1['ip_address']);
            if(!$ssh->login($config1['username'], $config1['password'])) {
                return new JsonModel($this->createResponse(400, "error", "Connection Error"));
            }

            $ssh->write("enable\n");
            $ssh->read('[prompt]');
            $ssh->write("wan edit service eth --protocol dhcp --vlan -1\n");
            $data=$ssh->read('[prompt]');
            $ssh->exec('save');
            $ssh->exec('logout');

            // $data="xyz";

            // return success
            return new JsonModel($this->createResponse(200, "success", "success", $data));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function firewallShowRebootAction() {
        $request = $this->getRequest();
        if($request->isPost()) {
            $config1=$this->_getFirewallTable()->getByType(1);
            if(!$config1) {
                return new JsonModel($this->createResponse(400, "no_config", "Connection is not configured, please add the connection first."));
            }

            // SSH Connection
            //set_include_path(get_include_path() . PATH_SEPARATOR . 'phpseclib');
            //require 'Net/SSH2.php';

            //define('NET_SSH2_LOGGING', 3);
            //$ssh = new \Net_SSH2($config1['ip_address']);
            $ssh = new SSH2($config1['ip_address']);
            if(!$ssh->login($config1['username'], $config1['password'])) {
                return new JsonModel($this->createResponse(400, "error", "Connection Error"));
            }

            // $ssh->write("enable\n");
            // $ssh->read('[prompt]');
            // $ssh->write("reboot");
            // $data=$ssh->read('[prompt]');
			$ssh->exec("reboot");
			$data=$ssh->read('[prompt]');
            // $data="xyz";

            // return success
            return new JsonModel($this->createResponse(200, "success", "success", $data));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }


    public function createResponse($status=400, $title="bad_request", $message=BAD_REQUEST, $data=NULL) {
        $response = array(
            'status'        =>  "$status",
            'title'         =>  $title,
            'message'       =>  $message,
            'response'      =>  $data
        );
        return $response;
    }

    protected $_listsTable;
    public function _getListsTable() {
        if(!$this->_listsTable) {
            $sm=$this->getServiceLocator();
            $this->_listsTable=$sm->get("Admin\Model\AsteriksListTable");
        }
        return $this->_listsTable;
    }

    protected $_inboundsTable;
    public function _getInboundsTable() {
        if(!$this->_inboundsTable) {
            $sm=$this->getServiceLocator();
            $this->_inboundsTable=$sm->get("Admin\Model\InboundsTable");
        }
        return $this->_inboundsTable;
    }

    protected $_outboundsTable;
    public function _getOutboundsTable() {
        if(!$this->_outboundsTable) {
            $sm=$this->getServiceLocator();
            $this->_outboundsTable=$sm->get("Admin\Model\OutboundsTable");
        }
        return $this->_outboundsTable;
    }

    protected $_trunksTable;
    public function _getTrunksTable() {
        if(!$this->_trunksTable) {
            $sm=$this->getServiceLocator();
            $this->_trunksTable=$sm->get("Admin\Model\TrunksTable");
        }
        return $this->_trunksTable;
    }

    protected $_ownerTypeTable;
    public function _getOwnerTypeTable() {
        if(!$this->_ownerTypeTable) {
            $sm=$this->getServiceLocator();
            $this->_ownerTypeTable=$sm->get("Admin\Model\OwnerTypeTable");
        }
        return $this->_ownerTypeTable;
    }

    protected $_firewallTable;
    public function _getFirewallTable() {
        if(!$this->_firewallTable) {
            $sm=$this->getServiceLocator();
            $this->_firewallTable=$sm->get("Admin\Model\FirewallTable");
        }
        return $this->_firewallTable;
    }
}
