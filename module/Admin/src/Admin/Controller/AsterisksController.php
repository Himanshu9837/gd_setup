<?php
/**
 * User: Abar Choudhary
 * Date: 15/04/2018
 */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class AsterisksController extends AbstractActionController {

    public function __construct() {
        $this->url=ASTERIK_URL."webrtc_api/";
        $this->headers=array(
            "Content-Type: application/json"
        );
    }

    public function indexAction() {
        die();
    }

    public function getList($action) {
        $URL=$this->url.$action;

        print_r($headers);
         die();

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $URL);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data)); 
        //curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $json_response = curl_exec($curl);
        $response = @json_decode($json_response, true);
        curl_close($curl);

        // print_r($response);
        // die();
        
        return $response;
    }

    public function getById($action, $data) {
        $URL=$this->url.$action;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $URL);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data)); 
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);

        $json_response = curl_exec($curl);
        $response = @json_decode($json_response, true);
        curl_close($curl);

        // print_r(json_encode($data));
        // die();
        
        return $response;
    }

    public function addInbound($data) {
        $URL=$this->url."add_inbound_route.php";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $URL);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data)); 
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);

        $json_response = curl_exec($curl);
        $response = @json_decode($json_response, true);
        curl_close($curl);

        // print_r($response);
        // die();
        
        return $response;
    }

    public function updateInbound($data) {
        $URL=$this->url."edit_inbound_route.php";
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $URL);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data)); 
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);

        $json_response = curl_exec($curl);
        $response = @json_decode($json_response, true);
        curl_close($curl);

        // print_r($response);
        // die();
        
        return $response;
    }

    public function deleteInbound($data) {
        $URL=$this->url."delete_inbound_route.php";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $URL);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);

        $json_response = curl_exec($curl);
        $response = @json_decode($json_response, true);
        curl_close($curl);

        // print_r($response);
        // die();
        
        return $response;
    }

    public function addOutbound($data) {
        $URL=$this->url."add_outbound_route.php";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $URL);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);

        $json_response = curl_exec($curl);
        $response = @json_decode($json_response, true);
        curl_close($curl);

        // print_r($response);
        // die();
        
        return $response;
    }

    public function updateOutbound($data) {
        $URL=$this->url."edit_outbound_route.php";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $URL);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);

        $json_response = curl_exec($curl);
        $response = @json_decode($json_response, true);
        curl_close($curl);

        // print_r($response);
        // die();
        
        return $response;
    }

    public function deleteOutbound($data) {
        $URL=$this->url."delete_outbound_route.php";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $URL);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);

        $json_response = curl_exec($curl);
        $response = @json_decode($json_response, true);
        curl_close($curl);

        // print_r($response);
        // die();
        
        return $response;
    }

    public function addTrunk($data) {
        $URL=$this->url."add_trunk.php";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $URL);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);

        $json_response = curl_exec($curl);
        $response = @json_decode($json_response, true);
        curl_close($curl);

        // print_r($response);
        // die();
        
        return $response;
    }

    public function updateTrunk($data) {
        $URL=$this->url."edit_trunk.php";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $URL);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);

        $json_response = curl_exec($curl);
        $response = @json_decode($json_response, true);
        curl_close($curl);

        // print_r($response);
        // die();
        
        return $response;
    }

    public function deleteTrunk($data) {
        $URL=$this->url."delete_trunk.php";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $URL);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);

        $json_response = curl_exec($curl);
        $response = @json_decode($json_response, true);
        curl_close($curl);

        // print_r($response);
        // die();
        
        return $response;
    }

    public function getCallLogs($data) {
        $URL=$this->url."call_log.php";

        $request=array($data);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $URL);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($request));
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);

        $json_response = curl_exec($curl);
        $response = @json_decode($json_response, true);
        curl_close($curl);   

        // print_r($response);
        // die();
        
        return $response;
    }

    public function getUptime($action) {
        $URL=$this->url.$action;

        // print_r($headers);
        // die();

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $URL);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        //curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $json_response = curl_exec($curl);
        $response = $json_response;
        curl_close($curl);

        // print_r($response);
        // die();
        
        return $response;
    }
	
	 public function externalIP($data) {
        $URL=$this->url."sipsetting_external.php";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $URL);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);

        $json_response = curl_exec($curl);
        $response = @json_decode($json_response, true);
        curl_close($curl);

        // print_r($response);
        // die();
        
        return $response;
    }
}
