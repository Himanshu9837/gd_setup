<?php

/**
 * Zend Framework (http://framework.zend.com/)
 * This class is used for Login, Log out, Forgot Password and Change Password.
 */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\AbstractRestfulController as RestfulController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Session\Container;
use Admin\Form as AdminForms;
use Zend\Mvc\MvcEvent;
use Admin\Entity as Entities;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Admin\Model\Configs;
use Admin\Model\TimeProfiles;
use Zend\Validator\Digits;

class TimeProfilesController extends AbstractActionController {

    protected $em;
    protected $authservice;

    public function onDispatch(MvcEvent $e) {

        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $username = $admin_session->username;
        $usertype = $admin_session->usertype;
        if (empty($username) && $usertype != 1) {
            /* if not logged in redirect the user to login page */
            return $this->redirect()->toRoute('adminlogin');
        }
        $this->layout('layout/adminlayout');
        return parent::onDispatch($e);
    }

    public function setEntityManager(EntityManager $em) {
        $this->em = $em;
    }

    public function getEntityManager() {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    public function getAuthService() {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()
                    ->get('AuthService');
        }

        return $this->authservice;
    }

    public function indexAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        $data = $this->_getTimeProfilesTable()->getList();
        $timezone = $this->_getConfigsTable()->getByType(3); // 3=for timezone
//         echo "<pre>";
//         print_r($data);
//         die();

        return new ViewModel(array(
            'records' => $data,
            'timezone' => $timezone['value']
        ));
    }

    public function addNewAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        return new ViewModel(array());
    }

    public function editAction() {
        $em = $this->getEntityManager();
        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        $this->checkAdminLoggedInOrNot();
        $this->layout('layout/adminlayout');

        $this->layout()->pageTitle = 'Grand Ducos | Admin';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

        $timeProfileId = $this->params('id');
        $data = $this->_getTimeProfilesTable()->getById($timeProfileId);
        return new ViewModel(array(
            'timeProfile' => $data
        ));
    }

    public function saveCodeAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();

            $time_profile_config = @trim($data['time_profile_config']);
            $time_profile_name = @trim($data['time_profile_name']);

            if (!$time_profile_config || !$time_profile_name)
                return new JsonModel($this->createResponse(400, "error", "All fields are required"));


            $adminSession = new Container('admin');
            $adminId = $adminSession->userId;
            if (!$adminId)
                return new JsonModel($this->createResponse(400, "error", "Login required"));

            // add now
            $codeData = array(
                "time_profile_config" => $time_profile_config,
                "time_profile_name" => $time_profile_name
            );
            if ($data['time_profile_id']) {
                $codeData['time_profile_id'] = $data['time_profile_id'];
            }
            $entity = new TimeProfiles();
            $entity->exchangeArray($codeData);

            if ($data['time_profile_id']) {
                $timeProfileId = $this->_getTimeProfilesTable()->edit($entity);
                $success_message = "Updated Successfully";
            } else {
                $timeProfileId = $this->_getTimeProfilesTable()->add($entity);
                $success_message = "Created Successfully";
            }
            if (!$timeProfileId)
                return new JsonModel($this->createResponse(400, "db_error", SOME_ERROR));


            return new JsonModel($this->createResponse(200, "success", $success_message));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function setTimezoneAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();

            $setTimezoneValue = @trim($data['setTimezoneValue']);

            if (!$setTimezoneValue)
                return new JsonModel($this->createResponse(400, "error", "Timezone is required"));

            $adminSession = new Container('admin');
            $adminId = $adminSession->userId;
            if (!$adminId)
                return new JsonModel($this->createResponse(400, "error", "Login required"));

            // update now
            $setTimezoneValueData = array("config_type" => 3, "value" => $setTimezoneValue);
            $entity = new Configs();
            $entity->exchangeArray($setTimezoneValueData);

            $this->_getConfigsTable()->insertOrUpdate($entity);

            return new JsonModel($this->createResponse(200, "success", "Updated successfully"));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function suspendAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();

            if (!isset($data['id']))
                return new JsonModel($this->createResponse(400, "wrong_data", WRONG_DATA));

            // update now
            $timeProfileId = $data['id'];
            $this->_getTimeProfilesTable()->updateStatus($timeProfileId);

            return new JsonModel($this->createResponse(200, "success", "Data Updated!"));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function deleteAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();

            if (!isset($data['id']))
                return new JsonModel($this->createResponse(400, "wrong_data", WRONG_DATA));

            // update now
            $timeProfileId = $data['id'];
            $this->_getTimeProfilesTable()->delete($timeProfileId);

            return new JsonModel($this->createResponse(200, "success", "Data Deleted!"));
        }

        return new JsonModel($this->createResponse(400, "error", SOME_ERROR));
    }

    public function checkAdminLoggedInOrNot() {
        /* checking if user logged in or not */
        $admin_session = new Container('admin');
        $username = $admin_session->username;
        if (empty($username)) {
            /* if not logged in redirect the user to login page */
            return $this->redirect()->toRoute('adminlogin');
        }
    }

    public function createResponse($status = 400, $title = "bad_request", $message = BAD_REQUEST, $data = NULL) {
        $response = array(
            'status' => "$status",
            'title' => $title,
            'message' => $message,
            'response' => $data
        );
        return $response;
    }

    public function validateDigits($value, $label, $min = 1, $max = false, $minValue = false, $maxValue = false) {
        // min max length
        if ($min && strlen($value) < $min) {
            return $response = $this->createResponse(400, 'short', "$label should be at least $min digit long.");
        }
        if ($max && strlen($value) > $max) {
            return $response = $this->createResponse(400, 'big', "$label should be at most $max digit long.");
        }

        // min max value
        if ($minValue && $value < $minValue) {
            return $response = $this->createResponse(400, 'min', "$label should be greater or equal to $minValue");
        }
        if ($maxValue && $value > $maxValue) {
            return $response = $this->createResponse(400, 'max', "$label should be less or equal to $minValue");
        }

        // valid digits
        $validator = new digits();
        if ($validator->isValid($value)) {
            return $response = $this->createResponse(200, 'valid', "$label is valid");
        }

        return $response = $this->createResponse(400, 'invalid', "$label is invalid, use only digits");
    }

    public function crypto_rand_secure($min, $max) {
        $range = $max - $min;
        if ($range < 1)
            return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }

    public function generateSecretKey($length = 10) {
        $scretKey = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        $max = strlen($codeAlphabet);

        for ($i = 0; $i < $length; $i++) {
            $scretKey .= $codeAlphabet[$this->crypto_rand_secure(0, $max)];
        }

        return $scretKey;
    }

    protected $_configsTable;

    public function _getConfigsTable() {
        if (!$this->_configsTable) {
            $sm = $this->getServiceLocator();
            $this->_configsTable = $sm->get("Admin\Model\ConfigsTable");
        }
        return $this->_configsTable;
    }

    protected $_timeProfilesTable;

    public function _getTimeProfilesTable() {
        if (!$this->_timeProfilesTable) {
            $sm = $this->getServiceLocator();
            $this->_timeProfilesTable = $sm->get("Admin\Model\TimeProfilesTable");
        }
        return $this->_timeProfilesTable;
    }

}
