<?php
/**
 * Zend Framework (http://framework.zend.com/)
 * This class is used for Login, Log out, Forgot Password and Change Password.
 */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Admin\Form as AdminForms;
use Zend\Mail as Mail;
use Zend\Mvc\MvcEvent;
use Admin\Entity as Entities;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

class UserlimitController extends AbstractActionController {

    protected $em;
    protected $authservice;
    
     public function onDispatch(MvcEvent $e) {

        $admin_session = new Container('admin');
        $usertype = $admin_session->usertype;
        $username = $admin_session->username;
        $usertype = $admin_session->usertype;
         if (empty($username) && $usertype != 1) {
            /* if not logged in redirect the user to login page */
            return $this->redirect()->toRoute('adminlogin');
        }
        $this->layout('layout/adminlayout');
        return parent::onDispatch($e);
    }

    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }
    
    public function getEntityManager()
    {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }
    
    public function getAuthService()
    {
        if (! $this->authservice) {
            $this->authservice = $this->getServiceLocator()
                                      ->get('AuthService');
        }
         
        return $this->authservice;
    }
    
    
    /**
     * Used to save maximum user can be added
     * This limit can be set only by masteradmin
     *
     * @author      Nishikant
     * @created_date    29th May, 2017
     * @modified_date   -------------
     */
    
    public function userlimitAction() {
        $em = $this->getEntityManager();  
        $request = $this->getRequest();      
        $this->layout()->pageTitle = 'Max User Limit';
        $admin_session = new Container('admin');
	$usertype = $admin_session->usertype;
	$firstname = $admin_session->firstname;
	$this->layout()->userType = $usertype;
	$this->layout()->firstname = $firstname;
	
	if($usertype != 1) {
            return $this->redirect()->toRoute('admindashboard');
            $flashMessenger = $this->flashMessenger();
            $flashMessenger->setNamespace('Not Authorised');
            $msg = "You are not authorised to access";
            $flashMessenger->addMessage($msg);
        }
	
       
        // get data from db
        $configId = $em->getRepository('Admin\Entity\GdUserlimit')->userlimitRecord();
        if ((!empty($configId)) && (isset($configId))) {
            $userlimitObj = $em->getRepository('Admin\Entity\GdUserlimit')->findOneBy(array('id' => $configId['id']));
            // edit mode           
            $maxUser = $userlimitObj->getMaxUser();
            // view in edit mode
            $form = new AdminForms\UserlimitForm($em, $configId['id']);
            $form->get('submitbutton')->setValue('Save');
            
            $form->get('userlimit')->setValue($maxUser);           
            $request = $this->getRequest();
            if ($request->isPost()) {
                $formValidator = new AdminForms\Validator\UserlimitFormValidator();
                $form->setInputFilter($formValidator->getInputFilter());
                $formdata = $request->getPost();
                $form->setData($request->getPost());
                if ($form->isValid()) {
                    $userlimitObj->setMaxUser($formdata['userlimit']);
                    $em->persist($userlimitObj);
                    $em->flush();
                    $flashMessenger = $this->flashMessenger();
                    $flashMessenger->setNamespace('success');
                    $msg = "Maximum User limit updated successfully";
                    $flashMessenger->addMessage($msg);
                    return $this->redirect()->toRoute('admindashboard');
                }
            }
        } else {
            $form = new AdminForms\UserlimitForm($em);
            $form->get('submitbutton')->setValue('Save');
            $this->layout()->pageTitle = 'Max User Limit';
            $formdata = $request->getPost();          
            $maxuserObj = new Entities\GdUserlimit();
            if ($request->isPost()) { 
                $formValidator = new AdminForms\Validator\UserlimitFormValidator();
                $form->setInputFilter($formValidator->getInputFilter());
                $formdata = $request->getPost();
                $form->setData($request->getPost());
                if ($form->isValid()) {
                    $currentDate = date('Y-m-d H:i:s');
                    $maxuserObj->setMaxUser($formdata['userlimit']);
                    $maxuserObj->setCreatedDate($currentDate);
                    $em->persist($maxuserObj);
                    $em->flush();
                    $flashMessenger = $this->flashMessenger();
                    $flashMessenger->setNamespace('success');
                    $msg = "Maximum User limit added successfully";
                    $flashMessenger->addMessage($msg);
                    return $this->redirect()->toRoute('admindashboard');
                }
            }
        }
        return array('form' => $form, 'success' => $msg);
    }
    
}
