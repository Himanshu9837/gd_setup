<?php
namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Admin\Entity as Entities;
use Admin\Model\GdFlatowner;
use Admin\Model\AccessCodes;
use MicrosoftAzure\Storage\File\FileRestProxy;

class UpdatecsvCronController extends AbstractActionController{

    public $data=array();
    protected $em;
    protected $authservice;
    public $url;

    public function __construct() {
        $this->url=ASTERIK_URL."api/";
        $this->headers=array(
            "Content-Type: application/json"
        );
    }
    
    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }
    
    public function getEntityManager()
    {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    public function indexAction(){
		//$logFile ='access_log_'.date('dmY').'.CSV';
		//$logFile = 'access_log_01062020.txt';
        //$this->removeFromAzureFile(AZURE_DIRECTORY,$logFile);

        die("access denied");
    }
    
    
    public function uploadLogFiletoAzureAction(){
		
		$this->callLog();
		
		$this->accessCodeLog();
		
		die("End");  
		
    }


    public function callLog(){
		
			$startTime=date("Y-m-d H:i:s");
			$logData="\n\n------------------------------------\n";
			$logData.="Cron For upload call log on azure file system started $startTime";
			
			$fopen=fopen(CRON_CALL_LOG_UPDATE_LOCK_PATH, 'w');
			if(!flock($fopen, LOCK_EX | LOCK_NB)) {
				$logData.="\nFile already locked.";
				$this->writeCronLogs( $logData, true);
				return false;
			}

			$request=array("number of record show"=>40, "page_number"=>1);

			$result = $this->getCallLogs($request);
        
			if(empty($result)){
			   $logData.="\nNo call log found for any date.";
			   return false;
			}
			
			$response  = $this->callLogFilter($result);
			
			if(!empty($response)){
			
			$result1 = array_map(function ($val){
				$dt1 = strtotime($val['calldate']);
				$dt2 = date('Y-m-d', $dt1);
				$val['dateFormat'] = $dt2;
				return $val;
			},$response);
		
			$dateArr = array();
			
			foreach($result1 as $value){
				$dateArr[$value['dateFormat']][] = $value;
			}
		
           $i = 0; 		
		   foreach($dateArr as $k=>$data){  $i++;
			
			if($k == date('Y-m-d')) { 
				
				$logData.="\nLog found for $k.";	
				
				$logFile ='call_log_'.date('dmY',strtotime($k)).'.CSV';
				
				$header = array_keys(current($data));
				
				$fh = fopen('php://temp', 'rw');
			
				 fputcsv($fh, $header);

				foreach($data as $row){
					fputcsv($fh, $row);
				}
				
				rewind($fh);
				
				$tempCSVContent = stream_get_contents($fh);
				
				fclose($fh);
				
				//$this->removeFromAzureFile(AZURE_DIRECTORY,$logFile);
				 
				 $this->uploadFiletoAzure($tempCSVContent,AZURE_DIRECTORY,$logFile); 
				 
				 $logData.="\nFile uploaded on azure file system with name $logFile.";
			 
		    }else{
				if($i ==1){
					$logData.="\nNo Call Log found for ".date('Y-m-d');
			    }	
			}
		}
		
	   }else{
		    $logData.="\nNo Call Log found for ".date('Y-m-d');
       }
		
	    $this->writeCronLogs($logData, true);
		
		return true;

    }
    
    public function callLogFilter($data){
        $result = array();
        if(!empty($data)){
            foreach($data as $value){
               if(!empty($value['dst'])){
                   $checkMobInDb = $this->_getFlatOwnerTable()->getByPhone($value['dst']);
                   if(!empty($checkMobInDb)){
                    $value['user_id'] = $checkMobInDb['user_id'];
                    $result[] = $value;
                   }
               }
            }
        }
        return $result;
    }
    
    public function accessCodeLog(){
		
		$startTime=date("Y-m-d H:i:s");
        $logData="\n\n------------------------------------\n";
        $logData.="Cron for upload acces log on azure file system started $startTime";
        $fopen=fopen(CRON_ACCESS_LOG_LOCK_PATH, 'w');
        if(!flock($fopen, LOCK_EX | LOCK_NB)) {
            $logData.="\nFile already locked.";
            $this->writeCronLogs( $logData, true);
            return false;
        }
		   
		$logFile ='access_log_'.date('dmY').'.CSV';
        
        $data=$this->_getAccessLogsTable()->getList();
        
        if(empty($data)){
		   $logData.="\nNo any data found.";
		   $this->writeCronLogs( $logData, true);
		   return false;
		}
		
		$csvArr = array();
        
        foreach($data as $log){
             $da['log_id'] = $log['log_id'];
             $da['access_id'] = $log['access_id'];
             $da['owner_id'] = $log['owner_id'];
             $da['owner_name'] = $log['owner_name'];
             $da['owner_mobile'] = $log['owner_mobile'];
             $da['owner_info'] = $log['owner_info'];
             $da['date_added'] = $log['date_added'];
             $da['modified_date'] = $log['modified_date'];
             $da['access_code'] = $log['access_code'];
             $da['usage_type'] = $log['usage_type'];
             $da['admin_id'] = $log['admin_id'];
             $da['user_id'] = $log['user_id'];
            $csvArr[] = $da;
        }
        
        $logData.="\nThere are ".count($data)." row found for uploading access log.";
        
        $csvHeaderArr = array_keys(current($data));

        //Create virtual csv for prepare csv content
        $fh = fopen('php://temp', 'rw');
        
        fputcsv($fh, $csvHeaderArr);

        foreach($csvArr as $csv){
            fputcsv($fh, $csv);
        }
        
        rewind($fh);
        $tempCSVContent = stream_get_contents($fh);
        fclose($fh);
         
        //$this->removeFromAzureFile(AZURE_DIRECTORY,$logFile);
       
        $this->uploadFiletoAzure($tempCSVContent,AZURE_DIRECTORY,$logFile);
        
        $logData.="\nData uploaded on azure file system with name $logFile.";
        
        $this->writeCronLogs($logData, true);
        
        return true;
	}

    
    
    public function removeFromAzureFile($dir,$fileName){
        $accountName = ACCOUNT_NAME;
        $accountKey = ACCOUNT_KEY;
        $shareName = SHARE_NAME;
        $connectionString = "DefaultEndpointsProtocol=https;AccountName=$accountName;AccountKey=$accountKey";
        $fileClient = FileRestProxy::createFileService($connectionString);
        $filename = $fileName;
        $bucketName = $dir;
        $dest = $bucketName.'/'.$filename;
        $fileClient->deleteFile($shareName, $dest, null);
        return true;
    }
     

   //Function is in used.
    public function deleteFromAzureAction(){
        $logFile ='access_log'.date('dmY').'.CSV';
        $folder = 'MainLobbyLogs';
        $accountName = ACCOUNT_NAME;
        $accountKey = ACCOUNT_KEY;
        $shareName = SHARE_NAME;
        $connectionString = "DefaultEndpointsProtocol=https;AccountName=$accountName;AccountKey=$accountKey";
        $fileClient = FileRestProxy::createFileService($connectionString);
        $filename = $logFile;
        $bucketName = $folder;
        $dest = $bucketName.'/'.$filename;
        $fileClient->deleteFile($shareName, $dest, null);  
        die("End");
    }
   

    public function uploadFiletoAzure($sourceFile,$bucketName,$fileName){
        $accountName = ACCOUNT_NAME;
        $accountKey = ACCOUNT_KEY;
        $shareName = SHARE_NAME;
        $connectionString = "DefaultEndpointsProtocol=https;AccountName=$accountName;AccountKey=$accountKey";
        $fileClient = FileRestProxy::createFileService($connectionString);
        $destination = $bucketName.'/'.$fileName;
        try {
            $fileClient->createFileFromContent($shareName, $destination, $sourceFile, null);  
            return true;
        } catch (\Throwable $th) {
            $code = $th->getCode();
            $error_message = $th->getMessage();
            echo $code . ": " . $error_message . PHP_EOL;
            die("issue while upload file");
        }
            return ;
    }


    public function updateFromAzureFileAction(){
        $accountName = ACCOUNT_NAME;
        $accountKey = ACCOUNT_KEY;
        $shareName = SHARE_NAME;
        $connectionString = "DefaultEndpointsProtocol=https;AccountName=$accountName;AccountKey=$accountKey";
        $localFilesArr = array();
        
        try {
            $fileClient = FileRestProxy::createFileService($connectionString);
            $list = $fileClient->listDirectoriesAndFiles($shareName);
            foreach ($list->getDirectories() as $dir) {
                $fileDirList = $fileClient->listDirectoriesAndFiles($shareName, $dir->getName());
                foreach ($fileDirList->getFiles() as &$file) {
                    $fileName = $file->getName();              
                    $fileHolder = $fileClient->getFile($shareName,$dir->getName().'/'.$fileName);
                    $source = stream_get_contents($fileHolder->getContentStream());
                    if (!unlink(AZURE_DOWNLOAD_FILE.$dir->getName()."/".$fileName)) {
                        mkdir(AZURE_DOWNLOAD_FILE.$dir->getName());
                    }
                    $localPath = AZURE_DOWNLOAD_FILE.$dir->getName()."/".$fileName;
                    $fp = fopen($localPath, 'w+');
                    chmod($localPath,0755);
                    fwrite($fp, $source); 
                    fclose($fp); 
                      
                    if($dir->getName() == AZURE_ACCESS_CODE){
						$localFilesArr[AZURE_ACCESS_CODE] = $localPath;
                       // $this->updateAccessCode($localPath);
                    }elseif ($dir->getName() == AZURE_FLAT_OWNER){
						$localFilesArr[AZURE_FLAT_OWNER] = $localPath;
                       // $this->updateFlatowner($localPath);
                    }
                }
            }
            
            if(!empty($localFilesArr)) {
            
            $this->updateFlatowner($localFilesArr[AZURE_FLAT_OWNER]);
            
            sleep(2);
            
            $this->updateAccessCode($localFilesArr[AZURE_ACCESS_CODE]); 
		   
		   }
            
        } catch (\Throwable $th) {

            echo $th->getMessage();
        }
        die("End");
    }

    public function updateFlatowner($requestedfile=''){
        $em = $this->getEntityManager();
        $startTime=date("Y-m-d H:i:s");
        $logData="\n\n------------------------------------\n";
        $logData.="Cron For update csv data of flat owner into databse started $startTime";
        $fopen=fopen(CRON_CSV_DATA_UPDATE_LOCK_PATH, 'w');
        if(!flock($fopen, LOCK_EX | LOCK_NB)) {
            $logData.="\nFile already locked.";
            $this->writeCronLogs( $logData, true);
            return false;
        }
        $csvUserIds = array();
        if (file_exists($requestedfile)) {
            $logData.="\nCSV Last uploaded on : ".date("F d Y H:i:s.", filectime($requestedfile));
            $row = 0;
            if (($handle = fopen($requestedfile, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    $num = count($data);
                    $row++;
                    if($row > 1) {

                        for ($c = 0; $c < $num; $c++) {
                            $col[$c] = $data[$c];
                        }
                        $offset = 0;
                        $maxlimit = 1;
                        $randomextension = $em->getRepository('Admin\Entity\GdFlatowner')
                                ->lastaddeduser($offset, $maxlimit);
                        $extension = $randomextension + 100 + 1;
                        $randomOtp = mt_rand(100000, 999999);
                        $sippassword = $this->generateStrongPassword();
                        $encriptedPass = $this->passEncryption($sippassword);
                        $followme1 = $extension;
                        //New csv pattern
                        $user_id  =     $col[0];   //user_id
                        $firstname  =   $col[1];   //firstname
                        $lastname  =    $col[2];   //lastname
                        $unitNumber  =  $col[3];   //unitNumber
                        $floor  =       $col[4];   //floor
                        $email  =       $col[5];   //email
                        $followme2 =    $col[6];   //followme1
                        $followme3  =   $col[7];   //followme2
                        $followme4  =   $col[8];   //followme3
                        $followme5  =   $col[9];   //followme4
                        $confirm_call = 'yes';
                        $voiceval = "default";
                        //$ring_type = "hunt-prim";         
                        $ring_type = "ringallv2-prim"; 
                        $csvUserIds[] = $user_id;
                        $dataArray = array(
                            'user_id'=> $user_id, 
                            'first_name'=> $firstname,
                            'last_name'=> $lastname,
                            'unit_number'=> $unitNumber,
                            'floor'=> $floor,
                            'email'=> $email,
                            'followme1'=>$followme1,
                            'followme2'=>$followme2,
                            'followme3'=>$followme3,
                            'followme4'=>$followme4,
                            'followme5'=>$followme5,
                            'confirmcall'=>$confirm_call,
                            'ring_type'=>$ring_type,
                            'voice_mail'=>$voiceval
                        );  
                                       
                        $getExistingUser = $this->_getFlatOwnerTable()->getByOwner($user_id);
                        if(!empty($getExistingUser)){
                         @list ($getFollowme) = $this->_getFlatOwnerTable()->getWhere(array('user_id'=>$user_id),array('followme2'));

                           if ($getFollowme['followme2'] != $followme2) {
                            
                            $dataArray['sip_password'] = $encriptedPass;
						    $dataArray['mobile'] = $extension;
						    $dataArray['sip_username'] = $extension;
						    $dataArray['parent_id'] = 0;
						    $dataArray['sip_extention_num'] = $extension;
                            $dataArray['sip_download_code'] = $randomOtp; 
                            $followme=$followme1.",".$followme2.",".$followme3.",".$followme4.",".$followme5;
                            $updateextapi = $this->updateextensionapi($extension, $followme, $encriptedPass, $ring_type, $confirm_call);

                           }

                            $this->_getFlatOwnerTable()->updateFlatOwner($dataArray,$user_id);
                            $logData.="\nRecord updated for user_id ".$user_id;
                        }else{
							
						$followme=$followme1.",".$followme2.",".$followme3.",".$followme4.",".$followme5;
							
						$addextapi = $this->addextensionapi($firstname, $lastname, $extension, $sippassword, $followme);
							
						$dataArray['sip_password'] = $encriptedPass;
						$dataArray['mobile'] = $extension;
						$dataArray['sip_username'] = $extension;
						$dataArray['parent_id'] = 0;
						$dataArray['sip_extention_num'] = $extension;
						$dataArray['sip_download_code'] = $randomOtp ; 
						$flatOwnerObj = new GdFlatowner();
						$flatOwnerObj->exchangeArray($dataArray); 
						$this->_getFlatOwnerTable()->add($flatOwnerObj);
						$logData.="\nNew record inserted for user_id ".$user_id;
                        } 
                    }
                }
              fclose($handle);

             $logData.="\nFile read completed.";
            }

            $getDatabaseUserId = $this->_getFlatOwnerTable()->getUserId();

            $getDbuserIds = array_column($getDatabaseUserId, 'user_id');
            
            $userIdNotAvailableinCSV = array_diff($getDbuserIds,$csvUserIds);

            if(empty($userIdNotAvailableinCSV)){
                $logData.="\nNo any extra user exist in table who's data is not available in csv list.";
                $this->writeCronLogs($logData, true);
                return true;
            }else{
                foreach($userIdNotAvailableinCSV as $user){
                    @list ($getExt) = $this->_getFlatOwnerTable()->getWhere(array('user_id'=>$user),array('mobile'));
                    $extension = $getExt['mobile'];
                    $removeExt=$this->deleteextensionapi($extension);
                    $this->_getFlatOwnerTable()->remove($user);
                }
                $logData.="\nUser removed from database ".implode(',',$userIdNotAvailableinCSV);
            }
            $this->writeCronLogs($logData, true);
            return true;
        }else{ 
            $logData.="\nFile not exist.";
            $this->writeCronLogs($logData, true);
            return true;
        }
        return true;
    }
    

    public function updateAccessCode($requestedfile = ''){
        $startTime=date("Y-m-d H:i:s");
        $logData="\n\n------------------------------------\n";
        $logData.="Cron For update csv data of access code into databse started $startTime";

        $fopen=fopen(CRON_CSV_DATA_ACCESS_CODE_LOCK_PATH, 'w');
        if(!flock($fopen, LOCK_EX | LOCK_NB)) {
            $logData.="\nFile already locked.";
            $this->writeCronLogs( $logData, true);
            return false;
        }

        if (file_exists($requestedfile)) {
            $logData.="\nCSV Last uploaded on : ".date("F d Y H:i:s.", filectime($requestedfile));
            $row = 0;
            if (($handle = fopen($requestedfile, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    $num = count($data);
                    $row++;
                    if ($row > 1) {
                        for ($c = 0; $c < $num; $c++) {
                            $col[$c] = $data[$c];
                        }
                       
                        $user_id  =     $col[0];   //user_id
                        $accessCode  =  $col[1];   //access code

                 if (!empty($user_id) && !empty($accessCode) && (strlen($accessCode) == 6)) {
					 
					 $getExistingUser = $this->_getFlatOwnerTable()->getByOwner($user_id);
					 
					 //echo '<pre>'; print_r($getExistingUser);  
					 
                     $csvUserIds[] = $user_id;
                     $userOwner_id[] = $getExistingUser['id'];
                     $codeData=array(
                                "owner_id"=>$getExistingUser['id'],
                                "access_code"=>$accessCode,
                                "usage_type"=> 2,
                                "access_date_time"=> 1,
                                "code_status"=>1,
                                "admin_id"=>1
                            );   

                     $condition = array('owner_id'=>$getExistingUser['id']);
                     @list($alreadyExistOwnerAccessCode) = $this->_getAccessCodesTable()->getWhere($condition);
                     if (!empty($alreadyExistOwnerAccessCode)) {
                         $this->_getAccessCodesTable()->updateOwnerAccessCode($codeData, $user_id);
                         $logData.="\nRecord updated for user_id ".$user_id;
                     } else {
                         $entity=new AccessCodes();
                         $entity->exchangeArray($codeData);
                         $accessId=$this->_getAccessCodesTable()->addCode($entity);
                         $logData.="\nNew record inserted for user_id ".$user_id;
                     }
                    }
                }
            }
              fclose($handle);

             $logData.="\nFile read completed.";
            }

            $getDatabaseOwnerUserId = $this->_getAccessCodesTable()->getOwnerUserId();

            $getDbuserIds = array_column($getDatabaseOwnerUserId, 'owner_id');
            
            //$userIdNotAvailableinCSV = array_diff($getDbuserIds,$csvUserIds);
            
            $userIdNotAvailableinCSV = array_diff($getDbuserIds,$userOwner_id);

            if(empty($userIdNotAvailableinCSV)){
                $logData.="\nNo any extra user exist in table who's data is not available in csv list.";
                $this->writeCronLogs($logData, true);
                return true;
            }else{
                foreach($userIdNotAvailableinCSV as $user){
                    $this->_getAccessCodesTable()->removeOwner($user);
                }
                $logData.="\nUser removed from database ".implode(',',$userIdNotAvailableinCSV);
            }

            $this->writeCronLogs($logData, true);
            return true;
        }else{ 
            $logData.="\nFile not exist.";
            $this->writeCronLogs($logData, true);
            return true; 
        }
        return true;
    }


    public function getCallLogs($data) {
        $URL=$this->url."call_log.php";
        $request=array($data);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $URL);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($request));
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);

        $json_response = curl_exec($curl);
        $response = @json_decode($json_response, true);
        curl_close($curl);

        // print_r($response);
        // die();
        
        return $response;
    }


    /*
     * add extension api function
     * this api will be used for adding extension to asterik server
     * @author Nishikant
     * @craeted_date 10th March, 2017
     */

    public function addextensionapi($fname, $lname, $ext, $pass, $followme, $email="", $voicemail="", $ringType="", $confirmCall="") {
        $name = $fname . " " . $lname;
        $service_url = ASTERIK_URL . 'Extension_FollowMe_api/add_extension.php';
        $curl = curl_init($service_url);
        $curl_post_data[] = array(
            "extension" => $ext,
            "password" => $pass,
            "name" => $name,
            "outboundcid" => $ext,
            "email"=>$email,
            "voicemail"=>$voicemail,
            "findmefollow" => $followme,
            "ring_type"=>$ringType,
            "confirm_calls"=>$confirmCall,
            "api_key" => "ce2cf9b55b4f9ed54fd32ea3bf657ae6",
            "transport" => "tcp",
            "action" => "INSERT"
        );

		$curl_post_datajson = json_encode($curl_post_data);
		
		// $this->getServiceLocator()->get('Zend\Log')->info($service_url);
		// $this->getServiceLocator()->get('Zend\Log')->info($curl_post_datajson);
        
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_datajson);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array
            (
            'Content-Type: application/json',
            'Content-Length: ' . strlen($curl_post_datajson))
        );
        $curl_response = curl_exec($curl);
        curl_close($curl);
        $responseSu = json_decode($curl_response);
        $succStatus = $responseSu[0]->status;
        return $succStatus;
    }
  

    /*
     * add extension api function
     * this api will be used for adding extension to asterik server
     * @author Nishikant
     * @craeted_date 10th March, 2017
     */

    public function updateextensionapi($ext, $followme, $sippass, $ringType="", $confirmCall="") {

        $service_url = ASTERIK_URL . 'Extension_FollowMe_api/add_extension.php';
        $curl = curl_init($service_url);
        $curl_post_data[] = array(
            "extension" => $ext,
			"password" => $sippass,
            "findmefollow" => $followme,
            "ring_type"=>$ringType,
            "confirm_calls"=>$confirmCall,
            "api_key" => "ce2cf9b55b4f9ed54fd32ea3bf657ae6",
            "action" => "UPDATE"
        );
        $curl_post_datajson = json_encode($curl_post_data);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_datajson);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array
            (
            'Content-Type: application/json',
            'Content-Length: ' . strlen($curl_post_datajson))
        );
        $curl_response = curl_exec($curl);
        curl_close($curl);
        $responseSu = json_decode($curl_response);
        $succStatus = $responseSu[0]->status;		
        return $succStatus;
    }
    

    /*
     * delete extension api function
     * this api will be used for delete extension to asterik server
     * @author Nishikant
     * @craeted_date 5th July, 2017
     */

    public function deleteextensionapi($ext) {      
        $service_url = ASTERIK_DELETE_API;
        $curl = curl_init($service_url);
        $curl_post_data[] = array(
            "extension" => $ext
        );
        $curl_post_datajson = json_encode($curl_post_data);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_datajson);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array
            (
            'Content-Type: application/json',
            'Content-Length: ' . strlen($curl_post_datajson))
        );
        $curl_response = curl_exec($curl);
        curl_close($curl);
        $responseSu = json_decode($curl_response);
        $succStatus = $responseSu[0]->status;        
        return $succStatus;
    }


    public function writeCronLogs($logData, $finished=false) {
        $fileName=date("Y-m-d");
        $logger = new \Zend\Log\Logger();
        $writer = new \Zend\Log\Writer\Stream("./data/crons/$fileName");
        $logger->addWriter($writer);
        
        if($finished) {
            $endTime=date("Y-m-d H:i:s");
            $logData.="\nCron finished at $endTime";
            $logData.="\n------------------------------------\n";
        }
        
        $logger->info($logData);
        return true;
    }


    function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'ludms') {
        $sets = array();
        if (strpos($available_sets, 'l') !== false) {
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        }
        if (strpos($available_sets, 'u') !== false) {
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        }
        if (strpos($available_sets, 'd') !== false) {
            $sets[] = '23456789';
        }

        if (strpos($available_sets, 'm') !== false) {
            $sets[] = '98765432';
        }
        if (strpos($available_sets, 's') !== false) {
            $sets[] = '!@#$%&*';
        }
        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];
        $password = str_shuffle($password);
        if (!$add_dashes)
            return $password;
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }

    public function passEncryption($pass) {
        $iv = mcrypt_create_iv(
                mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM
        );

        $key = ENCRYPTION_KEY;
        $encryptedpass = base64_encode(
                $iv .
                mcrypt_encrypt(
                        MCRYPT_RIJNDAEL_128, hash('sha256', $key, true), $pass, MCRYPT_MODE_CBC, $iv
                )
        );
        return $encryptedpass;
    }

    protected $_flatownerTable;
    protected function _getFlatOwnerTable() {
        if(!$this->_flatownerTable) {
            $sm=@$this->getServiceLocator();
            $this->_flatownerTable=$sm->get("Admin\Model\GdFlatownerTable");
        }
        return $this->_flatownerTable;
    }

    protected $_accessCodesTable;
    public function _getAccessCodesTable() {
        if(!$this->_accessCodesTable) {
            $sm=$this->getServiceLocator();
            $this->_accessCodesTable=$sm->get("Admin\Model\AccessCodesTable");
        }
        return $this->_accessCodesTable;
    }

    protected $_accessLogsTable;
    public function _getAccessLogsTable() {
        if(!$this->_accessLogsTable) {
            $sm=$this->getServiceLocator();
            $this->_accessLogsTable=$sm->get("Admin\Model\AccessLogsTable");
        }
        return $this->_accessLogsTable;
    } 

}
