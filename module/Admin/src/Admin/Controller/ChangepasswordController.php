<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Admin\Form as AdminForms;
use Zend\Mvc\MvcEvent;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\View\Model\JsonModel;

class ChangepasswordController extends AbstractActionController
{
	protected $em;
    protected $authservice;
	
    public function onDispatch(MvcEvent $e)
    {
    	$admin_session = new Container('admin');
       	$username = $admin_session->username;
       	$usertype = $admin_session->usertype;           
       	$this->layout('layout/adminlayout');
       	return parent::onDispatch($e);
    }
    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }
    
    public function getEntityManager()
    {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }
    
    public function getAuthService()
    {
        if (! $this->authservice) {
            $this->authservice = $this->getServiceLocator()->get('AuthService');
        }
        return $this->authservice;
    }    
  
  	public function changepasswordAction(){
  		$em = $this->getEntityManager();          
        $this->layout('layout/adminlayout');
        
        $admin_session = new Container('admin');
        $username = $admin_session->username;
        $id = $admin_session->userId;
        $usertype = $admin_session->usertype;
        $firstname = $admin_session->firstname;
        
        $this->layout()->pageTitle = 'Change password';
        $this->layout()->userType = $usertype;
        $this->layout()->firstname = $firstname;

 		$msg = "";
        $form = new AdminForms\ChangePasswordForm($em);
        $form->get('submitbutton')->setValue('Save');

        $request = $this->getRequest();
        if ($request->isPost()) {
        	$formValidator = new AdminForms\Validator\ChangePasswordFormValidator();
            $form->setInputFilter($formValidator->getInputFilter());
            $formdata = $request->getPost();
            $formdata['oldpass'] = md5(trim($formdata['oldpass']));
            $form->setData($request->getPost());
            if ($form->isValid()) { 
            $verifyuser = $em->getRepository('Admin\Entity\GdAdmin')
            ->findOneBY(array('id'=>$id, 'userType'=>$usertype, 'password'=>$formdata['oldpass']));
            	if($verifyuser){
            		$verifyuser->setPassword(trim($formdata['newpass']));
            		$em->persist($verifyuser);
                    $em->flush();

                    $flashMessenger = $this->flashMessenger();
                    $flashMessenger->setNamespace('success');
                    $msg = PASS_CHANGE;
                    $flashMessenger->addMessage($msg);
                    return $this->redirect()->toRoute('adminprofile');
            	}else{
                 	return array('form' => $form, 'error' =>INVALID_OLD_PASS);
            	}
            }else{
            	$error = "Something Went wrong! Please Try again.";
                 return array('form' => $form, 'error' => $error);
            }
        }
		return array('form' => $form, 'success' => $msg);
  	}

}