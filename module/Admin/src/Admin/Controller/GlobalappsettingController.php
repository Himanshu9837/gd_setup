<?php
/**
 * Zend Framework (http://framework.zend.com/)
 * This class is used for Login, Log out, Forgot Password and Change Password.
 */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Admin\Form as AdminForms;
use Zend\Mail as Mail;
use Zend\Mvc\MvcEvent;
use Admin\Entity as Entities;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

class GlobalappsettingController extends AbstractActionController {

protected $em;
    protected $authservice;
	
    /* This function is like old init() */
    public function onDispatch(MvcEvent $e)
    {
    	$admin_session = new Container('admin');
        $username = $admin_session->username;
        $usertype = $admin_session->usertype;
        if (empty($username) && $usertype == 2) {            
            return $this->redirect()->toRoute('adminlogin');
        }
       $this->layout('layout/adminlayout');
        return parent::onDispatch($e);
    }
    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }
    
    public function getEntityManager()
    {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }
    
    public function getAuthService()
    {
        if (! $this->authservice) {
            $this->authservice = $this->getServiceLocator()
                                      ->get('AuthService');
        }
        return $this->authservice;
    }
    
    
    /**
     * Used to save global app settings
     * @author		Nishikant
     * @created_date	10th Jan, 2016
     * @modified_date	-------------
     */
    
     public function globalAppSettingAction() {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $this->layout()->pageTitle = 'Global App Setting';
        $admin_session = new Container('admin');
    	$usertype = $admin_session->usertype;
    	$firstname = $admin_session->firstname;
    	$this->layout()->userType = $usertype;
    	$this->layout()->firstname = $firstname;
	
	 if($usertype == 2) {
            return $this->redirect()->toRoute('admindashboard');
            $flashMessenger = $this->flashMessenger();
            $flashMessenger->setNamespace('Not Authorised');
            $msg = "You are not authorised to access";
            $flashMessenger->addMessage($msg);
        }
        // get data from db
        $configId = $em->getRepository('Admin\Entity\GdAppSettings')->globalconfigrecord();
        if ((!empty($configId)) && (isset($configId))) {
            $gappObj = $em->getRepository('Admin\Entity\GdAppSettings')->findOneBy(array('id' => $configId['id']));
            // edit mode           
            $cmIphost = $gappObj->getCmIphost();
            $cmPort = $gappObj->getCmPort();
            $cmName = $gappObj->getCmName();
            $cmChannel = $gappObj->getCmChannel();
            $cmUsername = $gappObj->getCmUsername();
            $cmPwd = $gappObj->getCmPwd();
            $cmStreaming = $gappObj->getCmStreaming();
            $sipAddr = $gappObj->getSipAddr();
            $sipPort = $gappObj->getSipPort();
            $sipTransport = $gappObj->getSipTransport();
            $sipSer1addr = $gappObj->getSipSer1addr();
            $sipSer1port = $gappObj->getSipSer1Port();
            $sipSer1Transport = $gappObj->getSipSer1Transport();
            $sipSer1expires = $gappObj->getSipSer1Expires();
            $sipSer1Register = $gappObj->getSipSer1Register();
            $sipSer1Retrytmout = $gappObj->getSipSer1Retrytmout();
            $sipSer1Retrymaxct = $gappObj->getSipSer1Retrymaxct();
            $sipSer1Lnsztmout = $gappObj->getSipSer1Lnsztmout();
			$updateConfigUrl = $gappObj->getUpdateConfigUrl();

            // view in edit mode
            $form = new AdminForms\GlobalAppSettingForm($em, $configId['id']);
            $form->get('submitbutton')->setValue('Save');            

            $form->get('cmiphost')->setValue($cmIphost);
            $form->get('cmport')->setValue($cmPort);
            $form->get('cmname')->setValue($cmName);
            $form->get('cmchannel')->setValue($cmChannel);
            $form->get('cmunmae')->setValue($cmUsername);
            $form->get('cmpwd')->setValue($cmPwd);
            $form->get('streaming')->setValue($cmStreaming);
            $form->get('sipaddr')->setValue($sipAddr);
            $form->get('sipport')->setValue($sipPort);
            $form->get('siptransport')->setValue($sipTransport);
            $form->get('sipser1addr')->setValue($sipSer1addr);
            $form->get('sipser1port')->setValue($sipSer1port);
            $form->get('sipser1transport')->setValue($sipSer1Transport);
            $form->get('sipser1expires')->setValue($sipSer1expires);
            $form->get('sipser1register')->setValue($sipSer1Register);
            $form->get('sipser1retrytmout')->setValue($sipSer1Retrytmout);
            $form->get('sipser1retrymaxct')->setValue($sipSer1Retrymaxct);
            $form->get('sipser1lnsztmout')->setValue($sipSer1Lnsztmout);
			$form->get('configurl')->setValue($updateConfigUrl);

            $request = $this->getRequest();
            if ($request->isPost()) {
                $formValidator = new AdminForms\Validator\GlobalAppSettingFormValidator();
                $form->setInputFilter($formValidator->getInputFilter());
                $formdata = $request->getPost();
                $form->setData($request->getPost());
                if ($form->isValid()) {
                    $gappObj->setCmIphost($formdata['cmiphost']);
                    $gappObj->setCmPort($formdata['cmport']);
                    $gappObj->setCmName($formdata['cmname']);
                    $gappObj->setCmChannel($formdata['cmchannel']);
                    $gappObj->setCmUsername($formdata['cmunmae']);
                    $gappObj->setCmPwd($formdata['cmpwd']);
                    $gappObj->setCmStreaming($formdata['streaming']);
                    $gappObj->setSipAddr($formdata['sipaddr']);
                    $gappObj->setSipPort($formdata['sipport']);
                    $gappObj->setSipTransport($formdata['siptransport']);
                    $gappObj->setSipSer1addr($formdata['sipser1addr']);
                    $gappObj->setSipSer1Port($formdata['sipser1port']);
                    $gappObj->setSipSer1Transport($formdata['sipser1transport']);
                    $gappObj->setSipSer1Expires($formdata['sipser1expires']);
                    $gappObj->setSipSer1Register($formdata['sipser1register']);
                    $gappObj->setSipSer1Retrytmout($formdata['sipser1retrytmout']);
                    $gappObj->setSipSer1Retrymaxct($formdata['sipser1retrymaxct']);
                    $gappObj->setSipSer1Lnsztmout($formdata['sipser1lnsztmout']);
					$gappObj->setUpdateConfigUrl($formdata['configurl']);
                    $em->persist($gappObj);
                    $em->flush();
                    $flashMessenger = $this->flashMessenger();
                    $flashMessenger->setNamespace('success');
                    $msg = "Global app seetings updated successfully";
                    $flashMessenger->addMessage($msg);
                    return $this->redirect()->toRoute('admindashboard');
                }
            }
        } else {
            $form = new AdminForms\GlobalAppSettingForm($em);
            $form->get('submitbutton')->setValue('Save');
            $this->layout()->pageTitle = 'Global App Setting';
            $formdata = $request->getPost();
            $gappsettingObj = new Entities\GdAppSettings();
            if ($request->isPost()) {
                $formValidator = new AdminForms\Validator\GlobalAppSettingFormValidator();
                $form->setInputFilter($formValidator->getInputFilter());
                $formdata = $request->getPost();
                $form->setData($request->getPost());
                if ($form->isValid()) {
                    $currentDate = date('Y-m-d H:i:s');
                    $gappsettingObj->setCmIphost($formdata['cmiphost']);
                    $gappsettingObj->setCmPort($formdata['cmport']);
                    $gappsettingObj->setCmName($formdata['cmname']);
                    $gappsettingObj->setCmChannel($formdata['cmchannel']);
                    $gappsettingObj->setCmUsername($formdata['cmunmae']);
                    $gappsettingObj->setCmPwd($formdata['cmpwd']);
                    $gappsettingObj->setCmStreaming($formdata['streaming']);
                    $gappsettingObj->setSipAddr($formdata['sipaddr']);
                    $gappsettingObj->setSipPort($formdata['sipport']);
                    $gappsettingObj->setSipTransport($formdata['siptransport']);
                    $gappsettingObj->setSipSer1addr($formdata['sipser1addr']);
                    $gappsettingObj->setSipSer1Port($formdata['sipser1port']);
                    $gappsettingObj->setSipSer1Transport($formdata['sipser1transport']);
                    $gappsettingObj->setSipSer1Expires($formdata['sipser1expires']);
                    $gappsettingObj->setSipSer1Register($formdata['sipser1register']);
                    $gappsettingObj->setSipSer1Retrytmout($formdata['sipser1retrytmout']);
                    $gappsettingObj->setSipSer1Retrymaxct($formdata['sipser1retrymaxct']);
                    $gappsettingObj->setSipSer1Lnsztmout($formdata['sipser1lnsztmout']);
					$gappsettingObj->setUpdateConfigUrl($formdata['configurl']);
                    $gappsettingObj->setCreatedDate($currentDate);
                    $em->persist($gappsettingObj);
                    $em->flush();
                    $flashMessenger = $this->flashMessenger();
                    $flashMessenger->setNamespace('success');
                    $msg = "Global app seetings added successfully";
                    $flashMessenger->addMessage($msg);
                    return $this->redirect()->toRoute('admindashboard');
                }
            }
        }
        return array('form' => $form, 'success' => $msg);
    }
 
}
