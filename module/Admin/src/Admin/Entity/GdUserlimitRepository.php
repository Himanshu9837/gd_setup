<?php

namespace Admin\Entity;

use Doctrine\ORM\EntityRepository;

class GdUserlimitRepository extends EntityRepository {

    public function userlimitRecord(){
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder('u')
            ->from('Admin\Entity\GdUserlimit', 'u')
            ->select('u.id, u.maxUser') 
            ->where('u.status = ?1') 
            ->setParameter(1, '1')            
            ->getQuery();               
    $result = $query->getOneOrNullResult();        
    return $result;
    }

}