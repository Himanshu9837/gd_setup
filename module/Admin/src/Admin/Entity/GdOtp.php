<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GdOtp
 *
 * @ORM\Table(name="gd_otp")
 * @ORM\Entity(repositoryClass="GdOtpRepository")
 */
class GdOtp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=true)
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="otp", type="string", length=255, nullable=true)
     */
    private $otp;

    /**
     * @var string
     *
     * @ORM\Column(name="enc_otp", type="string", length=255, nullable=true)
     */
    private $encOtp;

    /**
     * @var integer
     *
     * @ORM\Column(name="device_type", type="string", nullable=true)
     */
    private $deviceType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="device_token", type="string", length=255, nullable=true)
     */
    private $deviceToken;

    /**
     * @var string
     *
     * @ORM\Column(name="unique_id", type="string", length=255, nullable=true)
     */
    private $uniqueId;

    /**
     * @var string
     *
     * @ORM\Column(name="otp_status", type="string", nullable=false)
     */
    private $otpStatus = 'new';

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="string", nullable=true)
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_date", type="string", nullable=false)
     */
    private $modifiedDate;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return GdOtp
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set otp
     *
     * @param string $otp
     *
     * @return GdOtp
     */
    public function setOtp($otp)
    {
        $this->otp = $otp;

        return $this;
    }

    /**
     * Get otp
     *
     * @return string
     */
    public function getOtp()
    {
        return $this->otp;
    }

    /**
     * Set encOtp
     *
     * @param string $encOtp
     *
     * @return GdOtp
     */
    public function setEncOtp($encOtp)
    {
        $this->encOtp = $encOtp;

        return $this;
    }

    /**
     * Get encOtp
     *
     * @return string
     */
    public function getEncOtp()
    {
        return $this->encOtp;
    }

    /**
     * Set deviceType
     *
     * @param integer $deviceType
     *
     * @return GdOtp
     */
    public function setDeviceType($deviceType)
    {
        $this->deviceType = $deviceType;

        return $this;
    }

    /**
     * Get deviceType
     *
     * @return integer
     */
    public function getDeviceType()
    {
        return $this->deviceType;
    }

    /**
     * Set deviceToken
     *
     * @param string $deviceToken
     *
     * @return GdOtp
     */
    public function setDeviceToken($deviceToken)
    {
        $this->deviceToken = $deviceToken;

        return $this;
    }

    /**
     * Get deviceToken
     *
     * @return string
     */
    public function getDeviceToken()
    {
        return $this->deviceToken;
    }

    /**
     * Set uniqueId
     *
     * @param string $uniqueId
     *
     * @return GdOtp
     */
    public function setUniqueId($uniqueId)
    {
        $this->uniqueId = $uniqueId;

        return $this;
    }

    /**
     * Get uniqueId
     *
     * @return string
     */
    public function getUniqueId()
    {
        return $this->uniqueId;
    }

    /**
     * Set otpStatus
     *
     * @param string $otpStatus
     *
     * @return GdOtp
     */
    public function setOtpStatus($otpStatus)
    {
        $this->otpStatus = $otpStatus;

        return $this;
    }

    /**
     * Get otpStatus
     *
     * @return string
     */
    public function getOtpStatus()
    {
        return $this->otpStatus;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return GdOtp
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     *
     * @return GdOtp
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     *
     * @return GdOtp
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;

        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }
}
