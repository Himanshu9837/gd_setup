<?php

namespace Admin\Entity;

use Doctrine\ORM\EntityRepository;

class GdFlatownerRepository extends EntityRepository {

    /**
     * This is used to listing Travellers 
     * 
     * @author        Nishikant
     * @param         mmixed $data
     * @return        mixed $result
     * @created_date  8th Oct, 2016
     * @modified_date --------------
     */
    public function getOwnerListing($sqlArr, $limited = 0) {
        if ($limited == 1) {
            /* For Paging in DataTables */
            $columnArr = ['u.firstName','u.firstName', 'u.lastName', 'u.unitNumber', 'u.floor'];
            $sortByColumnName = $columnArr[$sqlArr['sortcolumn']];

            $sortType = $sqlArr['sorttype'];
            $offSet = $sqlArr['iDisplayStart'];
            $limit = $sqlArr['limit'];

            $offSet = (int) $offSet;
            $limit = (int) $limit;

            $sortType = strtoupper($sortType);
            if ($sortType == "ASC") {
                $sortType = 'ASC';
            } else {
                $sortType = 'DESC';
            }
        }

        $searchKey = $sqlArr['searchKey'];
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder('u');
        $query = $query->from('Admin\Entity\GdFlatowner', 'u');
        $query = $query->select('u.id, u.parent_id, u.email, u.firstName, u.lastName, u.mobile, u.unitNumber, u.status, u.floor, u.ringType, u.confirmcall, u.followme1, u.followme2, u.followme3, u.followme4, u.followme5, u.createdDate');

        $query = $query->where('u.parent_id=0');

        if (trim($searchKey) != '') {
            $searchKey = str_replace("<br>", '', trim($searchKey));
            $query = $query->andWhere('u.unitNumber LIKE :searchterm OR u.firstName LIKE :searchterm OR u.lastName LIKE :searchterm OR u.floor LIKE :searchterm')
                    ->setParameter('searchterm', '%' . $searchKey . '%');
        }

        if ($limited == 1) {
            $query = $query->setMaxResults($limit);
            $query = $query->setFirstResult($offSet);
            $query = $query->orderBy($sortByColumnName, $sortType);
        }
        $query = $query->getQuery();
        $Result = $query->getResult();

        return $Result;
    }

    /**
     * getOwnerListingAtAdminForDataTable
     * @author Nishikant   
     * @param mixed $sqlArr - Data received from datatable
     * @return mixed Data to be sent to Datatable in desired format
     */
    public function getOwnerListingAtAdminForDataTable($sqlArr, $basePath, $ownerDeleteUrl, $ChangeStatusUrl) {

        $sEcho = $sqlArr['sEcho'];
        /* To fetch Total no of records */
        $Result = $this->getOwnerListing($sqlArr, 0);
        $totalRecordCount = count($Result);
        /* To fetch records with paging */
        $Result = $this->getOwnerListing($sqlArr, $limited = 1);


        $key = 0;
        $TravellerDataList['sEcho'] = $sEcho;
        $TravellerDataList['iTotalRecords'] = $totalRecordCount;
        $TravellerDataList['iTotalDisplayRecords'] = $totalRecordCount;

        foreach ($Result as $userRow) {
            $id = $userRow['id'];
            if ($userRow['status'] == '1') {
                $status = '<button title="Click here to deactivate" id="status_' . $id . '" onclick="activeInactiveUser(' . $id . ',\'inactive\');" class="btn btn-xs btn-danger" type="button">Deactivate</button>';
            } else {
                $status = '<button title="Click here to activate" id="status_' . $id . '" onclick="activeInactiveUser(' . $id . ',\'active\');" class="btn btn-xs btn-success" type="button">Activate</button>';
            }
            $deleteUrl11 = $ownerDeleteUrl . $id;
            $edit_url = $basePath . "/admin/owner/edit/" . $id;

            $editstr = '<a class="btn btn-app" id="edit" title="Click here to edit" alt="Edit" href="' . $edit_url . '" > <i class="fa fa-edit"></i>Edit</a>';
            $deleteUrl = '<a class="btn btn-app" id="del_' . $id . '" alt="Delete" title="Click here to delete this user" href="javascript:void(0);" onClick="deleteowner(' . $id . ')"> <i class="fa fa-trash"></i>Delete</a>';

            $loader = '<div id="ajax-loader-' . $id . '" style="display:none;position:absolute;right:154px;z-index:9;
                top: 24px;"><img src="/img/ajax-loader.gif" class="img-responsive" /></div>';

            /* $deleteUrl = '<button class="btn btn-xs btn-danger" type="button" data-toggle="modal" data-target="#confirmDelete"  data-title="'.$id.'" data-message="Are you sure you want to delete this user ?">
              <i class="glyphicon glyphicon-trash"></i> Delete
              </button>'; */

            $TravellerDataList['data'][$key][0] = $sqlArr['iDisplayStart']+$key+1;;
            $TravellerDataList['data'][$key][1] = $userRow['firstName'];
            $TravellerDataList['data'][$key][2] = $userRow['lastName'];
            $TravellerDataList['data'][$key][3] = $userRow['unitNumber'];
            $TravellerDataList['data'][$key][4] = $userRow['floor'];
            $TravellerDataList['data'][$key][5] = $userRow['mobile'];
            // $TravellerDataList['data'][$key][2]  =  $status."&nbsp;&nbsp;".$editstr."&nbsp;&nbsp;".$deleteUrl;
            $TravellerDataList['data'][$key][6] = $editstr . "&nbsp;&nbsp;" . $deleteUrl . "&nbsp;&nbsp;"  . $loader . "&nbsp;&nbsp;" . $status;

            $blindPath = $basePath . "/admin/owner/subowners/" . $id;
            $blindAddPath=$basePath . "/admin/owner/add?t=sub&u=".$id;
            $blindUser = '<a class="btn btn-app" id="childUser" title="View Blind Users" alt="View User" href="' . $blindPath . '" > <i class="fa fa-eye"></i>View</a> <a class="btn btn-app" title="Add Blind User" alt="Add User" href="' . $blindAddPath . '" > <i class="fa fa-plus"></i>Add</a>';
            $TravellerDataList['data'][$key][7] = $blindUser;

            $key++;
        }
        if ($key == 0) {
            $TravellerDataList['data'] = '';
        }
        return $TravellerDataList;
    }

    public function checkIfUserAlreadyExist($phone) {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder('u');
        $query = $query->from('Admin\Entity\GdFlatowner', 'u');
        $query = $query->select('u.id, u.firstName, u.lastName');
        $query = $query->where('u.mobile = :phone')->setParameter('phone', $phone);
        $query = $query->getQuery();
        $result = $query->getResult();
        return $result;
    }

    /**
     * checkIfEmailAlreadyExist - Check if mobile exist in DB or not, 
     * Need to pass notInUserId param if need to edit entry for particular user
     * @param string $email
     * @param integer $notInUserId
     * @return mixed
     * @author Nishikant
     */
    public function checkIfMobileAlreadyExist($mobileName, $notInUserId = 0) { //die("123");
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder('u');
        $query = $query->from('Admin\Entity\GdFlatowner', 'u');
        $query = $query->select('u.id');
        $query = $query->where('u.mobile = :mobile')->setParameter('mobile', $mobileName);
        if (!empty($notInUserId)) {
            $query = $query->andWhere("u.id != :userId")->setParameter("userId", $notInUserId);
        }
        $query = $query->getQuery();
        $result = $query->getResult();
        return $result;
    }

    public function totalOwner($notIn=false) {
       $em = $this->getEntityManager();
        $query = $em->createQueryBuilder('u');
        $query = $query->from('Admin\Entity\GdFlatowner', 'u');
        $query = $query->select('COUNT(u) as totalmember');
        $query = $query->where('u.status = :st')->setParameter('st', 1);
        $query = $query->andWhere("u.parent_id=0");
        if($notIn) $query = $query->andWhere("u.id NOT IN($notIn)");
        $query = $query->getQuery();
        $result = $query->getResult();
        return $result;
    }
	
	public function totalOwnerUnit($notIn=false) {
       $em = $this->getEntityManager();
        $query = $em->createQueryBuilder('u');
        $query = $query->from('Admin\Entity\GdFlatowner', 'u');
        $query = $query->select('COUNT(u) as totalmember');
        $query = $query->where('u.status = :st')->setParameter('st', 1);
        $query = $query->andWhere("u.parent_id=0");
	$query = $query->andWhere("u.unitNumber!=''");
        if($notIn) $query = $query->andWhere("u.id NOT IN($notIn)");
        $query = $query->getQuery();
        $result = $query->getResult();
        return $result;
    }

    
     /**
     * Check total users whether active or deactive
     * @author Nishikant
     */
    public function totalusers() {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder('u');
        $query = $query->from('Admin\Entity\GdFlatowner', 'u');
        $query = $query->select('COUNT(u) as totalmember');       
        $query = $query->getQuery();
        $result = $query->getResult();
        return $result;
    }
    
    public function ownersNamelist($offset, $maxlimit, $notIn=false) {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder('u');
        $query = $query->from('Admin\Entity\GdFlatowner', 'u');
        $query = $query->select('u.firstName, u.lastName, u.unitNumber, u.mobile, u.floor');
        $query = $query->where('u.status = :st')->setParameter('st', 1);
        $query = $query->andWhere("u.parent_id=0");
        $query = $query->andWhere("u.firstName!='' or u.lastName!=''");
        if($notIn) $query = $query->andWhere("u.id NOT IN($notIn)");
        $query = $query->orderBy('u.firstName');
        $query = $query->setFirstResult($offset);
        $query = $query->setMaxResults($maxlimit);
        $query = $query->getQuery();

//         echo $query->getSQL();
//         die();
        
        $result = $query->getResult();
        return $result;
    }

    public function jointsetA($offset, $maxlimit) {
        $em = $this->getEntityManager();
        $sql = "SELECT * FROM gd_flatowner WHERE parent_id=0 AND unit_number IS NOT NULL AND unit_number <>  '' ORDER BY floor, unit_number LIMIT " . $offset . "," . $maxlimit;
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function moreUnitlist($offset, $maxlimit) {
        $em = $this->getEntityManager();
        $sql = "SELECT * FROM gd_flatowner WHERE parent_id=0 AND unit_number IS NOT NULL AND unit_number <>  '' ORDER BY floor, unit_number LIMIT " . $offset . "," . $maxlimit;
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function unitnumlist() {

        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder('u');
        $query = $query->from('Admin\Entity\GdFlatowner', 'u');
        $query = $query->select('u.firstName, u.lastName, u.unitNumber, u.mobile, u.floor');
        $query = $query->getQuery();
        $result = $query->getResult();
        return $result;
    }

    public function searchowner($skey, $offset, $maxlimit, $inactive=true) {
        $em = $this->getEntityManager();
        $sql = "SELECT * FROM gd_flatowner WHERE "
                . "first_name REGEXP '^" . $skey . "' AND parent_id=0 AND id NOT IN(SELECT owner_id FROM gd_owner_type) LIMIT " . $offset . "," . $maxlimit;

        // change by Abar (because inactive accounts was showing in the searchName)
        if(!$inactive) {
            $sql = "SELECT * FROM gd_flatowner WHERE "
                . "first_name REGEXP '^" . $skey . "' AND status=1 AND parent_id=0 AND id NOT IN(SELECT owner_id FROM gd_owner_type) LIMIT " . $offset . "," . $maxlimit;
        }

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }
    
     public function totalsearchbyname($skey, $inactive=true) {
        $em = $this->getEntityManager();
        $sql = "SELECT count(first_name) as totalsearch FROM gd_flatowner WHERE "
                . "first_name REGEXP '^" . $skey . "' AND parent_id=0 AND id NOT IN(SELECT owner_id FROM gd_owner_type)";

        // change by Abar (because inactive accounts was showing in the searchName)
        if(!$inactive) {
            $sql = "SELECT count(first_name) as totalsearch FROM gd_flatowner WHERE "
                . "first_name REGEXP '^" . $skey . "' AND status=1 AND parent_id=0 AND id NOT IN(SELECT owner_id FROM gd_owner_type)";
        }

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result[0]['totalsearch'];
    }
    
    
     /**
     * verifyUserLogin - get user login on the basis of otp,
     * 
     * @param string $data, $otp
     * @return mixed
     * @author Nishikant
     * @created_date 29th Dec, 2016
     * @modified_date 12th Jan, 2017
     */
    public function verifyUserOtp($data, $otpstatus) {
        $rotp = trim($data['OTP']);
		$flatownerstatus = '1';
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder('u', 'a');
        $query = $query->select('u.id, u.firstName, u.lastName, u.unitNumber, u.mobile, u.floor, a.id as otpId,a.otp,a.otpStatus,a.status ,u.status,
		u.email, u.followme1, u.followme2, u.followme3, u.createSipUser, u.cameraOnly, u.sipUsername, u.sipPassword, u.lockedToDevice');
        $query = $query->from('Admin\Entity\GdFlatowner', 'u');
        $query = $query->join('Admin\Entity\GdOtp', 'a', 'WITH', 'a.userId = u.id');
        $query = $query->andWhere('u.status = :st')->setParameter('st', 1);
        $query = $query->andWhere('a.otp = :rotp')->setParameter('rotp', $rotp);
        $query = $query->andWhere('a.otpStatus = :snew')->setParameter('snew', $otpstatus);
        $query = $query->andWhere('a.status = :otps')->setParameter('otps', $flatownerstatus);
        $query = $query->getQuery();
        $result = $query->getResult();
        return $result;
    }

    /**
     * verifyUserLogin - get user login on the basis of otp,
     * 
     * @param string $data, $otp
     * @return mixed
     * @author Nishikant
     * @created_date 29th Dec, 2016
     * @modified_date 12th Jan, 2017
     */   
	
	public function verifyUserLogin($data, $otpstatus) {
        $uid = trim($data['userid']);
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder('u', 'a');
        $query = $query->select('u.id, u.firstName, u.lastName, u.unitNumber, u.mobile, u.floor, u.email, u.followme1, u.followme2, u.followme3, u.createSipUser, u.cameraOnly, u.sipUsername, u.sipPassword, u.sipExtentionNum, u.lockedToDevice, a.id as otpId,a.otp,a.otpStatus,a.status ');
        $query = $query->from('Admin\Entity\GdFlatowner', 'u');
        $query = $query->join('Admin\Entity\GdOtp', 'a', 'WITH', 'a.otp = u.sipDownloadCode');
        $query = $query->andWhere('u.status = :st')->setParameter('st', 1);
        $query = $query->andWhere('u.id = :uid')->setParameter('uid', $uid);
        $query = $query->andWhere('a.otpStatus = :scon')->setParameter('scon', $otpstatus);
        $query = $query->andWhere('a.status = :otps')->setParameter('otps', '1');
        $query = $query->getQuery();
        $result = $query->getResult();
        return $result;
    }
	
	 /**
     * get last inserted userid ,
     * 
     * @param string $offset, $maxlimit
     * @return mixed
     * @author Nishikant
     * @created_date 18th Jan, 2017
     * @modified_date
     */
    public function lastaddeduser($offset, $maxlimit){
     	$em = $this->getEntityManager();
        $query = $em->createQueryBuilder('u');
        $query = $query->from('Admin\Entity\GdFlatowner', 'u');
        $query = $query->select('u.id');
        // $query = $query->where('u.status = :st')->setParameter('st', 1);
        $query = $query->orderBy('u.id', 'DESC');
        $query = $query->setFirstResult($offset);
        $query = $query->setMaxResults($maxlimit);
        $query = $query->getQuery();
        $result = $query->getResult();       
        return $result[0]['id'];
                
    }
    
     /**
     * get IOS device token
     * 
     * @param $ext
     * @return mixed
     * @author Nishikant
     * @created_date 4rth May, 2017
     * @modified_date -------------
     */
    public function iosdevicetoken($ext) { 
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder('u');
        $q = $query->from('Admin\Entity\GdFlatowner', 'u')
                    ->select('u.id, u.mobile, a.deviceToken')    
                    ->join('Admin\Entity\GdOtp', 'a', 'WITH', 'a.userId = u.id') 
                    ->where('u.mobile = ?6')
                    ->andWhere('a.otpStatus = ?1')
                    ->andWhere('a.status = ?2')
                    ->andWhere('u.status = ?3')
                    ->andWhere('a.deviceType =?4')
                    ->setParameter(1, 'consumed')
                    ->setParameter(2, '1')
                    ->setParameter(3, '1')
                    ->setParameter(4, 1)
                    ->setParameter(6, $ext)
                    ->getQuery();
        $result = $q->getOneOrNullResult();     
        return $result;        
    }

    /**
     * get users lists forseterik restore
     * 
     * @param $ext
     * @return mixed
     * @author Nishikant
     * @created_date 4th July, 2017
     * @modified_date -------------
     */
    public function userslists() {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder('u');
        $query = $query->from('Admin\Entity\GdFlatowner', 'u');
        $query = $query->select('u.firstName, u.lastName, u.mobile, u.sipPassword, u.followme1, u.followme2, u.followme3, u.followme4, u.followme5, u.ringType, u.confirmcall, u.email, u.voiceMail');
        $query = $query->where('u.status = :st')->setParameter('st', 1);
        $query = $query->getQuery();
        $result = $query->getResult();
        return $result;
    }

    public function getblindUsers($parent_id) {
        // $em = $this->getEntityManager();
        // $query = $em->createQueryBuilder('u');
        // $query = $query->from('Admin\Entity\GdFlatowner', 'u');
        // $query = $query->select('u', 'a.firstName as parentName');
        // $query = $query->join('Admin\Entity\GdFlatowner', 'a', 'WITH', 'a.id = u.parent_id');
        // $query = $query->where('u.parent_id IS NOT NULL');
        // $query = $query->orderBy('u.firstName', 'ASC');
        // $query = $query->getQuery();
        // $result = $query->getResult();
        // return $result;

        $em = $this->getEntityManager();
        $sql="
            SELECT t1.*, t2.first_name as p_first_name, t2.mobile as p_mobile FROM gd_flatowner as t1
            LEFT JOIN gd_flatowner AS t2 ON t1.parent_id=t2.id
            WHERE t1.parent_id!=0
            ORDER BY t1.first_name DESC
        ";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getOwnerByExt($ext) {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder('u');
        $query = $query->from('Admin\Entity\GdFlatowner', 'u');
        $query = $query->select('u');
        $query = $query->where('u.mobile = :ext')->setParameter('ext', $ext);
        $query = $query->getQuery();
        $result = $query->getResult();
        return $result;
    }

    public function getOwnerChild($parent_id) {
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder('u');
        $query = $query->from('Admin\Entity\GdFlatowner', 'u');
        $query = $query->select('u');
        $query = $query->where('u.parent_id = :pid')->setParameter('pid', $parent_id);
        $query = $query->getQuery();
        $result = $query->getResult();
        return $result;
    }

    public function removeSubUsers($parent_id) {
        $em = $this->getEntityManager();
        $sql="DELETE FROM gd_flatowner WHERE parent_id=$parent_id";

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
    }

}
