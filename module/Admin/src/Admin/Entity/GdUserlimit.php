<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GdUserlimit
 *
 * @ORM\Table(name="gd_userlimit")
 * @ORM\Entity(repositoryClass="GdUserlimitRepository")
 */
class GdUserlimit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_user", type="integer", nullable=true)
     */
    private $maxUser;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="string", nullable=true)
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_date", type="string", nullable=false)
     */
    private $modifiedDate;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set maxUser
     *
     * @param integer $maxUser
     *
     * @return GdUserlimit
     */
    public function setMaxUser($maxUser)
    {
        $this->maxUser = $maxUser;

        return $this;
    }

    /**
     * Get maxUser
     *
     * @return integer
     */
    public function getMaxUser()
    {
        return $this->maxUser;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return GdUserlimit
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     *
     * @return GdUserlimit
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     *
     * @return GdUserlimit
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;

        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }
}
