<?php

namespace Admin\Entity;

use Doctrine\ORM\EntityRepository;

class GdOtpRepository extends EntityRepository {


     /**
     * This is used to update the ststus of consumed otp on 
     * regenerating otp
     *
     * @author        Nishikant
     * @param         integer $otp, $userid
     * @return        mixed $result
     * @created_date  12th Jan, 2017
     * @modified_date --------------
    */  
    
     public function updateOtpStatus($otp, $userid) {
        $em = $this->getEntityManager();
        $st = '0' ;
        $query = $em->createQueryBuilder('u');
        $q = $query->update('Admin\Entity\GdOtp', 'u')
                ->set('u.status', '?1')
                ->where('u.userId = ?2')
                ->andWhere('u.otp = ?3')
                ->setParameter(1, $st)
                ->setParameter(2, $userid)
                ->setParameter(3, $otp)                
                ->getQuery();
        $p = $q->execute();
    }
    
     /**
     * This is used to get device type
     *
     * @author        Nishikant
     * @param         integer $otp, $userid
     * @return        mixed $result
     * @created_date  19th Apr, 2017
     * @modified_date --------------
    */  
    public function devicetype($id, $status) { 
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder('u');
        $q = $query->from('Admin\Entity\GdOtp', 'u')
                    ->select('u.id, u.uniqueId, u.deviceType')                   
                    ->where('u.userId = ?1')
                    ->andWhere('u.otpStatus =?2')
                    ->andWhere('u.status =?3')
		    ->andWhere('u.deviceType IS NOT NULL')
                    ->setParameter(1, $id)
                    ->setParameter(2, $status)
                    ->setParameter(3, '1')
                    ->getQuery();
       // $result = $q->getOneOrNullResult();
	  $result = $q->execute();

	/* 
        try {
	    $result = $q->getOneOrNullResult(Query::HYDRATE_SINGLE_SCALAR);
	} catch (\Doctrine\ORM\NoResultException $e) {
	    // Handle the exception here. In this case, we are setting the variable to NULL
	    $result = null;
	}
	*/
        return $result;
        
    }
	
	/**
     * This is used to update the device token      
     *
     * @author        Nishikant
     * @param         integer $otp, $userid
     * @return        mixed $result
     * @created_date  20th Jan, 2017
     * @modified_date --------------
    */  
    
     public function updateDeviceToken($devicetoken, $userid, $otpstatus) {
		$s = '1';
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder('u');
        $q = $query->update('Admin\Entity\GdOtp', 'u')
                ->set('u.deviceToken', '?1')
				->where('u.userId = ?2')
                ->andWhere('u.otpStatus = ?3')				
                ->andWhere('u.status = ?4')
                ->setParameter(1, $devicetoken)
                ->setParameter(2, $userid)
                ->setParameter(3, $otpstatus)  
			 	->setParameter(4, $s)
                ->getQuery();
        $p = $q->execute();
		 return $p;
    }


}

