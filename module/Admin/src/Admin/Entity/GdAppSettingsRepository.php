<?php

namespace Admin\Entity;

use Doctrine\ORM\EntityRepository;

class GdAppSettingsRepository extends EntityRepository {

	/**
	* This is used for getting the camera app settings 
	* 
	* @author        Nishikant
	* @param         mmixed
	* @return        mixed $result
	* @created_date  11th Jan, 2017
	* @modified_date --------------
	*/
	
	public function cameraAppSettings() {
		$em = $this->getEntityManager();
		$query = $em->createQueryBuilder('u');
		$query = $query->from('Admin\Entity\GdAppSettings', 'u');
		$query = $query->select('u.cmIphost, u.cmName, u.cmChannel, u.cmUsername, u.cmPwd');
		$query = $query->getQuery();
		$result = $query->getResult();
		return $result[0];
	}
	
	
	/**
	* This is used for getting the sip app settings 
	* 
	* @author        Nishikant
	* @param         mmixed
	* @return        mixed $result
	* @created_date  11th Jan, 2017
	* @modified_date --------------
	*/
	
	public function sipAppSettings() {
		$em = $this->getEntityManager();
		$query = $em->createQueryBuilder('u');
		$query = $query->from('Admin\Entity\GdAppSettings', 'u');
		$query = $query->select('u.sipAddr, u.sipPort,
		u.sipTransport, u.sipSer1addr, u.sipSer1Port, u.sipSer1Transport, u.sipSer1Expires, u.sipSer1Register, u.sipSer1Retrytmout, u.sipSer1Retrymaxct, u.sipSer1Lnsztmout, u.updateConfigUrl');
		$query = $query->getQuery();
		$result = $query->getResult();
		return $result[0];
	}
	
	/**
	* This is used for getting the last updated config
	* 
	* @author        Nishikant
	* @return        mixed $result
	* @created_date  18th Apr, 2017
	* @modified_date --------------
	*/
	 public function globalconfigrecord(){
            $em = $this->getEntityManager();
            $query = $em->createQueryBuilder('u')
                ->from('Admin\Entity\GdAppSettings', 'u')
                ->select('u.id') 
                ->where('u.status = ?1') 
                ->setParameter(1, '1')            
                ->getQuery();               
        $result = $query->getOneOrNullResult();        
        return $result;
        }
	
	

}
