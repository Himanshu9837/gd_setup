<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GdAppSettings
 *
 * @ORM\Table(name="gd_app_settings")
 * @ORM\Entity(repositoryClass="GdAppSettingsRepository")
 */
class GdAppSettings
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sitename", type="string", length=255, nullable=true)
     */
    private $sitename;

    /**
     * @var string
     *
     * @ORM\Column(name="cm_iphost", type="string", length=255, nullable=true)
     */
    private $cmIphost;

    /**
     * @var string
     *
     * @ORM\Column(name="cm_port", type="string", length=255, nullable=true)
     */
    private $cmPort;

    /**
     * @var string
     *
     * @ORM\Column(name="cm_name", type="string", length=255, nullable=true)
     */
    private $cmName;

    /**
     * @var string
     *
     * @ORM\Column(name="cm_channel", type="string", length=255, nullable=true)
     */
    private $cmChannel;

    /**
     * @var string
     *
     * @ORM\Column(name="cm_username", type="string", length=255, nullable=true)
     */
    private $cmUsername;

    /**
     * @var string
     *
     * @ORM\Column(name="cm_pwd", type="string", length=255, nullable=true)
     */
    private $cmPwd;

    /**
     * @var string
     *
     * @ORM\Column(name="cm_streaming", type="string", length=50, nullable=true)
     */
    private $cmStreaming;

    /**
     * @var string
     *
     * @ORM\Column(name="sip_addr", type="string", length=255, nullable=true)
     */
    private $sipAddr;

    /**
     * @var string
     *
     * @ORM\Column(name="sip_port", type="string", length=255, nullable=true)
     */
    private $sipPort;

    /**
     * @var string
     *
     * @ORM\Column(name="sip_transport", type="string", length=255, nullable=true)
     */
    private $sipTransport;

    /**
     * @var string
     *
     * @ORM\Column(name="sip_ser1addr", type="string", length=255, nullable=true)
     */
    private $sipSer1addr;

    /**
     * @var string
     *
     * @ORM\Column(name="sip_ser1_port", type="string", length=255, nullable=true)
     */
    private $sipSer1Port;

    /**
     * @var string
     *
     * @ORM\Column(name="sip_ser1_transport", type="string", length=255, nullable=true)
     */
    private $sipSer1Transport;

    /**
     * @var string
     *
     * @ORM\Column(name="sip_ser1_expires", type="string", length=255, nullable=true)
     */
    private $sipSer1Expires;

    /**
     * @var string
     *
     * @ORM\Column(name="sip_ser1_register", type="string", length=255, nullable=true)
     */
    private $sipSer1Register;

    /**
     * @var string
     *
     * @ORM\Column(name="sip_ser1_retrytmout", type="string", length=255, nullable=true)
     */
    private $sipSer1Retrytmout;

    /**
     * @var string
     *
     * @ORM\Column(name="sip_ser1_retrymaxct", type="string", length=255, nullable=true)
     */
    private $sipSer1Retrymaxct;

    /**
     * @var string
     *
     * @ORM\Column(name="sip_ser1_lnsztmout", type="string", length=255, nullable=true)
     */
    private $sipSer1Lnsztmout;
	
	  /**
     * @var string
     *
     * @ORM\Column(name="update_config_url", type="string", length=255, nullable=true)
     */
    private $updateConfigUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="string", nullable=true)
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_date", type="string", nullable=false)
     */
    private $modifiedDate;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sitename
     *
     * @param string $sitename
     *
     * @return GdAppSettings
     */
    public function setSitename($sitename)
    {
        $this->sitename = $sitename;

        return $this;
    }

    /**
     * Get sitename
     *
     * @return string
     */
    public function getSitename()
    {
        return $this->sitename;
    }

    /**
     * Set cmIphost
     *
     * @param string $cmIphost
     *
     * @return GdAppSettings
     */
    public function setCmIphost($cmIphost)
    {
        $this->cmIphost = $cmIphost;

        return $this;
    }

    /**
     * Get cmIphost
     *
     * @return string
     */
    public function getCmIphost()
    {
        return $this->cmIphost;
    }

    /**
     * Set cmPort
     *
     * @param string $cmPort
     *
     * @return GdAppSettings
     */
    public function setCmPort($cmPort)
    {
        $this->cmPort = $cmPort;

        return $this;
    }

    /**
     * Get cmPort
     *
     * @return string
     */
    public function getCmPort()
    {
        return $this->cmPort;
    }

    /**
     * Set cmName
     *
     * @param string $cmName
     *
     * @return GdAppSettings
     */
    public function setCmName($cmName)
    {
        $this->cmName = $cmName;

        return $this;
    }

    /**
     * Get cmName
     *
     * @return string
     */
    public function getCmName()
    {
        return $this->cmName;
    }

    /**
     * Set cmChannel
     *
     * @param string $cmChannel
     *
     * @return GdAppSettings
     */
    public function setCmChannel($cmChannel)
    {
        $this->cmChannel = $cmChannel;

        return $this;
    }

    /**
     * Get cmChannel
     *
     * @return string
     */
    public function getCmChannel()
    {
        return $this->cmChannel;
    }

    /**
     * Set cmUsername
     *
     * @param string $cmUsername
     *
     * @return GdAppSettings
     */
    public function setCmUsername($cmUsername)
    {
        $this->cmUsername = $cmUsername;

        return $this;
    }

    /**
     * Get cmUsername
     *
     * @return string
     */
    public function getCmUsername()
    {
        return $this->cmUsername;
    }

    /**
     * Set cmPwd
     *
     * @param string $cmPwd
     *
     * @return GdAppSettings
     */
    public function setCmPwd($cmPwd)
    {
        $this->cmPwd = $cmPwd;

        return $this;
    }

    /**
     * Get cmPwd
     *
     * @return string
     */
    public function getCmPwd()
    {
        return $this->cmPwd;
    }

    /**
     * Set cmStreaming
     *
     * @param string $cmStreaming
     *
     * @return GdAppSettings
     */
    public function setCmStreaming($cmStreaming)
    {
        $this->cmStreaming = $cmStreaming;

        return $this;
    }

    /**
     * Get cmStreaming
     *
     * @return string
     */
    public function getCmStreaming()
    {
        return $this->cmStreaming;
    }

    /**
     * Set sipAddr
     *
     * @param string $sipAddr
     *
     * @return GdAppSettings
     */
    public function setSipAddr($sipAddr)
    {
        $this->sipAddr = $sipAddr;

        return $this;
    }

    /**
     * Get sipAddr
     *
     * @return string
     */
    public function getSipAddr()
    {
        return $this->sipAddr;
    }

    /**
     * Set sipPort
     *
     * @param string $sipPort
     *
     * @return GdAppSettings
     */
    public function setSipPort($sipPort)
    {
        $this->sipPort = $sipPort;

        return $this;
    }

    /**
     * Get sipPort
     *
     * @return string
     */
    public function getSipPort()
    {
        return $this->sipPort;
    }

    /**
     * Set sipTransport
     *
     * @param string $sipTransport
     *
     * @return GdAppSettings
     */
    public function setSipTransport($sipTransport)
    {
        $this->sipTransport = $sipTransport;

        return $this;
    }

    /**
     * Get sipTransport
     *
     * @return string
     */
    public function getSipTransport()
    {
        return $this->sipTransport;
    }

    /**
     * Set sipSer1addr
     *
     * @param string $sipSer1addr
     *
     * @return GdAppSettings
     */
    public function setSipSer1addr($sipSer1addr)
    {
        $this->sipSer1addr = $sipSer1addr;

        return $this;
    }

    /**
     * Get sipSer1addr
     *
     * @return string
     */
    public function getSipSer1addr()
    {
        return $this->sipSer1addr;
    }

    /**
     * Set sipSer1Port
     *
     * @param string $sipSer1Port
     *
     * @return GdAppSettings
     */
    public function setSipSer1Port($sipSer1Port)
    {
        $this->sipSer1Port = $sipSer1Port;

        return $this;
    }

    /**
     * Get sipSer1Port
     *
     * @return string
     */
    public function getSipSer1Port()
    {
        return $this->sipSer1Port;
    }

    /**
     * Set sipSer1Transport
     *
     * @param string $sipSer1Transport
     *
     * @return GdAppSettings
     */
    public function setSipSer1Transport($sipSer1Transport)
    {
        $this->sipSer1Transport = $sipSer1Transport;

        return $this;
    }

    /**
     * Get sipSer1Transport
     *
     * @return string
     */
    public function getSipSer1Transport()
    {
        return $this->sipSer1Transport;
    }

    /**
     * Set sipSer1Expires
     *
     * @param string $sipSer1Expires
     *
     * @return GdAppSettings
     */
    public function setSipSer1Expires($sipSer1Expires)
    {
        $this->sipSer1Expires = $sipSer1Expires;

        return $this;
    }

    /**
     * Get sipSer1Expires
     *
     * @return string
     */
    public function getSipSer1Expires()
    {
        return $this->sipSer1Expires;
    }

    /**
     * Set sipSer1Register
     *
     * @param string $sipSer1Register
     *
     * @return GdAppSettings
     */
    public function setSipSer1Register($sipSer1Register)
    {
        $this->sipSer1Register = $sipSer1Register;

        return $this;
    }

    /**
     * Get sipSer1Register
     *
     * @return string
     */
    public function getSipSer1Register()
    {
        return $this->sipSer1Register;
    }

    /**
     * Set sipSer1Retrytmout
     *
     * @param string $sipSer1Retrytmout
     *
     * @return GdAppSettings
     */
    public function setSipSer1Retrytmout($sipSer1Retrytmout)
    {
        $this->sipSer1Retrytmout = $sipSer1Retrytmout;

        return $this;
    }

    /**
     * Get sipSer1Retrytmout
     *
     * @return string
     */
    public function getSipSer1Retrytmout()
    {
        return $this->sipSer1Retrytmout;
    }

    /**
     * Set sipSer1Retrymaxct
     *
     * @param string $sipSer1Retrymaxct
     *
     * @return GdAppSettings
     */
    public function setSipSer1Retrymaxct($sipSer1Retrymaxct)
    {
        $this->sipSer1Retrymaxct = $sipSer1Retrymaxct;

        return $this;
    }

    /**
     * Get sipSer1Retrymaxct
     *
     * @return string
     */
    public function getSipSer1Retrymaxct()
    {
        return $this->sipSer1Retrymaxct;
    }

    /**
     * Set sipSer1Lnsztmout
     *
     * @param string $sipSer1Lnsztmout
     *
     * @return GdAppSettings
     */
    public function setSipSer1Lnsztmout($sipSer1Lnsztmout)
    {
        $this->sipSer1Lnsztmout = $sipSer1Lnsztmout;

        return $this;
    }

    /**
     * Get sipSer1Lnsztmout
     *
     * @return string
     */
    public function getSipSer1Lnsztmout()
    {
        return $this->sipSer1Lnsztmout;
    }
	
	 /**
     * Set updateConfigUrl
     *
     * @param string $updateConfigUrl
     *
     * @return GdAppSettings
     */
    public function setUpdateConfigUrl($updateConfigUrl)
    {
        $this->updateConfigUrl = $updateConfigUrl;

        return $this;
    }

    /**
     * Get updateConfigUrl
     *
     * @return string
     */
    public function getUpdateConfigUrl()
    {
        return $this->updateConfigUrl;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return GdAppSettings
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     *
     * @return GdAppSettings
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     *
     * @return GdAppSettings
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;

        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }
}
