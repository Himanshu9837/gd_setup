<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GdFlatowner
 *
 * @ORM\Table(name="gd_flatowner")
 * @ORM\Entity(repositoryClass="GdFlatownerRepository")
 */
class GdFlatowner
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

     /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $user_id=0;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=false)
     */
    private $parent_id=0;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=100, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=100, nullable=true)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="unit_number", type="string", length=100, nullable=true)
     */
    private $unitNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="floor", type="integer", nullable=true)
     */
    private $floor;

    /**
     * @var integer
     *
     * @ORM\Column(name="mobile", type="integer", nullable=true)
     */
    private $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;
  
    /**
     * @var string
     *
     * @ORM\Column(name="followme1", type="string", length=100, nullable=true)
     */
    private $followme1;

    /**
     * @var string
     *
     * @ORM\Column(name="followme2", type="string", length=100, nullable=true)
     */
    private $followme2;

    /**
     * @var string
     *
     * @ORM\Column(name="followme3", type="string", length=100, nullable=true)
     */
    private $followme3;

    /**
     * @var string
     *
     * @ORM\Column(name="followme4", type="string", length=100, nullable=true)
     */
    private $followme4;

    /**
     * @var string
     *
     * @ORM\Column(name="followme5", type="string", length=100, nullable=true)
     */
    private $followme5;

    /**
     * @var string
     *
     * @ORM\Column(name="confirmcall", type="string", length=100, nullable=true)
     */
    private $confirmcall;

    /**
     * @var string
     *
     * @ORM\Column(name="ring_type", type="string", length=100, nullable=true)
     */
    private $ringType;

    /**
     * @var string
     *
     * @ORM\Column(name="voice_mail", type="string", length=100, nullable=true)
     */
    private $voiceMail;

    /**
     * @var string
     *
     * @ORM\Column(name="create_sip_user", type="string", length=100, nullable=true)
     */
    private $createSipUser;

    /**
     * @var string
     *
     * @ORM\Column(name="camera_only", type="string", length=100, nullable=true)
     */
    private $cameraOnly;

    /**
     * @var string
     *
     * @ORM\Column(name="sip_username", type="string", length=255, nullable=true)
     */
    private $sipUsername;

    /**
     * @var string
     *
     * @ORM\Column(name="sip_password", type="string", length=255, nullable=true)
     */
    private $sipPassword;

    /**
     * @var integer
     *
     * @ORM\Column(name="sip_extention_num", type="bigint", nullable=true)
     */
    private $sipExtentionNum;

    /**
     * @var integer
     *
     * @ORM\Column(name="sip_download_code", type="bigint", nullable=true)
     */
    private $sipDownloadCode;

    /**
     * @var string
     *
     * @ORM\Column(name="locked_to_device", type="string", length=255, nullable=true)
     */
    private $lockedToDevice;

    /**
     * @var string
     *
     * @ORM\Column(name="sites", type="string", length=255, nullable=true)
     */
    private $sites;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="string", nullable=true)
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_date", type="string", nullable=false)
     */
    private $modifiedDate;


    /**
     * Set parentId
     *
     * @param integer $parent_id
     *
     * @return GdFlatowner
     */
    public function setParentId($parent_id) {
        $this->parent_id = $parent_id;
        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer
     */
    public function getParentId() {
        return $this->parent_id;
    }



     /**
     * Set userId
     *
     * @param integer $user_id
     *
     * @return GdFlatowner
     */
    public function setUserId($user_id) {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId() {
        return $this->user_id;
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return GdFlatowner
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return GdFlatowner
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set unitNumber
     *
     * @param string $unitNumber
     *
     * @return GdFlatowner
     */
    public function setUnitNumber($unitNumber)
    {
        $this->unitNumber = $unitNumber;

        return $this;
    }

    /**
     * Get unitNumber
     *
     * @return string
     */
    public function getUnitNumber()
    {
        return $this->unitNumber;
    }

    /**
     * Set floor
     *
     * @param integer $floor
     *
     * @return GdFlatowner
     */
    public function setFloor($floor)
    {
        $this->floor = $floor;

        return $this;
    }

    /**
     * Get floor
     *
     * @return integer
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * Set mobile
     *
     * @param integer $mobile
     *
     * @return GdFlatowner
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return integer
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return GdFlatowner
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

     /**
     * Set followme1
     *
     * @param string $followme1
     *
     * @return GdFlatowner
     */
    public function setFollowme1($followme1)
    {
        $this->followme1 = $followme1;

        return $this;
    }

    /**
     * Get followme1
     *
     * @return string
     */
    public function getFollowme1()
    {
        return $this->followme1;
    }

    /**
     * Set followme2
     *
     * @param string $followme2
     *
     * @return GdFlatowner
     */
    public function setFollowme2($followme2)
    {
        $this->followme2 = $followme2;

        return $this;
    }

    /**
     * Get followme2
     *
     * @return string
     */
    public function getFollowme2()
    {
        return $this->followme2;
    }

    /**
     * Set followme3
     *
     * @param string $followme3
     *
     * @return GdFlatowner
     */
    public function setFollowme3($followme3)
    {
        $this->followme3 = $followme3;

        return $this;
    }

    /**
     * Get followme3
     *
     * @return string
     */
    public function getFollowme3()
    {
        return $this->followme3;
    }

    /**
     * Set followme4
     *
     * @param string $followme4
     *
     * @return GdFlatowner
     */
    public function setFollowme4($followme4)
    {
        $this->followme4 = $followme4;

        return $this;
    }

    /**
     * Get followme4
     *
     * @return string
     */
    public function getFollowme4()
    {
        return $this->followme4;
    }

    /**
     * Set followme5
     *
     * @param string $followme5
     *
     * @return GdFlatowner
     */
    public function setFollowme5($followme5)
    {
        $this->followme5 = $followme5;

        return $this;
    }

    /**
     * Get followme5
     *
     * @return string
     */
    public function getFollowme5()
    {
        return $this->followme5;
    }

    /**
     * Set confirmcall
     *
     * @param string $confirmcall
     *
     * @return GdFlatowner
     */
    public function setConfirmcall($confirmcall)
    {
        $this->confirmcall = $confirmcall;

        return $this;
    }

    /**
     * Get confirmcall
     *
     * @return string
     */
    public function getConfirmcall()
    {
        return $this->confirmcall;
    }

    /**
     * Set ringType
     *
     * @param string $ringType
     *
     * @return GdFlatowner
     */
    public function setRingType($ringType)
    {
        $this->ringType = $ringType;

        return $this;
    }

    /**
     * Get ringType
     *
     * @return string
     */
    public function getRingType()
    {
        return $this->ringType;
    }

    /**
     * Set voiceMail
     *
     * @param string $voiceMail
     *
     * @return GdFlatowner
     */
    public function setVoiceMail($voiceMail)
    {
        $this->voiceMail = $voiceMail;

        return $this;
    }

    /**
     * Get voiceMail
     *
     * @return string
     */
    public function getVoiceMail()
    {
        return $this->voiceMail;
    }

    /**
     * Set createSipUser
     *
     * @param string $createSipUser
     *
     * @return GdFlatowner
     */
    public function setCreateSipUser($createSipUser)
    {
        $this->createSipUser = $createSipUser;

        return $this;
    }

    /**
     * Get createSipUser
     *
     * @return string
     */
    public function getCreateSipUser()
    {
        return $this->createSipUser;
    }

    /**
     * Set cameraOnly
     *
     * @param string $cameraOnly
     *
     * @return GdFlatowner
     */
    public function setCameraOnly($cameraOnly)
    {
        $this->cameraOnly = $cameraOnly;

        return $this;
    }

    /**
     * Get cameraOnly
     *
     * @return string
     */
    public function getCameraOnly()
    {
        return $this->cameraOnly;
    }

    /**
     * Set sipUsername
     *
     * @param string $sipUsername
     *
     * @return GdFlatowner
     */
    public function setSipUsername($sipUsername)
    {
        $this->sipUsername = $sipUsername;

        return $this;
    }

    /**
     * Get sipUsername
     *
     * @return string
     */
    public function getSipUsername()
    {
        return $this->sipUsername;
    }

    /**
     * Set sipPassword
     *
     * @param string $sipPassword
     *
     * @return GdFlatowner
     */
    public function setSipPassword($sipPassword)
    {
        $this->sipPassword = $sipPassword;

        return $this;
    }

    /**
     * Get sipPassword
     *
     * @return string
     */
    public function getSipPassword()
    {
        return $this->sipPassword;
    }

    /**
     * Set sipExtentionNum
     *
     * @param integer $sipExtentionNum
     *
     * @return GdFlatowner
     */
    public function setSipExtentionNum($sipExtentionNum)
    {
        $this->sipExtentionNum = $sipExtentionNum;

        return $this;
    }

    /**
     * Get sipExtentionNum
     *
     * @return integer
     */
    public function getSipExtentionNum()
    {
        return $this->sipExtentionNum;
    }

    /**
     * Set sipDownloadCode
     *
     * @param integer $sipDownloadCode
     *
     * @return GdFlatowner
     */
    public function setSipDownloadCode($sipDownloadCode)
    {
        $this->sipDownloadCode = $sipDownloadCode;

        return $this;
    }

    /**
     * Get sipDownloadCode
     *
     * @return integer
     */
    public function getSipDownloadCode()
    {
        return $this->sipDownloadCode;
    }

    /**
     * Set lockedToDevice
     *
     * @param string $lockedToDevice
     *
     * @return GdFlatowner
     */
    public function setLockedToDevice($lockedToDevice)
    {
        $this->lockedToDevice = $lockedToDevice;

        return $this;
    }

    /**
     * Get lockedToDevice
     *
     * @return string
     */
    public function getLockedToDevice()
    {
        return $this->lockedToDevice;
    }

    /**
     * Set sites
     *
     * @param string $sites
     *
     * @return GdFlatowner
     */
    public function setSites($sites)
    {
        $this->sites = $sites;

        return $this;
    }

    /**
     * Get sites
     *
     * @return string
     */
    public function getSites()
    {
        return $this->sites;
    }


    /**
     * Set status
     *
     * @param string $status
     *
     * @return GdFlatowner
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     *
     * @return GdFlatowner
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     *
     * @return GdFlatowner
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;

        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }


    public function getArrayCopy() {
        return get_object_vars($this);
    }
}
