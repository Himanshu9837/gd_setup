<?php

namespace Admin\Entity;

use Doctrine\ORM\EntityRepository;


class GdAdminRepository extends EntityRepository
{
     
    /**
     * Description      : This is used to verify admin
     * @param           : mixed $data 
     * @return          : mixed $result
     * @author          : Nishikant
    */ 
    
    public function verifyAdmin( $data = array() )
    { 
        $em = $this->getEntityManager();
        $vrifiedPass = $data['password'];
        //encode plain password to md5
        $vrifiedPass = md5($vrifiedPass);
        //$role = 'admin';
        $status = '1';
        $query = $em->createQueryBuilder('u');
        $query = $query->from('Admin\Entity\GdAdmin', 'u');
        $query = $query->select('u.id, u.email, u.firstName, u.lastName, u.phone, u.gender, u.role, u.userType, u.status');		
        $query = $query->where('u.email = :adminemail')->setParameter('adminemail', $data['email']);
        $query = $query->andWhere('u.password = :password')->setParameter('password', $vrifiedPass);
        $query = $query->andWhere('u.status = :status')->setParameter('status', $status);
        //$query = $query->andWhere('u.role = :role')->setParameter('role', $role);
        $query = $query->getQuery();
        $result = $query->getResult();
        return $result;     
    }
    
    
    /**
     * Description      : This is used to get the admin details
     * @param           : integer $adminid
     * @return          : mixed $result
     * @author          : Nishikant
    */ 
    
    public function getAdminDetail($adminid){ 
    $em = $this->getEntityManager();
	$query = $em->createQueryBuilder('u');
	$query = $query->from('Admin\Entity\GdAdmin', 'u');
	$query = $query->select('u.id, u.phone, u.email, u.password, u.firstName, u.lastName, u.gender, u.role, u.status');				
        $query = $query->where('u.id= :adminid')->setParameter('adminid', $adminid);
	$query = $query->getQuery();
	$Result = $query->getResult();	        
        return $Result;               
    }
    
    /**
     * Description      : This is used to get the subadmin lists
     * @param           : $data
     * @return          : mixed $result
     * @author          : Nishikant
     * @created_date    : 1st june, 2017
     * @modified_date   : -------------
     * @copyright       : Mads Technologies Pvt. Ltd
    */ 
    
     public function getSubAdminLists($utype){ 
        $em = $this->getEntityManager();
        $status = '1';
        $query = $em->createQueryBuilder('u');
        $query =$query->from('Admin\Entity\GdAdmin', 'u');
        $query =$query->select('u.id, u.phone, u.email, u.username, u.firstName, u.lastName, u.gender, u.role, u.userType, u.status') ;        
        $query =$query->where('u.userType!= :utype')->setParameter('utype', $utype);
        $query =$query->andWhere('u.status= :st')->setParameter('st', $status);
        $query =$query->getQuery();
        $Result = $query->getResult();          
        return $Result;               
    }

     /**
     * checkIfEmailAlreadyExist - Check if mobile exist in DB or not, 
     * 
     * @param string $email
     * @return mixed
     * @author Nishikant
     */
    public function checkIfEmailExist($email) { 
        $em = $this->getEntityManager();
        $query = $em->createQueryBuilder('u');
        $query = $query->from('Admin\Entity\GdAdmin', 'u');
        $query = $query->select('u.id');
        $query = $query->where('u.email = :inemail')->setParameter('inemail', $email);        
        $query = $query->getQuery();
        $result = $query->getResult();
        return $result;

    }
}
