<?php
/**
 * User: Abar Choudhary
 * Date: 30/12/2018
 */
 
namespace Admin\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Stdlib\Hydrator;

use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;

use Admin\Model\Inbounds;

class AccessLogsTable {
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway=$tableGateway;
	}

    public function addLog(AccessLogs $data) {
        $hydrator = new Hydrator\ArraySerializable();
        $data = $hydrator->extract($data); // object to array

        if($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        }
        else return false;
    }

    public function getWhere($where) {
        $data=$this->tableGateway->select($where)->toArray();
        return $data;
    }

    public function getList() {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);

        $select = $sql->select();
        $select->from(array('t1' => 'gd_access_logs'));
        //$select->columns(array());

        $select->join(
            array('t2' => 'gd_access_codes'),
            new Expression('t1.access_id=t2.access_id'),
            array(
                "access_code", "usage_type", "admin_id"
            ),
            'LEFT'
        );
        
        $select->join(
            array('t3' => 'gd_flatowner'),
            new Expression('t1.owner_id=t3.id'),
            array(
                "user_id"
            ),
            'LEFT'  
        );

        $select->order("t1.log_id DESC");
        $select->limit(10000);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($results);
        $result = $resultSet->toArray();

         //print_r(@$sql->getSqlStringForSqlObject($select));
         //die();

        return $result;
    }
    
    public function getExportAccessLogsList() {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);

        $select = $sql->select();
        $select->from(array('t1' => 'gd_access_logs'));
        //$select->columns(array());

        $select->join(
            array('t2' => 'gd_access_codes'),
            new Expression('t1.access_id=t2.access_id'),
            array(
                "access_code", "usage_type", "admin_id"
            ),
            'LEFT'
        );

        $select->order("t1.log_id DESC");
        $select->limit(10000);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($results);
        $result = $resultSet->toArray();

        // print_r(@$sql->getSqlStringForSqlObject($select));
        // die();

        return $result;
    }

    public function getColumns(){
        $rawSql = "SELECT `COLUMN_NAME` 
        FROM `INFORMATION_SCHEMA`.`COLUMNS` 
        WHERE `TABLE_NAME`='gd_access_logs'"; 
        //$rawSql = "SHOW COLUMNS FROM gd_access_codes"; 
        $adapter = $this->tableGateway->getAdapter();
        $resultSet=$adapter->query($rawSql, array());
        $result= $resultSet->toArray();
        return $result;
    }
}
