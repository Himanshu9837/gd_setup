<?php
/**
 * User: Abar Choudhary
 * Date: 29/05/2018
 */

namespace Admin\Model;

class AsteriksList {
	public $type_id;
	public $data;

	public function exchangeArray($data) {
		$this->type_id = !empty($data['type_id']) ? $data['type_id'] : null;
		$this->data = !empty($data['data']) ? $data['data'] : null;
	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

	public function getTypeId()  {return $this->type_id;}
	public function setTypeId($type_id) {$this->type_id = $type_id;}

	public function getData()  {return $this->data;}
	public function setData($data) {$this->data = $data;}
}