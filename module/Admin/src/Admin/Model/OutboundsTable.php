<?php
/**
 * User: Abar Choudhary
 * Date: 06/06/2018
 */
 
namespace Admin\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Stdlib\Hydrator;

use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;

use Admin\Model\Outbounds;

class OutboundsTable {
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway=$tableGateway;
	}

    public function addOutbound(Outbounds $data) {
        $hydrator = new Hydrator\ArraySerializable();
        $data = $hydrator->extract($data); // object to array

        if($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        }
        else return false;
    }

    public function updateOutbound(Outbounds $data) {
        $update=$data->getArrayCopy();
        $routeId=$update['route_id'];

        if($this->tableGateway->update($update, array('route_id' => $routeId)) !== false) return true;
        return false;
    }

    public function getByRoute($rid) {
        @list($data)=$this->tableGateway->select(array("route_id"=>$rid))->toArray();
        return $data;
    }

    public function getList() {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);

        $select = $sql->select();
        $select->from(array('t1' => 'gd_outbounds'));
        $select->columns(array("route_id", "route_name", "route_cid"));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($results);
        $result = $resultSet->toArray();

        // print_r(@$sql->getSqlStringForSqlObject($select));
        // die();

        return $result;
    }

    public function fetchAll() {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);

        $select = $sql->select();
        $select->from(array('t1' => 'gd_outbounds'));
        //$select->columns(array());

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($results);
        $result = $resultSet->toArray();

        // print_r(@$sql->getSqlStringForSqlObject($select));
        // die();

        return $result;
    }

    public function removeOutbound($rid) {
        if($this->tableGateway->delete(array("route_id"=>$rid))) {
            return true;
        }
        else return false;
    }
}