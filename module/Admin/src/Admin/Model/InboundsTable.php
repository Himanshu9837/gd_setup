<?php
/**
 * User: Abar Choudhary
 * Date: 06/06/2018
 */
 
namespace Admin\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Stdlib\Hydrator;

use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;

use Admin\Model\Inbounds;

class InboundsTable {
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway=$tableGateway;
	}

    public function addInbound(Inbounds $data) {
        $hydrator = new Hydrator\ArraySerializable();
        $data = $hydrator->extract($data); // object to array

        if($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        }
        else return false;
    }

    public function updateInbound(Inbounds $data) {
        $update=$data->getArrayCopy();
        $inboundId=$update['row_id'];

        if($this->tableGateway->update($update, array('row_id' => $inboundId)) !== false) return true;
        return false;
    }

    public function getByExt($ext) {
        @list($data)=$this->tableGateway->select(array("did_number"=>$ext))->toArray();
        return $data;
    }

    public function getList() {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);

        $select = $sql->select();
        $select->from(array('t1' => 'gd_inbounds'));
        $select->columns(array("row_id", "did_number", "cid_number", "set_destination"));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($results);
        $result = $resultSet->toArray();

        // print_r(@$sql->getSqlStringForSqlObject($select));
        // die();

        return $result;
    }

    public function fetchAll() {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);

        $select = $sql->select();
        $select->from(array('t1' => 'gd_inbounds'));
        //$select->columns(array());

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($results);
        $result = $resultSet->toArray();

        // print_r(@$sql->getSqlStringForSqlObject($select));
        // die();

        return $result;
    }

    public function removeInbound($ext) {
        if($this->tableGateway->delete(array("did_number"=>$ext))) {
            return true;
        }
        else return false;
    }
}