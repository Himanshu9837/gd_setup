<?php
/**
 * User: Abar Choudhary
 * Date: 29/05/2018
 */
 
namespace Admin\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Stdlib\Hydrator;

use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;

class AsteriksListTable {
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway=$tableGateway;
	}

    public function insertOrUpdate(AsteriksList $data) {
        $type=$data->getTypeId();
        $data=$data->getData();

        // echo "<pre>";
        // print_r($response);
        // die();

        $rawSql="
            INSERT INTO gd_lists (type_id, data)
            VALUES ($type, '$data')
            ON DUPLICATE KEY UPDATE data='$data'
        ";

        $adapter = $this->tableGateway->getAdapter();
        $resultSet=$adapter->query($rawSql, array());

        if($resultSet->getGeneratedValue()) return true;
        return false;
    }

	public function fetchAll() {
		$data=$this->tableGateway->select()->toArray();
		return $data;
	}

    public function getByType($type) {
        $data=$this->tableGateway->select(array("type_id"=>$type))->toArray();
        return $data;
    }

    public function getWhere($where=array(), $filterStartDate="") {
        $adapter=$this->tableGateway->getAdapter();
        $sql=new Sql($adapter);
        
        $select=$sql->select();
        $select->from(array("t1"=>"gd_lists"));

        $select->where($where);
        if($filterStartDate) $select->where(array("DATE_FORMAT(start_date_india, '%Y-%m-%d') = '$filterStartDate'"));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($results);
        $result = $resultSet->toArray();

        // print_r(@$select->getSqlString());
        // die();

        return $result;
    }
}