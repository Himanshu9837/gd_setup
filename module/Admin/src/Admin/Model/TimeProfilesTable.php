<?php
/**
 * User: Abar Choudhary
 * Date: 30/12/2018
 */
 
namespace Admin\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Stdlib\Hydrator;

use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;

class TimeProfilesTable {
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway=$tableGateway;
	}

    public function add(TimeProfiles $data) {
       $addData=array(
            "time_profile_id"=>$data->getTimeProfileId(),
            "time_profile_config"=>$data->getTimeProfileConfig(),
            "time_profile_name"=>$data->getTimeProfileName()
         );
        if($this->tableGateway->insert($addData)) {
            return $this->tableGateway->lastInsertValue;
        }
        else return false;
    }
    
    public function edit(TimeProfiles $data) {
         $UPDATE=array(
            "time_profile_config"=>$data->getTimeProfileConfig(),
            "time_profile_name"=>$data->getTimeProfileName()
        );

        $WHERE=array("time_profile_id"=>$data->getTimeProfileId());
        
        if($this->tableGateway->update($UPDATE, $WHERE)!==false) return true;
        return false;
    }

    public function getList() {
        $adapter=$this->tableGateway->getAdapter();
        $sql=new Sql($adapter);
        $select=$sql->select();

        $select->from(array("t1"=>"gd_time_profiles"));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($results);
        $result = $resultSet->toArray();

        return $result;
    }

    public function getById($timeProfileId) {
        $adapter=$this->tableGateway->getAdapter();
        $sql=new Sql($adapter);
        $select=$sql->select();

        $select->from(array("t1"=>"gd_time_profiles"));
        $select->where(array("time_profile_id"=>$timeProfileId));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($results);
       @list($result) = $resultSet->toArray();
    
         // print_r(@$sql->getSqlStringForSqlObject($select));
        // die();
        return $result;
    }
    
    public function delete($tpid)
    {
	$adapter=$this->tableGateway->getAdapter();
        $sql=new Sql($adapter);
        $delete = $sql->delete('gd_time_profiles')->where(["time_profile_id"=>$tpid]);
       
//         print_r(@$sql->getSqlStringForSqlObject($delete));
  //       die();
        $stmt   = $sql->prepareStatementForSqlObject($delete);
        $result = $stmt->execute();
        if($result!==false) return true;
        return false;
    }

    public function updateStatus($tpid) {
        $adapter=$this->tableGateway->getAdapter();
        $sql=new Sql($adapter);
        $update = $sql->update();
        $update->table('gd_time_profiles');
        $update->set(['time_profile_status' => new Expression("(time_profile_status - 1)*-1")]);
        $update->where(["time_profile_id"=>$tpid]);
        
        $statement = $sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
        
//          print_r(@$sql->getSqlStringForSqlObject($update));
//         die();
        
        if($results!==false) return true;
        return false;
    }
}
