<?php
/**
 * User: Abar Choudhary
 * Date: 30/12/2018
 */

namespace Admin\Model;

class Configs {
	public $config_type;
	public $value;
	public $modified_date;

	public function exchangeArray($data) {
		$this->config_type = !empty($data['config_type']) ? $data['config_type'] : null;
		$this->value = !empty($data['value']) ? $data['value'] : null;
		$this->modified_date = !empty($data['modified_date']) ? $data['modified_date'] : date("Y-m-d H:i:s");
	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

	public function getConfigType()  {return $this->config_type;}
	public function setConfigType($config_type) {$this->config_type = $config_type;}

	public function getValue()  {return $this->value;}
	public function setValue($value) {$this->value = $value;}

	public function getModifiedDate()  {return $this->modified_date;}
	public function setModifiedDate($modified_date) {$this->modified_date = $modified_date;}
}