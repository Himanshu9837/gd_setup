<?php

namespace Admin\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Stdlib\Hydrator;

use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Admin\Model\GdFlatowner;

class GdFlatownerTable {
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway){
        $this->tableGateway=$tableGateway;
    }

    public function add(GdFlatowner $data) {
        $hydrator=new Hydrator\ArraySerializable();
        $data=$hydrator->extract($data);
        if($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        }
        return false;
    }

    public function updateFlatOwner($update, $owner_id) {
        if($this->tableGateway->update($update, array('user_id'=>$owner_id))!==false) return true;
        return false;
    }

    public function getByOwner($owner_id) {
        @list($data)=$this->tableGateway->select(array("user_id"=>$owner_id))->toArray();
        return $data;
    }

    public function getUserId($where = array())
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from(array("t1" => "gd_flatowner"));
        $select->columns(array("user_id"));
        if ($where) $select->where($where);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultSet = new ResultSet();
        $resultSet->initialize($results);
        $result = $resultSet->toArray();
        // print_r(@$sql->getSqlStringForSqlObject($select));
        // die();
        return $result;
    }

    public function remove($userId)
    {
        if ($this->tableGateway->delete(array("user_id" => $userId))) {
            return true;
        } else return false;
    }
    public function getByPhone($value){
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from(array("t1" => "gd_flatowner"));
        $select->columns(array("user_id"));
        if ($value) $select->where(array('mobile'=>$value));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultSet = new ResultSet();
        $resultSet->initialize($results);
       @list($result) = $resultSet->toArray();
        // print_r(@$sql->getSqlStringForSqlObject($select));
        // die();
        return $result;

    }


    public function getBlindUser(){
        $rawSql = "SELECT t1.*, t2.first_name as p_first_name, t2.mobile as p_mobile FROM gd_flatowner as t1
        LEFT JOIN gd_flatowner AS t2 ON t1.parent_id=t2.id
        WHERE t1.parent_id!=0
        ORDER BY t1.first_name DESC"; 
        //$rawSql = "SHOW COLUMNS FROM gd_access_codes"; 
        $adapter = $this->tableGateway->getAdapter();
        $resultSet=$adapter->query($rawSql, array());
        $result= $resultSet->toArray();
        return $result;
    }

    public function getWhere($where ,$columns){
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);

        $select = $sql->select();
        $select->from(array('t1' => 'gd_flatowner'));
        if(!empty($columns)){
            $select->columns($columns);
        }

        if(!empty($where)){
            $select->where($where);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($results);
        $result = $resultSet->toArray();

         //print_r(@$sql->getSqlStringForSqlObject($select));
         //die();

        return $result;
    }

} 
