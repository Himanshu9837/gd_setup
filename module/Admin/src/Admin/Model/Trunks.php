<?php
/**
 * User: Abar Choudhary
 * Date: 07/06/2018
 */

namespace Admin\Model;

class Trunks {
	public $trunk_id;
	public $tech;
	public $trunk_name;
	public $hide_cid;
	public $outbound_cid;
	public $cid_options;
	public $maximum_channels;
	public $dial_options_button;
	public $dial_option_value;
	public $continue_if_busy;
	public $disable_trunk;
	public $prepend_digits;
	public $match_prefix;
	public $match_pattern;
	public $match_cid;

	public $outbound_dial_prefix;
	public $channel_id;
	public $peer_details;
	public $peer_values;
	public $user_context;
	public $user_details;
	public $user_values;
	public $register_string;

	public $date_added;
	public $modified_date;

	public function exchangeArray($data) {
		$this->trunk_id = !empty($data['trunk_id']) ? $data['trunk_id'] : null;
		$this->tech = !empty($data['tech']) ? $data['tech'] : null;
		$this->trunk_name = !empty($data['trunk_name']) ? $data['trunk_name'] : null;
		$this->hide_cid = !empty($data['hide_cid']) ? 1 : 0;
		$this->outbound_cid = !empty($data['outbound_cid']) ? $data['outbound_cid'] : null;
		$this->cid_options = !empty($data['cid_options']) ? $data['cid_options'] : "off";
		$this->maximum_channels = !empty($data['maximum_channels']) ? $data['maximum_channels'] : 0;
		$this->dial_options_button = !empty($data['dial_options_button']) ? $data['dial_options_button'] : "system";
		$this->dial_option_value = !empty($data['dial_option_value']) ? $data['dial_option_value'] : null;
		$this->continue_if_busy = !empty($data['continue_if_busy']) ? $data['continue_if_busy'] : "off";
		$this->disable_trunk = !empty($data['disable_trunk']) ? $data['disable_trunk'] : "off";
		$this->prepend_digits = !empty($data['prepend_digits']) ? $data['prepend_digits'] : null;
		$this->match_prefix = !empty($data['match_prefix']) ? $data['match_prefix'] : null;
		$this->match_pattern = !empty($data['match_pattern']) ? $data['match_pattern'] : null;
		$this->match_cid = !empty($data['match_cid']) ? $data['match_cid'] : null;

		$this->outbound_dial_prefix = !empty($data['outbound_dial_prefix']) ? $data['outbound_dial_prefix'] : null;
		$this->channel_id = !empty($data['channel_id']) ? $data['channel_id'] : null;
		$this->peer_details = !empty($data['peer_details']) ? $data['peer_details'] : null;
		$this->peer_values = !empty($data['peer_values']) ? $data['peer_values'] : null;
		$this->user_context = !empty($data['user_context']) ? $data['user_context'] : null;
		$this->user_details = !empty($data['user_details']) ? $data['user_details'] : null;
		$this->user_values = !empty($data['user_values']) ? $data['user_values'] : null;
		$this->register_string = !empty($data['register_string']) ? $data['register_string'] : null;

		$this->date_added = !empty($data['date_added']) ? $data['date_added'] : date("Y-m-d H:i:s");
		$this->modified_date = !empty($data['modified_date']) ? $data['modified_date'] : date("Y-m-d H:i:s");
	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

	public function getTrunkId()  {return $this->trunk_id;}
	public function setTrunkId($trunk_id) {$this->trunk_id = $trunk_id;}

	public function getTech()  {return $this->tech;}
	public function setTech($tech) {$this->tech = $tech;}

	public function getTrunkName()  {return $this->trunk_name;}
	public function setTrunkName($trunk_name) {$this->trunk_name = $trunk_name;}

	public function getHideCid()  {return $this->hide_cid;}
	public function setHideCid($hide_cid) {$this->hide_cid = $hide_cid;}

	public function getOutboundCid()  {return $this->outbound_cid;}
	public function setOutboundCid($outbound_cid) {$this->outbound_cid = $outbound_cid;}

	public function getCidOptions()  {return $this->cid_options;}
	public function setCidOptions($cid_options) {$this->cid_options = $cid_options;}

	public function getMaximumChannels()  {return $this->maximum_channels;}
	public function setMaximumChannels($maximum_channels) {$this->maximum_channels = $maximum_channels;}

	public function getDialButton()  {return $this->dial_options_button;}
	public function setDialButton($dial_options_button) {$this->dial_options_button = $dial_options_button;}

	public function getDialValue()  {return $this->dial_option_value;}
	public function setDialValue($dial_option_value) {$this->dial_option_value = $dial_option_value;}

	public function getContinue()  {return $this->continue_if_busy;}
	public function setContinue($continue_if_busy) {$this->continue_if_busy = $continue_if_busy;}

	public function getDisableTrunk()  {return $this->disable_trunk;}
	public function setDisableTrunk($disable_trunk) {$this->disable_trunk = $disable_trunk;}

	public function getPrependDigits()  {return $this->prepend_digits;}
	public function setPrependDigits($prepend_digits) {$this->prepend_digits = $prepend_digits;}

	public function getMatchPrefix()  {return $this->match_prefix;}
	public function setMatchPrefix($match_prefix) {$this->match_prefix = $match_prefix;}

	public function getMatchPattern()  {return $this->match_pattern;}
	public function setMatchPattern($match_pattern) {$this->match_pattern = $match_pattern;}

	public function getMatchCid()  {return $this->match_cid;}
	public function setMatchCid($match_cid) {$this->match_cid = $match_cid;}



	public function getPrefix()  {return $this->outbound_dial_prefix;}
	public function setPrefix($outbound_dial_prefix) {$this->outbound_dial_prefix = $outbound_dial_prefix;}

	public function getChannelId()  {return $this->channel_id;}
	public function setChannelId($channel_id) {$this->channel_id = $channel_id;}

	public function getPeerDetails()  {return $this->peer_details;}
	public function setPeerDetails($peer_details) {$this->peer_details = $peer_details;}

	public function getPeerValues()  {return $this->peer_values;}
	public function setPeerValues($peer_values) {$this->peer_values = $peer_values;}

	public function getUserContext()  {return $this->user_context;}
	public function setUserContext($user_context) {$this->user_context = $user_context;}

	public function getUserDetails()  {return $this->user_details;}
	public function setUserDetails($user_details) {$this->user_details = $user_details;}

	public function getUserValues()  {return $this->user_values;}
	public function setUserValues($user_values) {$this->user_values = $user_values;}

	public function getRegister()  {return $this->register_string;}
	public function setRegister($register_string) {$this->register_string = $register_string;}



	public function getDateAdded()  {return $this->date_added;}
	public function setDateAdded($date_added) {$this->date_added = $date_added;}

	public function getModifiedDate()  {return $this->modified_date;}
	public function setModifiedDate($modified_date) {$this->modified_date = $modified_date;}
}