<?php
/**
 * User: Abar Choudhary
 * Date: 30/12/2018
 */
 
namespace Admin\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Stdlib\Hydrator;

use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;

class ConfigsTable {
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway=$tableGateway;
	}

    public function insertOrUpdate(Configs $data) {
        $type=$data->getConfigType();
        $value=$data->getValue();
        $modifiedDate=$data->getModifiedDate();

        $rawSql="
            INSERT INTO gd_configs (config_type, value, modified_date)
            VALUES ($type, '$value', '$modifiedDate')
            ON DUPLICATE KEY UPDATE value='$value', modified_date='$modifiedDate'
        ";
        
        $adapter = $this->tableGateway->getAdapter();
        $resultSet=$adapter->query($rawSql, array());

        if($resultSet->getGeneratedValue()) return true;
        return false;
    }

    public function getByType($type) {
        $adapter=$this->tableGateway->getAdapter();
        $sql=new Sql($adapter);
        $select=$sql->select();

        $select->from(array("t1"=>"gd_configs"));
        //$select->columns(array());

        $select->where(array("t1.config_type"=>$type));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($results);
        @list($result) = $resultSet->toArray();

        return $result;
    }

    public function getTimeOut($type) {
        $adapter=$this->tableGateway->getAdapter();
        $sql=new Sql($adapter);
        $select=$sql->select();

        $select->from(array("t1"=>"gd_owner_type"));
        $select->columns(array("owner_id"));

        $select->where(array("t1.owner_type"=>$type));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($results);
        @list($result) = $resultSet->toArray();

        $ownerId=@$result['owner_id'];
        $ownerId=$ownerId ? $ownerId : false;

        return $ownerId;
    }

    public function remove($oid) {
        if($this->tableGateway->delete(array("owner_id"=>$oid))) {
            return true;
        }
        else return false;
    }
}