<?php
/**
 * User: Abar Choudhary
 * Date: 07/06/2018
 */

namespace Admin\Model;

class ExternalIp {
	public $ip_id;
        public $external_ip;
	public $added_date;
	public $modified_date;

	public function exchangeArray($data) {
		$this->ip_id = !empty($data['ip_id']) ? $data['ip_id'] : null;
		$this->external_ip = !empty($data['external_ip']) ? $data['external_ip'] : null;
		$this->added_date = !empty($data['added_date']) ? $data['added_date'] : date("Y-m-d H:i:s");
		$this->modified_date = !empty($data['modified_date']) ? $data['modified_date'] : date("Y-m-d H:i:s");
	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

	public function getIPId()  {return $this->ip_id;}
	public function setIpId($ip_id) {$this->ip_id = $ip_id;}

	public function getExternalIp()  {return $this->external_ip;}
	public function setExternalIp($external_ip) {$this->external_ip = $external_ip;}

	
	public function getAddedDate()  {return $this->added_date;}
	public function setAddedDate($added_date) {$this->added_date = $added_date;}

	public function getModifiedDate()  {return $this->modified_date;}
	public function setModifiedDate($modified_date) {$this->modified_date = $modified_date;}
}