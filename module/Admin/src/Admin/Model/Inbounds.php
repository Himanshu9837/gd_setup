<?php
/**
 * User: Abar Choudhary
 * Date: 06/06/2018
 */

namespace Admin\Model;

class Inbounds {
	public $row_id;
	public $did_number;
	public $cid_number;
	public $cid_priority_route;
	public $alert_info;
	public $ringer_volume;
	public $cid_name_prefix;
	public $music_on_hold;
	public $set_destination;
	public $privacy_manager;
	public $max_attempts;
	public $min_length;
	public $description;
	public $date_added;
	public $modified_date;

	public function exchangeArray($data) {
		$this->row_id = !empty($data['row_id']) ? $data['row_id'] : null;
		$this->did_number = !empty($data['did_number']) ? $data['did_number'] : null;
		$this->cid_number = !empty($data['cid_number']) ? $data['cid_number'] : null;
		$this->cid_priority_route = !empty($data['cid_priority_route']) ? 1 : 0;
		$this->alert_info = !empty($data['alert_info']) ? $data['alert_info'] : null;
		$this->ringer_volume = !empty($data['ringer_volume']) ? $data['ringer_volume'] : 0;
		$this->cid_name_prefix = !empty($data['cid_name_prefix']) ? $data['cid_name_prefix'] : null;
		$this->music_on_hold = !empty($data['music_on_hold']) ? 1 : 0;
		$this->set_destination = !empty($data['set_destination']) ? $data['set_destination'] : null;
		$this->privacy_manager = !empty($data['privacy_manager']) ? 1 : 0;
		$this->max_attempts = !empty($data['max_attempts']) ? $data['max_attempts'] : 0;
		$this->min_length = !empty($data['min_length']) ? $data['min_length'] : 0;
		$this->description = !empty($data['description']) ? $data['description'] : null;
		$this->date_added = !empty($data['date_added']) ? $data['date_added'] : date("Y-m-d H:i:s");
		$this->modified_date = !empty($data['modified_date']) ? $data['modified_date'] : date("Y-m-d H:i:s");
	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

	public function getRowId()  {return $this->row_id;}
	public function setRowId($row_id) {$this->row_id = $row_id;}

	public function getDidNumber()  {return $this->did_number;}
	public function setDidNumber($did_number) {$this->did_number = $did_number;}

	public function getCidNumber()  {return $this->cid_number;}
	public function setCidNumber($cid_number) {$this->cid_number = $cid_number;}

	public function getPriorityRoute()  {return $this->cid_priority_route;}
	public function setPriorityRoute($cid_priority_route) {$this->cid_priority_route = $cid_priority_route;}

	public function getAlertInfo()  {return $this->alert_info;}
	public function setAlertInfo($alert_info) {$this->alert_info = $alert_info;}

	public function getRingerVolume()  {return $this->ringer_volume;}
	public function setRingerVolume($ringer_volume) {$this->ringer_volume = $ringer_volume;}

	public function getCidPrefix()  {return $this->cid_name_prefix;}
	public function setCidPrefix($cid_name_prefix) {$this->cid_name_prefix = $cid_name_prefix;}

	public function getMusicOnHold()  {return $this->music_on_hold;}
	public function setMusicOnHold($music_on_hold) {$this->music_on_hold = $music_on_hold;}

	public function getDestination()  {return $this->set_destination;}
	public function setDestination($set_destination) {$this->set_destination = $set_destination;}

	public function getPrivacyManager()  {return $this->privacy_manager;}
	public function setPrivacyManager($privacy_manager) {$this->privacy_manager = $privacy_manager;}

	public function getMaxAttempts()  {return $this->max_attempts;}
	public function setMaxAttempts($max_attempts) {$this->max_attempts = $max_attempts;}

	public function getMinLength()  {return $this->min_length;}
	public function setMinLength($min_length) {$this->min_length = $min_length;}

	public function getDescription()  {return $this->description;}
	public function setDescription($description) {$this->description = $description;}

	public function getDateAdded()  {return $this->date_added;}
	public function setDateAdded($date_added) {$this->date_added = $date_added;}

	public function getModifiedDate()  {return $this->modified_date;}
	public function setModifiedDate($modified_date) {$this->modified_date = $modified_date;}
}