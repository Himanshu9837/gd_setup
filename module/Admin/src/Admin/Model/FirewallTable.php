<?php
/**
 * User: Abar Choudhary
 * Date: 25/07/2018
 */
 
namespace Admin\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Stdlib\Hydrator;

use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;

class FirewallTable {
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway=$tableGateway;
	}

    public function insertOrUpdate(Firewall $data) {
        $type=$data->getConfigType();
        $ip=$data->getIpAddress();
        $username=$data->getUsername();
        $password=$data->getPassword();
        $mask=$data->getMask();
        $gateway=$data->getGateway();
        $dns1=$data->getDns1();
        $dns2=$data->getDns2();
        $modifiedDate=$data->getModifiedDate();

        // echo "<pre>";
        // print_r($response);
        // die();

        $rawSql="
            INSERT INTO gd_firewall_configs (config_type, ip_address, username, password, mask, gateway, dns1, dns2, modified_date)
            VALUES ($type, '$ip', '$username', '$password', '$mask', '$gateway', '$dns1', '$dns2', '$modifiedDate')
            ON DUPLICATE KEY UPDATE ip_address='$ip', username='$username', password='$password', mask='$mask', gateway='$gateway', dns1='$dns1', dns2='$dns2', modified_date='$modifiedDate'
        ";

        $adapter = $this->tableGateway->getAdapter();
        $resultSet=$adapter->query($rawSql, array());

        if($resultSet->getGeneratedValue()) return true;
        return false;
    }

	public function fetchAll() {
		$data=$this->tableGateway->select()->toArray();
		return $data;
	}

    public function getByType($type) {
        $adapter=$this->tableGateway->getAdapter();
        $sql=new Sql($adapter);
        $select=$sql->select();

        $select->from(array("t1"=>"gd_firewall_configs"));
        //$select->columns(array());

        $select->where(array("t1.config_type"=>$type));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($results);
        @list($result) = $resultSet->toArray();

        return $result;
    }

    public function remove($type) {
        if($this->tableGateway->delete(array("config_type"=>$type))) {
            return true;
        }
        else return false;
    }
}