<?php
/**
 * User: Abar Choudhary
 * Date: 07/06/2018
 */
 
namespace Admin\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Stdlib\Hydrator;

use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;

use Admin\Model\Trunks;

class TrunksTable {
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway=$tableGateway;
	}

    public function addTrunk(Trunks $data) {
        $hydrator = new Hydrator\ArraySerializable();
        $data = $hydrator->extract($data); // object to array

        if($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        }
        else return false;
    }

    public function updateTrunk(Trunks $data) {
        $update=$data->getArrayCopy();
        $trunkId=$update['trunk_id'];

        if($this->tableGateway->update($update, array('trunk_id' => $trunkId)) !== false) return true;
        return false;
    }

    public function getByTrunk($tid) {
        @list($data)=$this->tableGateway->select(array("trunk_id"=>$tid))->toArray();
        return $data;
    }

    public function getList() {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);

        $select = $sql->select();
        $select->from(array('t1' => 'gd_trunks'));
        $select->columns(array("trunk_id", "tech", "trunk_name", "outbound_cid", "disable_trunk"));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($results);
        $result = $resultSet->toArray();

        // print_r(@$sql->getSqlStringForSqlObject($select));
        // die();

        return $result;
    }

    public function fetchAll() {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);

        $select = $sql->select();
        $select->from(array('t1' => 'gd_trunks'));
        //$select->columns(array());

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($results);
        $result = $resultSet->toArray();

        // print_r(@$sql->getSqlStringForSqlObject($select));
        // die();

        return $result;
    }

    public function removeTrunk($tid) {
        if($this->tableGateway->delete(array("trunk_id"=>$tid))) {
            return true;
        }
        else return false;
    }
}