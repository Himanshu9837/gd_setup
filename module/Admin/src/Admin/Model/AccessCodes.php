<?php
/**
 * User: Abar Choudhary
 * Date: 20/12/2018
 */

namespace Admin\Model;

class AccessCodes {
	public $access_id;
	public $owner_id;
	public $access_code;
	public $usage_type;
	public $code_status;
	public $times_used;
	public $access_date_time;
	public $admin_id;
	public $date_added;
	public $modified_date;

	public function exchangeArray($data) {
		$this->access_id = !empty($data['access_id']) ? $data['access_id'] : null;
		$this->owner_id = !empty($data['owner_id']) ? $data['owner_id'] : null;
		$this->access_code = !empty($data['access_code']) ? $data['access_code'] : null;
		$this->usage_type = !empty($data['usage_type']) ? $data['usage_type'] : 1;
		$this->code_status = !empty($data['code_status']) ? $data['code_status'] : 0;
		$this->times_used = !empty($data['times_used']) ? $data['times_used'] : 0;
		$this->access_date_time = !empty($data['access_date_time']) ? $data['access_date_time'] : null;
		$this->admin_id = !empty($data['admin_id']) ? $data['admin_id'] : 0;
		$this->date_added = !empty($data['date_added']) ? $data['date_added'] : date("Y-m-d H:i:s");
		$this->modified_date = !empty($data['modified_date']) ? $data['modified_date'] : date("Y-m-d H:i:s");
	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

	public function getAccessId()  {return $this->access_id;}
	public function setAccessId($access_id) {$this->access_id = $access_id;}

	public function getOwnerId()  {return $this->owner_id;}
	public function setOwnerId($owner_id) {$this->owner_id = $owner_id;}

	public function getAccessCode()  {return $this->access_code;}
	public function setAccessCode($access_code) {$this->access_code = $access_code;}

	public function getUsageType()  {return $this->usage_type;}
	public function setUsageType($usage_type) {$this->usage_type = $usage_type;}

	public function getCodeStatus()  {return $this->code_status;}
	public function setCodeStatus($code_status) {$this->code_status = $code_status;}

	public function getTimesUsed()  {return $this->times_used;}
	public function setTimesUsed($times_used) {$this->times_used = $times_used;}
        
	public function getAccessDateTime()  {return $this->access_date_time;}
	public function setAccessDateTime($access_date_time) {$this->access_date_time = $access_date_time;}

	public function getAdminId()  {return $this->admin_id;}
	public function setAdminId($admin_id) {$this->admin_id = $admin_id;}

	public function getDateAdded()  {return $this->date_added;}
	public function setDateAdded($date_added) {$this->date_added = $date_added;}

	public function getModifiedDate()  {return $this->modified_date;}
	public function setModifiedDate($modified_date) {$this->modified_date = $modified_date;}
}