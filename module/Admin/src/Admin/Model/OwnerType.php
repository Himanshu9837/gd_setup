<?php
/**
 * User: Abar Choudhary
 * Date: 22/06/2018
 */

namespace Admin\Model;

class OwnerType {
	public $row_id;
	public $owner_type;
	public $owner_id;
	public $created_date;
	public $modified_date;

	public function exchangeArray($data) {
		$this->row_id = !empty($data['row_id']) ? $data['row_id'] : null;
		$this->owner_type = !empty($data['owner_type']) ? $data['owner_type'] : null;
		$this->owner_id = !empty($data['owner_id']) ? $data['owner_id'] : null;
		$this->created_date = !empty($data['created_date']) ? $data['created_date'] : date("Y-m-d H:i:s");
		$this->modified_date = !empty($data['modified_date']) ? $data['modified_date'] : date("Y-m-d H:i:s");
	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

	public function getRowId()  {return $this->row_id;}
	public function setRowId($row_id) {$this->row_id = $row_id;}

	public function getOwnerType()  {return $this->owner_type;}
	public function setOwnerType($owner_type) {$this->owner_type = $owner_type;}

	public function getOwnerId()  {return $this->owner_id;}
	public function setOwnerId($owner_id) {$this->owner_id = $owner_id;}

	public function getCreatedDate()  {return $this->created_date;}
	public function setCreatedDate($created_date) {$this->created_date = $created_date;}

	public function getModifiedDate()  {return $this->modified_date;}
	public function setModifiedDate($modified_date) {$this->modified_date = $modified_date;}
}