<?php
/**
 * User: Abar Choudhary
 * Date: 07/06/2018
 */
 
namespace Admin\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Stdlib\Hydrator;

use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;

use Admin\Model\ExternalIp;

class ExternalIpTable {
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway=$tableGateway;
	}

//  public function AddIp(ExternalIp $data) {
//       $insert=array(
//            "external_ip"=>$data->getExternalIp(),           
//            "added_date"=>$data->getAddedDate(), 
//            "modified_date"=>$data->getModifiedDate()           
//        );       
//        if($this->tableGateway->insert($insert)) {
//            return $this->tableGateway->lastInsertValue;
//        }
//        else return false;
//    }
        
      public function AddIp($ip) {
       $insert=array(
            "external_ip"=>$ip,           
            "added_date"=> date("Y-m-d H:i:s"), 
            "modified_date"=>date("Y-m-d H:i:s")           
        );       
        if($this->tableGateway->insert($insert)) {
            return $this->tableGateway->lastInsertValue;
        }
        else return false;
    }  
        
    public function updateIp($ip,$ip_id) {

        if($this->tableGateway->update(array('external_ip' => $ip), array('ip_id' => $ip_id)) !== false) return true;
        return false;
    }

    public function getData() {
        @list($data)=$this->tableGateway->select()->toArray();
        return $data;
    }

}