<?php
/**
 * User: Abar Choudhary
 * Date: 22/06/2018
 */
 
namespace Admin\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Stdlib\Hydrator;

use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;

class OwnerTypeTable {
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway=$tableGateway;
	}

    public function insertOrUpdate(OwnerType $data) {
        $type=$data->getOwnerType();
        $id=$data->getOwnerId();
        $dateAdded=$data->getCreatedDate();
        $modifiedDate=$data->getModifiedDate();

        // echo "<pre>";
        // print_r($response);
        // die();

        $rawSql="
            INSERT INTO gd_owner_type (owner_type, owner_id, created_date, modified_date)
            VALUES ($type, $id, '$dateAdded', '$modifiedDate')
            ON DUPLICATE KEY UPDATE owner_id=$id, modified_date='$modifiedDate'
        ";

        $adapter = $this->tableGateway->getAdapter();
        $resultSet=$adapter->query($rawSql, array());

        if($resultSet->getGeneratedValue()) return true;
        return false;
    }

	public function fetchAll() {
		$data=$this->tableGateway->select()->toArray();
		return $data;
	}

    public function getByType($type) {
        $adapter=$this->tableGateway->getAdapter();
        $sql=new Sql($adapter);
        $select=$sql->select();

        $select->from(array("t1"=>"gd_owner_type"));
        $select->columns(array());

        $select->join(
            array("t2"=>"gd_flatowner"),
            new Expression("t1.owner_id=t2.id"),
            array("owner_id"=>"id"),
            'LEFT'
        );

        $select->where(array("t1.owner_type"=>$type));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($results);
        @list($result) = $resultSet->toArray();

        $ownerId=@$result['owner_id'];
        $ownerId=$ownerId ? $ownerId : false;

        return $ownerId;
    }

    public function getTimeOut($type) {
        $adapter=$this->tableGateway->getAdapter();
        $sql=new Sql($adapter);
        $select=$sql->select();

        $select->from(array("t1"=>"gd_owner_type"));
        $select->columns(array("owner_id"));

        $select->where(array("t1.owner_type"=>$type));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($results);
        @list($result) = $resultSet->toArray();

        $ownerId=@$result['owner_id'];
        $ownerId=$ownerId ? $ownerId : false;

        return $ownerId;
    }

    public function remove($oid) {
        if($this->tableGateway->delete(array("owner_id"=>$oid))) {
            return true;
        }
        else return false;
    }
}