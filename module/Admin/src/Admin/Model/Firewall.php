<?php
/**
 * User: Abar Choudhary
 * Date: 25/07/2018
 */

namespace Admin\Model;

class Firewall {
	public $config_type;
	public $ip_address;
	public $username;
	public $password;
	public $mask;
	public $gateway;
	public $dns1;
	public $dns2;
	public $modified_date;

	public function exchangeArray($data) {
		$this->config_type = !empty($data['config_type']) ? $data['config_type'] : null;
		$this->ip_address = !empty($data['ip_address']) ? $data['ip_address'] : null;
		$this->username = !empty($data['username']) ? $data['username'] : null;
		$this->password = !empty($data['password']) ? $data['password'] : null;
		$this->mask = !empty($data['mask']) ? $data['mask'] : null;
		$this->gateway = !empty($data['gateway']) ? $data['gateway'] : null;
		$this->dns1 = !empty($data['dns1']) ? $data['dns1'] : null;
		$this->dns2 = !empty($data['dns2']) ? $data['dns2'] : null;
		$this->modified_date = !empty($data['modified_date']) ? $data['modified_date'] : date("Y-m-d H:i:s");
	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

	public function getConfigType()  {return $this->config_type;}
	public function setConfigType($config_type) {$this->config_type = $config_type;}

	public function getIpAddress()  {return $this->ip_address;}
	public function setIpAddress($ip_address) {$this->ip_address = $ip_address;}

	public function getUsername()  {return $this->username;}
	public function setUsername($username) {$this->username = $username;}

	public function getPassword()  {return $this->password;}
	public function setPassword($password) {$this->password = $password;}

	public function getMask()  {return $this->mask;}
	public function setMask($mask) {$this->mask = $mask;}

	public function getGateway()  {return $this->gateway;}
	public function setGateway($gateway) {$this->gateway = $gateway;}

	public function getDns1()  {return $this->dns1;}
	public function setDns1($dns1) {$this->dns1 = $dns1;}

	public function getDns2()  {return $this->dns2;}
	public function setDns2($dns2) {$this->dns2 = $dns2;}

	public function getModifiedDate()  {return $this->modified_date;}
	public function setModifiedDate($modified_date) {$this->modified_date = $modified_date;}
}