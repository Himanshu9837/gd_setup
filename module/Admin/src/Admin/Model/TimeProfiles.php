<?php
/**
 * User: Abar Choudhary
 * Date: 30/12/2018
 */

namespace Admin\Model;

class TimeProfiles {
	public $time_profile_id;    
	public $time_profile_config;
	public $time_profile_name;

	public function exchangeArray($data) {
		$this->time_profile_id = !empty($data['time_profile_id']) ? $data['time_profile_id'] : null;
		$this->time_profile_config = !empty($data['time_profile_config']) ? $data['time_profile_config'] : null;
		$this->time_profile_name = !empty($data['time_profile_name']) ? $data['time_profile_name'] : null;
	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

	public function getTimeProfileId()  {return $this->time_profile_id;}
	public function setTimeProfileId($time_profile_id) {$this->time_profile_id = $time_profile_id;}

	public function getTimeProfileConfig()  {return $this->time_profile_config;}
	public function setTimeProfileConfig($time_profile_config) {$this->time_profile_config = $time_profile_config;}

	public function getTimeProfileName()  {return $this->time_profile_name;}
	public function setTimeProfileName($time_profile_name) {$this->time_profile_name = $time_profile_name;}
}