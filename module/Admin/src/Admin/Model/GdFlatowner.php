<?php
namespace Admin\Model;

class GdFlatowner{
    public $id; 
    public $user_id; 
    public $parent_id;  
    public $first_name; 
    public $last_name; 
    public $unit_number; 
    public $floor; 
    public $mobile; 
    public $email; 
    public $followme1;
    public $followme2;
    public $followme3;
    public $followme4;
    public $followme5;
    public $confirmcall;
    public $ring_type;
    public $voice_mail;
    public $create_sip_user;
    public $camera_only;
    public $sip_username;
    public $sip_password;
    public $sip_extention_num;
    public $sip_download_code; 
    public $locked_to_device;
    public $sites;
    public $status ;
	public $created_date ; 
	public $modified_date ;


    public function exchangeArray($data) { 
        $this->id= !empty($data['id']) ? $data['id'] : null;
        $this->user_id= !empty($data['user_id']) ? $data['user_id'] : 0; 
        $this->parent_id=  !empty($data['parent_id']) ? $data['parent_id'] : 0;
        $this->first_name= !empty($data['first_name']) ? $data['first_name'] : null;
        $this->last_name= !empty($data['last_name']) ? $data['last_name'] : null;
        $this->unit_number= !empty($data['unit_number']) ? $data['unit_number']: null;
        $this->floor= !empty($data['floor']) ? $data['floor']: null;
        $this->mobile= !empty($data['mobile']) ? $data['mobile']: null;
        $this->email= !empty($data['email']) ? $data['email']: null;
        $this->followme1=!empty($data['followme1']) ? $data['followme1']: null;
        $this->followme2=!empty($data['followme2']) ? $data['followme2']: null;
        $this->followme3=!empty($data['followme3']) ? $data['followme3']: null;
        $this->followme4=!empty($data['followme4']) ? $data['followme4']: null;
        $this->followme5=!empty($data['followme5']) ? $data['followme5']: null;
        $this->confirmcall=!empty($data['confirmcall']) ? $data['confirmcall']: null;
        $this->ring_type=!empty($data['ring_type']) ? $data['ring_type']: null;
        $this->voice_mail=!empty($data['voice_mail']) ? $data['voice_mail']: null;
        $this->create_sip_user=!empty($data['create_sip_user']) ? $data['create_sip_user']: null;
        $this->camera_only=!empty($data['camera_only']) ? $data['camera_only']: null;
        $this->sip_username=!empty($data['sip_username']) ? $data['sip_username']: null;
        $this->sip_password=!empty($data['sip_password']) ? $data['sip_password']: null;
        $this->sip_extention_num=!empty($data['sip_extention_num']) ? $data['sip_extention_num']: null;
        $this->sip_download_code= !empty($data['sip_download_code']) ? $data['sip_download_code']: null;
        $this->locked_to_device=!empty($data['locked_to_device']) ? $data['locked_to_device']: null;
        $this->sites=!empty($data['sites']) ? $data['sites']: null;
        $this->status =!empty($data['status']) ? $data['status']: '1';
		$this->created_date = !empty($data['created_date']) ? $data['created_date'] : date("Y-m-d H:i:s");
		$this->modified_date = !empty($data['modified_date']) ? $data['modified_date'] : date("Y-m-d H:i:s");
	}



    public function getArrayCopy() { return get_object_vars($this); }
    
    public function setParentId($parent_id) {  $this->parent_id = $parent_id;}
    public function getParentId() { return $this->parent_id; }
    
    public function setUserId($user_id) { $this->user_id = $user_id;}
    public function getUserId() {return $this->user_id;}
   
    public function setId($id) {$this->id = $id;}
    public function getId(){ return $this->id; }

    public function setFirstName($first_name){ $this->first_name = $first_name;}
    public function getFirstName(){ return $this->first_name;}
    
    public function setLastName($last_name){ $this->last_name = $last_name;}
    public function getLastName(){return $this->last_name;}

    public function setUnitNumber($unit_number){$this->unit_number = $unit_number;}
    public function getUnitNumber(){ return $this->unit_number;}
    
    public function setFloor($floor){ $this->floor = $floor; }
    public function getFloor(){ return $this->floor; }
   
    public function setMobile($mobile){$this->mobile = $mobile;}
    public function getMobile() { return $this->mobile; }
    
    public function setEmail($email){  $this->email = $email; }
    public function getEmail(){ return $this->email; }
  
    public function setFollowme1($followme1){ $this->followme1 = $followme1; }
    public function getFollowme1(){ return $this->followme1; }

    public function setFollowme2($followme2){ $this->followme2 = $followme2; }
    public function getFollowme2(){ return $this->followme2; }

    public function setFollowme3($followme3){ $this->followme3 = $followme3;}
    public function getFollowme3(){return $this->followme3;}  

    public function setFollowme4($followme4){ $this->followme4 = $followme4; }
    public function getFollowme4(){ return $this->followme4; }

    public function setFollowme5($followme5) { $this->followme5 = $followme5; }
    public function getFollowme5(){return $this->followme5; } 

    public function setConfirmcall($confirmcall){ $this->confirmcall = $confirmcall;}
    public function getConfirmcall(){ return $this->confirmcall; }
    
    public function setRingType($ring_type){ $this->ring_type = $ring_type; }
    public function getRingType(){ return $this->ring_type;}

    public function setVoiceMail($voice_mail){ $this->voice_mail = $voice_mail; }
    public function getVoiceMail(){ return $this->voice_mail; }

    public function setCreateSipUser($create_sip_user){ $this->create_sip_user = $create_sip_user; }
    public function getCreateSipUser(){ return $this->create_sip_user; }
 
    public function setCameraOnly($camera_only){ $this->camera_only = $camera_only;}
    public function getCameraOnly(){ return $this->camera_only;}
   
    public function setSipUsername($sip_username){ $this->sip_username = $sip_username; }
    public function getSipUsername(){ return $this->sip_username; }

    public function setSipPassword($sip_password){ $this->sip_password = $sip_password; }
    public function getSipPassword(){ return $this->sip_password; }

    public function setSipExtentionNum($sip_extention_num){ $this->sip_extention_num = $sip_extention_num;}
    public function getSipExtentionNum(){ return $this->sipExtentionNum; }

    public function setSipDownloadCode($sip_download_code){ $this->sip_download_code = $sip_download_code;}
    public function getSipDownloadCode(){ return $this->sip_download_code;}
    
    public function setLockedToDevice($locked_to_device){ $this->locked_to_device = $locked_to_device;}
    public function getLockedToDevice(){ return $this->locked_to_device; }

    public function setSites($sites){ $this->sites = $sites;}
    public function getSites(){ return $this->sites; }

    public function setStatus($status){ $this->status = $status;}
    public function getStatus(){ return $this->status; }

    public function setCreatedDate($created_date){ $this->created_date = $created_date; }
    public function getCreatedDate(){ return $this->created_date; }

    public function setModifiedDate($modified_date){$this->modified_date = $modified_date;}
    public function getModifiedDate(){ return $this->modified_date;}

}