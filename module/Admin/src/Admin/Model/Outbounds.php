<?php
/**
 * User: Abar Choudhary
 * Date: 06/06/2018
 */

namespace Admin\Model;

class Outbounds {
	public $route_id;
	public $route_name;
	public $route_cid;
	public $override_extension;
	public $route_password;
	public $emergency;
	public $intra_company;
	public $music_on_hold;
	public $route_position;
	public $trunk_sequence;
	public $optional_destination;
	public $prepend_digits;
	public $match_prefix;
	public $match_pattern;
	public $match_cid;
	public $date_added;
	public $modified_date;

	public function exchangeArray($data) {
		$this->route_id = !empty($data['route_id']) ? $data['route_id'] : null;
		$this->route_name = !empty($data['route_name']) ? $data['route_name'] : null;
		$this->route_cid = !empty($data['route_cid']) ? $data['route_cid'] : null;
		$this->override_extension = !empty($data['override_extension']) ? 1 : 0;
		$this->route_password = !empty($data['route_password']) ? $data['route_password'] : null;
		$this->emergency = !empty($data['emergency']) ? $data['emergency'] : "no";
		$this->intra_company = !empty($data['intra_company']) ? $data['intra_company'] : "no";
		$this->music_on_hold = !empty($data['music_on_hold']) ? 1 : 0;
		$this->route_position = !empty($data['route_position']) ? $data['route_position'] : 0;
		$this->trunk_sequence = !empty($data['trunk_sequence']) ? $data['trunk_sequence'] : null;
		$this->optional_destination = !empty($data['optional_destination']) ? $data['optional_destination'] : null;
		$this->prepend_digits = !empty($data['prepend_digits']) ? $data['prepend_digits'] : null;
		$this->match_prefix = !empty($data['match_prefix']) ? $data['match_prefix'] : null;
		$this->match_pattern = !empty($data['match_pattern']) ? $data['match_pattern'] : null;
		$this->match_cid = !empty($data['match_cid']) ? $data['match_cid'] : null;
		$this->date_added = !empty($data['date_added']) ? $data['date_added'] : date("Y-m-d H:i:s");
		$this->modified_date = !empty($data['modified_date']) ? $data['modified_date'] : date("Y-m-d H:i:s");
	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

	public function getRouteId()  {return $this->route_id;}
	public function setRouteId($route_id) {$this->route_id = $route_id;}

	public function getRouteName()  {return $this->route_name;}
	public function setRouteName($route_name) {$this->route_name = $route_name;}

	public function getRouteCid()  {return $this->route_cid;}
	public function setRouteCid($route_cid) {$this->route_cid = $route_cid;}

	public function getOverrideExtension()  {return $this->override_extension;}
	public function setOverrideExtension($override_extension) {$this->override_extension = $override_extension;}

	public function getRoutePassword()  {return $this->route_password;}
	public function setRoutePassword($route_password) {$this->route_password = $route_password;}

	public function getEmergency()  {return $this->emergency;}
	public function setEmergency($emergency) {$this->emergency = $emergency;}

	public function getIntraCompany()  {return $this->intra_company;}
	public function setIntraCompany($intra_company) {$this->intra_company = $intra_company;}

	public function getMusicOnHold()  {return $this->music_on_hold;}
	public function setMusicOnHold($music_on_hold) {$this->music_on_hold = $music_on_hold;}

	public function getRoutePosition()  {return $this->route_position;}
	public function setRoutePosition($route_position) {$this->route_position = $route_position;}

	public function getTrunkSequence()  {return $this->trunk_sequence;}
	public function setTrunkSequence($trunk_sequence) {$this->trunk_sequence = $trunk_sequence;}

	public function getDestination()  {return $this->optional_destination;}
	public function setDestination($optional_destination) {$this->optional_destination = $optional_destination;}

	public function getPrependDigits()  {return $this->prepend_digits;}
	public function setPrependDigits($prepend_digits) {$this->prepend_digits = $prepend_digits;}

	public function getMatchPrefix()  {return $this->match_prefix;}
	public function setMatchPrefix($match_prefix) {$this->match_prefix = $match_prefix;}

	public function getMatchPattern()  {return $this->match_pattern;}
	public function setMatchPattern($match_pattern) {$this->match_pattern = $match_pattern;}

	public function getMatchCid()  {return $this->match_cid;}
	public function setMatchCid($match_cid) {$this->match_cid = $match_cid;}

	public function getDateAdded()  {return $this->date_added;}
	public function setDateAdded($date_added) {$this->date_added = $date_added;}

	public function getModifiedDate()  {return $this->modified_date;}
	public function setModifiedDate($modified_date) {$this->modified_date = $modified_date;}
}