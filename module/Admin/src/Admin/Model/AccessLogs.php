<?php
/**
 * User: Abar Choudhary
 * Date: 30/12/2018
 */

namespace Admin\Model;

class AccessLogs {
	public $log_id;
	public $access_id;
	public $owner_id;
	public $owner_name;
	public $owner_mobile;
	public $owner_info;
	public $date_added;

	public function exchangeArray($data) {
		$this->log_id = !empty($data['log_id']) ? $data['log_id'] : null;
		$this->access_id = !empty($data['access_id']) ? $data['access_id'] : null;
		$this->owner_id = !empty($data['owner_id']) ? $data['owner_id'] : null;
		$this->owner_name = !empty($data['owner_name']) ? $data['owner_name'] : null;
		$this->owner_mobile = !empty($data['owner_mobile']) ? $data['owner_mobile'] : null;
		$this->owner_info = !empty($data['owner_info']) ? $data['owner_info'] : null;
		$this->date_added = !empty($data['date_added']) ? $data['date_added'] : date("Y-m-d H:i:s");
	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

	public function getLogId()  {return $this->log_id;}
	public function setLogId($log_id) {$this->log_id = $log_id;}

	public function getAccessId()  {return $this->access_id;}
	public function setAccessId($access_id) {$this->access_id = $access_id;}

	public function getOwnerId()  {return $this->owner_id;}
	public function setOwnerId($owner_id) {$this->owner_id = $owner_id;}

	public function getOwnerName()  {return $this->owner_name;}
	public function setOwnerName($owner_name) {$this->owner_name = $owner_name;}

	public function getOwnerMobile()  {return $this->owner_mobile;}
	public function setOwnerMobile($owner_mobile) {$this->owner_mobile = $owner_mobile;}

	public function getOwnerInfo()  {return $this->owner_info;}
	public function setOwnerInfo($owner_info) {$this->owner_info = $owner_info;}

	public function getDateAdded()  {return $this->date_added;}
	public function setDateAdded($date_added) {$this->date_added = $date_added;}
}