<?php
/**
 * User: Abar Choudhary
 * Date: 30/12/2018
 */
 
namespace Admin\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Stdlib\Hydrator;

use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;

use Admin\Model\Inbounds;

class AccessCodesTable {
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway=$tableGateway;
	}

    public function addCode(AccessCodes $data) {
        $hydrator = new Hydrator\ArraySerializable();
        $data = $hydrator->extract($data); // object to array
        
        if($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        }
        else return false;
    }

    public function getWhere($where) {
        $data=$this->tableGateway->select($where)->toArray();
        return $data;
    }

    public function getWhereInfo($where) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);

        $select = $sql->select();
        $select->from(array('t1' => 'gd_access_codes'));
        //$select->columns(array());

        $select->join(
            array('t2' => 'gd_flatowner'),
            new Expression('t1.owner_id=t2.id'),
            array(
                "first_name", "last_name", "unit_number", "floor", "mobile", "status"
            ),
            'LEFT'
        );        
        
        $select->join(
            array('t3' => 'gd_time_profiles'),
            new Expression('t1.access_date_time=t3.time_profile_id'),
            array(
                "time_profile_id", "time_profile_name", "time_profile_config","time_profile_status"
            ),
            'LEFT'
        );

        $select->where($where);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($results);
        $result = $resultSet->toArray();

        // print_r(@$sql->getSqlStringForSqlObject($select));
        // die();

        return $result;
    }

    public function getList($status=1) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);

        $select = $sql->select();
        $select->from(array('t1' => 'gd_access_codes'));
        //$select->columns(array());

        $select->join(
            array('t2' => 'gd_flatowner'),
            new Expression('t1.owner_id=t2.id'),
            array(
                "first_name", "last_name", "unit_number", "floor", "mobile"
            ),
            'LEFT'
        );
        $select->join(
            array('t3' => 'gd_time_profiles'),
            new Expression('t1.access_date_time=t3.time_profile_id'),
            array(
                "time_profile_name"
            ),
            'LEFT'
        );

        if($status==1) $select->where(array("t1.code_status"=>1));

        $select->order("t1.access_id DESC");

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($results);
        $result = $resultSet->toArray();

        // print_r(@$sql->getSqlStringForSqlObject($select));
        // die();

        return $result;
    }

    public function getOwners() {
        $rawSql="
            SELECT id, first_name, mobile FROM gd_flatowner
            ORDER BY first_name ASC
        ";

        // print_r($rawSql);
        // die();

        $adapter = $this->tableGateway->getAdapter();
        $resultSet=$adapter->query($rawSql, array());

        $result=$resultSet->toArray();
        return $result;
    }

    public function updateStatus($status, $accessId) {
        $update=array("code_status"=>$status, "modified_date"=>date("Y-m-d H:i:s"));
        $where=array("access_id"=>$accessId);

        if($this->tableGateway->update($update, $where) !== false) return true;
        return false;
    }

    public function removeCode($accessId) {
        if($this->tableGateway->delete(array("access_id"=>$accessId))) {
            return true;
        }
        else return false;
    }
   
    public function updateOwnerAccessCode($update, $owner_id) {
        if($this->tableGateway->update($update, array('owner_id'=>$owner_id))!==false) return true;
        return false;
    }

    public function getOwnerUserId($where = array())
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from(array("t1" => "gd_access_codes"));
        $select->columns(array("owner_id"));
        if ($where) $select->where($where);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultSet = new ResultSet();
        $resultSet->initialize($results);
        $result = $resultSet->toArray();
        // print_r(@$sql->getSqlStringForSqlObject($select));
        // die();
        return $result;
    }

    public function removeOwner($userId)
    {
        if ($this->tableGateway->delete(array("owner_id" => $userId))) {
            return true;
        } else return false;
    }

    


}
