<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
    'router' => array(
        'routes' => array(
            /* 'home' => array(
              'type' => 'Zend\Mvc\Router\Http\Literal',
              'options' => array(
              'route'    => '/admin',
              'defaults' => array(
              'controller' => 'Admin\Controller\Index',
              'action'     => 'index',
              ),
              ),
              ), */
            'adminlogin' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/[:action]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    //'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Index',
                        'action' => 'login',
                    ),
                ),
            ),
            'admindashboard' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin/dashboard',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Index',
                        'action' => 'dashboard',
                    ),
                ),
            ),
            'adminlogout' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin/logout',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Index',
                        'action' => 'logout',
                    ),
                ),
            ),
            'adminprofile' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin/profile',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Index',
                        'action' => 'adminprofile',
                    ),
                ),
            ),
             'addsubadmin' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin/subadmin/add',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Subadmin',
                        'action' => 'subadmin',
                    ),
                ),
            ),
            'subadminlists' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin/subadmin/list',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Subadmin',
                        'action' => 'subadminlists',
                    ),
                ),
            ),
             'sbadmindelete' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/subadmin/delete/[:id]',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Subadmin',
                        'action' => 'sbadmindelete',
                    ),
                ),
            ),
            'ajaxdashboard' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin/index/ajaxdashboard',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Index',
                        'action' => 'ajaxdashboard',
                    ),
                ),
            ),
            'owneradd' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin/owner/add',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Owner',
                        'action' => 'addnewowner',
                    ),
                ),
            ),
            'owneredit' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/owner/edit/[:id]',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Owner',
                        'action' => 'editowner',
                    ),
                ),
            ),
            'ownerdelete' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/owner/delete/[:id]',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Owner',
                        'action' => 'ownerdelete',
                    ),
                ),
            ),            
            'timeprofiles' => array(
                'type' => 'segment',
                'options' => array(
                    "route" => "/admin/timeprofiles[/:action][/id/:id]",
                    "constraints" => array(
                        "action" => "[a-zA-Z][a-zA-Z0-9_-]*",
                        "id" => "[0-9]+",
                    ),
                    'defaults' => array(
                        'controller' => 'Admin\Controller\TimeProfiles',
                        'action' => 'index',
                    ),
                ),
            ),
            'userchangestatus' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/owner/changestatus/[:type][/:id]',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Owner',
                        'action' => 'ownerchangestatus',
                    ),
                ),
            ),
            'blindusers' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin/owner/blindusers',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Owner',
                        'action' => 'blindUsers',
                    ),
                ),
            ),
            'ownersubowners' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/owner/subowners[/:id]',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Owner',
                        'action' => 'subOwners',
                    ),
                ),
            ),
            'time' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin/index/time',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Index',
                        'action' => 'time',
                    ),
                ),
            ),
            'createsipuser' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin/owner/createsipuser',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Owner',
                        'action' => 'createSipUser',
                    ),
                ),
            ),
            'generateotp' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin/owner/generateotp',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Owner',
                        'action' => 'generateOtp',
                    ),
                ),
            ),
            'regenerateotp' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin/owner/regenerateotp',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Owner',
                        'action' => 'reGenerateOtp',
                    ),
                ),
            ),
	    'generateextension' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin/owner/generateextension',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Owner',
                        'action' => 'generateExtension',
                    ),
                ),
            ),           
            'gappsetting' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin/globalappsetting/gappsetting',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Globalappsetting',
                        'action' => 'globalAppSetting',
                    ),
                ),
            ),
            'gappsettingn' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin/globalappsetting/gappsettingn',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Globalappsetting',
                        'action' => 'globalAppSettingn',
                    ),
                ),
            ),
            'userlimit' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin/userlimit/userlimit',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Userlimit',
                        'action' => 'userlimit',
                    ),
                ),
            ),
            'dbexport' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin/index/dbexport',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Index',
                        'action' => 'dbexport',
                    ),
                ),
            ),
            'dbbackup' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin/index/dbbackup',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Index',
                        'action' => 'dbbackup',
                    ),
                ),
            ),
            'importcsv' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin/owner/importcsv',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Owner',
                        'action' => 'importcsv',
                    ),
                ),
            ),
            'changepass'=>array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                     'route'    => '/admin/changepassword',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Changepassword',
                        'action' => 'changepassword',
                    ),
                ),
            ),
            'asterikrestore' => array(
                'type' => 'Zend\Mvc\Router\Http\segment',
                'options' => array(
                    'route' => '/admin/asterisk/[:action]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        //'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Asterisk',
                        'action' => 'asterisk',
                    ),
                ),
            ),

            "screensaver" => array(
                "type"    => "segment",
                "options"=>array(
                    "route" => "/admin/screensaver[/:action][/:id]",
                    "constraints" => array(
                        "action" => "[a-zA-Z][a-zA-Z0-9_-]*",
                        "id" => "[0-9]+",
                    ),
                    "defaults" => array(
                        "controller" => "Admin\Controller\Index",
                        "action" => "screensaver",
                    )
                )
            ),

            "inbounds" => array(
                "type"    => "segment",
                "options"=>array(
                    "route" => "/admin/inbounds[/:action][/:id]",
                    "constraints" => array(
                        "action" => "[a-zA-Z][a-zA-Z0-9_-]*",
                        "id" => "[0-9]+",
                    ),
                    "defaults" => array(
                        "controller" => "Admin\Controller\Index",
                        "action" => "inbounds",
                    )
                )
            ),

            "outbounds" => array(
                "type"    => "segment",
                "options"=>array(
                    "route" => "/admin/outbounds[/:action][/:id]",
                    "constraints" => array(
                        "action" => "[a-zA-Z][a-zA-Z0-9_-]*",
                        "id" => "[0-9]+",
                    ),
                    "defaults" => array(
                        "controller" => "Admin\Controller\Index",
                        "action" => "outbounds",
                    )
                )
            ),

            "trunks" => array(
                "type"    => "segment",
                "options"=>array(
                    "route" => "/admin/trunks[/:action][/:id]",
                    "constraints" => array(
                        "action" => "[a-zA-Z][a-zA-Z0-9_-]*",
                        "id" => "[0-9]+",
                    ),
                    "defaults" => array(
                        "controller" => "Admin\Controller\Index",
                        "action" => "trunks",
                    )
                )
            ),

            "calldetails" => array(
                "type"    => "segment",
                "options"=>array(
                    "route" => "/admin/calldetails[/:action][/:id]",
                    "constraints" => array(
                        "action" => "[a-zA-Z][a-zA-Z0-9_-]*",
                        "id" => "[0-9]+",
                    ),
                    "defaults" => array(
                        "controller" => "Admin\Controller\Index",
                        "action" => "callLogs",
                    )
                )
            ),

            "chansippeers" => array(
                "type"    => "segment",
                "options"=>array(
                    "route" => "/admin/chansippeers[/:action][/:id]",
                    "constraints" => array(
                        "action" => "[a-zA-Z][a-zA-Z0-9_-]*",
                        "id" => "[0-9]+",
                    ),
                    "defaults" => array(
                        "controller" => "Admin\Controller\Index",
                        "action" => "chanSipPeers",
                    )
                )
            ),

            "chansipchannels" => array(
                "type"    => "segment",
                "options"=>array(
                    "route" => "/admin/chansipchannels[/:action][/:id]",
                    "constraints" => array(
                        "action" => "[a-zA-Z][a-zA-Z0-9_-]*",
                        "id" => "[0-9]+",
                    ),
                    "defaults" => array(
                        "controller" => "Admin\Controller\Index",
                        "action" => "chanSipChannels",
                    )
                )
            ),

            "chansipregistry" => array(
                "type"    => "segment",
                "options"=>array(
                    "route" => "/admin/chansipregistry[/:action][/:id]",
                    "constraints" => array(
                        "action" => "[a-zA-Z][a-zA-Z0-9_-]*",
                        "id" => "[0-9]+",
                    ),
                    "defaults" => array(
                        "controller" => "Admin\Controller\Index",
                        "action" => "chanSipRegistry",
                    )
                )
            ),

            "iax2peers" => array(
                "type"    => "segment",
                "options"=>array(
                    "route" => "/admin/iax2peers[/:action][/:id]",
                    "constraints" => array(
                        "action" => "[a-zA-Z][a-zA-Z0-9_-]*",
                        "id" => "[0-9]+",
                    ),
                    "defaults" => array(
                        "controller" => "Admin\Controller\Index",
                        "action" => "iax2Peers",
                    )
                )
            ),

            "uptime" => array(
                "type"    => "segment",
                "options"=>array(
                    "route" => "/admin/uptime[/:action][/:id]",
                    "constraints" => array(
                        "action" => "[a-zA-Z][a-zA-Z0-9_-]*",
                        "id" => "[0-9]+",
                    ),
                    "defaults" => array(
                        "controller" => "Admin\Controller\Index",
                        "action" => "uptime",
                    )
                )
            ),

            "firewall" => array(
                "type"    => "segment",
                "options"=>array(
                    "route" => "/admin/firewall[/:action][/:id]",
                    "constraints" => array(
                        "action" => "[a-zA-Z][a-zA-Z0-9_-]*",
                        "id" => "[0-9]+",
                    ),
                    "defaults" => array(
                        "controller" => "Admin\Controller\Index",
                        "action" => "firewall",
                    )
                )
            ),

            "accesscodes" => array(
                "type"    => "segment",
                "options"=>array(
                    "route" => "/admin/accesscodes[/:action][/:id]",
                    "constraints" => array(
                        "action" => "[a-zA-Z][a-zA-Z0-9_-]*",
                        "id" => "[0-9]+",
                    ),
                    "defaults" => array(
                        "controller" => "Admin\Controller\AccessCodes",
                        "action" => "index",
                    )
                )
            ),

            "accesslogs" => array(
                "type"    => "segment",
                "options"=>array(
                    "route" => "/admin/accesscodes/accesslogs",
                    "defaults" => array(
                        "controller" => "Admin\Controller\AccessCodes",
                        "action" => "logs",
                    )
                )
            ),
            
            'crons' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/crons[/:action][/id/:id]',
                    "constraints" => array(
                        "action" => "[a-zA-Z][a-zA-Z0-9_-]*",
                        "id" => "[0-9]+",
                    ),
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Crons',
                        'action' => 'index',
                    ),
                ),
            ),
            

            'updatecsv-data' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/updatecsv-data[/:action][/id/:id]',
                    "constraints" => array(
                        "action" => "[a-zA-Z][a-zA-Z0-9_-]*",
                        "id" => "[0-9]+",
                    ),
                    'defaults' => array(
                        'controller' => 'Admin\Controller\UpdatecsvCron',
                        'action' => 'index',
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Admin\Controller\Index' => 'Admin\Controller\IndexController',
            'Admin\Controller\Owner' => 'Admin\Controller\OwnerController',
            'Admin\Controller\TimeProfiles' => 'Admin\Controller\TimeProfilesController',
            'Admin\Controller\Globalappsetting' => 'Admin\Controller\GlobalappsettingController',
            'Admin\Controller\Userlimit' => 'Admin\Controller\UserlimitController',
            'Admin\Controller\Subadmin' => 'Admin\Controller\SubadminController',
            'Admin\Controller\Changepassword' => 'Admin\Controller\ChangepasswordController',
            'Admin\Controller\Asterisk' => 'Admin\Controller\AsteriskController',
            'Admin\Controller\Inbound' => 'Admin\Controller\InboundController',
            'Admin\Controller\AccessCodes' => 'Admin\Controller\AccessCodesController',
            "Admin\Controller\Crons" => "Admin\Controller\CronsController",
            "Admin\Controller\UpdatecsvCron" => "Admin\Controller\UpdatecsvCronController",
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            'Admin_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/Admin/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Admin\Entity' => 'Admin_driver'
                ),
            ),
        ),
        'configuration' => array(
            'orm_default' => array(
                'string_functions' => array(
                    'FindInSet' => 'DoctrineExtensions\Query\Mysql\FindInSet'
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'layout/adminlayout' => __DIR__ . '/../view/layout/adminlayout.phtml',
            'layout/layoutscreen' => __DIR__ . '/../view/layout/layout.phtml',
            'layout/layout-login' => __DIR__ . '/../view/layout/layout-login.phtml',
            'layout/blank' => __DIR__ . '/../view/layout/blank.phtml',
            'admin/index/index' => __DIR__ . '/../view/admin/index/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
