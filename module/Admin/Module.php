<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

use Admin\Model\AsteriksList;
use Admin\Model\AsteriksListTable;

use Admin\Model\Inbounds;
use Admin\Model\InboundsTable;
use Admin\Model\Outbounds;
use Admin\Model\OutboundsTable;
use Admin\Model\Trunks;
use Admin\Model\TrunksTable;
use Admin\Model\OwnerType;
use Admin\Model\OwnerTypeTable;
use Admin\Model\Firewall;
use Admin\Model\FirewallTable;

use Admin\Model\AccessCodes;
use Admin\Model\AccessCodesTable;
use Admin\Model\TimeProfiles;
use Admin\Model\TimeProfilesTable;
use Admin\Model\AccessLogs;
use Admin\Model\AccessLogsTable;
use Admin\Model\Configs;
use Admin\Model\ConfigsTable;

use Admin\Model\GdFlatowner;
use Admin\Model\GdFlatownerTable;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig() {
        return array(
            "factories"=>array(
                "Admin\Model\AsteriksListTable"=>function($sm) {
                    $tableGateway=$sm->get("AsteriksListTableGateway");
                    $table=new AsteriksListTable($tableGateway);
                    return $table;
                },
                "AsteriksListTableGateway"=>function($sm) {
                    $dbAdapter=$sm->get("Zend\Db\Adapter\Adapter");
                    $resultSetPrototype=new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new AsteriksList());
                    return new TableGateway("gd_lists",$dbAdapter,null,$resultSetPrototype);
                },
                "Admin\Model\InboundsTable"=>function($sm) {
                    $tableGateway=$sm->get("InboundsTableGateway");
                    $table=new InboundsTable($tableGateway);
                    return $table;
                },
                "InboundsTableGateway"=>function($sm) {
                    $dbAdapter=$sm->get("Zend\Db\Adapter\Adapter");
                    $resultSetPrototype=new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Inbounds());
                    return new TableGateway("gd_inbounds",$dbAdapter,null,$resultSetPrototype);
                },
                "Admin\Model\OutboundsTable"=>function($sm) {
                    $tableGateway=$sm->get("OutboundsTableGateway");
                    $table=new OutboundsTable($tableGateway);
                    return $table;
                },
                "OutboundsTableGateway"=>function($sm) {
                    $dbAdapter=$sm->get("Zend\Db\Adapter\Adapter");
                    $resultSetPrototype=new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Outbounds());
                    return new TableGateway("gd_outbounds",$dbAdapter,null,$resultSetPrototype);
                },
                "Admin\Model\TrunksTable"=>function($sm) {
                    $tableGateway=$sm->get("TrunksTableGateway");
                    $table=new TrunksTable($tableGateway);
                    return $table;
                },
                "TrunksTableGateway"=>function($sm) {
                    $dbAdapter=$sm->get("Zend\Db\Adapter\Adapter");
                    $resultSetPrototype=new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Trunks());
                    return new TableGateway("gd_trunks",$dbAdapter,null,$resultSetPrototype);
                },
                "Admin\Model\OwnerTypeTable"=>function($sm) {
                    $tableGateway=$sm->get("OwnerTypeTableGateway");
                    $table=new OwnerTypeTable($tableGateway);
                    return $table;
                },
                "OwnerTypeTableGateway"=>function($sm) {
                    $dbAdapter=$sm->get("Zend\Db\Adapter\Adapter");
                    $resultSetPrototype=new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new OwnerType());
                    return new TableGateway("gd_owner_type",$dbAdapter,null,$resultSetPrototype);
                },  
                "Admin\Model\TimeProfilesTable"=>function($sm) {
                    $tableGateway=$sm->get("TimeProfilesTableGateway");
                    $table=new TimeProfilesTable($tableGateway);
                    return $table;
                },
                "TimeProfilesTableGateway"=>function($sm) {
                    $dbAdapter=$sm->get("Zend\Db\Adapter\Adapter");
                    $resultSetPrototype=new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new TimeProfiles());
                    return new TableGateway("gd_time_profiles",$dbAdapter,null,$resultSetPrototype);
                },
                "Admin\Model\FirewallTable"=>function($sm) {
                    $tableGateway=$sm->get("FirewallTableGateway");
                    $table=new FirewallTable($tableGateway);
                    return $table;
                },
                "FirewallTableGateway"=>function($sm) {
                    $dbAdapter=$sm->get("Zend\Db\Adapter\Adapter");
                    $resultSetPrototype=new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Firewall());
                    return new TableGateway("gd_firewall_configs",$dbAdapter,null,$resultSetPrototype);
                },

                "Admin\Model\AccessCodesTable"=>function($sm) {
                    $tableGateway=$sm->get("AccessCodesTableGateway");
                    $table=new AccessCodesTable($tableGateway);
                    return $table;
                },
                "AccessCodesTableGateway"=>function($sm) {
                    $dbAdapter=$sm->get("Zend\Db\Adapter\Adapter");
                    $resultSetPrototype=new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new AccessCodes());
                    return new TableGateway("gd_access_codes",$dbAdapter,null,$resultSetPrototype);
                },
                "Admin\Model\AccessLogsTable"=>function($sm) {
                    $tableGateway=$sm->get("AccessLogsTableGateway");
                    $table=new AccessLogsTable($tableGateway);
                    return $table;
                },
                "AccessLogsTableGateway"=>function($sm) {
                    $dbAdapter=$sm->get("Zend\Db\Adapter\Adapter");
                    $resultSetPrototype=new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new AccessLogs());
                    return new TableGateway("gd_access_logs",$dbAdapter,null,$resultSetPrototype);
                },
                "Admin\Model\ConfigsTable"=>function($sm) {
                    $tableGateway=$sm->get("ConfigsTableGateway");
                    $table=new ConfigsTable($tableGateway);
                    return $table;
                },
                "ConfigsTableGateway"=>function($sm) {
                    $dbAdapter=$sm->get("Zend\Db\Adapter\Adapter");
                    $resultSetPrototype=new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Configs());
                    return new TableGateway("gd_configs",$dbAdapter,null,$resultSetPrototype);
                },

                "Admin\Model\GdFlatownerTable"=>function($sm) {
                    $tableGateway=$sm->get("GdFlatownerTableGateway");
                    $table=new GdFlatownerTable($tableGateway);
                    return $table;
                },
                "GdFlatownerTableGateway"=>function($sm) {
                    $dbAdapter=$sm->get("Zend\Db\Adapter\Adapter");
                    $resultSetPrototype=new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new GdFlatowner());
                    return new TableGateway("gd_flatowner",$dbAdapter,null,$resultSetPrototype);
                },
            )
        );
    }
}
