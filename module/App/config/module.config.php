<?php

namespace App;

return ['router' => array(
    'routes' => array(
       'login' => array(
            'type' => 'segment',
            'options' => array(
                'route' => '/login',
                'constraints' => array(
                        'id' => '[0-9]+',
                ),
                'defaults' => array(
                   'controller' => 'App\Controller\User'
                ),
            ),
        ),
	   'idlogin' => array(
            'type' => 'segment',
            'options' => array(
                'route' => '/idlogin',
                'constraints' => array(
                        'id' => '[0-9]+',
                ),
                'defaults' => array(
                   'controller' => 'App\Controller\User'
                ),
            ),
        ),
       'entrycodes' => array(
            'type' => 'segment',
            'options' => array(
                'route' => '/entrycodes',
                'defaults' => array(
                   'controller' => 'App\Controller\User'
                ),
            ),
        ),
    ),
),
 
'service_manager' => array(
    'aliases'   => array(
        'ZF\ApiProblem\ApiProblemListener'  => 'ZF\ApiProblem\Listener\ApiProblemListener',
        'ZF\ApiProblem\RenderErrorListener' => 'ZF\ApiProblem\Listener\RenderErrorListener',
        'ZF\ApiProblem\ApiProblemRenderer'  => 'ZF\ApiProblem\View\ApiProblemRenderer',
        'ZF\ApiProblem\ApiProblemStrategy'  => 'ZF\ApiProblem\View\ApiProblemStrategy',
    ),
    'factories' => array(
        'ZF\ApiProblem\Listener\ApiProblemListener'             => 'ZF\ApiProblem\Factory\ApiProblemListenerFactory',
        'ZF\ApiProblem\Listener\RenderErrorListener'            => 'ZF\ApiProblem\Factory\RenderErrorListenerFactory',
        'ZF\ApiProblem\Listener\SendApiProblemResponseListener' => 'ZF\ApiProblem\Factory\SendApiProblemResponseListenerFactory',
        'ZF\ApiProblem\View\ApiProblemRenderer'                 => 'ZF\ApiProblem\Factory\ApiProblemRendererFactory',
        'ZF\ApiProblem\View\ApiProblemStrategy'                 => 'ZF\ApiProblem\Factory\ApiProblemStrategyFactory',
    ),
),

'view_manager' => array( //Add this config
    'strategies' => array(
       'ViewJsonStrategy',
   ),
),  
'translator' => array(
    'locale' => 'en_US',
    'translation_file_patterns' => array(
        array(
            'type' => 'gettext',
            'base_dir' => __DIR__ . '/../language',
            'pattern' => '%s.mo',
        ),
    ),
),
'controllers' => array(
    'invokables' => array(
       'App\Controller\User' => 'App\Controller\UserController'

    ),
),
'doctrine' => array(
    'driver' => array(
        'App_driver' => array(
            'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
            'cache' => 'array',
            'paths' => array(__DIR__ . '/../src/App/Entity')
        ),
        'orm_default' => array(
            'drivers' => array(
                 'App\Entity' =>  'App_driver'
            ),
        ),
    ),
    
    //the code below applies only for custom connections. orm_default is already configured by default
    'entitymanager' => array(
        'orm_alternative' => array(
            'connection' => 'orm_alternative',
            'configuration' => 'orm_alternative',
             
        ),
    ),
    'eventmanager' => array(
        'orm_alternative' => array(
            'drivers' => array(
                 'App\Entity' =>  'App_driver'
            ),
        ),
    ),
    'sql_logger_collector' => array(
        'orm_alternative' => array(),
    ),
    'entity_resolver' => array(
        'orm_alternative' => array(),
    ),
    'configuration' => array(
            'orm_default' => array(
                'string_functions' => array(
                    'FindInSet' => 'DoctrineExtensions\Query\Mysql\FindInSet'
                ),
            ),
    ),
),
    
// Placeholder for console routes
'console' => array(
    'router' => array(
        'routes' => array(
        ),
    ),
),

];
