<?php

/**
 * Index Controller
 * 
 * @category  Zend
 * @version   Ver 2.5
 * @license   BSD-3-Clause
 *  
 */

namespace App\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Mvc\MvcEvent;
use Admin\Entity as Entities;
use Zend\View\Model\JsonModel;
use Zend\Http\Request;
use Zend\Http\Headers;
use Admin\Model\Configs;

class UserController extends AbstractRestfulController {

    protected $em;
    protected $authservice;

    public function setEntityManager(EntityManager $em) {
        $this->em = $em;
    }

    public function getEntityManager() {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }
	
    /**
     * This function is used for logging after verifying OTP
     * 
     * @author          Nishikant
     * @created_date    29th, Dec 2016
     * @modified_date   -------------
    */
    
    public function create($data) {
        switch($data['option']){
            case 'login':
                $send = $this->userlogin($data);
                break;
			case 'idlogin':
                $send = $this->userData($data);
                break;
            case 'add_code':
                $send = $this->addCode($data);
                break;
            case 'verify_code':
                $send = $this->verifyCode($data);
                break;
        }
        return new JsonModel($send);
    }
    
    /**
     * This function is used for logging after verifying OTP
     * 
     * @author          Nishikant
     * @param           mixed $data
     * @created_date    29th, Dec 2016
     * @modified_date   -------------
    */
    
    public function userlogin($data) {        
        $em = $this->getEntityManager();
        $otpstatus = "new";
        $request = $this->getRequest(); 
        if ((REQUEST_METHOD === 'POST') && (API_OPTROUTE === 'login')) {
			$verifiedUser = $em->getRepository('Admin\Entity\GdFlatowner')->verifyUserOtp($data, $otpstatus); 
            if((isset($verifiedUser)) && (!empty($verifiedUser))){ 
                $otp_id =  $verifiedUser[0]['otpId'];
               	$otpObj = $em->getRepository('Admin\Entity\GdOtp')->findOneBy(array('id' => $otp_id));
                $otpObj->setOtpStatus('consumed');
				$otpObj->setDeviceType($data['device_type']);
                $otpObj->setUniqueId($data['unique_id']);                
                $otpObj->setDeviceToken($data['device_token']);
                $em->persist($otpObj);
                $em->flush();
				$verifiedUser[0]['sipPassword'] = $this->passDecryption($verifiedUser[0]['sipPassword']);
				$camera = $em->getRepository('Admin\Entity\GdAppSettings')->cameraAppSettings($data, $otpstatus); 
				$sip = $em->getRepository('Admin\Entity\GdAppSettings')->sipAppSettings($data, $otpstatus);			
				$verifiedUser[]=$camera;
				$verifiedUser[]=$sip;				
				$userobj = call_user_func_array('array_merge', $verifiedUser);
                return $result = array('status' => 200,
                'message' => 'Logged in successfully!', 
                 'response'=> $userobj);   
            }else{
                return $result = array('status' => 401, 'message' => 'Invalid User!');
                return new JsonModel($result);
            }
        }
		else {
				return $result = array('status' => 500,
					'message' => 'Invalid Request! Please check API Url'
				);
		}    
    }
	
	/**
     * This function is used for logging after verifying OTP
     * 
     * @author          Nishikant
     * @param           mixed $data
     * @created_date    29th, Dec 2016
     * @modified_date   18th Apr, 2017
    */
   
    public function userData($data) {        
        $em = $this->getEntityManager();
        $otpstatus = "consumed";
		//$otpstatus = "new";
        $request = $this->getRequest(); 
        if ((REQUEST_METHOD === 'POST') && (API_OPTROUTE === 'idlogin')) {
			$verifiedUser = $em->getRepository('Admin\Entity\GdFlatowner')->verifyUserLogin($data, $otpstatus); 
            if((isset($verifiedUser)) && (!empty($verifiedUser))){ 
                 $otp_id =  $verifiedUser[0]['otpId'];
                /* $otpObj = $em->getRepository('Admin\Entity\GdOtp')->findOneBy(array('id' => $otp_id));
                 $otpObj->setOtpStatus('consumed');
                 $em->persist($otpObj);
                 $em->flush();	*/	
				if(!empty($data['device_token'])){
					$em->getRepository('Admin\Entity\GdOtp')->updateDeviceToken($data['device_token'], $data['userid'], $otpstatus);
				}
				
                 $verifiedUser[0]['sipPassword'] = $this->passDecryption($verifiedUser[0]['sipPassword']);                 
				 $camera = $em->getRepository('Admin\Entity\GdAppSettings')->cameraAppSettings(); 
				 $sip = $em->getRepository('Admin\Entity\GdAppSettings')->sipAppSettings();		
				 $verifiedUser[]=$camera;
				 $verifiedUser[]=$sip;				
				 $userobj = call_user_func_array('array_merge', $verifiedUser);
					return $result = array('status' => 200,
						'message' => 'Logged in successfully!', 
						'response'=> $userobj);   
            }else{
				$this->getResponse()->setStatusCode(401);
                return $result = array('status' => 401, 'message' => 'Invalid User!');
                return new JsonModel($result);
            }
        }else {
				return $result = array('status' => 500,
					'message' => 'Invalid Request! Please check API Url'
				);
		}    
    }
	
	    
     /**
     * Used for password decryption
     * @author		Nishikant
     * @created_date    14th March, 2017
     * @modified_date	-------------
     */
    public function passDecryption($encrypted) { 
     	$enpass = base64_decode(trim($encrypted));
 	$dkey = ENCRYPTION_KEY;
        $iv1 = substr($enpass, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));
        $decrypted = rtrim(
                mcrypt_decrypt(
                        MCRYPT_RIJNDAEL_128, hash('sha256', $dkey, true), substr($enpass, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)), MCRYPT_MODE_CBC, $iv1
                ), "\0"
        );       
        return $decrypted;
    }


    // Abar
    public function addCode($data) {
        $ownerId=@trim($data['owner_id']);

        if(!$ownerId) return $this->createResponse(401, "owner_required", "Owner required");

        ### check max code limit ###
        // max limit
        $config=$this->_getConfigsTable()->getByType(1); // 1=for access code limit
        $limit=@$config['value'] ? @$config['value'] : 0;

        // count current user active codes
        $getWhere=array("owner_id"=>$ownerId, "code_status"=>1);
        $activeCodes=$this->_getAccessCodesTable()->getWhere($getWhere);
        
        if(count($activeCodes)>=$limit)
        return $this->createResponse(401, "error", "You have already issued the maximum access codes.");

        // generate access code
        $maxTry=20;
        $accessCode="";
        $tryId=0;
        for($try=1; $try<=$maxTry; $try++) {
            $tryId++;

            $accessCode=rand(1, 999999);
            $accessCode=str_pad($accessCode, 6, "0", STR_PAD_LEFT);

            // check code already exists and active
            $where=array("access_code"=>$accessCode, "code_status"=>1);
            @list($exists)=$this->_getAccessCodesTable()->getWhere($where);

            if(!$exists) $try=$maxTry+10;
            else $accessCode="";
        }

        if(!$accessCode)
        return $this->createResponse(400, "max_try", "Code not generated, please try again.", $tryId);

        // add now   "access_date_time"=>json_encode(explode(",",$accessDateTime)),
        $codeData=array(
            "owner_id"=>$ownerId, 
            "access_code"=>$accessCode,
            "usage_type"=>1, 
            "code_status"=>1
        );
        $entity=new \Admin\Model\AccessCodes();
        $entity->exchangeArray($codeData);

        $accessId=$this->_getAccessCodesTable()->addCode($entity);
        if(!$accessId) return $this->createResponse(401, "db_error", SOME_ERROR);

        return $this->createResponse(200, "success", "Access code created successfully.");
    }

    public function verifyCode($data) {
        $accessCode=@trim($data['code']);
        if(!$accessCode) return $this->createResponse(401, "code_required", "Entry code required");

        // validate code
        $validCode=$this->validateDigits($accessCode, "Code", 6, 6);
        if($validCode['status']!=200) return $validCode;

        // check valid code
        $checkWhere=array("access_code"=>$accessCode, "code_status"=>1);
        @list($exists)=$this->_getAccessCodesTable()->getWhereInfo($checkWhere);
        if(!$exists) return $this->createResponse(401, "error", "Code is invalid");

        $accessId=@$exists['access_id'];
        $accessType=@$exists['usage_type'];
        if(!$accessId || @$exists['status']!=1) return $this->createResponse(401, "error", "Code is invalid");
      
       
        if(!$accessId || @$exists['time_profile_status']!=1) return $this->createResponse(401, "error", "Code time profile is not active.");
         
        if(!$accessId || @$exists['time_profile_config']=="" || @$exists['time_profile_config']==null) 
        {
          return $this->createResponse(401, "error", "No code time profile found.");
        }
        $access_time_profile_config = json_decode(@$exists['time_profile_config'],true);
        
        $timezone = $this->_getConfigsTable()->getByType(3); // 3=for timezone
        if($timezone["value"]){
            date_default_timezone_set($timezone["value"]);       
        }
        
        $current_day = strtolower(date("l"));
        $current_time = intval(date("Hi"));
        
        $access_time_profile_config_range = explode(" to ", $access_time_profile_config[$current_day]);
       
        $timeFrom = intval(str_replace(":","",$access_time_profile_config_range[0]));
        $timeTo = intval(str_replace(":","",$access_time_profile_config_range[1]));
        if($current_time <= $timeFrom || $current_time >= $timeTo)
        {
            return $this->createResponse(401, "error", "Code is expired for the day.");
        }
        
        if($accessType==1) { // for single use only
            // update status to used
            $this->_getAccessCodesTable()->updateStatus(0, $accessId);
        }

        // maintain logs
        $ownerId=$exists['owner_id'];
        $ownerName=trim($exists['first_name']." ".$exists['last_name']);
        $ownerMobile=$exists['mobile'];
        $ownerInfo=array("unit_number"=>$exists['unit_number'], "floor"=>$exists['floor']);

        $logData=array(
            "access_id"=>$accessId,
            "owner_id"=>$ownerId, "owner_name"=>$ownerName, "owner_mobile"=>$ownerMobile,
            "owner_info"=>json_encode($ownerInfo)
        );
        $entity=new \Admin\Model\AccessLogs();
        $entity->exchangeArray($logData);
        $this->_getAccessLogsTable()->addLog($entity);

        return $this->createResponse(200, "success", "Code is valid.");
    }

    public function createResponse($status=401, $title="bad_request", $message=BAD_REQUEST, $data=NULL) {
        $response = array(
            'status'        =>  (int) $status,
            'title'         =>  $title,
            'message'       =>  $message,
            'response'      =>  $data
        );
        return $response;
    }

    public function validateDigits($value, $label, $min=1, $max=false, $minValue=false, $maxValue=false) {
        // min max length
        if($min && strlen($value)<$min) {
            return $response=$this->createResponse(401, 'short',  "$label should be at least $min digit long.");
        }
        if($max && strlen($value)>$max) {
            return $response=$this->createResponse(401, 'big', "$label should be at most $max digit long.");
        }

        // min max value
        if($minValue && $value<$minValue) {
            return $response=$this->createResponse(401, 'min', "$label should be greater or equal to $minValue");
        }
        if($maxValue && $value>$maxValue) {
            return $response=$this->createResponse(401, 'max', "$label should be less or equal to $minValue");
        }

        // valid digits
        $validator = new \Zend\Validator\Digits();
        if($validator->isValid($value)) {
            return $response=$this->createResponse(200, 'valid', "$label is valid");
        }

        return $response=$this->createResponse(401, 'invalid', "$label is invalid, use only digits");
    }

    protected $_configsTable;
    public function _getConfigsTable() {
        if(!$this->_configsTable) {
            $sm=$this->getServiceLocator();
            $this->_configsTable=$sm->get("Admin\Model\ConfigsTable");
        }
        return $this->_configsTable;
    }

    protected $_accessCodesTable;
    public function _getAccessCodesTable() {
        if(!$this->_accessCodesTable) {
            $sm=$this->getServiceLocator();
            $this->_accessCodesTable=$sm->get("Admin\Model\AccessCodesTable");
        }
        return $this->_accessCodesTable;
    }

    protected $_accessLogsTable;
    public function _getAccessLogsTable() {
        if(!$this->_accessLogsTable) {
            $sm=$this->getServiceLocator();
            $this->_accessLogsTable=$sm->get("Admin\Model\AccessLogsTable");
        }
        return $this->_accessLogsTable;
    }

}
