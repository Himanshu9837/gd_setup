<?php

/**
 * Index Controller
 * 
 * @category  Zend
 * @version   Ver 2.5
 * @license   BSD-3-Clause
 *  
 */

namespace App\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\JsonModel;

class IndexController extends AbstractRestfulController {

    protected $em;
    protected $authservice;

    public function setEntityManager(EntityManager $em) {
        $this->em = $em;
    }

    public function getEntityManager() {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    public function create($data) {
        if ($data['option'] === 'getadmindetails') {
            $send = $this->getAdminDetails($data);
        } else if ($data['option'] === 'getadminlogin') {
            $send = $this->getAdminLogin($data);
        } else {
            $send = array('status' => 0,
                'message' => $this->translate('Not valid request')
            );
        }
        return new JsonModel($send);
    }

}
